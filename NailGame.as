﻿package  
{
	import com.DaveLibrary.clsSound;
	import com.DaveLibrary.clsSoundTransformEX;
	import com.DaveLibrary.clsSupport;
	import com.adobe.images.PNGEncoder;
	import com.coreyoneil.collision.CollisionList;
	import com.cupcake.App;
	import com.cupcake.DeviceInfo;
	import com.cupcake.ExternalImage;
	import com.cupcake.SoundEx;
	import com.cupcake.SpriteSheetSequence;
	import com.cupcake.Utils;
	import com.doitflash.consts.Easing;
	import com.doitflash.consts.Orientation;
	import com.doitflash.consts.ScrollConst;
	import com.doitflash.events.ScrollEvent;
	import com.doitflash.utils.scroll.TouchScroll;
	import com.greensock.TweenMax;
	import com.greensock.easing.Bounce;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.MP3Loader;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.CameraRoll;
	import flash.media.Sound;
	import flash.sampler.NewObjectSample;
	import flash.utils.ByteArray;
	import flash.utils.getTimer;
	
	import org.osflash.signals.natives.NativeSignal;
	
	public class NailGame extends WBZ_BookPage
	{

		// Game data
		public var strings_TitleKey:String;
	
		//All Games Variables.
		private var EndOfLevel:clsGames_EndOfLevelSummary;		
		public var Background:Rectangle;
		
		//sounds.
		private var sndEnd:clsSound;
		private var sndIntro:clsSound;		
		public var soundTrans:clsSoundTransformEX = new clsSoundTransformEX(1);
		
		private var soundChange:SoundEx;
		private var soundGeneric:SoundEx;
		private var soundClearAll:SoundEx;
		private var soundPatterns:SoundEx;
		private var soundPhoto:SoundEx;
		private var soundSkin:SoundEx;
		private var soundStickers:SoundEx;
		private var soundUndo:SoundEx;
		

		// SSC New
		private var _scroller:TouchScroll;
		private var _scrollerMoving:Boolean = false;
		
		private var _nailBaseNames:Vector.<String> = new <String>["SSC_NAILART_GAME_NAIL_01_","SSC_NAILART_GAME_NAIL_02_","SSC_NAILART_GAME_NAIL_03_","SSC_NAILART_GAME_NAIL_04_","SSC_NAILART_GAME_NAIL_05_"]
		private var _nailContainers:Vector.<MovieClip>;
		private var _ringBaseName:String = "SSC_NAILART_GAME_RINGS_";
		private var _ringContainers:Vector.<MovieClip>;
		private var _stickerBaseName:String = "SSC_NAILART_GAME_STICKERS_";
		private var _stickerContainers:Vector.<MovieClip>;
		private var _seqIds:Vector.<String> = new <String>["0001","0002","0003","0004","0005","0006","0007","0008","0009","0010","0011","0012","0013","0014","0015","0016","0017","0018","0019","0020"
			,"0021","0022","0023","0024","0025","0026","0027","0028","0029","0030"];
		
		private var _skinColors:Vector.<uint> = new <uint>[0x853500,0x924514,0x9F5626,0xAC6839,0xBA7949,0xC88A5B,0xD59B6B,0xE2AD7C,0xEFBE8C,0xFCCF9E]
		private var _skinHLColors:Vector.<uint> = new <uint>[0x701a0a,0x7d2416,0x8b3021,0x983f2d,0xa64c38,0xb85c43,0xc66c4d,0xd67b55,0xe89364,0xfbb886]
				
		private var _nailChooserText:String = "SSC_NAILART_GAME_NAIL_CHOICES_";
		private var _skinChooserText:String = "SSC_NAILART_GAME_HAND_CHOICES_";
		private var _ringChooserText:String = "SSC_NAILART_GAME_RING_CHOICES_";
		private var _stickerChooserText:String = "SSC_NAILART_GAME_STICKER_CHOICES_";
		
		private var _hitNails:Vector.<MovieClip>;
		private var _hitRings:Vector.<MovieClip>;
		
		private var _dragClip:MovieClip = new MovieClip();
		private var _changeClip:MovieClip = new MovieClip();
		private var _dragMode:String;
		
		private var _undoVectors:Vector.<Vector.<MovieClip>> = new Vector.<Vector.<MovieClip>>();
		private var _allDraggers:Vector.<MovieClip>;
		private var _allChoosers:Vector.<MovieClip>;
		
		//saving loading.
		public var camRoll:CameraRoll						= new CameraRoll;
		public var canSave:Boolean							= false;
		
		private var previousY:Number = 0;  
		private var currentY:Number = 0;  
		private var yDir:String;  
		private var camSave:Boolean;
		
		private var underlayColor:MovieClip;
		private var underlayPattern:MovieClip;
		private var underlayStickers:MovieClip;
		
		public function NailGame(pageIndex:int = 0){
			
			super();
			
			//Cammera.
			camSave = false;
			camRoll = new CameraRoll();
			if((camRoll != null) && CameraRoll.supportsAddBitmapData){camSave = true;}
			
			//clsMain.Main.cNavBar.visible = false;
			
			strings_TitleKey = "Nail_Title";
			
			
			reusableFolder = CONFIG::DATA + "images/games/";
			imageFolder = CONFIG::DATA + "images/games/NailGame/";
			
			
			// Base Image Assets
			
			assets.push( new ExternalImage( "SSC_NAILART_GAME_BG_01.png", scene_mc.Background_MC));
			assets.push( new ExternalImage( "SSC_NAILART_GAME_HUD_01.png", scene_mc._hudBottom._hudBg));
			
			imagesLoader.Add( "SSC_NAILART_GAME_HUD_BUT_COLORS", Align.CENTER, false, scene_mc._hudBottom._butColors );
			imagesLoader.Add( "SSC_NAILART_GAME_HUD_BUT_HAND", Align.CENTER , false, scene_mc._hudBottom._butHand);
			imagesLoader.Add( "SSC_NAILART_GAME_HUD_BUT_HOME", Align.CENTER , false, scene_mc._hudBottom._butHome);
			imagesLoader.Add( "SSC_NAILART_GAME_HUD_BUT_DESIGN", Align.CENTER , false, scene_mc._hudBottom._butDesign);

			assets.push( new ExternalImage( "SSC_NAILART_GAME_HUD_02.png", scene_mc._hudTop._hudBg));
			
			imagesLoader.Add( "SSC_NAILART_GAME_HUD_BUT_CLEAR", Align.CENTER, false, scene_mc._hudTop._butClear);
			imagesLoader.Add( "SSC_NAILART_GAME_HUD_BUT_UNDO", Align.CENTER, false, scene_mc._hudTop._butUndo);
			imagesLoader.Add( "SSC_NAILART_GAME_HUD_BUT_PHOTO", Align.CENTER, false, scene_mc._hudTop._butPhoto);
			
			//imagesLoader.Add( "SSC_Trash", Align.CENTER, true, scene_mc.Trash_MC);
			//scene_mc.Trash_MC.visible = true;
			
			assets.push( new ExternalImage( "SSC_NAILART_GAME_HAND_FLAT.png", scene_mc._hand._skin));
			assets.push( new ExternalImage( "SSC_NAILART_GAME_HAND_GREY.png", scene_mc._hand._skinHighlight));
			assets.push( new ExternalImage( "SSC_NAILART_GAME_HAND_GREY.png", scene_mc._hand._skinGrey));
			
			scene_mc._hand._skinHighlight.alpha = 0.7;
			scene_mc._hand._skinGrey.alpha = 0.4;
			
			
			// Initial signals
			
			new NativeSignal(scene_mc._hudTop._butClear,MouseEvent.MOUSE_DOWN,MouseEvent).add(resetScene);
			new NativeSignal(scene_mc._hudTop._butUndo,MouseEvent.MOUSE_DOWN,MouseEvent).add(undoEvent);
			new NativeSignal(scene_mc._hudTop._butPhoto,MouseEvent.MOUSE_DOWN,MouseEvent).add(Save_To_Cam);
			
			new NativeSignal(scene_mc._hudBottom._butColors,MouseEvent.MOUSE_DOWN,MouseEvent).add(chooseColors);
			new NativeSignal(scene_mc._hudBottom._butHand,MouseEvent.MOUSE_DOWN,MouseEvent).add(chooseHand);
			new NativeSignal(scene_mc._hudBottom._butDesign,MouseEvent.MOUSE_DOWN,MouseEvent).add(chooseDesign);
			new NativeSignal(scene_mc._hudBottom._butHome,MouseEvent.MOUSE_DOWN,MouseEvent).add(chooseHome);
			
			new NativeSignal(scene_mc._dragger,MouseEvent.MOUSE_UP,MouseEvent).add(dragOff);
			new NativeSignal(scene_mc._dragger,MouseEvent.MOUSE_MOVE,MouseEvent).add(dragMove);
			

			// Setup hit containers
			_hitNails = new <MovieClip>[scene_mc._hitNail1,scene_mc._hitNail2,scene_mc._hitNail3,scene_mc._hitNail4,scene_mc._hitNail5];
			_hitRings = new <MovieClip>[scene_mc._hitRing1,scene_mc._hitRing2,scene_mc._hitRing3,scene_mc._hitRing4,scene_mc._hitRing5];
			
			for (var k:int = 0; k < _hitNails.length; k++) 
			{
				new NativeSignal(_hitNails[k],MouseEvent.MOUSE_DOWN,MouseEvent).add(tapHitAreas);
			}
			
			for (var l:int = 0; l < _hitRings.length; l++) 
			{
				new NativeSignal(_hitRings[l],MouseEvent.MOUSE_DOWN,MouseEvent).add(tapHitAreas);
			}
			
			// Setup Nails into tips
			
			_nailContainers = new <MovieClip>[scene_mc._hand._nail1,scene_mc._hand._nail2,scene_mc._hand._nail3,scene_mc._hand._nail4,scene_mc._hand._nail5];
			
			for (var i:int = 0; i < _seqIds.length; i++) 
			{
				for (var j:int = 0; j < _nailContainers.length; j++) 
				{
					var nail:MovieClip = new MovieClip();
					nail.id = i;
					nail.name = String(i);
					nail.finger = j;
					imagesLoader.Add( (_nailBaseNames[j] + _seqIds[i]), Align.UPPER_LEFT, false, nail);
					nail.visible = false;
					_nailContainers[j].addChild(nail);
				};
			};
			
			
			// Setup rings onto fingers
			
			_ringContainers = new <MovieClip>[scene_mc._hand._ring1,scene_mc._hand._ring2,scene_mc._hand._ring3,scene_mc._hand._ring4,scene_mc._hand._ring5];
			
			
			for (var i:int = 0; i < 10; i++) 
			{
				for (var j:int = 0; j < _ringContainers.length; j++) 
				{
					var ring:MovieClip = new MovieClip();
					ring.id = i;
					ring.name = String(i);
					ring.finger = j;
					imagesLoader.Add( (_ringBaseName + _seqIds[i]), Align.UPPER_LEFT, true, ring);
					ring.visible = false;
					_ringContainers[j].addChild(ring);
				};
			};
			
			
			// Setup stickers onto nails
			
			_stickerContainers = new <MovieClip>[scene_mc._hand._sticker1,scene_mc._hand._sticker2,scene_mc._hand._sticker3,scene_mc._hand._sticker4,scene_mc._hand._sticker5];
			
			
			for (var i:int = 0; i < 15; i++) 
			{
				for (var j:int = 0; j < _stickerContainers.length; j++) 
				{
					var sticker:MovieClip = new MovieClip();
					sticker.id = i;
					sticker.name = String(i);
					sticker.finger = j;
					imagesLoader.Add( (_stickerBaseName + _seqIds[i]), Align.UPPER_LEFT, true, sticker);
					sticker.visible = false;
					_stickerContainers[j].addChild(sticker);
				};
			};
			
			
			
			
			// Setup Chooser containers for scroll system

			
			// Hand Menu
			var handMenuChooser:MovieClip = new MovieClip();
			
			var skinChoice:MovieClip = new MovieClip();
			skinChoice.x = 200;
			skinChoice.y = 2;
			imagesLoader.Add("SSC_NAILART_GAME_SKIN_BUTTON", Align.UPPER_LEFT, false, skinChoice);
			new NativeSignal(skinChoice,MouseEvent.MOUSE_DOWN,MouseEvent).add(chooseSkin);
			handMenuChooser.addChild(skinChoice);
				
			var ringChoice:MovieClip = new MovieClip();
			ringChoice.x = 500;
			ringChoice.y = 12;
			imagesLoader.Add("SSC_NAILART_GAME_RINGS_BUTTON", Align.UPPER_LEFT, false, ringChoice);
			new NativeSignal(ringChoice,MouseEvent.MOUSE_DOWN,MouseEvent).add(chooseRings);
			handMenuChooser.addChild(ringChoice);
		
			scene_mc._hudBottom._scrollerContainer._scrollerHand.visible = false;
			scene_mc._hudBottom._scrollerContainer._scrollerHand.addChild(handMenuChooser);
			
			// Design Menu
			var designMenuChooser:MovieClip = new MovieClip();
			
			var stickerChoice:MovieClip = new MovieClip();
			stickerChoice.x = 200;
			stickerChoice.y = 3;
			imagesLoader.Add("SSC_NAILART_GAME_STICKER_BUTTON", Align.UPPER_LEFT, false, stickerChoice);
			new NativeSignal(stickerChoice,MouseEvent.MOUSE_DOWN,MouseEvent).add(chooseStickers);
			designMenuChooser.addChild(stickerChoice);
			
			var patternChoice:MovieClip = new MovieClip();
			patternChoice.x = 500;
			patternChoice.y = 12;
			imagesLoader.Add("SSC_NAILART_GAME_PATTERNS_BUTTON", Align.UPPER_LEFT, false, patternChoice);
			new NativeSignal(patternChoice,MouseEvent.MOUSE_DOWN,MouseEvent).add(choosePatterns);
			designMenuChooser.addChild(patternChoice);
			
			scene_mc._hudBottom._scrollerContainer._scrollerDesign.visible = false;
			scene_mc._hudBottom._scrollerContainer._scrollerDesign.addChild(designMenuChooser);
			
			
			_allDraggers = new <MovieClip>[];
			_allChoosers  = new <MovieClip>[];

			// Nail Colors
			var nailColorChooser:MovieClip = new MovieClip();
			
			//SSC_NAILART_GAME_SELECTION_UL
			
			underlayColor = new MovieClip();
			imagesLoader.Add( "SSC_NAILART_GAME_SELECTION_UL", Align.UPPER_LEFT, true, underlayColor);
			underlayColor.scaleX = underlayColor.scaleY = 0.85;
			underlayColor.visible = false;
			scene_mc._hudBottom._scrollerContainer._scrollerColorNails.addChild(underlayColor);
			
			for (var i:int = 0; i < 15; i++) 
			{
				var nailChoice:MovieClip = new MovieClip();
				nailChoice.id = i + 15;
				nailChoice.x = i * 64;
				nailChoice.y = 20;
				
				imagesLoader.Add( (_nailChooserText + _seqIds[(i+15)]), Align.UPPER_LEFT, false, nailChoice);
				new NativeSignal(nailChoice,MouseEvent.MOUSE_DOWN,MouseEvent).add(selectNail);
				nailColorChooser.addChild(nailChoice);
				_allChoosers.push(nailChoice);

				var nailDrag:MovieClip = new MovieClip();
				nailDrag.name = String(i + 15);
				nailDrag.visible = false;
				imagesLoader.Add( (_nailChooserText + _seqIds[(i+15)]), Align.CENTER, false, nailDrag);
				scene_mc._dragger._dragColor.addChild(nailDrag);
				_allDraggers.push(nailDrag);
			};
			scene_mc._hudBottom._scrollerContainer._scrollerColorNails.visible = false;
			scene_mc._hudBottom._scrollerContainer._scrollerColorNails.addChild(nailColorChooser);

			// Nail Patterns
			
			underlayPattern = new MovieClip();
			imagesLoader.Add( "SSC_NAILART_GAME_SELECTION_UL", Align.UPPER_LEFT, false, underlayPattern);
			underlayPattern.scaleX = underlayPattern.scaleY = 0.85;
			underlayPattern.visible = false;
			scene_mc._hudBottom._scrollerContainer._scrollerPatternsNails.addChild(underlayPattern);
			
			
			var nailPatternChooser:MovieClip = new MovieClip();
			
			for (var i:int = 0; i < 15; i++) 
			{
				var nailChoice:MovieClip = new MovieClip();
				nailChoice.id = i;
				nailChoice.x = i * 64;
				nailChoice.y = 20;
				imagesLoader.Add( (_nailChooserText + _seqIds[i]), Align.UPPER_LEFT, false, nailChoice);
				new NativeSignal(nailChoice,MouseEvent.MOUSE_DOWN,MouseEvent).add(selectNail);
				nailPatternChooser.addChild(nailChoice);
				_allChoosers.push(nailChoice);
				
				var nailDrag:MovieClip = new MovieClip();
				nailDrag.name = String(i);
				nailDrag.visible = false;
				imagesLoader.Add( (_nailChooserText + _seqIds[i]), Align.CENTER, false, nailDrag);
				scene_mc._dragger._dragPattern.addChild(nailDrag);
				_allDraggers.push(nailDrag);
			};
			scene_mc._hudBottom._scrollerContainer._scrollerPatternsNails.visible = false;
			scene_mc._hudBottom._scrollerContainer._scrollerPatternsNails.addChild(nailPatternChooser);
			
			// Rings
			var ringChooser:MovieClip = new MovieClip();
			
			for (var i:int = 0; i < 10; i++) 
			{
				var ringChoice:MovieClip = new MovieClip();
				ringChoice.id = i;
				ringChoice.x = i * 124;
				ringChoice.y = 25;
				imagesLoader.Add( (_ringChooserText + _seqIds[i]), Align.UPPER_LEFT, false, ringChoice);
				new NativeSignal(ringChoice,MouseEvent.MOUSE_DOWN,MouseEvent).add(selectRing);
				ringChooser.addChild(ringChoice);
				_allChoosers.push(ringChoice);
				
				var ringDrag:MovieClip = new MovieClip();
				ringDrag.name = String(i);
				ringDrag.visible = false;
				imagesLoader.Add( (_ringChooserText + _seqIds[i]), Align.CENTER, false, ringDrag);
				scene_mc._dragger._dragRing.addChild(ringDrag);
				_allDraggers.push(ringDrag);
			};
			scene_mc._hudBottom._scrollerContainer._scrollerRings.visible = false;
			scene_mc._hudBottom._scrollerContainer._scrollerRings.addChild(ringChooser);
			
			// Stickers
			
			underlayStickers = new MovieClip();
			imagesLoader.Add( "SSC_NAILART_GAME_SELECTION_UL", Align.UPPER_LEFT, true, underlayStickers);
			underlayStickers.scaleX = underlayStickers.scaleY = 0.85;
			underlayStickers.visible = false;
			scene_mc._hudBottom._scrollerContainer._scrollerStickers.addChild(underlayStickers);
			
			var stickerChooser:MovieClip = new MovieClip();
			
			for (var i:int = 0; i < 15; i++) 
			{
				var stickerChoice:MovieClip = new MovieClip();
				stickerChoice.id = i;
				stickerChoice.x = i * 92;
				stickerChoice.y = 20;
				imagesLoader.Add( (_stickerChooserText + _seqIds[i]), Align.UPPER_LEFT, false, stickerChoice);
				new NativeSignal(stickerChoice,MouseEvent.MOUSE_DOWN,MouseEvent).add(selectSticker);
				stickerChooser.addChild(stickerChoice);
				_allChoosers.push(stickerChoice);
				
				var stickerDrag:MovieClip = new MovieClip();
				stickerDrag.name = String(i);
				stickerDrag.visible = false;
				imagesLoader.Add( (_stickerChooserText + _seqIds[i]), Align.CENTER, false, stickerDrag);
				scene_mc._dragger._dragSticker.addChild(stickerDrag);
				_allDraggers.push(stickerDrag);
			};
			scene_mc._hudBottom._scrollerContainer._scrollerStickers.visible = false;
			scene_mc._hudBottom._scrollerContainer._scrollerStickers.addChild(stickerChooser);
			
			
			// Skin
			var skinChooser:MovieClip = new MovieClip();
			for (var i:int = 0; i < 10; i++) 
			{
				var skinChoice:MovieClip = new MovieClip();
				skinChoice.id = i;
				skinChoice.x = i * 86;
				imagesLoader.Add( (_skinChooserText + _seqIds[i]), Align.UPPER_LEFT, false, skinChoice);
				new NativeSignal(skinChoice,MouseEvent.MOUSE_DOWN,MouseEvent).add(changeSkin);
				skinChooser.addChild(skinChoice);
			};
			scene_mc._hudBottom._scrollerContainer._scrollerSkin.visible = false;
			scene_mc._hudBottom._scrollerContainer._scrollerSkin.addChild(skinChooser);
			
			scene_mc._dragger._hit._collided = false;
			
			// Standard game stuff
			
			Background = new Rectangle( 0, 0, 1024, 768 );
			
			var ldr:MP3Loader = new MP3Loader( CONFIG::DATA + "sounds/games/NailGame/Instructions_NailGame.mp3", { autoPlay: false, onComplete: introLoaded } );
			ldr.load();
			
			var endldr:MP3Loader = new MP3Loader( CONFIG::DATA + "sounds/games/MazeGame/GREAT JOB_A.mp3", { autoPlay: false, onComplete: endLoaded } );
			endldr.load();						

			EndOfLevel = null;

			this.mouseEnabled = false;
			this.mouseChildren = false;		
			
			
			
			// Sounds
			
			soundChange = new SoundEx("soundChange", CONFIG::DATA + "sounds/games/NailGame/sfx.mp3", App.soundVolume,false,null,false,0.3 );
			soundGeneric = new SoundEx("but_generic", CONFIG::DATA + "sounds/games/NailGame/but_generic.mp3", App.soundVolume,false,null,false,0.3 );
			soundClearAll = new SoundEx("but_clearAll", CONFIG::DATA + "sounds/games/NailGame/but_clearAll.mp3", App.soundVolume,false,null,false,1 );
			soundPatterns = new SoundEx("but_patterns", CONFIG::DATA + "sounds/games/NailGame/but_patterns.mp3", App.soundVolume,false,null,false,1 );
			soundPhoto = new SoundEx("but_photo", CONFIG::DATA + "sounds/games/NailGame/but_photo.mp3", App.soundVolume,false,null,false,1 );
			soundSkin = new SoundEx("but_skin", CONFIG::DATA + "sounds/games/NailGame/but_skin.mp3", App.soundVolume,false,null,false,1 );
			soundStickers = new SoundEx("but_stickers", CONFIG::DATA + "sounds/games/NailGame/but_stickers.mp3", App.soundVolume,false,null,false,1 );
			soundUndo = new SoundEx("but_undo", CONFIG::DATA + "sounds/games/NailGame/but_undo.mp3", App.soundVolume,false,null,false,0.5 );
			
			
			// Finsihed setup, reset and set scroller
			
			resetScene(null,true);
			setScroller();
			chooseColors(null,true);
		}
		
		// Main Undo Function
		private function undoEvent(evt:MouseEvent=null):void
		{
			if(_undoVectors.length != 0){
				trace(" undo length is " + _undoVectors.length);
				var cVector:Vector.<MovieClip> = _undoVectors.pop();
				
				if(cVector[0].lastPick != null){
					var lastpick:MovieClip = cVector[0].getChildByName(cVector[0].lastPick) as MovieClip;
					lastpick.visible = false;
				} else {
					if(cVector[1] != null){
						cVector[1].visible = false;
					};
				}
				
				if(cVector[1] != null){
					cVector[1].visible = true;
					cVector[0].lastPick = cVector[1].name;
				} else {
					cVector[0].lastPick = null;
				}
				
			} else {
				resetScene(null, true)
			}
			
			soundUndo.Start();
		}
		
		private function resetScene(evt:MouseEvent=null, first:Boolean=false):void{

			for (var j:int = 0; j < _nailContainers.length; j++) 
			{
				if(_nailContainers[j].lastPick != null){
					var lastnail:MovieClip = _nailContainers[j].getChildByName(_nailContainers[j].lastPick) as MovieClip;
					lastnail.visible = false;
					_nailContainers[j].lastPick = null;
				}
				
				for (var i:int = 0; i < 30; i++) 
				{
					_nailContainers[j].getChildByName(String(i)).visible=false; 
				}
				
				var nail:MovieClip = _nailContainers[j].getChildByName(String(15)) as MovieClip;
				nail.visible = true;
			};
			
			
			for (var j:int = 0; j < _stickerContainers.length; j++) 
			{
				if(_stickerContainers[j].lastPick != null){
					var laststicker:MovieClip = _stickerContainers[j].getChildByName(_stickerContainers[j].lastPick) as MovieClip;
					laststicker.visible = false;
					_stickerContainers[j].lastPick = null;
				}
				
				for (var i:int = 0; i < 15; i++) 
				{
					_stickerContainers[j].getChildByName(String(i)).visible=false; 
				}
			};
			
			for (var j:int = 0; j < _ringContainers.length; j++) 
			{
				if(_ringContainers[j].lastPick != null){
					var lastring:MovieClip = _ringContainers[j].getChildByName(_ringContainers[j].lastPick) as MovieClip;
					if(lastring != null){lastring.visible = false;};
					_ringContainers[j].lastPick = null;
				}
				
				for (var i:int = 0; i < 10; i++) 
				{
					_ringContainers[j].getChildByName(String(i)).visible=false; 
				}
				
			};
			
			if(scene_mc._hand.rotation == 0 && !first){
				TweenMax.to(scene_mc._hand, 0.1,{rotation:10});
				TweenMax.to(scene_mc._hand, 0.1,{rotation:-10,delay:0.1});
				TweenMax.to(scene_mc._hand, 0.1,{rotation:10,delay:0.2});
				TweenMax.to(scene_mc._hand, 0.1,{rotation:-10,delay:0.3});
				TweenMax.to(scene_mc._hand, 0.1,{rotation:0,delay:0.4});
				soundClearAll.Start();
			};
			_undoVectors.length = 0;
		}
		
		
		
		private function chooseHome(evt:MouseEvent=null):void{
			soundGeneric.Start();
			cMain.cNavBar.visible = true;
			cMain.PrevPage( -1 );
		}
		
		
		// Nail Code
		private function selectNail(evt:MouseEvent=null,nailId:int=15):void{
			clearSelected();
			//evt.currentTarget.alpha = 0.5;
			
			var id:int;
			if(evt == null){
				id = nailId;
			} else {
				trace("changing nails on " + evt.currentTarget.id);
				id = int(evt.currentTarget.id);
			}
			if(id<15){
				_dragClip = scene_mc._dragger._dragPattern.getChildByName(String(id)) as MovieClip;
				_dragMode = "pattern";
				underlayPattern.visible = true;
				underlayPattern.x = ((id*64)-26);
				underlayPattern.y = 5;
			} else {
				_dragClip = scene_mc._dragger._dragColor.getChildByName(String(id)) as MovieClip;
				_dragMode = "color";
				underlayColor.visible = true;
				underlayColor.x = ((id-15)*64-26);
				underlayColor.y = 5;
			}
			selectObject();
		}
		
		private function selectRing(evt:MouseEvent=null):void{
			clearSelected();
			//evt.currentTarget.alpha = 0.5;
			
			var id:int = 0;
			if(evt != null){
				id = int(evt.currentTarget.id);
			}
			_dragClip = scene_mc._dragger._dragRing.getChildByName(String(id)) as MovieClip;
			_dragMode = "ring";
			selectObject();
		}
		
		private function selectSticker(evt:MouseEvent=null):void{
			clearSelected();
			//evt.currentTarget.alpha = 0.5;
			
			var id:int = 0;
			if(evt != null){
				id = int(evt.currentTarget.id);
			}
			_dragClip = scene_mc._dragger._dragSticker.getChildByName(String(id)) as MovieClip;
			_dragMode = "sticker";
			selectObject();
			
			underlayStickers.visible = true;
			underlayStickers.x = ((id*92)-14);
			underlayStickers.y = 5;
		}
		
		private function selectObject():void{
			if(yDir == "up"){
				dragOff();
				_dragClip.visible = true;
				scene_mc._dragger.startDrag(true);
				soundGeneric.Start();
			}
		}
		
		
		private function tapHitAreas(evt:MouseEvent=null):void{
			
			var _name:String = evt.currentTarget.name;

			if(_dragMode !== "ring"){
				switch(_name)
				{
					case "_hitNail1":{changeNailContents(0);break;}
					case "_hitNail2":{changeNailContents(1);break;}
					case "_hitNail3":{changeNailContents(2);break;}	
					case "_hitNail4":{changeNailContents(3);break;}
					case "_hitNail5":{changeNailContents(4);break;}
						
					
						
					default:{break;}
				}
			} 
				
			switch(_name)
			{	
				case "_hitRing1":{removeRing(0);break;}		
				case "_hitRing2":{removeRing(1);break;}		
				case "_hitRing3":{removeRing(2);break;}	
				case "_hitRing4":{removeRing(3);break;}
				case "_hitRing5":{removeRing(4);break;}
					
				default:{break;}
			}
		}
		
		private function changeNailContents(nail:int=0):void{
			
			_changeClip = null;
			
			if(_dragMode == "color" || _dragMode == "pattern" ){
				if( _nailContainers[nail].lastPick != _dragClip.name){
					scene_mc._dragger._hit._collided = true;
					
					dragOff();
					soundChange.Start();
					
					if(_nailContainers[nail].lastPick != null){
						_changeClip = _nailContainers[nail].getChildByName(_nailContainers[nail].lastPick) as MovieClip;
						_changeClip.visible = false;
					} else {
						_changeClip = _nailContainers[nail].getChildByName(String(15)) as MovieClip;
						_changeClip.visible = false;
					}
					
					var nVec:Vector.<MovieClip> = new <MovieClip>[_nailContainers[nail],_changeClip];
					_undoVectors.push(nVec); 
					
					trace("dropping " + _dragClip.name + " on mode " + _dragMode);
					_changeClip = _nailContainers[nail].getChildByName(_dragClip.name) as MovieClip;
					
					trace("changed clip is  "+  _changeClip + " " + _dragClip.name + " on mode " + _dragMode);
					_nailContainers[nail].lastPick = _dragClip.name;
				};
			} else if (_dragMode == "sticker" ){
				
				if( _stickerContainers[nail].lastPick != _dragClip.name){
					scene_mc._dragger._hit._collided = true;
					
					
					dragOff();
					soundChange.Start();
					
					if(_stickerContainers[nail].lastPick != null){
						_changeClip = _stickerContainers[nail].getChildByName(_stickerContainers[nail].lastPick) as MovieClip;
						_changeClip.visible = false;
					}
					
					var sVec:Vector.<MovieClip> = new <MovieClip>[_stickerContainers[nail],_changeClip];
					_undoVectors.push(sVec); 
					
					_changeClip = _stickerContainers[nail].getChildByName(_dragClip.name) as MovieClip;
					_stickerContainers[nail].lastPick = _dragClip.name;
				};
			}
			if(_changeClip != null){
				_changeClip.visible = true;
			}
		}
		
		
		private function removeRing(ring:int=0):void{
			if(_ringContainers[ring].lastPick != null){
				trace("we have a ring, lets remove it");
				
				_changeClip = _ringContainers[ring].getChildByName(_ringContainers[ring].lastPick) as MovieClip;
				_changeClip.visible = false;
				_ringContainers[ring].lastPick = null;
				soundUndo.Start();

			}
			
		};
		
		private function changeRingContents(ring:int=0):void{
			if(_ringContainers[ring].lastPick != _dragClip.name){
				scene_mc._dragger._hit._collided = true;
				dragOff();
				soundChange.Start();
				if(_ringContainers[ring].lastPick != null){
					_changeClip = _ringContainers[ring].getChildByName(_ringContainers[ring].lastPick) as MovieClip;
					_changeClip.visible = false;
				}
				
				var rVec:Vector.<MovieClip> = new <MovieClip>[_ringContainers[ring],_changeClip];
				_undoVectors.push(rVec); 
				
				_changeClip = _ringContainers[ring].getChildByName(_dragClip.name) as MovieClip;
				_ringContainers[ring].lastPick = _dragClip.name;
				_changeClip.visible = true;
			};
		}
		
		private function dragMove(evt:MouseEvent=null):void{
			//trace("checkCollision for " + _dragMode);
			
			if(_dragMode !== "ring"){
				for (var i:int = 0; i < _hitNails.length; i++) 
				{
					if(scene_mc._dragger._hit.hitTestObject(_hitNails[i]) && scene_mc._dragger._hit._collided == false){
						changeNailContents(i);
					}
				}
			} else {
				for (var i:int = 0; i < _hitRings.length; i++) 
				{
					if(scene_mc._dragger._hit.hitTestObject(_hitRings[i]) && scene_mc._dragger._hit._collided == false){
						changeRingContents(i);
					}
				}
			}
			
			
		}
		
		private function clearSelected():void{
			
			underlayColor.visible = false;
			underlayPattern.visible = false;
			underlayStickers.visible = false;
			/*
			for (var j:int = 0; j < _allChoosers.length; j++){
				_allChoosers[j].alpha = 1;
			}
			*/
		}
		
		
		private function dragOff(evt:MouseEvent=null):void{
			trace("dragged Off");
			scene_mc._dragger.stopDrag();
			scene_mc._dragger.visible = false;
			
			for (var i:int = 0; i < _allDraggers.length; i++){
				_allDraggers[i].visible = false;
			}
			
			

			scene_mc._dragger.x = 0;
			scene_mc._dragger.y = 800;
			scene_mc._dragger._hit._collided = false;
		}
		
		
		// Skin Change
		private function changeSkin(evt:MouseEvent=null,skinId:int=10):void{
			
			var id:int ;
			if(evt == null){
				id = skinId;
			} else {
				trace("changing skin on " + evt.currentTarget.id);
				id = int(evt.currentTarget.id);
			}
			
			TweenMax.to(scene_mc._hand._skin,1,{tint:_skinColors[id]});
			TweenMax.to(scene_mc._hand._skinHighlight,1,{tint:_skinHLColors[id]});
			soundGeneric.Start();
		}
		
		
		// Ring Change
		private function changeRings(evt:MouseEvent=null,skinId:int=-1):void{
			var id:int;
			if(evt == null){
				id = -1;
			} else {
				trace("changing rings on " + evt.currentTarget.id);
				id = int(evt.currentTarget.id);
				
				for (var j:int = 0; j < _ringContainers.length; j++) 
				{
					if(_ringContainers[j].lastRing != null){
						_ringContainers[j].lastRing.visible = false;
					}
					var ring:MovieClip = _ringContainers[j].getChildByName(String(id)) as MovieClip;
					ring.visible = true;
					_ringContainers[j].lastRing = ring;
				};
			}
		}
		
		//Sticker Change
		private function changeStickers(evt:MouseEvent=null,skinId:int=10):void{
			var id:int;
			if(evt == null){
				id = -1;
			} else {
				trace("changing stickers on " + evt.currentTarget.id);
				id = int(evt.currentTarget.id);
				
				for (var j:int = 0; j < _stickerContainers.length; j++) 
				{
					if(_stickerContainers[j].lastSticker != null){
						_stickerContainers[j].lastSticker.visible = false;
					}
					var sticker:MovieClip = _stickerContainers[j].getChildByName(String(id)) as MovieClip;
					sticker.visible = true;
					_stickerContainers[j].lastSticker = sticker;
				};
			}
		}
		
		// Chooser Scroll Setup
		private function resetChooser():void{
			_scroller.xPerc = 0;
			_scroller.maskContent = null;
			scene_mc._hudBottom._scrollerContainer._scrollerColorNails.visible = false;
			scene_mc._hudBottom._scrollerContainer._scrollerDesign.visible = false;
			scene_mc._hudBottom._scrollerContainer._scrollerPatternsNails.visible = false;
			scene_mc._hudBottom._scrollerContainer._scrollerStickers.visible = false;
			scene_mc._hudBottom._scrollerContainer._scrollerHand.visible = false;
			scene_mc._hudBottom._scrollerContainer._scrollerRings.visible = false;
			scene_mc._hudBottom._scrollerContainer._scrollerSkin.visible = false;
		}
		
		private function chooseColors(evt:MouseEvent=null,first:Boolean=false):void{
			resetChooser();
			scene_mc._hudBottom._scrollerContainer._scrollerColorNails.visible = true;
			_scroller.maskContent = scene_mc._hudBottom._scrollerContainer._scrollerColorNails;
			if(!first){
				soundGeneric.Start();
			};
		}
		
		private function chooseDesign(evt:MouseEvent=null):void{
			resetChooser();
			scene_mc._hudBottom._scrollerContainer._scrollerDesign.visible = true;
			_scroller.maskContent = scene_mc._hudBottom._scrollerContainer._scrollerDesign;
			soundGeneric.Start();
		}
		
		private function choosePatterns(evt:MouseEvent=null):void{
			resetChooser();
			scene_mc._hudBottom._scrollerContainer._scrollerPatternsNails.visible = true;
			_scroller.maskContent = scene_mc._hudBottom._scrollerContainer._scrollerPatternsNails;
			soundPatterns.Start();
		}
		
		private function chooseStickers(evt:MouseEvent=null):void{
			resetChooser();
			scene_mc._hudBottom._scrollerContainer._scrollerStickers.visible = true;
			_scroller.maskContent = scene_mc._hudBottom._scrollerContainer._scrollerStickers;
			soundStickers.Start();
		}
		
		private function chooseHand(evt:MouseEvent=null):void{
			resetChooser();
			scene_mc._hudBottom._scrollerContainer._scrollerHand.visible = true;
			_scroller.maskContent = scene_mc._hudBottom._scrollerContainer._scrollerHand;
			soundGeneric.Start();
		}
		
		private function chooseRings(evt:MouseEvent=null):void{
			resetChooser();
			scene_mc._hudBottom._scrollerContainer._scrollerRings.visible = true;
			_scroller.maskContent = scene_mc._hudBottom._scrollerContainer._scrollerRings;
			soundGeneric.Start();
		}
		
		private function chooseSkin(evt:MouseEvent=null):void{
			resetChooser();
			scene_mc._hudBottom._scrollerContainer._scrollerSkin.visible = true;
			_scroller.maskContent = scene_mc._hudBottom._scrollerContainer._scrollerSkin;
			soundSkin.Start();
		}
		
		
		
		private function NavDown():void{
			TweenMax.to(scene_mc._hudTop,  1, {y:-13});
			TweenMax.to(scene_mc._hudBottom, 1, {y:560});				
		}
		
		
		// Scroller Code
		private function setScroller():void
		{
			if (!_scroller) 
			{
				_scroller =  new TouchScroll();
			}
			
			//------------------------------------------------------------------------------ set Scroller
			//_scroller.maskContent = scene_mc._hudBottom._scrollerContainer._scrollerSkin;
			_scroller.enableVirtualBg = true;
			_scroller.mouseWheelSpeed = 5;
			
			_scroller.orientation = Orientation.HORIZONTAL; // accepted values: Orientation.AUTO, Orientation.VERTICAL, Orientation.HORIZONTAL
			_scroller.easeType = Easing.Strong_easeOut;
			_scroller.scrollSpace = 0;
			_scroller.aniInterval = 0.5;
			_scroller.blurEffect = false;
			_scroller.lessBlurSpeed = 15;
			_scroller.yPerc = 0; // min value is 0, max value is 100
			_scroller.xPerc = 0; // min value is 0, max value is 100
			_scroller.mouseWheelSpeed = 2;
			_scroller.isMouseScroll = false;
			_scroller.isTouchScroll = true;
			_scroller.bitmapMode = ScrollConst.WEAK; // use it for smoother scrolling, special when working on mobile devices, accepted values: "normal", "weak", "strong"
			_scroller.buttonMode = true;
			_scroller.isStickTouch = false;
			_scroller.holdArea = 7;
			_scroller.maskWidth = 830;
			_scroller.maskHeight = 140;
			_scroller.x = 0;
			_scroller.y = 0;
			new NativeSignal(_scroller, MouseEvent.MOUSE_MOVE, MouseEvent).add(checkDirection); 
			scene_mc._hudBottom._scrollerContainer.addChild(_scroller);
		}


		
		// Determine Mouse direction so we can drag off scroller when needed.
		

		
		
		private function checkDirection(evt:MouseEvent):void  
		{  
			previousY = currentY;  
			currentY = stage.mouseY;  
			
			_scrollerMoving = true;
			
			if (previousY > currentY)  
			{  
				yDir = "up";  
				scene_mc._dragger.visible = true;
			}  
		}  
		
		
		//Page Shown.
		public override function PageShown(LoadedFromSource:int):void{
			super.loadedFrom = LoadedFromSource;
			
			if(LoadedFromSource != 2) cMain.showArrows();
		}
		
		//Core.
		public override function Init():void{
			super.Init();
			Reset();
		}	
		
		public override function Destroy():void
		{
			if ( CONFIG::DEBUG ) { clsMain.log("Destroyed Nail Game"); }
			
			cMain.removeEnterFrame(ENTER_FRAME);	
			clsSupport.DisposeOfSprite(this);
			
			cMain.cNavBar.visible = true;
			
			if(EndOfLevel != null) EndOfLevel.destroy();
			EndOfLevel = null;	
											
			
			if(sndIntro != null) sndIntro.destroy();
			sndIntro = null;
		
			super.Destroy();
		}		
		public override function Reset():void{
			
			if ( CONFIG::DEBUG ) { clsMain.log("Reset Nail Game"); }
			
			cMain.removeEnterFrame(ENTER_FRAME);
			
			this.mouseEnabled = false;
			this.mouseChildren = false;					
			cMain.cNavBar.visible = true;
			
			if(EndOfLevel != null) { EndOfLevel.destroy(); }						
			if ( sndIntro != null ) sndIntro.Stop(false);
			//GameOver = true;

			super.Reset();
			step = STEP_DONE;	
		}		
		public override function Start():void{
			if ( CONFIG::DEBUG ) { clsMain.log("Start Nail Game"); }
			super.Start();
			musicFile = "ssctheme1";			
			this.mouseEnabled = false;
			this.mouseChildren = false;	
			// Adjust to length of instructions narration
			clsMain.Main.cNavBar.visible = false;
			
			TweenMax.delayedCall(1, EnableGame);
			
			TweenMax.delayedCall( 2, NavDown );
			
			if ( sndIntro != null ) sndIntro.Start();
		}		
		public override function Stop():void{			
			if ( CONFIG::DEBUG ) { clsMain.log("Stop Nail Game"); }
			cMain.removeEnterFrame(ENTER_FRAME);	
			if ( sndIntro != null ) sndIntro.Stop(false);
			if ( sndEnd != null ) sndEnd.Stop(false);
			super.Stop();
		}		
		
		
		public function ENTER_FRAME(e:Event):void
		{
			
			if(cMain.Paused) return;
			
		}
		
		
		
		//Conditions.
		public function EnableGame():void{
			if ( CONFIG::DEBUG ) { clsMain.log("GameStarted"); }
			
			
			
			cMain.addEnterFrame(ENTER_FRAME);	
			this.mouseEnabled = true;			
			this.mouseChildren = true;

		}
		
		private function IntroComplete(snd:clsSound):void { if(isRunning) doMusicLoad(); }
		private function Finale_Complete():void{
			if ( isRunning )
			{
				var returnToPrevPage:Boolean = this.loadedFrom == 2;
			
				/*
				// static value "2" for "Great Job!"
				EndOfLevel = new clsGames_EndOfLevelSummary(cMain, "Maze", returnToPrevPage, sndEnd, "You found your way through the maze!" );
				EndOfLevel.x = Background.x;
				EndOfLevel.y = Background.y;
				this.addChild(EndOfLevel);
				*/ 
			}
		}
		
		//Sound - Music.
		private function introLoaded( e:LoaderEvent ):void{
			sndIntro = new clsSound( e.target.content as Sound, soundTrans, false, IntroComplete, false, 1);
			( e.target as MP3Loader ).dispose( true );
		}		
		private function endLoaded( e:LoaderEvent ):void{
			sndEnd = new clsSound( e.target.content as Sound, soundTrans, false, null, false);
			( e.target as MP3Loader ).dispose( true );
		}		
		
		
		// Save Nail Photo to Roll
		
		//Save To Roll.
		private function Save_To_Cam(evt:MouseEvent= null):void{
			
			
			//trace(cMain.get_localWidth() + " " + cMain.get_localHeight());
			var Distance:int = ((1366 - cMain.get_localWidth()) / 2);
			//trace(Distance);
			
			//Save To Cam.
			if(camSave){
				
				camSave = false;
				
				
				var Scene_Bitmap:BitmapData = new BitmapData(cMain.get_localWidth(), 768, true, 0x000000);
				var Full_Scene:BitmapData   = new BitmapData(1366, 768, true, 0x000000);
				Full_Scene.draw(scene_mc);
				
				var Draw_Rect:Rectangle = new Rectangle(Distance, 0, cMain.get_localWidth(), scene_mc.height);
				var Draw_Pos:Point      = new Point(0, scene_mc.y);				
				Scene_Bitmap.copyPixels(Full_Scene, Draw_Rect, Draw_Pos, null, null, true);
				camRoll.addBitmapData(Scene_Bitmap);
				
				soundPhoto.Start();
				
				TweenMax.delayedCall( 1, Save_Done );
				
			}			
			
			
			
			
		}
		public function Save_Done():void{
			TweenMax.killDelayedCallsTo( Save_Done );					
			camSave = true;
		}
		
	
		
		private function Save_Canvas():void{
			var Color_Canvas:Bitmap = null;	
			Color_Canvas.bitmapData = new BitmapData(	1366, 
				768,
				false, 0xFFFFFF);
			
			var png:PNGEncoder = new PNGEncoder();
			var byteArray:ByteArray = png.encode( scene_mc.bitmapData );			
			
			// set an filename
			var filename:String = (("Strawberry_Nails_" + (Math.random()*100).toString()) + ".png");		
			
			// get current path
			var file:File = File.applicationStorageDirectory.resolvePath( filename );
			
			// get the native path
			var wr:File = new File( file.nativePath );			
			
			// create filestream
			var stream:FileStream = new FileStream();
			
			//  open/create the file, set the filemode to write in order to save.
			stream.open( wr , flash.filesystem.FileMode.WRITE);
			
			// write your byteArray into the file.
			stream.writeBytes ( byteArray, 0, byteArray.length );
			
			// close the file.
			stream.close();			
			
		}	
		
		
		
		
	}
}

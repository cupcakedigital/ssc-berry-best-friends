﻿package  
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.*;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.geom.Point;
	import flash.media.CameraRoll;
	import flash.geom.Matrix;	
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	
	import com.cupcake.Images;
	import com.cupcake.ExternalImage;
	import com.cupcake.App;
	import com.cupcake.SoundCollection;
	import com.cupcake.SoundEx;
	
	import com.greensock.TweenLite;
	import com.greensock.easing.Linear;
	import com.greensock.events.TweenEvent;
	import com.greensock.loading.LoaderMax;
	import com.greensock.loading.ImageLoader;
	import com.greensock.events.LoaderEvent;
	
	public class DressUpGame extends WBZ_BookPage
	{		
	
		//Main Hud.
		private var Drop_Down_Menu_Retracted:Boolean;
		private var Background:Sprite;
		private var Drop_Down_Menu:Sprite;
		private var Main_Hud:Sprite;		
		private var Hud_Buttons:Sprite;
		
		//Scrolling Huds.
		private var Selected_Bar:int;
		private var Scrolling_Menus:Array;
		private var Scrolling_Speed:int;
		private var Old_Mouse:Point;
		private var New_Mouse:Point;
		private var Sta_Mouse:Point;
		private var Panel_Transition_In_Progress:Boolean;
		
		//Static Huds Templates.
		private var Static_Menu_Outfits:Sprite;
		private var Static_Menu_Accessories:Sprite;				
		
		//Scrolling Huds Templates.
		private var Bodies_Template:SheetTemplate;
		private var Dresses_Template:SheetTemplate;
		private var Hats_Template:SheetTemplate;
		private var Headbands_Template:SheetTemplate;
		private var Pants_Template:SheetTemplate;
		private var Shoes_Template:SheetTemplate;
		private var Skirts_Template:SheetTemplate;
		private var Tops_Template:SheetTemplate;		
		private var Purses_Template:SheetTemplate;		
		
		//Dressup Body.
		private var GIRLS:Array;
		private var GIRLS_Item_Selected:Point;
		private var GIRLS_Item_Selected_Limit:int;
		private var Mouse_Item_Click_Position:Point;
		private var Mouse_World_Click_Position:Point;
		
		//Glitter.
		private var Glitter_Holder:Sprite;
		private var Glitter_Sprite:Sprite;
		private var Glitter_Accum:int;
		
		//Undos.
		private var Undos:Array;		
		
		//Cammera.
		public var camRoll:CameraRoll;
		public var camSave:Boolean;		
		
		//Moding.
		private var Touch_1_ID:int;
		private var Touch_1_Pos:Point;
		private var Touch_2_ID:int;
		private var Touch_2_Pos:Point;
		
		//Sounds.
		private var SFX_ChooseGirl:SoundEx;
		private var SFX_Excessories:SoundEx;
		private var SFX_Hats:SoundEx;
		private var SFX_Outfits:SoundEx;
		private var SFX_Shoes:SoundEx;
		private var SFX_Startup:SoundEx;
		
		
		//---------------------------------------------------------------------	
		//Core.
		public function DressUpGame(){			
			
			super();					
			
			//Cammera.
			camSave = false;
			camRoll = new CameraRoll();
			if((camRoll != null) && CameraRoll.supportsAddBitmapData){camSave = true;}
			
			Old_Mouse = new Point(-1, -1);
			New_Mouse = new Point(-1, -1);
			Sta_Mouse = new Point(-1, -1);
			
			Undos 	  = new Array();
			
			imageFolder = CONFIG::DATA + "images/pages/DressUpGame/";						
			
			SFX_ChooseGirl 		= new SoundEx( "ChooseGirl", 	CONFIG::DATA + "sounds/DressUp/ChooseGirl.mp3",		App.voiceVolume );
			SFX_Excessories 	= new SoundEx( "Excessories", 	CONFIG::DATA + "sounds/DressUp/Excessories.mp3", 	App.voiceVolume );
			SFX_Hats 			= new SoundEx( "Hats", 			CONFIG::DATA + "sounds/DressUp/Hats.mp3",			App.voiceVolume );
			SFX_Outfits 		= new SoundEx( "Outfits", 		CONFIG::DATA + "sounds/DressUp/Outfits.mp3", 		App.voiceVolume );
			SFX_Shoes 			= new SoundEx( "Shoes", 		CONFIG::DATA + "sounds/DressUp/Shoes.mp3", 			App.voiceVolume );
			SFX_Startup 		= new SoundEx( "Startup", 		CONFIG::DATA + "sounds/DressUp/Startup.mp3", 		App.voiceVolume );
			
			Background = new Sprite();
			Images.Add( "Background", { container: Background, fromFile: true } );
			Background.x 	= -171;
			Background.y 	= 0;			
			scene_mc.addChild(Background);			
			
			Main_Hud = new Sprite();		
			Images.Add( "Hud", { container:Main_Hud, fromFile: true } );
			Main_Hud.visible = false;
			scene_mc.addChild(Main_Hud);
			
			Drop_Down_Menu = new Sprite();		
			Images.Add( "DropdownBar", { container:Drop_Down_Menu, fromFile: true } );
			Drop_Down_Menu.visible = false;
			scene_mc.addChild(Drop_Down_Menu);	
			
			Glitter_Sprite = new Sprite();
			Images.Add( "Effects/Glitter", { container:Glitter_Sprite, fromFile: true } );			
			
			Build_Templates();
			Create_A_Girl();
			Setup_A_Girl();
			Initialize_Glitter_Bombs();
			Initialize_Touch();
			
			cMain.cNavBar.Hide_NavBar();
			
		}	
		public override function Init():void{
			Initialize_Huds();			
			super.Init();
		}			
		public override function Destroy():void{
			
			cMain.cNavBar.Show_NavBar();
			cMain.removeStageMouseUp( onUp );		
			cMain.removeStageMouseDown( onDown );	
			Destroy_Touch();
			Destroy_Glitter();
			
			if(SFX_ChooseGirl 	!= null){SFX_ChooseGirl.Destroy();	SFX_ChooseGirl = null;}
			if(SFX_Excessories 	!= null){SFX_Excessories.Destroy();	SFX_Excessories = null;}
			if(SFX_Hats 		!= null){SFX_Hats.Destroy();		SFX_Hats = null;}
			if(SFX_Outfits 		!= null){SFX_Outfits.Destroy();		SFX_Outfits = null;}
			if(SFX_Shoes 		!= null){SFX_Shoes.Destroy();		SFX_Shoes = null;}
			if(SFX_Startup 		!= null){SFX_Startup.Destroy();		SFX_Startup = null;}			
			
			scene_mc.removeEventListener(TouchEvent.TOUCH_BEGIN, onTouchBegin);
			scene_mc.removeEventListener(TouchEvent.TOUCH_MOVE,  onTouchMove);
			scene_mc.removeEventListener(TouchEvent.TOUCH_END,   onTouchEnd);		
			
			super.Destroy();
			
		}			
		public override function Reset():void{
			cMain.removeStageMouseUp( onUp );		
			cMain.removeStageMouseDown( onDown );		
			super.Reset();
		}		
		public override function Start():void{
			super.Start();
			Assemble_Templates();
			musicFile = "Game_Mid";			
			cMain.addStageMouseUp( onUp );
			cMain.addStageMouseDown( onDown );
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			scene_mc.addEventListener(TouchEvent.TOUCH_BEGIN, onTouchBegin);
			scene_mc.addEventListener(TouchEvent.TOUCH_MOVE,  onTouchMove);
			scene_mc.addEventListener(TouchEvent.TOUCH_END,   onTouchEnd);		
			SFX_Startup.Start();
			TweenLite.delayedCall( 2, NavDown );
		}				
		public override function Stop():void{			
			cMain.removeStageMouseUp( onUp );		
			cMain.removeStageMouseDown( onDown );	
			super.Stop();
		}		
		//---------------------------------------------------------------------	
		
		//---------------------------------------------------------------------	
		//Controlers.
		private function onUp( e:MouseEvent ):void{			
			if(isRunning && !isPaused){
				
				
				//trace("X " + (GIRLS[0][0].getChildAt(8).x) + " Y " + (GIRLS[0][0].getChildAt(8).y) + " Girl Selected " + GIRLS[0][2][0][0]);
				
				
				
				Scrolling_Speed = 0;			
				Old_Mouse.x 	= -1; Old_Mouse.y = -1; 
				Stop_Moving_Item();
				Button_Hud_Navigation();
			}			
		}		
		private function onDown( e:MouseEvent ):void{			
			if(isRunning && !isPaused){		
			
				if(!Drop_Down_Menu.hitTestPoint(mouseX, mouseY, false)){
					if(!Drop_Down_Menu_Retracted){
						Extend_Drop_Down_Menu();
					}
				}
				
			
				Select_Moving_Item(0);
				if(Hit_Template(Scrolling_Menus[Selected_Bar])){
					Old_Mouse.x = mouseX;
					Old_Mouse.y = mouseY;
					New_Mouse.x = mouseX;
					New_Mouse.y = mouseY;
					Sta_Mouse.x = mouseX;
					Sta_Mouse.y = mouseY;
				}
				
			}			
		}			
		protected override function handleFrame( e:Event ):void{			
			if(isRunning && !isPaused){				
				Update_Moving_Item();
				if((Old_Mouse.x != -1) && (Old_Mouse.y != -1)){
					New_Mouse.x = mouseX;
					New_Mouse.y = mouseY;				
					Scrolling_Speed = int(New_Mouse.x - Old_Mouse.x);
					Old_Mouse.x = mouseX;
					Old_Mouse.y = mouseY;	
				}				
				Scroll_Selection_Panel();
			}
		}				
		//---------------------------------------------------------------------			
		
		//---------------------------------------------------------------------	
		//This is the Hud section.
		private function NavDown():void{
			Initialize_Main_Hud_Buttons();
			Initialize_Drop_Down_Menu_Buttons();
			TweenLite.to(Main_Hud, 		 1, {y:(768 - Main_Hud.height)});
			TweenLite.to(Drop_Down_Menu, 1, {y:((-Drop_Down_Menu.height) + 180)});				
		}
		private function Initialize_Huds():void{						
			Initialize_MainHud();
			Initialize_DropDownHud();
		}			
		private function Initialize_MainHud():void{			
			Main_Hud.x 			= (512 - (Main_Hud.width / 2));	
			Main_Hud.y 			= (768 + (Main_Hud.height / 2));		
			Main_Hud.visible 	= true;
		}
		private function Initialize_DropDownHud():void{			
			Drop_Down_Menu_Retracted 	= true;
			Drop_Down_Menu.x 			= 0;
			Drop_Down_Menu.y 			= -Drop_Down_Menu.height;		
			Drop_Down_Menu.visible 		= true;
		}		
		//Hud Buttons.
		private function Initialize_Main_Hud_Buttons():void{			
			
			var Button_Bit_1:Bitmap 	= new Bitmap();			
			Button_Bit_1.bitmapData 	= new BitmapData(64, 64, true, 0x000000);
			Button_Bit_1.x 				= -(Button_Bit_1.width  / 2);
			Button_Bit_1.y 				= -(Button_Bit_1.height / 2);
			
			var Button_Bit_2:Bitmap 	= new Bitmap();			
			Button_Bit_2.bitmapData 	= new BitmapData(64, 64, true, 0x000000);
			Button_Bit_2.x 				= -(Button_Bit_2.width  / 2);
			Button_Bit_2.y 				= -(Button_Bit_2.height / 2);
			
			var Button_Bit_3:Bitmap 	= new Bitmap();			
			Button_Bit_3.bitmapData 	= new BitmapData(64, 64, true, 0x000000);
			Button_Bit_3.x 				= -(Button_Bit_3.width  / 2);
			Button_Bit_3.y 				= -(Button_Bit_3.height / 2);
			
			var Button_Bit_4:Bitmap 	= new Bitmap();			
			Button_Bit_4.bitmapData 	= new BitmapData(64, 64, true, 0x000000);
			Button_Bit_4.x 				= -(Button_Bit_4.width  / 2);
			Button_Bit_4.y 				= -(Button_Bit_4.height / 2);
			
			var Button_1:Sprite 	= new Sprite(); Button_1.addChild(Button_Bit_1); Button_1.name = "0";
			var Button_2:Sprite 	= new Sprite(); Button_2.addChild(Button_Bit_2); Button_2.name = "1";
			var Button_3:Sprite 	= new Sprite(); Button_3.addChild(Button_Bit_3); Button_3.name = "2";
			var Button_4:Sprite 	= new Sprite(); Button_4.addChild(Button_Bit_4); Button_4.name = "3";			
			
			Button_1.x = 58;  Button_1.y = 64;
			Button_2.x = 148; Button_2.y = 64;
			Button_3.x = 58;  Button_3.y = 148;
			Button_4.x = 148; Button_4.y = 148;
			
			Main_Hud.addChild(Button_1);
			Main_Hud.addChild(Button_2);
			Main_Hud.addChild(Button_3);
			Main_Hud.addChild(Button_4);			
			
		}
		private function Click_Main_Hud():int{	
			//Scrolling.
			for(var X:int = 1; X < Main_Hud.numChildren; X++){	
				if(Main_Hud.getChildAt(X).hitTestPoint(mouseX, mouseY, false)){
					return int(Main_Hud.getChildAt(X).name);
				}				
			}
			return -1;
		}			
		private function Initialize_Drop_Down_Menu_Buttons():void{			
			
			var Button_Bit_1:Bitmap 	= new Bitmap();			
			Button_Bit_1.bitmapData 	= new BitmapData(100, 100, true, 0x000000);
			Button_Bit_1.x 				= -(Button_Bit_1.width  / 2);
			Button_Bit_1.y 				= -(Button_Bit_1.height / 2);
			
			var Button_Bit_2:Bitmap 	= new Bitmap();			
			Button_Bit_2.bitmapData 	= new BitmapData(100, 100, true, 0x000000);
			Button_Bit_2.x 				= -(Button_Bit_2.width  / 2);
			Button_Bit_2.y 				= -(Button_Bit_2.height / 2);
			
			var Button_Bit_3:Bitmap 	= new Bitmap();			
			Button_Bit_3.bitmapData 	= new BitmapData(100, 100, true, 0x000000);
			Button_Bit_3.x 				= -(Button_Bit_3.width  / 2);
			Button_Bit_3.y 				= -(Button_Bit_3.height / 2);
			
			var Button_Bit_4:Bitmap 	= new Bitmap();			
			Button_Bit_4.bitmapData 	= new BitmapData(100, 100, true, 0x000000);
			Button_Bit_4.x 				= -(Button_Bit_4.width  / 2);
			Button_Bit_4.y 				= -(Button_Bit_4.height / 2);
			
			var Button_1:Sprite 		= new Sprite(); Button_1.addChild(Button_Bit_1); Button_1.name = "0";
			var Button_2:Sprite 		= new Sprite(); Button_2.addChild(Button_Bit_2); Button_2.name = "1";
			var Button_3:Sprite 		= new Sprite(); Button_3.addChild(Button_Bit_3); Button_3.name = "2";
			var Button_4:Sprite 		= new Sprite(); Button_4.addChild(Button_Bit_4); Button_4.name = "3";			
			
			Button_1.x = 103;  Button_1.y = 64;
			Button_2.x = 103;  Button_2.y = 170;
			Button_3.x = 103;  Button_3.y = 280;
			Button_4.x = 103;  Button_4.y = 452;
			
			Drop_Down_Menu.addChild(Button_1);
			Drop_Down_Menu.addChild(Button_2);
			Drop_Down_Menu.addChild(Button_3);
			Drop_Down_Menu.addChild(Button_4);			
			
		}
		private function Click_Drop_Down_Menu():int{	
			//Scrolling.
			for(var X:int = 1; X < Drop_Down_Menu.numChildren; X++){	
				if(Drop_Down_Menu.getChildAt(X).hitTestPoint(mouseX, mouseY, false)){
					return int(Drop_Down_Menu.getChildAt(X).name);
				}				
			}
			return -1;
		}	
		private function Extend_Drop_Down_Menu():void{	
			TweenLite.killTweensOf(Drop_Down_Menu);
			if(Drop_Down_Menu_Retracted){
				Drop_Down_Menu_Retracted = false;
				TweenLite.to(Drop_Down_Menu, 1, {y:0});
			}else{
				Drop_Down_Menu_Retracted = true;
				TweenLite.to(Drop_Down_Menu, 1, {y:((-Drop_Down_Menu.height) + 180)});		
			}
		}
		//Navigation
		private function Button_Hud_Navigation():void{
			
			if(Panel_Transition_In_Progress){return;}
			
			//trace("---------------------------------------------");
			//trace(Click_Main_Hud() + " Hud Selection");			
			//trace(Click_Drop_Down_Menu() + " Drop Down Selection");					
			//trace("---------------------------------------------");
			
			switch(Click_Main_Hud()){				
				case 0:{Show_Selection_Panel(0); SFX_Outfits.Start(); 		return;}break; /*Outfits*/
				case 1:{Show_Selection_Panel(2); SFX_ChooseGirl.Start(); 	return;}break; /*Girls*/
				case 2:{cMain.cNavBar.Show_NavBar(); cMain.PrevPage( -1 );	return;}break; /*Home*/
				case 3:{Show_Selection_Panel(1); SFX_Excessories.Start(); 	return;}break; /*Accessorys*/
				//End.
				case -1:{/*No Buttons*/}break;				
			}
			
			switch(Click_Drop_Down_Menu()){				
				case 0:{Rest_Girl_Popup.Show(Reset_Girl); return;}break; /*Clear All*/
				case 1:{Use_Undo(); return;}break; /*Undo*/
				case 2:{Save_Image_Temp(); return; Captcha.Show(Save_Image); return;}break; /*Photo*/
				case 3:{Extend_Drop_Down_Menu(); return;}break;
				//End.
				case -1:{/*No Buttons*/}break;				
			}		
			
			switch(Selected_Bar){				
				case 0:{/*Outfits*/
					switch(Click_Template(Scrolling_Menus[Selected_Bar])){		
						case 0:{Show_Selection_Panel(9); return;}break; /*Tops*/
						case 1:{Show_Selection_Panel(8); return;}break; /*Bottoms*/
						case 2:{Show_Selection_Panel(3); return;}break; /*Dresses*/
						case 3:{Show_Selection_Panel(6); return;}break; /*Pants*/
					}					
				}break;
				case 1:{/*Accessorys*/	
					switch(Click_Template(Scrolling_Menus[Selected_Bar])){		
						case 0:{Show_Selection_Panel(4);  SFX_Hats.Start();  return;}break; /*Hats*/
						case 1:{Show_Selection_Panel(7);  SFX_Shoes.Start(); return;}break; /*Shoes*/
						case 2:{Show_Selection_Panel(5);  return;}break; /*Headbands*/
						case 3:{Show_Selection_Panel(10); return;}break; /*Purses*/
					}					
				}break;						
				case 2:{/*Girls*/					
					trace(Click_Template(Scrolling_Menus[Selected_Bar]));
					Change_Item(0, 0, Click_Template(Scrolling_Menus[Selected_Bar]));
				}break;
				case 3:{/*Dresses*/					
					Change_Item(0, 5, Click_Template(Scrolling_Menus[Selected_Bar]));
				}break;
				case 4:{/*Hats*/					
					Change_Item(0, 9, Click_Template(Scrolling_Menus[Selected_Bar]));
				}break;
				case 5:{/*Headbands*/					
					Change_Item(0, 8, Click_Template(Scrolling_Menus[Selected_Bar]));
				}break;
				case 6:{/*Pants*/					
					Change_Item(0, 1, Click_Template(Scrolling_Menus[Selected_Bar]));
				}break;
				case 7:{/*Shoes*/					
					Change_Item(0, 2, Click_Template(Scrolling_Menus[Selected_Bar]));
				}break;
				case 8:{/*Skirts*/					
					Change_Item(0, 4, Click_Template(Scrolling_Menus[Selected_Bar]));
				}break;
				case 9:{/*Tops*/					
					Change_Item(0, 3, Click_Template(Scrolling_Menus[Selected_Bar]));
				}break;
				case 10:{/*Purses*/					
					Change_Item(0, 6, Click_Template(Scrolling_Menus[Selected_Bar]));
				}break;								
			}					
		}
		//---------------------------------------------------------------------			
		
		//---------------------------------------------------------------------		
		//This is the panels section.
		private function Build_Templates():void{
			
			Selected_Bar = 0;
			Scrolling_Speed = 0;
			Panel_Transition_In_Progress = false;
			
			Static_Menu_Outfits = new Sprite();		
			Images.Add( "Outfits", { container:Static_Menu_Outfits, fromFile: true } );
			Static_Menu_Outfits.visible = false;
			scene_mc.addChild(Static_Menu_Outfits);
			
			Static_Menu_Accessories = new Sprite();		
			Images.Add( "Accessories", { container:Static_Menu_Accessories, fromFile: true } );
			Static_Menu_Accessories.visible = false;
			scene_mc.addChild(Static_Menu_Accessories);
			
			Bodies_Template 		= new SheetTemplate(assets, imageFolder, "", "Panel_Girls");
			Dresses_Template 		= new SheetTemplate(assets, imageFolder, "", "Panel_Dresses");
			Hats_Template 			= new SheetTemplate(assets, imageFolder, "", "Panel_Hats");
			Headbands_Template 		= new SheetTemplate(assets, imageFolder, "", "Panel_HeadBands");
			Pants_Template 			= new SheetTemplate(assets, imageFolder, "", "Panel_Pants");
			Shoes_Template 			= new SheetTemplate(assets, imageFolder, "", "Panel_Shoes");
			Skirts_Template 		= new SheetTemplate(assets, imageFolder, "", "Panel_Skirts");
			Tops_Template 			= new SheetTemplate(assets, imageFolder, "", "Panel_Tops");	
			Purses_Template			= new SheetTemplate(assets, imageFolder, "", "Panel_Purses");	
						
		}
		private function Assemble_Templates():void{
			
			Scrolling_Menus = new Array();	
			Scrolling_Menus.push(Create_Selection_Panel(null, 				60, false, 0, 0, 4, new Rectangle(0, 0, 150, 100), Static_Menu_Outfits));
			Scrolling_Menus.push(Create_Selection_Panel(null, 				60, false, 0, 0, 4, new Rectangle(0, 0, 150, 100), Static_Menu_Accessories));
			Scrolling_Menus.push(Create_Selection_Panel(Bodies_Template, 	-1, true, 40));	
			Scrolling_Menus.push(Create_Selection_Panel(Dresses_Template, 	-1, true));	
			Scrolling_Menus.push(Create_Selection_Panel(Hats_Template, 		-1, true));	
			Scrolling_Menus.push(Create_Selection_Panel(Headbands_Template, -1, true));	
			Scrolling_Menus.push(Create_Selection_Panel(Pants_Template, 	-1, true, 25, 16));	
			Scrolling_Menus.push(Create_Selection_Panel(Shoes_Template, 	-1, true, 25));	
			Scrolling_Menus.push(Create_Selection_Panel(Skirts_Template, 	-1, true));	
			Scrolling_Menus.push(Create_Selection_Panel(Tops_Template, 		-1, true));	
			Scrolling_Menus.push(Create_Selection_Panel(Purses_Template, 	-1, true));
			
			Hide_Templates();		
			
			for(var X:int = 0; X < Scrolling_Menus.length; X++){
				scene_mc.addChild(Scrolling_Menus[X][2]);	
			}			
			
		}			
		private function Create_Selection_Panel(Template:SheetTemplate  = null,
												SpacingLimit:int 	 	= -1,
												Scolling:Boolean 	 	= true,
												OffsetX:int		 	 	= 0,
												OffsetY:int		 	 	= 0,
												ButtonCount		 	 	= 0,
												ButtonRect:Rectangle 	= null,
												Graphic:Sprite 			= null
												):Array{
			//---------------------------------------------------------
			
			//Conditions.			
			if(!Scolling){if(Graphic == null){return new Array();}if(ButtonRect == null){return new Array();}}
			else{if(Template == null){return new Array();}}
			
			//This panel.
			var Panel_Array:Array = new Array();			
			Panel_Array.push(0);
			Panel_Array.push(Scolling);
			
			//This bar.
			var Visible_Bar:Sprite = new Sprite();	
			Visible_Bar.x = 608; //Position on screen.
			Visible_Bar.y = 700;
			Visible_Bar.visible = false;
			Panel_Array.push(Visible_Bar);
			
			//Extra vars.
			Panel_Array.push(2); 				//Movement Speed.
			Panel_Array.push(0); 				//Movement Current.
			Panel_Array.push(0); 				//Movement Max.
			Panel_Array.push(new Point(0, 0));  //Pos center.	
			
			
			//The mask for this bar.
			var Mask:Bitmap 	= new Bitmap();			
			Mask.bitmapData 	= new BitmapData(800, 200, false, 0x000000);
			Mask.x 				= -(Mask.width / 2);
			Mask.y 				= -(Mask.height / 2);
			Mask.name       	= "The_Mask";		
			Visible_Bar.mask 	= Mask;	
			Visible_Bar.addChild(Mask);			
			
			//Spacing for elements.
			var Next_X:int = 0;	
			var PositionArray:Array = new Array();
			
			//If its just static image.
			if(!Scolling){				
				
				var PanelImage:Bitmap	= new Bitmap();			
				PanelImage.bitmapData 	= new BitmapData(Graphic.width, Graphic.height, true, 0x000000);
				PanelImage.x = -(PanelImage.width  / 2);
				PanelImage.y = -(PanelImage.height / 2);
				PanelImage.bitmapData.draw(Graphic);
				
				Visible_Bar.addChild(PanelImage);	
				
				var ButtonProps:Array = new Array();
				//Add prop.
				ButtonProps.push(ButtonCount);
				
				PositionArray.push(ButtonProps);
				
				for(var B:int = 0; B < ButtonCount; B++){			
					
					var NewButton:Sprite 	= new Sprite();
					var ButtonImage:Bitmap	= new Bitmap();			
					ButtonImage.bitmapData 	= new BitmapData(ButtonRect.width, ButtonRect.height, true, 0x000000);
					ButtonImage.x 			= 0;
					ButtonImage.y 			= 0;
					NewButton.addChild(ButtonImage);
					
					//Set position.
					NewButton.x = ((-(PanelImage.width / 2)) + (B * (PanelImage.width / ButtonCount)));
					NewButton.y = -(NewButton.height / 2);		
					NewButton.name = B.toString();
				
					Visible_Bar.addChild(NewButton);						
				}					
				
				//Set mask top.
				Visible_Bar.setChildIndex(Mask, (Visible_Bar.numChildren - 1));
				
				Panel_Array.push(PositionArray);  //prop array.
				
				return Panel_Array;
				
			}					
			
			//If its scrolling image.
			for(var X:int = 0; X < Template.Frame_Count(); X++){				
				
				//Get image.
				var New_Sprite:Sprite = Template.Get_Sprite(X, false, true);
				var SpriteProps:Array = new Array();
				
				//Set position.
				New_Sprite.x = Next_X;
				New_Sprite.y = -(New_Sprite.height / 2) + OffsetY;
				
				//Set X position for next.
				if(SpacingLimit == -1){Next_X  = (Next_X + New_Sprite.width) + OffsetX;}
				else{Next_X = (Next_X + SpacingLimit) + OffsetX;}							
				
				//Set the limit.
				if(SpacingLimit == -1){
					Panel_Array[5] = (Panel_Array[5] + New_Sprite.width) + OffsetX;
				}else{
					Panel_Array[5] = (Panel_Array[5] + SpacingLimit) + OffsetX;
				}		
				
				//Add prop.
				SpriteProps.push(New_Sprite.x);
					
				//Add element to bar.
				Visible_Bar.addChild(New_Sprite);		
				
				PositionArray.push(SpriteProps);
			
			}			
			
			Panel_Array.push(PositionArray);  //prop array.
			
			//Set mask top.
			Visible_Bar.setChildIndex(Mask, (Visible_Bar.numChildren - 1));
			
			//---------------------------------------------------------		
			
			return Panel_Array;
		}		
		
		private function Scroll_Template(Holder_Array:Array, Speed:int = 0){						
			
			if(!Holder_Array[2].visible){Holder_Array[2].visible = true;}
			
			if(!Holder_Array[1]){return;}
						
			Holder_Array[6].x 	= -(Holder_Array[5] / 2);
			Holder_Array[6].y 	= 0;			
			Holder_Array[3] 	= Speed;			
			Holder_Array[4]     = (Holder_Array[4] + Holder_Array[3]); //Current Position.
			
			if(Holder_Array[4] > Holder_Array[5]){ Holder_Array[4] = 0;}
			if(Holder_Array[4] < -Holder_Array[5]){Holder_Array[4] = 0;}			
						
			for(var X:int = 0; X < (Holder_Array[2].numChildren - 1); X++){			
				
				var TS:Sprite = Holder_Array[2].getChildAt(X);				
				
				TS.x = int((Holder_Array[6].x + Holder_Array[Holder_Array.length - 1][X][0]) + Holder_Array[4]);
				//To Low.
				if(TS.x < Holder_Array[6].x){
					TS.x = int((Holder_Array[6].x + Holder_Array[5]) - (Holder_Array[6].x - TS.x));
				}				
				//To High.
				if(TS.x > (Holder_Array[6].x + Holder_Array[5])){					
					TS.x = int(Holder_Array[6].x + (TS.x - (Holder_Array[6].x + Holder_Array[5])));
				}
				
			}
			
		}	
		private function Click_Template(Holder_Array:Array):int{				
			
			//Not Scrolling.
			if(!Holder_Array[1]){
				
				if(!Holder_Array[2].getChildAt(0).hitTestPoint(mouseX, mouseY, false)){return -1;}
				
				for(var B:int = 1; B < Holder_Array[2].numChildren; B++){				
					if(Holder_Array[2].getChildAt(B).hitTestPoint(mouseX, mouseY, false)){
						return Holder_Array[2].getChildAt(B).name;
					}				
				}
				
				return -1;
			}			
			
			if(!Holder_Array[2].getChildAt((Holder_Array[2].numChildren - 1)).hitTestPoint(mouseX, mouseY, false)){return -1;}
			
			//Scrolling.
			for(var X:int = 0; X < (Holder_Array[2].numChildren - 1); X++){	
				
				var TS:Sprite = Holder_Array[2].getChildAt(X);	
				
				if(TS.hitTestPoint(mouseX, mouseY, false)){
					return (X + 1);
				}
				
			}

			return -1;
		}	
		private function Hit_Template(Holder_Array:Array):Boolean{					
			//Not Scrolling.
			if(!Holder_Array[1]){if(Holder_Array[2].getChildAt(0).hitTestPoint(mouseX, mouseY, false)){return true;}}					
			if(Holder_Array[2].getChildAt((Holder_Array[2].numChildren - 1)).hitTestPoint(mouseX, mouseY, false)){return true;}
			return false;
		}	
		private function Scroll_Selection_Panel():void{
			if(Selected_Bar < 0){return;}			
			if(Panel_Transition_In_Progress){return;}
			Scroll_Template(Scrolling_Menus[Selected_Bar], Scrolling_Speed);		
			Scrolling_Menus[Selected_Bar][2].y = (Main_Hud.y + 130);
		}
		//Transitions.
		private function Show_Selection_Panel(GotoPanel:int = 0):void{						
			Panel_Transition_In_Progress = true;
			Hide_Templates(GotoPanel);					
		}		
		private function Hide_Templates(GotoPanel:int = 0){			
			for(var X:int = 0; X < Scrolling_Menus.length; X++){				
				if(Scrolling_Menus[X][2].visible){
					TweenLite.to(Scrolling_Menus[X][2], 0.5, {y:(Main_Hud.y + 500), onComplete:Transition_Phase_1_Completed, onCompleteParams:[X, GotoPanel]});
				}
			}				
		}
		private function Transition_Phase_1_Completed(Sel:int, GotoPanel:int):void{			
			Scrolling_Menus[Sel][2].visible = false;			
			//Selected_Bar = (Selected_Bar + 1);
			Selected_Bar = GotoPanel;
			if(Selected_Bar == Scrolling_Menus.length){Selected_Bar = 0;}			
			Scrolling_Menus[Selected_Bar][2].y = Scrolling_Menus[Sel][2].y;
			Scroll_Template(Scrolling_Menus[Selected_Bar]);
			Scrolling_Menus[Selected_Bar][2].visible = true;
			TweenLite.to(Scrolling_Menus[Selected_Bar][2], 0.5, {y:(Main_Hud.y + 130), onComplete:Transition_Phase_2_Completed});			
		}
		private function Transition_Phase_2_Completed():void{
			Panel_Transition_In_Progress = false;			
		}
		//---------------------------------------------------------------------		

		//---------------------------------------------------------------------
		//Girl section.
		private function Create_A_Girl():void{			
			
			if(GIRLS == null){GIRLS = new Array();}
			
			var newGirl:Array  = new Array();
			var Offsets:Array  = new Array();
			var Clothing:Array = new Array();
			
			var GIRL_Sprite:Sprite 				= new Sprite(); //Main holder.
			GIRL_Sprite.name = "Girl";
			
			var Body_Sprite:Sprite 				= new Sprite();
			var Head_Sprite:Sprite				= new Sprite();
			var Hat_Sprite:Sprite 				= new Sprite();						
			var Top_Sprite:Sprite 				= new Sprite();			
			var Skirt_Sprite:Sprite				= new Sprite();
			var Pants_Sprite:Sprite 			= new Sprite();	
			var Dress_Sprite:Sprite 			= new Sprite();			
			var Shoes_Sprite:Sprite 			= new Sprite();
			var Purse_Sprite:Sprite 			= new Sprite();			
			var HeadBand_Sprite:Sprite			= new Sprite();
			
			var Body_Sprite_Offset:Point 		= new Point(0, 0);
			var Head_Sprite_Offset:Point 		= new Point(21, -118);
			var Hat_Sprite_Offset:Point 		= new Point(18, -180);		
			var HeadBand_Sprite_Offset:Point 	= new Point(28, -186);
			var Pants_Sprite_Offset:Point 		= new Point(11, 167); //new Point(13, 167);			
			var Top_Sprite_Offset:Point 		= new Point(20, 13);				
			var Skirt_Sprite_Offset:Point 		= new Point(33, 125);			
			var Dress_Sprite_Offset:Point 		= new Point(-3, 108); //new Point(-2, 112);
			var Shoes_Sprite_Offset:Point 		= new Point(0, 222);  //new Point(2, 222);
			var Purse_Sprite_Offset:Point 		= new Point(-46, 46);							
			
			GIRL_Sprite.x = 479; GIRL_Sprite.y = 320;
			
			//Center of Girl.
			Body_Sprite.x = Body_Sprite_Offset.x;
			Body_Sprite.y = Body_Sprite_Offset.y;
			
			//Head.
			Head_Sprite.x = (Body_Sprite.x + Head_Sprite_Offset.x);
			Head_Sprite.y = (Body_Sprite.y + Head_Sprite_Offset.y);
			
			//Hat.
			Hat_Sprite.x = (Body_Sprite.x + Hat_Sprite_Offset.x);
			Hat_Sprite.y = (Body_Sprite.y + Hat_Sprite_Offset.y);
			
			//HeadBand.
			HeadBand_Sprite.x = (Body_Sprite.x + HeadBand_Sprite_Offset.x);
			HeadBand_Sprite.y = (Body_Sprite.y + HeadBand_Sprite_Offset.y);
			
			//Top.
			Top_Sprite.x = (Body_Sprite.x + Top_Sprite_Offset.x);
			Top_Sprite.y = (Body_Sprite.y + Top_Sprite_Offset.y);
			
			//Skirt.
			Skirt_Sprite.x = (Body_Sprite.x + Skirt_Sprite_Offset.x);
			Skirt_Sprite.y = (Body_Sprite.y + Skirt_Sprite_Offset.y);
			
			//Pants.
			Pants_Sprite.x = (Body_Sprite.x + Pants_Sprite_Offset.x);
			Pants_Sprite.y = (Body_Sprite.y + Pants_Sprite_Offset.y);
			
			//Dress.
			Dress_Sprite.x = (Body_Sprite.x + Dress_Sprite_Offset.x);
			Dress_Sprite.y = (Body_Sprite.y + Dress_Sprite_Offset.y);			
			
			//Shoes.
			Shoes_Sprite.x = (Body_Sprite.x + Shoes_Sprite_Offset.x);
			Shoes_Sprite.y = (Body_Sprite.y + Shoes_Sprite_Offset.y);
			
			//Purse.
			Purse_Sprite.x = (Body_Sprite.x + Purse_Sprite_Offset.x);
			Purse_Sprite.y = (Body_Sprite.y + Purse_Sprite_Offset.y);						
			
			Offsets.push(Body_Sprite_Offset);
			Offsets.push(Pants_Sprite_Offset);
			Offsets.push(Shoes_Sprite_Offset);			
			Offsets.push(Top_Sprite_Offset);
			Offsets.push(Skirt_Sprite_Offset);
			Offsets.push(Dress_Sprite_Offset);
			Offsets.push(Purse_Sprite_Offset);
			Offsets.push(Head_Sprite_Offset);
			Offsets.push(HeadBand_Sprite_Offset);						
			Offsets.push(Hat_Sprite_Offset);
			
			GIRL_Sprite.addChild(Body_Sprite);
			GIRL_Sprite.addChild(Pants_Sprite);
			GIRL_Sprite.addChild(Shoes_Sprite);			
			GIRL_Sprite.addChild(Top_Sprite);
			GIRL_Sprite.addChild(Skirt_Sprite);
			GIRL_Sprite.addChild(Dress_Sprite);	
			GIRL_Sprite.addChild(Purse_Sprite);
			GIRL_Sprite.addChild(Head_Sprite);
			GIRL_Sprite.addChild(HeadBand_Sprite);	
			GIRL_Sprite.addChild(Hat_Sprite);			
			
			Clothing.push([5, true]);
			Clothing.push([1, true]);
			Clothing.push([1, true]);
			Clothing.push([1, true]);
			Clothing.push([1, true]);
			Clothing.push([1, true]);
			Clothing.push([1, true]);
			Clothing.push([1, true]);
			Clothing.push([1, true]);
			Clothing.push([1, true]);
			
			scene_mc.addChild(GIRL_Sprite);	
			
			newGirl.push(GIRL_Sprite);
			newGirl.push(Offsets);		
			newGirl.push(Clothing);	
			
			GIRLS.push(newGirl);
			
		}		
		private function Change_Item(Girl:int, Part:int, Sel:int, Reset:Boolean = false, Show:Boolean = true):void{			
			
			if(!Reset){if(Point.distance(Sta_Mouse, New_Mouse) > 32){return;}}
			
			if(Sel == -1){return;}	
			
			GIRLS[Girl][2][Part][0] = Sel;
			
			if(!Reset){				
				Add_Undo(Girl);					
				GIRLS[Girl][2][Part][0] = Sel;
				GIRLS[Girl][2][Part][1] = Show;					
			}
			if(!Show){
				GIRLS[Girl][2][Part][1] = false;
				GIRLS[Girl][0].getChildAt(Part).visible = false;
			}
			
			var Folder:String  = "";
			var ImgName:String = "";
			switch(Part){
				case 0:{
					Folder  = "Bodies";	
					ImgName = "Body_";
					var imgURL:String = (imageFolder + "Fashion/Bodies/Head_" + Sel.toString() + ".png");
					var myHead:ImageLoader = new ImageLoader(imgURL, {container:GIRLS[Girl][0].getChildAt(7), onComplete:Girl_AssetSwap, centerRegistration:true, pGirl:Girl, pPart:7, pShow:Show, pReset:Reset});
					myHead.load();	
					if(!Reset){Setup_A_Girl(); return;}					
				}break;				
				case 1:{Folder = "Pants"; 		ImgName = "Pants_";}break;
				case 2:{Folder = "Shoes";		ImgName = "Shoes_";}break;					
				case 3:{Folder = "Tops";		ImgName = "Top_";}break;		
				case 4:{Folder = "Skirts";		ImgName = "Skirt_";}break;
				case 5:{Folder = "Dresses"; 	ImgName = "Dress_";}break;
				case 6:{Folder = "Purses";		ImgName = "Purs_";}break;	
				case 8:{Folder = "Headbands"; 	ImgName = "HeadBand_";}break;
				case 9:{Folder = "Hats"; 		ImgName = "Hat_";}break;											
			}
			
			var imgURL:String = (imageFolder + "Fashion/" + Folder + "/" + ImgName + Sel.toString() + ".png");
			//trace(imgURL);
			var myImage:ImageLoader = new ImageLoader(imgURL, {container:GIRLS[Girl][0].getChildAt(Part), onComplete:Girl_AssetSwap, centerRegistration:true, pGirl:Girl, pPart:Part, pShow:Show, pReset:Reset});
			myImage.load();
		}		
		private function Girl_AssetSwap(e:LoaderEvent):void{			
			
			if((e.target.vars.pPart != 0) && (!e.target.vars.pReset)){
				GIRLS[e.target.vars.pGirl][0].getChildAt(e.target.vars.pPart).x = (GIRLS[e.target.vars.pGirl][1][0].x + GIRLS[e.target.vars.pGirl][1][e.target.vars.pPart].x);
				GIRLS[e.target.vars.pGirl][0].getChildAt(e.target.vars.pPart).y = (GIRLS[e.target.vars.pGirl][1][0].y + GIRLS[e.target.vars.pGirl][1][e.target.vars.pPart].y);
			}
			
			if(GIRLS[e.target.vars.pGirl][0].getChildAt(e.target.vars.pPart).getChildAt(0) != null){
				if(GIRLS[e.target.vars.pGirl][0].getChildAt(e.target.vars.pPart).numChildren > 1){
					GIRLS[e.target.vars.pGirl][0].getChildAt(e.target.vars.pPart).removeChildAt(0);
				}
			}			
			
			if(e.target.vars.pShow){
				
				if(!GIRLS[e.target.vars.pGirl][0].getChildAt(e.target.vars.pPart).visible){
				   GIRLS[e.target.vars.pGirl][2][e.target.vars.pPart][1] = true;
				   GIRLS[e.target.vars.pGirl][0].getChildAt(e.target.vars.pPart).visible = true;
				}
				
				if(!e.target.vars.pReset){					
					
					switch(e.target.vars.pPart){					
						//Hat.
						case 9:{Hide_Items(e.target.vars.pGirl, [8]);}break;//Hide Headband.
						//Headband.
						case 8:{Hide_Items(e.target.vars.pGirl, [9]);}break;//Hide Hat.					
						//Dress.					
						case 5:{Hide_Items(e.target.vars.pGirl, [4, 3]);}break;
						//Pants.
						case 1:{
							if(GIRLS[e.target.vars.pGirl][0].getChildAt(5).visible){
								Hide_Items(e.target.vars.pGirl, [5]);
								Show_Random_Item(e.target.vars.pGirl, [3]);
							}
						}break;//Hide Dress and Skirt. show a top.
						//Skirt.
						case 4:{
							if(GIRLS[e.target.vars.pGirl][0].getChildAt(5).visible){
								Hide_Items(e.target.vars.pGirl, [5]);
								Show_Random_Item(e.target.vars.pGirl, [3]);
							}
						}break;//Hide Dress and Pants. show a top.					
						//Top.
						case 3:{
							if(GIRLS[e.target.vars.pGirl][0].getChildAt(5).visible){
								Hide_Items(e.target.vars.pGirl, [5]);
								Show_Random_Item(e.target.vars.pGirl, [1]);
							}										
						}break;//Hide Dress if on. Show random pants.						
					}					
					
					Spawn_Glitter_Bombs(e.target.vars.pGirl, e.target.vars.pPart);
				}
				
			}
			
		}
		private function Setup_A_Girl():void{
			
			trace(GIRLS[0][2][0][0]);
			
			switch(GIRLS[0][2][0][0]){
				
				case 1:{//Plum.	
				
					GIRLS[0][0].getChildAt(8).x = -5;
					GIRLS[0][0].getChildAt(8).y = -205;
				
					Change_Item(0, 0, 1, true, true);  //Head.
					Change_Item(0, 1, 9, true, true);  //Pants.
					Change_Item(0, 2, 7, true, true);  //Shoes.			
					Change_Item(0, 3, 3, true, false); //Tops.
					Change_Item(0, 4, 4, true, false); //Skirts.
					Change_Item(0, 5, 5, true, true);  //Dresses.
					Change_Item(0, 6, 6, true, false); //Purses.
					Change_Item(0, 8, 6, true, true);  //Headbands.
					Change_Item(0, 9, 8, true, false); //Hats.					
					return;
				}break;
				
				case 2:{//Cherry.		
				
					GIRLS[0][0].getChildAt(8).x = -5;
					GIRLS[0][0].getChildAt(8).y = -209;
				
					Change_Item(0, 0, 2, true, true);  //Head.
					Change_Item(0, 1, 1, true, true);  //Pants.
					Change_Item(0, 2, 1, true, true);  //Shoes.			
					Change_Item(0, 3, 3, true, false); //Tops.
					Change_Item(0, 4, 4, true, false); //Skirts.
					Change_Item(0, 5, 2, true, true);  //Dresses.
					Change_Item(0, 6, 6, true, false); //Purses.
					Change_Item(0, 8, 1, true, true);  //Headbands.
					Change_Item(0, 9, 8, true, false); //Hats.					
					return;
				}break;
				
				case 3:{//Orange.		
				
					GIRLS[0][0].getChildAt(8).x = 3;
					GIRLS[0][0].getChildAt(8).y = -208;
				
					Change_Item(0, 0, 3, true, true);  //Head.
					Change_Item(0, 1, 3, true, true);  //Pants.
					Change_Item(0, 2, 3, true, true);  //Shoes.			
					Change_Item(0, 3, 3, true, false); //Tops.
					Change_Item(0, 4, 4, true, false); //Skirts.
					Change_Item(0, 5, 4, true, true);  //Dresses.
					Change_Item(0, 6, 6, true, false); //Purses.
					Change_Item(0, 8, 3, true, true);  //Headbands.
					Change_Item(0, 9, 8, true, false); //Hats.					
					return;
				}break;
				
				case 4:{//Lemon.			
				
					GIRLS[0][0].getChildAt(8).x = 3;
					GIRLS[0][0].getChildAt(8).y = -208;
					
					Change_Item(0, 0, 4, true, true);  //Head.
					Change_Item(0, 1, 4, true, true);  //Pants.
					Change_Item(0, 2, 4, true, true);  //Shoes.			
					Change_Item(0, 3, 3, true, false); //Tops.
					Change_Item(0, 4, 4, true, false); //Skirts.
					Change_Item(0, 5, 1, true, true);  //Dresses.
					Change_Item(0, 6, 6, true, false); //Purses.
					Change_Item(0, 8, 4, true, true);  //Headbands.
					Change_Item(0, 9, 8, true, false); //Hats.					
					return;
				}break;
				
				case 5:{//Strawberry.					
				
					GIRLS[0][0].getChildAt(9).x = 28;
					GIRLS[0][0].getChildAt(9).y = -186;
					
					Change_Item(0, 0, 5, true, true);  //Head.
					Change_Item(0, 1, 2, true, true);  //Pants.
					Change_Item(0, 2, 6, true, true);  //Shoes.			
					Change_Item(0, 3, 3, true, false); //Tops.
					Change_Item(0, 4, 4, true, false); //Skirts.
					Change_Item(0, 5, 7, true, true);  //Dresses.
					Change_Item(0, 6, 6, true, false); //Purses.
					Change_Item(0, 8, 4, true, false); //Headbands.
					Change_Item(0, 9, 6, true, true);  //Hats.					
					return;
				}break;
				
				case 6:{//Rasberry.						
					
					GIRLS[0][0].getChildAt(8).x = 1;
					GIRLS[0][0].getChildAt(8).y = -198;
					
					Change_Item(0, 0, 6, true, true);  //Head.
					Change_Item(0, 1, 5, true, true);  //Pants.
					Change_Item(0, 2, 5, true, true);  //Shoes.			
					Change_Item(0, 3, 3, true, false); //Tops.
					Change_Item(0, 4, 4, true, false); //Skirts.
					Change_Item(0, 5, 6, true, true);  //Dresses.
					Change_Item(0, 6, 6, true, false); //Purses.
					Change_Item(0, 8, 5, true, true);  //Headbands.
					Change_Item(0, 9, 8, true, false); //Hats.					
					return;
				}break;
				
				case 7:{//Blueberry.	
				
					GIRLS[0][0].getChildAt(8).x = -2;
					GIRLS[0][0].getChildAt(8).y = -211;
					
					Change_Item(0, 0, 7, true, true);  //Head.
					Change_Item(0, 1, 2, true, true);  //Pants.
					Change_Item(0, 2, 2, true, true);  //Shoes.			
					Change_Item(0, 3, 3, true, false); //Tops.
					Change_Item(0, 4, 4, true, false); //Skirts.
					Change_Item(0, 5, 3, true, true);  //Dresses.
					Change_Item(0, 6, 6, true, false); //Purses.
					Change_Item(0, 8, 2, true, true);  //Headbands.
					Change_Item(0, 9, 8, true, false); //Hats.					
					return;
				}break;				
				
			}
			
		}	
		private function Hide_Items(Girl:int, Locs:Array):void{			
			for(var X:int = 0; X < Locs.length; X++){				
				GIRLS[Girl][2][Locs[X]][1] = false;
				GIRLS[Girl][0].getChildAt(Locs[X]).visible = false;
			}			
		}	
		private function Show_Random_Item(Girl:int, Locs:Array):void{			
			for(var X:int = 0; X < Locs.length; X++){
				Change_Item(Girl, Locs[X], 5, true, true);
			}			
		}
		//Moving items.
		private function Select_Moving_Item(GIRL:int):void{			
			if(GIRLS_Item_Selected == null){GIRLS_Item_Selected = new Point(0, 0);}				
			if(Mouse_Item_Click_Position == null){Mouse_Item_Click_Position = new Point(0, 0);}	
			if(Mouse_World_Click_Position == null){Mouse_World_Click_Position = new Point(0, 0);}				
			var Able_Items:Array = [6, 8, 9, 4, 3, 1, 5];
			GIRLS_Item_Selected.x = GIRL;		
			for(var X:int = 0; X < Able_Items.length; X++){
				if(GIRLS[GIRL][0].getChildAt(Able_Items[X]).visible){
					if(GIRLS[GIRL][0].getChildAt(Able_Items[X]).hitTestPoint(mouseX, mouseY, true)){
						GIRLS_Item_Selected.y = Able_Items[X];
						Mouse_Item_Click_Position.x = GIRLS[GIRL][0].getChildAt(Able_Items[X]).x;
						Mouse_Item_Click_Position.y = GIRLS[GIRL][0].getChildAt(Able_Items[X]).y;					
						Mouse_World_Click_Position.x = (mouseX - GIRLS[GIRL][0].x);
						Mouse_World_Click_Position.y = (mouseY - GIRLS[GIRL][0].y);	
						GIRLS_Item_Selected_Limit = 100;
						return;
					}					
				}
			}
			GIRLS_Item_Selected.x = -1;
			GIRLS_Item_Selected.y = -1;					
		}
		private function Update_Moving_Item():void{
			
			if(GIRLS_Item_Selected == null){return;}
			if(GIRLS_Item_Selected.x == -1){return;}
			if(GIRLS_Item_Selected.y == -1){return;}			
			
			var G_Object:Sprite = GIRLS[GIRLS_Item_Selected.x][0].getChildAt(GIRLS_Item_Selected.y);			
			
			if((Touch_1_ID != -1) && (Touch_2_ID != -1) && (GIRLS_Item_Selected.y == 9)){
						
				var Max_S:Number = 0.5;
				var Min_S:Number = 0.0;
				
				var Max_D:Number = 200;
				var Min_D:Number = 64;						
				
				var Distance:Number = Point.distance(Touch_1_Pos, Touch_2_Pos);
				if(Distance > Max_D){Distance = Max_D;}
				if(Distance < Min_D){Distance = Min_D;}
				
				var Scaler:Number = ((Distance * Max_S) / Max_D) + 0.5;				
				
				G_Object.scaleX = Scaler;
				G_Object.scaleY = Scaler;
				
				return;
										
			}				
			
			
			G_Object.x = ((mouseX - GIRLS[GIRLS_Item_Selected.x][0].x) + (Mouse_Item_Click_Position.x - Mouse_World_Click_Position.x));
			G_Object.y = ((mouseY - GIRLS[GIRLS_Item_Selected.x][0].y) + (Mouse_Item_Click_Position.y - Mouse_World_Click_Position.y));
			
			var G_Loc:Point = new Point(G_Object.x,
										G_Object.y);			
										
			var L_Loc:Point = new Point(GIRLS[GIRLS_Item_Selected.x][1][GIRLS_Item_Selected.y].x,
										GIRLS[GIRLS_Item_Selected.x][1][GIRLS_Item_Selected.y].y);
			
			//Change_Item(0, 0, 7, true, true);  //Head.
			//Change_Item(0, 1, 2, true, true);  //Pants.
			//Change_Item(0, 2, 2, true, true);  //Shoes.			
			//Change_Item(0, 3, 3, true, false); //Tops.
			//Change_Item(0, 4, 4, true, false); //Skirts.
			//Change_Item(0, 5, 3, true, true);  //Dresses.
			//Change_Item(0, 6, 6, true, false); //Purses.
			//Change_Item(0, 8, 2, true, true);  //Headbands.
			//Change_Item(0, 9, 8, true, false); //Hats.	
			//var Able_Items:Array = [6, 8, 9, 4, 3, 1, 5];
			switch(GIRLS_Item_Selected.y){
				case 1:{GIRLS_Item_Selected_Limit = 5;}break;
				//case 2:{GIRLS_Item_Selected_Limit = 100;}break;
				case 3:{GIRLS_Item_Selected_Limit = 5;}break;
				case 4:{GIRLS_Item_Selected_Limit = 5;}break;
				case 5:{GIRLS_Item_Selected_Limit = 5;}break;
				case 6:{GIRLS_Item_Selected_Limit = 100;}break;
				case 8:{GIRLS_Item_Selected_Limit = 100;}break;
				case 9:{GIRLS_Item_Selected_Limit = 100;}break;
			}
										
			
			if(Point.distance(L_Loc, G_Loc) > GIRLS_Item_Selected_Limit){
				
				var Angle:Number = Math.atan2((G_Loc.y - L_Loc.y), 
											  (G_Loc.x - L_Loc.x));
				
				G_Object.x = (L_Loc.x + (GIRLS_Item_Selected_Limit * Math.cos(Angle)));
				G_Object.y = (L_Loc.y + (GIRLS_Item_Selected_Limit * Math.sin(Angle)));				
				
			}		
			
		}
		private function Stop_Moving_Item():void{	
			if(GIRLS_Item_Selected == null){return;}
			GIRLS_Item_Selected.x = -1;
			GIRLS_Item_Selected.y = -1;
		}
		//---------------------------------------------------------------------		
		
		//---------------------------------------------------------------------		
		//Glitter Bombs.
		private function Initialize_Glitter_Bombs():void{
			Glitter_Accum = 0;
			Glitter_Holder = new Sprite();
			Glitter_Holder.x = 0;
			Glitter_Holder.y = 0;
			scene_mc.addChild(Glitter_Holder);
		}
		private function Spawn_Glitter_Bombs(GIRL:int, PART:int):void{
						
			var bounds:Rectangle = GIRLS[GIRL][0].getChildAt(PART).getBounds(GIRLS[GIRL][0].getChildAt(PART));
			var Spotter_BitmapData:BitmapData = new BitmapData(bounds.width, bounds.height, true, 0);			
			Spotter_BitmapData.draw(GIRLS[GIRL][0].getChildAt(PART), new Matrix(1, 0, 0, 1, -bounds.x, -bounds.y));
			
			trace("-------------------------------------");			
			for(var X:int = 0; X < 60; X++){
				
				var pX:int = Math.floor(Math.random() * Spotter_BitmapData.width);
				var pY:int = Math.floor(Math.random() * Spotter_BitmapData.height);
				var Value:uint = Spotter_BitmapData.getPixel32(pX, pY);		
				
				if(Value != 0){
					var dX:int = ((((GIRLS[GIRL][0].getChildAt(PART).x) - (GIRLS[GIRL][0].getChildAt(PART).width  / 2)) + pX) + GIRLS[GIRL][0].x);
					var dY:int = ((((GIRLS[GIRL][0].getChildAt(PART).y) - (GIRLS[GIRL][0].getChildAt(PART).height / 2)) + pY) + GIRLS[GIRL][0].y);
					Spawn_Glitter(dX, dY);					
				}
				
			}			
			trace("-------------------------------------");
			
			Spotter_BitmapData.dispose();
			
		}
		private function Spawn_Glitter(pX:int, pY:int):void{
			
			if(Glitter_Sprite.numChildren == 0){return;}
			
			Glitter_Accum = (Glitter_Accum + 1);
			
			var New_Glitter:Sprite = new Sprite();
			var Glit_Bit:Bitmap = new Bitmap();
			Glit_Bit.bitmapData = new BitmapData(Glitter_Sprite.width, Glitter_Sprite.height, true, 0x000000);
			Glit_Bit.bitmapData.draw(Glitter_Sprite);
			Glit_Bit.x = -(Glit_Bit.width  / 2);
			Glit_Bit.y = -(Glit_Bit.height / 2);
			
			New_Glitter.x = pX;
			New_Glitter.y = pY;
			New_Glitter.scaleX = 0.3;
			New_Glitter.scaleY = 0.3;
			New_Glitter.name = Glitter_Accum.toString();
			
			New_Glitter.addChild(Glit_Bit);			
			Glitter_Holder.addChild(New_Glitter);
			
			var End_Y:int = Math.floor(pY + ((Math.random() * 5) + 5));
			
			TweenLite.to(New_Glitter, 1, {y:End_Y, alpha:0, rotation:(Math.random() * 360), onComplete:Glitter_Done, onCompleteParams:[New_Glitter.name]});
		}
		private function Glitter_Done(Name:String):void{		
			Glitter_Holder.removeChild(Glitter_Holder.getChildByName(Name));		
		}
		private function Destroy_Glitter():void{
			for(var X:int = 0; X < Glitter_Holder.numChildren; X++){				
				TweenLite.killTweensOf(Sprite(Glitter_Holder.getChildAt(X)));
				Glitter_Holder.removeChild(Sprite(Glitter_Holder.getChildAt(X)));
			}
		}
		//---------------------------------------------------------------------				
		
		//---------------------------------------------------------------------
		//Other functions.
		private function Add_Undo(Girl:int):void{	
		
			trace("Added Unod");
		
			var Girl_Sheet:Array 	= new Array();			
			
			var a_Body:Array  		= [GIRLS[Girl][2][0][0], GIRLS[Girl][2][0][1]]; //Sel, visible.
			var a_Pants:Array 		= [GIRLS[Girl][2][1][0], GIRLS[Girl][2][1][1]]; //Sel, visible.
			var a_Shoes:Array 		= [GIRLS[Girl][2][2][0], GIRLS[Girl][2][2][1]]; //Sel, visible.
			var a_Skirt:Array 		= [GIRLS[Girl][2][3][0], GIRLS[Girl][2][3][1]]; //Sel, visible.
			var a_Top:Array 		= [GIRLS[Girl][2][4][0], GIRLS[Girl][2][4][1]]; //Sel, visible.
			var a_Dress:Array 		= [GIRLS[Girl][2][5][0], GIRLS[Girl][2][5][1]]; //Sel, visible.
			var a_Purse:Array 		= [GIRLS[Girl][2][6][0], GIRLS[Girl][2][6][1]]; //Sel, visible.
			var a_Head:Array 		= [GIRLS[Girl][2][7][0], GIRLS[Girl][2][7][1]]; //Sel, visible.
			var a_HeadBand:Array 	= [GIRLS[Girl][2][8][0], GIRLS[Girl][2][8][1]]; //Sel, visible.
			var a_Hat:Array 		= [GIRLS[Girl][2][9][0], GIRLS[Girl][2][9][1]]; //Sel, visible.				
			
			Girl_Sheet.push(a_Body);
			Girl_Sheet.push(a_Pants);
			Girl_Sheet.push(a_Shoes);
			Girl_Sheet.push(a_Skirt);
			Girl_Sheet.push(a_Top);
			Girl_Sheet.push(a_Dress);
			Girl_Sheet.push(a_Purse);
			Girl_Sheet.push(a_Head);
			Girl_Sheet.push(a_HeadBand);
			Girl_Sheet.push(a_Hat);
			
			Undos.push(Girl_Sheet);
			
		}
		private function Use_Undo():void{			
			trace(Undos.length);
			if(Undos.length <= 0){return;}			
			for(var X:int = 0; X < Undos[(Undos.length - 1)].length; X++){
				if(X == 7){continue;}				
				//trace("Part " + X + " Selection " + Undos[(Undos.length - 1)][X][0] + " Visible " + Undos[(Undos.length - 1)][X][1]);				
				Change_Item(0, X, Undos[(Undos.length - 1)][X][0], true, Undos[(Undos.length - 1)][X][1]);				
			}
			if(Undos.length > 1){
				Undos.splice((Undos.length - 1), 1);	
			}
			
		}
		private function Reset_Undos():void{	
			Undos.length = 0;
		}
		private function Save_Image(Correct:Boolean, Cancel:Boolean):void{					
			
			if(Cancel){return;}
			if(!Correct){return;}						
			
			//trace(cMain.get_localWidth() + " " + cMain.get_localHeight());
			var Distance:int = ((1366 - cMain.get_localWidth()) / 2);
						
			//Save To Cam.
			if(camSave){			
			
				for(var X:int = 0; X < scene_mc.numChildren; X++){
					scene_mc.getChildAt(X).visible = false;
				}			
				
				scene_mc.getChildAt(0).visible  = true;
				scene_mc.getChildByName("Girl").visible = true;
				
				var Scene_Bitmap:BitmapData = new BitmapData(cMain.get_localWidth(), 768, true, 0x000000);
				var Full_Scene:BitmapData   = new BitmapData(1366, 768, true, 0x000000);
				Full_Scene.draw(scene_mc);
				//Full_Scene.draw(scene_mc.GIRL_Sprite);
				
				var Draw_Rect:Rectangle = new Rectangle(Distance, 0, cMain.get_localWidth(), 768);
				var Draw_Pos:Point      = new Point(0, 0);				
				Scene_Bitmap.copyPixels(Full_Scene, Draw_Rect, Draw_Pos, null, null, true);				
				
				for(var S:int = 0; S < scene_mc.numChildren; S++){
					scene_mc.getChildAt(S).visible = true;
				}
				
				camRoll.addBitmapData(Scene_Bitmap);	
				
				/*var TemBit:Bitmap = new Bitmap();
				TemBit.bitmapData  = new BitmapData(Scene_Bitmap.width, Scene_Bitmap.height, true, 0x000000);
				TemBit.bitmapData.draw(Scene_Bitmap);
				
				//TemBit.x = -171;
				scene_mc.addChild(TemBit);*/
				
			}		
			
		}
		private function Save_Image_Temp():void{		
			
			//trace(cMain.get_localWidth() + " " + cMain.get_localHeight());
			var Distance:int = ((1366 - cMain.get_localWidth()) / 2);
						
			//Save To Cam.
			if(camSave){			
			
				var Show_Locs:Array = new Array();
			
				for(var X:int = 0; X < scene_mc.numChildren; X++){
					if(scene_mc.getChildAt(X).visible){Show_Locs.push(X);}
					scene_mc.getChildAt(X).visible = false;
				}			
				
				scene_mc.getChildAt(0).visible  = true;
				scene_mc.getChildByName("Girl").visible = true;
				
				var Scene_Bitmap:BitmapData = new BitmapData(cMain.get_localWidth(), 768, true, 0x000000);
				var Full_Scene:BitmapData   = new BitmapData(1366, 768, true, 0x000000);
				Full_Scene.draw(scene_mc);
				//Full_Scene.draw(scene_mc.GIRL_Sprite);
				
				var Draw_Rect:Rectangle = new Rectangle(0, 0, cMain.get_localWidth(), 768);
				var Draw_Pos:Point      = new Point(0, 0);				
				Scene_Bitmap.copyPixels(Full_Scene, Draw_Rect, Draw_Pos, null, null, true);				
				
				for(var S:int = 0; S < Show_Locs.length; S++){
					scene_mc.getChildAt(Show_Locs[S]).visible = true;
				}
				
				camRoll.addBitmapData(Scene_Bitmap);	
				
				Show_Locs.length = 0;
				
			}		
			
		}
		private function Reset_Girl(Correct:Boolean):void{		
			if(Correct){
				Setup_A_Girl();
				Reset_Undos();
				trace("Reset Girl");	
			}
		}
		//---------------------------------------------------------------------
		
		//---------------------------------------------------------------------
		//Multy Touch for Ipad.
		//---------------------------------------------------------------------
		private function Initialize_Touch():void{
			Touch_1_ID 	= -1;
			Touch_1_Pos = new Point(0, 0);
			Touch_2_ID 	= -1;
			Touch_2_Pos = new Point(0, 0);
		}
		private function Destroy_Touch():void{
			Touch_1_Pos = null;
			Touch_2_Pos = null;
		}
		private function onTouchBegin(e:TouchEvent):void{					
						
			//Check if full up.
			if((Touch_1_ID != -1) && (Touch_2_ID != -1)){return;}		
			
			if(Touch_1_ID != -1){				
				Touch_2_ID 		= e.touchPointID;
				Touch_2_Pos.x 	= e.stageX; 
				Touch_2_Pos.y 	= e.stageY; 				
				return;
			}else{				
				Touch_1_ID 		= e.touchPointID;
				Touch_1_Pos.x 	= e.stageX; 
				Touch_1_Pos.y 	= e.stageY;
				return;				
			}			
		}
		private function onTouchMove(e:TouchEvent):void{						
			if(Touch_1_ID == e.touchPointID){
				Touch_1_Pos.x 	= e.stageX; 
				Touch_1_Pos.y 	= e.stageY;	
			}			
			if(Touch_2_ID == e.touchPointID){
				Touch_2_Pos.x 	= e.stageX; 
				Touch_2_Pos.y 	= e.stageY;	
			}			
		} 
		private function onTouchEnd(e:TouchEvent):void{				
			if(Touch_1_ID == e.touchPointID){
				Touch_1_ID 	  = -1;
				Touch_2_ID    = -1;							
			}
			if(Touch_2_ID == e.touchPointID){
				Touch_1_ID 	  = -1;
				Touch_2_ID    = -1;					
			}
		}		
		//---------------------------------------------------------------------	
		
	}
}
﻿package {

	import com.cupcake.Utils;
	import com.greensock.TweenMax;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import org.osflash.signals.Signal;
	
	public class Bejeweled extends Sprite 
	{
		private const ROWS:int 				= 6;
		private const COLS:int				= 10;
		
		public var gameOver:Boolean			= false;
		
		public var SignalStalk:Signal		= new Signal(int);
		public var SignalPopped:Signal		= new Signal;
		
		private var clickPossible:Boolean	= false;
		private var inaRow:uint				= 0;
		private var itemSize:Number			= 75;
		private var match:Boolean			= true;
		private var moveState:Boolean		= false;
		private var score:uint				= 0;
		private var selectorRow:int			= -10;
		private var selectorColumn:int		= -10;
		private var swapState:Boolean		= false;
		
		private var jewel1:Class			= Jewel1;
		private var jewel2:Class			= Jewel2;
		private var jewel3:Class			= Jewel3;
		private var jewel4:Class			= Jewel4;
		private var jewel5:Class			= Jewel5;
		
		private var aGem:MovieClip;
		private var colours_array:Array		= new Array(jewel1,jewel2,jewel3,jewel4,jewel5);
		private var delayHint:TweenMax;
		private var gemContainer:MovieClip	= new MovieClip;
		private var gems_array:Array		= new Array;
		private var hint1:MovieClip			= new JewelHint;
		private var hint2:MovieClip			= new JewelHint;
		private var score_txt:TextField		= new TextField;
		private var selectorBox:MovieClip	= new JewelSelector;
		
		public function Bejeweled() 
		{
			addChild(gemContainer);
			
			// Create Gems in rows and columns
			for (var i:uint=0; i<= ROWS; i++) {
				gems_array[i]=new Array();
				for (var j:uint=0; j <= COLS; j++) {
					do {
						gems_array[i][j]=Math.floor(Math.random()*5);
					}
					while (rowLineLength(i,j)>2 || columnLineLength(i,j)>2);
					aGem = new colours_array[gems_array[i][j]]();
					aGem.name=i+"_"+j;
					aGem.x=j*itemSize;
					aGem.y=i*itemSize;
					gemContainer.addChild(aGem);
				}
			}
			// Create and style selector box
			addChild(selectorBox);
			selectorBox.visible=false;
			
			hint1.width = hint1.height = hint2.width = hint2.height = itemSize;
			addChild(hint1);
			hint1.visible=false;
			addChild(hint2);
			hint2.visible=false;
			
			// Listen for user input
			this.addEventListener(MouseEvent.MOUSE_DOWN,onClick);
			this.addEventListener(MouseEvent.MOUSE_MOVE,onMove);
			this.addEventListener(MouseEvent.MOUSE_UP,onUp);
			this.addEventListener(Event.ENTER_FRAME,everyFrame);
		}
		
		// Every frame...
		private function everyFrame(e:Event):void 
		{			
			if(!swapState && !gameOver)
			{
				var gemsAreFalling:Boolean=false;
				
				// Check each gem for space below it
				for (var i:int=ROWS - 1; i>=0; i--) {
					for (var j:uint=0; j<=COLS; j++) {
						// If a spot contains a gem, and has an empty space below...
						if (gems_array[i][j] != -1 && gems_array[i+1][j]==-1) {
							// Set gems falling
							gemsAreFalling=true;
							gems_array[i+1][j]=gems_array[i][j];
							gems_array[i][j]=-1;
							TweenMax.to(gemContainer.getChildByName(i+"_"+j),0.2,{y:(i*itemSize) + itemSize});
							gemContainer.getChildByName(i+"_"+j).name=(i+1)+"_"+j;
							break;
						}
					}
					// If a gem is falling
					if (gemsAreFalling) {
						// don't allow any more to start falling
						break;
					}
				}
				// If no gems are falling
				if (!gemsAreFalling) {
					// Assume no new gems are needed
					var needNewGem:Boolean=false;
					// but check all spaces...
					for (i=ROWS; i>=0; i--) {
						for (j=0; j<=COLS; j++) {
							// and if a spot is empty
							if (gems_array[i][j]==-1) {
								// now we know we need a new gem
								needNewGem=true;
								// pick a random color for the gem
								gems_array[0][j]=Math.floor(Math.random()*5);
								// create the gem
								aGem= new colours_array[gems_array[0][j]]();
								// ID it
								aGem.name="0_"+j;
								// position it
								aGem.x=j*itemSize;
								aGem.y=0;
								// show it
								gemContainer.addChild(aGem);
								// stop creating new gems
								break;
							}
						}
						// if a new gem was created, stop checking
						if (needNewGem) {
							break;
						}
					}
					// If no new gems were needed...
					if (! needNewGem) {
						// assume no more/new lines are on the board
						var moreLinesAvailable:Boolean=false;
						// check all gems
						for (i=ROWS; i>=0; i--) {
							for (j=0; j<=COLS; j++) {
								// if a line is found
								if (rowLineLength(i,j)>2 || columnLineLength(i,j)>2) {
									// then we know more lines are available
									moreLinesAvailable=true;
									// creat a new array, set the gem type of the line, and where it is
									var lineGems:Array=[i+"_"+j];
									var gemType:uint=gems_array[i][j];
									var linePosition:int;
									// check t's a horizontal line...
									if (rowLineLength(i,j)>2) {
										// if so, find our how long it is and put all the line's gems into the array
										linePosition=j;
										while (sameGemIsHere(gemType,i,linePosition-1)) {
											linePosition--;
											lineGems.push(i+"_"+linePosition);
										}
										linePosition=j;
										while (sameGemIsHere(gemType,i,linePosition+1)) {
											linePosition++;
											lineGems.push(i+"_"+linePosition);
										}
									}
									// check t's a vertical line...
									if (columnLineLength(i,j)>2) {
										// if so, find our how long it is and put all the line's gems into the array
										linePosition=i;
										while (sameGemIsHere(gemType,linePosition-1,j)) {
											linePosition--;
											lineGems.push(linePosition+"_"+j);
										}
										linePosition=i;
										while (sameGemIsHere(gemType,linePosition+1,j)) {
											linePosition++;
											lineGems.push(linePosition+"_"+j);
										}
									}
									
									// pause to wait for line 
									TweenMax.delayedCall((0.5+(lineGems.length*0.2)),destroyLine);
									// change the state so we dont check until its done.
									swapState = true;
									
									// explode lines
									for (i=0; i<lineGems.length; i++) {
										var mc:MovieClip = gemContainer.getChildByName(lineGems[i]) as MovieClip;
										TweenMax.to(mc.graphic,0.6,{delay:(i*0.2)+0.1, scaleX:1.3, scaleY:1.3});
										TweenMax.delayedCall((i*0.2),mc.graphic.play);
										TweenMax.delayedCall((i*0.2),SignalPopped.dispatch);
									}
									
									resetHintTimer();
									
									function destroyLine():void{
									// for all gems in the line...
										for (i=0; i<lineGems.length; i++) {
											var mc:MovieClip = gemContainer.getChildByName(lineGems[i]) as MovieClip;
											if(mc.jewelNum == null){
												trace("no name");
											} else {
												SignalStalk.dispatch(mc.jewelNum);
											}
											// remove it from the program
											gemContainer.removeChild(mc);
											// find where it was in the array
											var cd:Array=lineGems[i].split("_");
											// set it to an empty gem space
											gems_array[cd[0]][cd[1]]=-1;
											// set the new score
											score+=inaRow;
											// set the score setter up
											inaRow++;
											//Dispatch Id of Jewel for our blenders
										}
										
										swapState = false;
										startHintTimer();
									};
									// if a row was made, stop the loop
									break;
								}
							}
							// if a line was made, stop making more lines
							if (moreLinesAvailable) {
								break;
							}
						}
						// if no more lines were available...
						if (! moreLinesAvailable) {
							// allow new moves to be made
							clickPossible=true;
							// remove score multiplier
							inaRow=0;
						}
					}
				}
			};
			// display new score
			score_txt.text=score.toString();
			
		}
		
		
		private function onUp(e:MouseEvent){
			//onClick(e);
			moveState = false;
		}
		
		private function onMove(e:MouseEvent){
			if(moveState && !swapState){
				onClick(e);
			};
		}
		
		// When the user clicks
		private function onClick(e:MouseEvent):void {
			// If a click is allowed
			
			resetHintTimer();
			
			if (clickPossible && !swapState) {
				// If the click is within the game area...
				//if (mouseX<1300&&mouseX>0&&mouseY<600&&mouseY>0) {
				if (mouseX<1300&&mouseX>0&&mouseY<600&&mouseY>0) {
					moveState = true;
					// Find which row and column were clicked
					var clickedRow:uint=Math.floor(mouseY/itemSize);
					var clickedColumn:uint=Math.floor(mouseX/itemSize);
					// keep row and column within bounds to prevent stepping off right/bottom
					clickedRow = Utils.Clamp( clickedRow, 0, ROWS );
					clickedColumn = Utils.Clamp( clickedColumn, 0, COLS );
					
					// Check if the clicked gem is adjacent to the selector
					// If not...
					if (!(((clickedRow==selectorRow+1 || clickedRow==selectorRow-1)&&clickedColumn==selectorColumn)||((clickedColumn==selectorColumn+1 || clickedColumn==selectorColumn-1) && clickedRow==selectorRow))) {
						// Find row and colum the selector should move to
						selectorRow=clickedRow;
						selectorColumn=clickedColumn;
						// Move it to the chosen position
						selectorBox.x=itemSize*selectorColumn;
						selectorBox.y=itemSize*selectorRow;
						// If hidden, show it.
						selectorBox.visible=true;
					}
						// If it is not next to it...
					else {
						
						// enter swap state
						swapState = true;
						TweenMax.delayedCall(0.4, swapStateOff);
						
						function swapStateOff():void{
							swapState = false;
							selectorRow=-1;
							selectorColumn=-1;
							selectorBox.x=-itemSize;
							selectorBox.y=-itemSize;
						}
						
						// If hidden, show it.
						selectorBox.visible=true;
						selectorBox.x=itemSize*selectorColumn;
						selectorBox.y=itemSize*selectorRow;
						TweenMax.to(selectorBox,0.3,{x:itemSize*clickedColumn,y:itemSize*clickedRow, onComplete:function hideSelect():void{selectorBox.visible=false}});
						

						// Swap the gems;
						swapGems(selectorRow,selectorColumn,clickedRow,clickedColumn);
						// identify the gems
						var gem1:MovieClip = gemContainer.getChildByName(selectorRow+"_"+selectorColumn) as MovieClip;
						var gem2:MovieClip = gemContainer.getChildByName(clickedRow+"_"+clickedColumn) as MovieClip;
						
						// If they make a line...
						if (rowLineLength(selectorRow,selectorColumn)>2 || columnLineLength(selectorRow,selectorColumn)>2||rowLineLength(clickedRow,clickedColumn)>2 || columnLineLength(clickedRow,clickedColumn)>2) {
							// remove the hint 
							resetHintTimer();
							// dis-allow a new move until cascade has ended (removes glitches)
							clickPossible=false;
							moveState = false;
							
							// move and rename the gems

							TweenMax.to(gem1,0.3,{x:clickedColumn*itemSize,y:clickedRow*itemSize});
							TweenMax.to(gem2,0.3,{x:selectorColumn*itemSize,y:selectorRow*itemSize});
							
							gem1.name="t";
							gem2.name=selectorRow+"_"+selectorColumn;

							gemContainer.getChildByName("t").name=clickedRow+"_"+clickedColumn;
							match = true;
						}
							// If not...
						else {
							// Switch them back
							
							TweenMax.to(gem1,0.3,{x:clickedColumn*itemSize,y:clickedRow*itemSize});
							TweenMax.to(gem2,0.3,{x:selectorColumn*itemSize,y:selectorRow*itemSize});
							TweenMax.to(gem2,0.1,{x:clickedColumn*itemSize,y:clickedRow*itemSize, delay:0.3});
							TweenMax.to(gem1,0.1,{x:selectorColumn*itemSize,y:selectorRow*itemSize, delay:0.3});
							
							swapGems(selectorRow,selectorColumn,clickedRow,clickedColumn);
							match = false;
							
							startHintTimer();
						}
					}
				}
			}
		}
		
		
		private function resetHintTimer():void{
			hint1.visible = false;
			hint2.visible = false;
			hint1.gotoAndStop(1);
			hint2.gotoAndStop(1);
			TweenMax.killDelayedCallsTo(hintJems);
		}
		
		public function startHintTimer():void{
			if(!hint1.visible){
				TweenMax.delayedCall(6,hintJems);
			}
		}
		
		private function hintJems():void{
			// move hints to top
			this.setChildIndex(hint1, this.numChildren-1);
			this.setChildIndex(hint1, this.numChildren-1);
			
			// For gems in all rows...
			for (var i:uint=0; i<6; i++) {
				// and columns...
				for (var j:uint=0; j<11; j++) {
					// if they're not too close to the side... 
					if (i<9) {
						// swap them horizontally
						swapGems(i,j,i+1,j);
						// check if they form a line
						if ((rowLineLength(i,j)>2||columnLineLength(i,j)>2||rowLineLength(i+1,j)>2||columnLineLength(i+1,j)>2)) {
							// if so, name the move made
							//selectorBox.x = j*itemSize;
							//selectorBox.y = i*itemSize;
							//selectorBox.visible = true;
							
							/*
							hint1.x = j*itemSize;
							hint1.y = i*itemSize;
							hint1.visible = true;
							hint2.x = (j)*itemSize;
							hint2.y = (i+1)*itemSize;
							hint2.alpha = 0.2
							hint2.visible = true;
							
							trace("horizontal hint");
							*/
							//hint_txt.text = (i+1).toString()+","+(j+1).toString()+"->"+(i+2).toString()+","+(j+1).toString();
						}
						// swap the gems back
						swapGems(i,j,i+1,j);
					}
					// then if they're not to close to the bottom...
					if (j<10) {
						// swap it vertically
						swapGems(i,j,i,j+1);
						// check if it forms a line
						if ((rowLineLength(i,j)>2||columnLineLength(i,j)>2||rowLineLength(i,j+1)>2||columnLineLength(i,j+1)>2) ) {
							// if so, name it
							//selectorBox.x = j*itemSize;
							//selectorBox.y = i*itemSize;
							//selectorBox.visible = true;
							
							hint1.x = j*itemSize;
							hint1.y = i*itemSize+3;
							hint1.visible = true;
							hint2.x = (j+1)*itemSize;
							hint2.y = (i)*itemSize+3;
							hint2.visible = true;
							
							hint1.gotoAndPlay(1);
							hint2.gotoAndPlay(1);
							
							//trace("verticle hint");
							
							//hint_txt.text = (i+1).toString()+","+(j+1).toString()+"->"+(i+1).toString()+","+(j+2).toString();
						}
						// swap the gems back
						swapGems(i,j,i,j+1);
					}
				}
			}
		}
		
		
		
		
		//Swap given gems
		private function swapGems(fromRow:uint,fromColumn:uint,toRow:uint,toColumn:uint):void {
			//Save the original position
			var originalPosition:uint=gems_array[fromRow][fromColumn];
			//Move original gem to new position
			gems_array[fromRow][fromColumn]=gems_array[toRow][toColumn];
			//move second gem to saved, original gem's position
			gems_array[toRow][toColumn]=originalPosition;
		}
		//Find out if there us a horizontal line
		private function rowLineLength(row:uint,column:uint):uint {
			var gemType:uint=gems_array[row][column];
			var lineLength:uint=1;
			var checkColumn:int=column;
			//check how far left it extends
			while (sameGemIsHere(gemType,row,checkColumn-1)) {
				checkColumn--;
				lineLength++;
			}
			checkColumn=column;
			//check how far right it extends
			while (sameGemIsHere(gemType,row,checkColumn+1)) {
				checkColumn++;
				lineLength++;
			}
			// return total line length
			return (lineLength);
		}
		//Find out if there us a vertical line
		private function columnLineLength(row:uint,column:uint):uint {
			var gemType:uint=gems_array[row][column];
			var lineLength:uint=1;
			var checkRow:int=row;
			//check how low it extends
			while (sameGemIsHere(gemType,checkRow-1,column)) {
				checkRow--;
				lineLength++;
			}
			//check how high it extends
			checkRow=row;
			while (sameGemIsHere(gemType,checkRow+1,column)) {
				checkRow++;
				lineLength++;
			}
			// return total line length
			return (lineLength);
		}
		private function sameGemIsHere(gemType:uint,row:int,column:int):Boolean {
			//Check there are gems in the chosen row
			if (gems_array[row]==null) {
				return false;
			}
			//If there are, check if there is a gem in the chosen slot
			if (gems_array[row][column]==null) {
				return false;
			}
			//If there is, check if it's the same as the chosen gem type
			return gemType==gems_array[row][column];
		}
		
		public function destroy():void{
			this.removeEventListener(MouseEvent.MOUSE_DOWN,onClick);
			this.removeEventListener(MouseEvent.MOUSE_MOVE,onMove);
			this.removeEventListener(MouseEvent.MOUSE_UP,onUp);
			this.removeEventListener(Event.ENTER_FRAME,everyFrame);
			TweenMax.killDelayedCallsTo(hintJems);
			this.removeChildren();
		}
	}
}

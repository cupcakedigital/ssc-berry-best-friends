﻿package  
{
	import com.cupcake.App;
	import com.pushwoosh.nativeExtensions.PushNotification;
	import com.pushwoosh.nativeExtensions.PushNotificationEvent;

	public class AppCode 
	{
		public static function preLoad():void
		{
			//register for push notifications
			var pushwoosh:PushNotification = PushNotification.getInstance();
			pushwoosh.addEventListener(PushNotificationEvent.PERMISSION_GIVEN_WITH_TOKEN_EVENT, onToken);
			pushwoosh.addEventListener(PushNotificationEvent.PERMISSION_REFUSED_EVENT, onError);
			pushwoosh.addEventListener(PushNotificationEvent.PUSH_NOTIFICATION_RECEIVED_EVENT, onPushReceived);
			pushwoosh.registerForPushNotification();
		}
		
		public static function onToken(e:PushNotificationEvent):void
			{ App.Log("\n TOKEN: " + e.token + " "); }
		 
		public static function onError(e:PushNotificationEvent):void
			{ App.Log("\n TOKEN: " + e.errorMessage+ " "); }
		 
		public static function onPushReceived(e:PushNotificationEvent):void
			{ App.Log("\n TOKEN: " + JSON.stringify(e.parameters) + " "); }
		
		public static function postLoad():void { updateLanguage(); }
		
		public static function updateLanguage():void { /* nut n honey */ }	
	}
}

﻿package  
{
	import com.DaveLibrary.clsSoundTransformEX;
	import com.DaveLibrary.clsSupport;
	import com.cupcake.App;
	import com.cupcake.ExternalImage;
	import com.cupcake.ProgressDisplay;
	import com.cupcake.SoundEx;
	import com.cupcake.SpriteSheet;
	import com.cupcake.SpriteSheetSequence;
	import com.greensock.TimelineMax;
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.MP3Loader;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.*;
	
	public class BejeweledGame extends WBZ_BookPage
	{
		public var Background:Rectangle;
		public var strings_TitleKey:String;
		private var prog:ProgressDisplay;
		private var ProgBar_LocOffset:Point = new Point();
		private var ProgBar_FillColor = null;
		private var ElapsedTime:Number = 0;
		private var TitleBar:clsTitleBar;
		private var IntroText:clsGameIntroText;
		private var Finale:clsGameFinale;
		private var EndOfLevel:clsGames_EndOfLevelSummary;
		private var GameOver:Boolean = false;
		
		// increase this amount to make game end faster
		private var scoreMultiplier:Number = 2.5;
		
		public var soundTrans:clsSoundTransformEX = new clsSoundTransformEX(0.7);

		//jb -30 + -200;
		
		//Sounds
		private var sndEnd:SoundEx;
		private var sndIntro:SoundEx;
		private var Crowd:CheeringCrowd;
		public var winSound:SoundEx = new SoundEx( "win", CONFIG::DATA + "sounds/games/GameCompleted.mp3", soundTrans);
		public var blenderSound:SoundEx = new SoundEx( "blender", CONFIG::DATA + "sounds/games/BejeweledGame/blender.mp3", soundTrans);
		public var fruitPop:SoundEx = new SoundEx( "fruitPop", CONFIG::DATA + "sounds/games/BejeweledGame/fruit.mp3", soundTrans);
		
		//Placeholders
		public var Background_MC:Placeholder;
		
		
		public var Board_MC:Placeholder;
		public var Tutorial_MC:MovieClip;
		
		private var _blendersFull:Vector.<Boolean> = new <Boolean>[false,false,false,false,false];
		
		private var _blenderColors:Vector.<uint> = new <uint>[0xEB3A36,0x3B6CB9,0xF9D103,0x7B439D,0xF4B702];
		
		public var _blender1:MovieClip;
		public var _blender2:MovieClip;
		public var _blender3:MovieClip;
		public var _blender4:MovieClip;
		public var _blender5:MovieClip;
		
		
		private var _blenderClips:Vector.<MovieClip>;
		
		private var _blender1Anim:TapSpritesheet;
		private var _blender2Anim:TapSpritesheet;
		private var _blender3Anim:TapSpritesheet;
		private var _blender4Anim:TapSpritesheet;
		private var _blender5Anim:TapSpritesheet;
		
		private var _blenderAnims:Vector.<TapSpritesheet> = new <TapSpritesheet>[_blender1Anim,_blender2Anim,_blender3Anim,_blender4Anim,_blender5Anim];
		
		private var bejeweled:Bejeweled;

		public function BejeweledGame(pageIndex:int=0):void
		{
			super();
			
			strings_TitleKey = "BejeweledGame_Title";
			ProgBar_LocOffset.x = 0;
			ProgBar_LocOffset.y = 0;
			reusableFolder = CONFIG::DATA + "images/games/";
			imageFolder = CONFIG::DATA + "images/games/BejeweledGame/";
			
			
			assets.push(new ExternalImage("SSC-Bejewled-BG.png", Background_MC));
			
			//assets.push(new ExternalImage("GAME04_BOARD_TUTORIAL.png", Tutorial_MC.BoardTutorial_MC));
			
			assets.push(new ExternalImage("SSC_JEWELGAME_BLENDER_0001.png", _blender1.blender));
			assets.push(new ExternalImage("SSC_JEWELGAME_BLENDER_0002.png", _blender2.blender));
			assets.push(new ExternalImage("SSC_JEWELGAME_BLENDER_0003.png", _blender3.blender));
			assets.push(new ExternalImage("SSC_JEWELGAME_BLENDER_0004.png", _blender4.blender));
			assets.push(new ExternalImage("SSC_JEWELGAME_BLENDER_0005.png", _blender5.blender));
			
			
			_blenderClips = new <MovieClip>[_blender1,_blender2,_blender3,_blender4,_blender5];
			
			for (var i:int = 0; i < _blenderClips.length; i++) 
			{
				TweenMax.allTo([_blenderClips[i].juiceAnim,_blenderClips[i].juiceBox],0,{tint:_blenderColors[i]});
			}
			
			
			


			
			Background = new Rectangle( 0, 0, 1024, 768 );
			
			var endldr:MP3Loader = new MP3Loader( CONFIG::DATA + "sounds/games/GREAT JOB.mp3", { autoPlay: false, onComplete: endLoaded } );
			endldr.load();

			Finale = new clsGameFinale(cMain, "", gc.pages_GameFinale_Text, Finale_Complete);
			Finale.x = Background.x + (Background.width/2) + gc.pages_GameFinale_Text.offset.x;
			Finale.y = Background.y + (Background.height/2) + gc.pages_GameFinale_Text.offset.y;
			Finale.Reset();
			Finale.mouseEnabled = false;
			Finale.mouseChildren = false;
			this.addChild(Finale);
			
			EndOfLevel = null;
			
			Crowd = new CheeringCrowd( this );
			
			sndIntro = new SoundEx( "instructions", CONFIG::DATA + "sounds/games/BejeweledGame/Instructions_BejeweledGame.mp3", soundTrans);
			sndIntro.callback = IntroComplete;
			
			
			bejeweled = new Bejeweled();
			bejeweled.SignalStalk.add(updateBlenders);
			bejeweled.SignalPopped.add(poppedJewel);
			bejeweled.x = 100;
			bejeweled.y = 24;
			bejeweled.visible = false;
			bejeweled.alpha = 0;
			
			this.addChild(bejeweled);
			
		}
		
		private function updateBlenders(item:int):void
		{
			trace("adding juice to " + item);
			
			if(_blenderClips[item].juiceBox.y >= -180){
				_blenderClips[item].juiceAnim.play();
				_blenderClips[item].glassAnim.play();
				TweenMax.to(_blenderClips[item].juiceBox, 1, {y:"-15", onComplete:checkFull, onCompleteParams:[item]});
				trace("adding juice to " + item);
				blenderSound.Start();
			} else {
				if(_blenderClips[item].rotation == 0){
					TweenMax.to(_blenderClips[item], 0.1, {y:"15",yoyo:true,repeat:3});
					TweenMax.to(_blenderClips[item], 0.1, {y:753.95,delay:0.3});
				};
				_blenderClips[item].glassAnim.play();
				fruitPop.Start();
				checkAllBlenders();
			}
		
			function checkFull(item:int):void{
				if(_blenderClips[item].juiceBox.y <= -170){
					_blendersFull[item] = true;
				};
			}
		}
		
		private function checkAllBlenders():void
		{
			
			var count:int = 0;
			
			for (var i:int = 0; i < _blendersFull.length; i++) 
			{
				if(_blendersFull[i] == 1){count++};
			}
			
			
			if(count == _blendersFull.length){
				if(!GameOver){
					bejeweled.gameOver = true;
					gameOver();
				};
			};
		}
		
		private function poppedJewel():void
		{
			fruitPop.Start();
		}
			
		/*
		private function startTutorial():void
		{
			// TODO Auto Generated method stub
			
			var tlm:TimelineMax = new TimelineMax({onComplete:startGame});
			
			tlm.append( new TweenMax(Tutorial_MC.cursor,2,{x:210,y:178,alpha:1, delay:1.5}));
			tlm.append( new TweenMax(Tutorial_MC.cursor,1,{x:301}));
			tlm.append( new TweenMax(Tutorial_MC.cursor,1,{x:210}));
			tlm.appendMultiple([ new TweenMax(Tutorial_MC.cursor,1,{x:210, y:238}),new TweenMax(Tutorial_MC.selector,1,{alpha:1})]);
			tlm.appendMultiple([ new TweenMax(Tutorial_MC.cursor,1,{x:301, y:238}),new TweenMax(Tutorial_MC.selector,1,{x:"80"}),new TweenMax(Tutorial_MC.jewel3,1,{x:"80"}),new TweenMax(Tutorial_MC.jewel4,1,{x:"-80"})]);
			tlm.appendMultiple([ new TweenMax(Tutorial_MC.cursor,1,{y:"-200", x:"-200", alpha:0}),new TweenMax(Tutorial_MC.selector,1,{alpha:0})]);		
			tlm.appendMultiple([ new TweenMax(Tutorial_MC.jewel1.graphic,0.8,{delay:0, scaleX:1.3, scaleY:1.3, onStart:function pop():void{Tutorial_MC.jewel1.graphic.play();jewelPop.Start();}}),
				new TweenMax(Tutorial_MC.jewel2.graphic,0.8,{delay:0.2, scaleX:1.3, scaleY:1.3, onStart:function pop():void{Tutorial_MC.jewel2.graphic.play();jewelPop.Start();}}),
				new TweenMax(Tutorial_MC.jewel3.graphic,0.8,{delay:0.4, scaleX:1.3, scaleY:1.3, onStart:function pop():void{Tutorial_MC.jewel3.graphic.play();jewelPop.Start();}, onComplete:jewelSound.Start})]);
			
			tlm.append( new TweenMax(Tutorial_MC,1,{autoAlpha:0, delay:2}));
		
		}
		*/
		
		private function startGame():void{
			bejeweled.startHintTimer();
			bejeweled.visible = true;
			TweenMax.to(bejeweled,1,{alpha:1, delay:0.2});
		}
		
		
		public override function PageShown(LoadedFromSource:int):void
		{
			super.loadedFrom = LoadedFromSource;
			
			if(LoadedFromSource != 2) cMain.showArrows();
		}
		
		public final override function Init():void
		{
			super.Init();
			musicFile = "sscmusic2";	
			Reset();
			Start();
		}
		
		private function IntroComplete(snd:SoundEx):void
		{
			if(isRunning) doMusicLoad();
		}
		
		public final override function Reset():void
		{
			super.Reset();
			
			cMain.removeEnterFrame(ENTER_FRAME);
			
			Finale.Reset();
			if ( Crowd != null ) { Crowd.Reset(); }
			if ( EndOfLevel != null ) { EndOfLevel.destroy(); }
			
			//prog.scale = 0;	
			GameOver = false;
			
			step = STEP_DONE;
		}
		
		public override function Destroy():void
		{
			cMain.removeEnterFrame(ENTER_FRAME);
			
			clsSupport.DisposeOfSprite(this);

			//TitleBar.destroy();
			//TitleBar = null;
			
			if(IntroText != null)
			{
				IntroText.destroy();
				IntroText = null;
			}
			if(Finale != null)
			{
				Finale.destroy();
				Finale = null;
			}
			
			if(EndOfLevel != null) EndOfLevel.destroy();
			EndOfLevel = null;
			
			if(Crowd != null) { Crowd.Destroy(); }
			Background = null;									
			
			if(bejeweled != null){ bejeweled.destroy() };
			
			super.Destroy()
		}
		
		public override function Start():void
		{
			sndIntro.Start();
			//startTutorial();
			startGame();
			super.Start();
			cMain.addEnterFrame(ENTER_FRAME);
			Finale.Reset();
		}
		
		
		
		public override function Stop():void
		{
			if ( sndEnd != null ) sndEnd.Stop();
			if(Finale != null)
			{
				Finale.Stop();
				Finale.Reset();
			}
			super.Stop();
		}
		
		private function endLoaded( e:LoaderEvent ):void
		{
			sndEnd = new SoundEx( "sndEnd", GlobalData.langFolder + "sounds/games/GREAT JOB.mp3", App.voiceVolume );	
			( e.target as MP3Loader ).dispose( true );
		}
		
		private function Finale_Complete():void
		{
			if ( isRunning )
			{
				var returnToPrevPage:Boolean = this.loadedFrom == 2;
		
				EndOfLevel = new clsGames_EndOfLevelSummary(cMain, "BejeweledGame", returnToPrevPage, sndEnd );
				EndOfLevel.x = Background.x;
				EndOfLevel.y = Background.y;
				this.addChild(EndOfLevel);
			}
		}

		
		private function gameOver():void
		{
			
				GameOver = true;
				winSound.Start();
				TweenLite.delayedCall(2, Finale_Complete);
				this.setChildIndex(Crowd, this.numChildren-1);
				Crowd.Start();		
		}
		
		public function ENTER_FRAME(e:Event):void
		{
			if(cMain.Paused) return;	
		}
	}
}
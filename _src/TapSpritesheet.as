﻿package  
{
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.MouseEvent;

	
	import com.cupcake.App;
	import com.cupcake.IControllable;
	import com.cupcake.SoundCollection;
	import com.cupcake.SpriteSheet;
	import com.cupcake.SpriteSheetAnimation;
	import com.cupcake.SpriteSheetSequence;
	import com.cupcake.Utils;
	
	import com.greensock.loading.LoaderMax;
	
	public class TapSpritesheet implements IControllable 
	{
		public static var page:WBZ_BookPage			= null;
		public static var addMouseDown:Function		= null;
		public static var removeMouseDown:Function	= null;
		
		public var loop:Boolean						= false;
		public var reverse:Boolean					= false;
		public var reversing:Boolean				= false;
		public var sheetDataFile:String				= null;
		public var sheetImageFile:String			= null;
		
		public var actor:Sprite						= null;
		public var anim:SpriteSheetAnimation		= null;
		public var hit:SimpleButton					= null;
		public var initSequences:Vector.<SpriteSheetSequence>;
		public var onComplete:Function				= null;
		public var onFrame:Function					= null;
		public var onLoad:Function					= null;
		public var onTap:Function					= null;
		public var sheet:SpriteSheet				= null;
		public var sounds:SoundCollection			= null;
		public var soundRange:Array					= null;
		public var vars:Object						= null;
		
		private var currentSequence:int				= 0;
		private var sequenceCount:int				= 0;
		
		private var _endFrames:Vector.<int>			= null;
		private var sequences:Vector.<String>		= null;
		
		private var _initialized:Boolean			= false;
		private var _paused:Boolean					= false;
		private var _playing:Boolean				= false;
		private var _running:Boolean				= false;
		
		private var FrameArray:Array = new Array();
		private var FunctionArray:Array = new Array();


		public function TapSpritesheet( hitButton:SimpleButton, container:Sprite, spritesheet:SpriteSheet, frameRate:int = 30,
									    _sounds:SoundCollection = null, _soundRange:Array = null, endFrames:Vector.<int> = null, 
										sequenceList:Vector.<SpriteSheetSequence> = null ) 
		{
			hit = hitButton;
			actor = container;
			sheet = spritesheet;
			sounds = _sounds;
			soundRange = _soundRange;
			_endFrames = endFrames;
			initSequences = sequenceList;
			
			anim = new SpriteSheetAnimation( actor, spritesheet, frameRate );
			anim.onFrame = OnFrame;
			anim.onLoad = animLoaded;
			anim.onComplete = animComplete;
		}
		
		// Optional vars:
		// actorClip (MovieClip): animation container
		// button (SimpleButton): button that triggers the control
		// endFrames (Vector.<int>): frames to stop on
		// frameRate (int): default frame rate (default: 30)
		// imageName (String): the name of the sprite sheet files (minus the .png & .json extension) to use
		// onComplete (Function): callback function when an animation section has completed
		// onFrame (Function): callback function when the animation goes to the next frame
		// onLoad (Function): callback function when the sprite sheet files are loaded
		// onTap (Function): callback function when the control is tapped
		// reverse (Boolean): animation will yo-yo on subsequent taps
		// reversing (Boolean): animation yo-yos if true
		
		
		public static function Add( asset:String, vars:Object ):TapSpritesheet
		{
			var control:TapSpritesheet	= null;
			var actorClip:MovieClip		= ( vars.actorClip ) ? ( ( vars.actorClip is MovieClip ) ? vars.actorClip : page.scene_mc.getChildByName( vars.actorClip ) as MovieClip )
														 	 : ( page.scene_mc.getChildByName( asset ) as MovieClip );
			var button:SimpleButton 	= ( vars.button ) ? ( ( vars.button is SimpleButton ) ? vars.button : page.scene_mc.getChildByName( vars.button ) as SimpleButton )
													  	  : ( page.scene_mc.getChildByName( asset + "_btn" ) as SimpleButton );
												
			if ( actorClip == null ) { App.Log( "TapSpritesheet.Add(): actorClip '" + asset + "' is null." ); return null; }
			
			var externalSounds:Boolean	= vars.hasOwnProperty( "sounds" );
			var endFrames:Vector.<int>	= ( vars.endFrames ) ? vars.endFrames : null;
			var frameRate:int			= ( vars.frameRate ) ? vars.frameRate : 30;
			var imageName:String		= ( vars.imageName ) ? vars.imageName : actorClip.name;
			var onComplete:Function 	= ( vars.onComplete ) ? vars.onComplete : null;
			var onFrame:Function 		= ( vars.onFrame ) ? vars.onFrame : null;
			var onLoad:Function			= ( vars.onLoad ) ? vars.onLoad : null;
			var onTap:Function 			= ( vars.onTap ) ? vars.onTap : null;
			var reverse:Boolean			= ( vars.reverse ) ? vars.reverse : false;
			var reversing:Boolean		= ( vars.reversing ) ? vars.reversing : false;
			var sounds:SoundCollection	= ( vars.sounds ) ? vars.sounds : SoundCollection.GetSounds( asset, vars );
			var soundRange:Array		= ( vars.soundRange ) ? vars.soundRange : null;			
			
			var reusable:Boolean		= ( vars.reusable ) ? vars.reusable : false;
			var prefix:String			= ( reusable ) ? "Reusable/" : "";
			
			var sequences:Vector.<SpriteSheetSequence> = ( vars.sequences ) ? vars.sequences : null;

			var sheet:SpriteSheet = new SpriteSheet( prefix + imageName + ".png", prefix + imageName + ".json" );
			page.animAssets.push( sheet );
			if ( button != null ) { page.CheckSparkler( imageName, button ); }
			control = new TapSpritesheet( button, actorClip, sheet, frameRate, sounds, soundRange, endFrames, sequences );
			control.reverse = reverse;
			control.reversing = reversing;
			control.onComplete = onComplete;
			control.onFrame = onFrame;
			control.onLoad = onLoad;
			control.onTap = onTap;
			page.controls.push( control );
			
			return control;
		}
		
		public function get currentFrame():int
		{
			return anim.anim.frame;
		}
		
		public function setSequence( playSequence:int)
		{
			if(!(playSequence < 0) && !(playSequence > sequenceCount))
			{
				currentSequence = playSequence;
			}
		}
		
		public function doTap( overridePlaying:Boolean = false ):Boolean
		{
			var res:Boolean = false;
			var cancel:Boolean = false;
			if ( isRunning && !isPaused && ( !_playing || overridePlaying ) )
			{
				if ( onTap != null ) cancel = onTap( this );
				
				if ( !cancel )
				{
					res = true;
					if ( sounds != null ) { sounds.PlayRandom( soundRange ); }
					//App.Log( "playing sequence: " + sequences[ currentSequence ] );
					anim.playSequence( sequences[ currentSequence ] );
					_playing = true;
					
					currentSequence++;
					if ( currentSequence >= sequenceCount ) currentSequence = 0;
				}
			}
			return res;
		}
		
		public function endAnimation():void
		{
			anim.Stop();
			_playing = false;
		}
		
		public function playSequence( sequence:String, withSound:Boolean = true ):void
		{
			if ( isRunning && !isPlaying && !isPaused )
			{
				anim.playSequence( sequence );
				if ( withSound && sounds != null ) { sounds.PlayRandom( soundRange ); } 
			}
		}
		
		public function setSequences( seqs:Vector.<SpriteSheetSequence> = null ):void
		{
			sequences = new Vector.<String>;
			
			if ( seqs != null )
			{
				sequenceCount = seqs.length;
				for ( var t:int = 0 ; t < seqs.length ; t++ )
				{
					anim.anim.addSequence( seqs[t].name, seqs[t].frames, seqs[t].rate, seqs[t].loop );
					sequences.push( seqs[t].name );
				}
			}
			else
			{
				if ( _endFrames == null )
				{
					if ( reversing )
					{
						sequenceCount = 1;
						sequences.push( "reversing" );
					}
					else
					{
						sequenceCount = reverse ? 2 : 1;
						sequences.push( "noloop" );
						if ( reverse ) sequences.push( "reverse" );
					}
				}
				else
				{
					var nextFrame:int = 1;
					sequenceCount = _endFrames.length;
					
					for ( var seqDex:int = 0 ; seqDex < sequenceCount ; seqDex++ )
					{
						var arrSeq:Array = [];
						for ( var u:int = nextFrame ; u <= _endFrames[seqDex] ; u++ )
						{
							if ( u > 1 && sequenceCount > 1 ) arrSeq.push( u );
						}
						if ( seqDex == sequenceCount - 1 && sequenceCount > 1 ) arrSeq.push(1);
						anim.anim.addSequence( "seq" + seqDex, arrSeq, 30, loop );
						sequences.push( "seq" + 1 );
						nextFrame = _endFrames[seqDex] + 1;
					}
				}
			}
		}
		
		public final function get isInitialized():Boolean { return _initialized; }
		public final function get isPaused():Boolean { return _paused; }
		public final function get isPlaying():Boolean { return _playing; }
		public final function get isRunning():Boolean { return _running; }
		
		public final function Destroy():void
		{ 
			if ( isRunning ) Stop(); 
			anim.Destroy();
		}
		
		public final function Init():void
		{
			if ( isInitialized ) return;
			
			anim.Init();
			setSequences( initSequences );
			
			_initialized = true;
		}
		
		public final function Pause():void
		{ 
			if ( isRunning && !isPaused )
			{
				anim.Pause();
				_paused = true;
			} 
		}
		
		public final function Reset():void
		{
			if ( !isInitialized ) Init(); 
			if ( isRunning ) Stop(); 
			
			currentSequence = 0;
			anim.sequence = sequences[ 0 ];
			anim.Reset();
			_playing = false;
		}
		
		public final function Resume():void
		{
			if ( isRunning && isPaused )
			{
				anim.Resume();
				_paused = false;
			}
		}
		
		public final function Start():void
		{
			if ( !isInitialized ) Init();
			if ( isRunning ) Reset();
		
			startMouseDown();
		
			_running = true;
			_paused = false;
		}
		
		public final function Stop():void
		{
			if ( isRunning )
			{
				if ( sounds != null ) { sounds.Stop(); }
				stopMouseDown();
				anim.Stop();
				_playing = false;
				_running = false;
			}
		}
		
		// Private functions
		private function animLoaded( e:SpriteSheetAnimation ):void
		{
			if ( onLoad != null ) { onLoad( this ); }
		}
		
		private final function hitTapped( e:MouseEvent ):void
			{ if ( isRunning && !isPaused && e.target == hit ) { doTap(); } }
			
		private final function animComplete( sheet:SpriteSheetAnimation ):void
		{
			_playing = false;
			if ( onComplete != null ) onComplete( this );
		}
		
		private final function OnFrame( e:SpriteSheetAnimation, frame:uint ):void
		{
			if ( onFrame != null ) { onFrame( this, frame ); }
		}
		
		private final function startMouseDown():void
		{
			if ( addMouseDown != null ) addMouseDown( hitTapped );
				else Utils.addHandler( hit, MouseEvent.MOUSE_DOWN, hitTapped );
		}
		
		private final function stopMouseDown():void
		{
			if ( removeMouseDown != null ) removeMouseDown( hitTapped );
				else Utils.removeHandler( hit, MouseEvent.MOUSE_DOWN, hitTapped );
		}
	}
}

﻿package  
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.media.SoundTransform;
	import flash.text.TextFormatAlign;
	
	import asData.clsGlobalConstants;
	import asData.clsMsgboxDefaults;
	
	import com.DaveLibrary.clsGradientDynamicText;
	import com.DaveLibrary.clsStandardButton;
	import com.DaveLibrary.clsSupport;
	import com.DaveLibrary.clsTextData;
	
	import com.cupcake.App;
	import com.cupcake.DragTracker;
	import com.cupcake.TimerEx;
	import com.cupcake.Utils;
	import com.cupcake.VideoBox;
	
	import com.greensock.TweenLite;

	public class WBZ_VideoBox extends VideoBox
	{
		public var closeCallback:Function			= null;
		
		public var top:Number						= 0;
		public var bottom:Number					= 0;
		public var span:Number						= 0;
		public var vol:DragTracker					= null;
		
		public var close_btn:clsStandardButton		= null;
		public var pause_btn:clsStandardButton		= null;
		public var play_btn:clsStandardButton		= null;
		public var stop_btn:clsStandardButton		= null;
		public var subtitle:clsGradientDynamicText	= null;
		public var subtitleText:clsTextData;
		public var volume_bar:Sprite				= null;
		public var volume_bg:Bitmap					= null;
		
		private var fadeTimer:TimerEx				= new TimerEx( 4000, 1 );
		
		private var cMain:clsMain;
		private var MsgboxInfo:clsMsgboxDefaults;
		
		public function WBZ_VideoBox( Main:clsMain ) 
		{
			videoFolder = CONFIG::VIDEOS;
			
			cMain = Main;
			
			subtitleText = new clsTextData();
			subtitleText.alignment = TextFormatAlign.CENTER;
			subtitleText.bounds.width = 1024;
			subtitleText.bounds.height = 400;
			subtitleText.color = cMain.gc.gradient_NavbarBlue;
			subtitleText.font = new fontVAGRoundedBlack();
			subtitleText.fontSize = 50;
			subtitleText.offset.x = 0;
			subtitleText.offset.y = 3;
			subtitleText.stroke = 4;
			subtitleText.strokeColor = 0x025169;
			
			init();
		}
		
		public override function destroy():void
		{
			cMain.removeStageMouseDown( beginTap );
			cMain.removeStageMouseUp( endTap );
			Utils.removeHandler( fadeTimer.timer, TimerEvent.TIMER, fadeTimer_Tick );
			if ( close_btn != null ) { close_btn.destroy(); close_btn = null; }
			if ( play_btn != null ) { play_btn.destroy(); play_btn = null; }
			if ( pause_btn != null ) { pause_btn.destroy(); pause_btn = null; }
			if ( stop_btn != null ) { stop_btn.destroy(); stop_btn = null; }
			if ( subtitle != null ) { subtitle.destroy(); subtitle = null; }
			if ( subtitleText != null ) { subtitleText.destroy(); subtitleText = null; }
			if ( vol != null ) { vol.Destroy(); vol = null; }
			super.destroy();
		}
		
		public override function init():void
		{
			width = clsMain.localWidth;
			height = clsMain.localHeight;
			x = -clsMain.offsetX;
			super.init();
			subtitleTrack = 2;
			MsgboxInfo = cMain.gc.messages_Mainmenu_Info;
			var fontSize = MsgboxInfo.ok_Settings.textInfo.fontSize;
			MsgboxInfo.ok_Settings.textInfo.fontSize -= 6;
			
			var buttonScale:Number = 1;
			
			pause_btn = new clsStandardButton(cMain, "", MsgboxInfo.ok_Settings, GlobalData.getSheetBitmap( "Videos_Pause_s" ), GlobalData.getSheetBitmap( "Videos_Pause_h" ), pauseClick, null, null, null, true);
			
			pause_btn.x = this.width / 2 + pause_btn.width / 2;
			pause_btn.y = this.height - pause_btn.height / 2;
			pause_btn.alpha = 0;
			addChild( pause_btn );
			
			// make sure buttons aren't too big for the player!
			if ( pause_btn.width * 3 > width )
			{
				// scale them down
				buttonScale = width / ( pause_btn.width * 3 );
				pause_btn.scaleX = buttonScale;
				pause_btn.scaleY = buttonScale;
				pause_btn.y = height - pause_btn.height / 2;
			}
			
			play_btn = new clsStandardButton(cMain, "", MsgboxInfo.ok_Settings, GlobalData.getSheetBitmap( "Videos_Play_s" ), GlobalData.getSheetBitmap( "Videos_Play_h" ), playClick, null, null, null, true);
			play_btn.scaleX = buttonScale;
			play_btn.scaleY = buttonScale;
			play_btn.x = this.width / 2 - play_btn.width / 2;
			play_btn.y = pause_btn.y;
			play_btn.alpha = 0;
			addChild( play_btn );
			
			close_btn = new clsStandardButton(cMain, "", MsgboxInfo.ok_Settings, GlobalData.getSheetBitmap( "Videos_Close_s" ), GlobalData.getSheetBitmap( "Videos_Close_h" ), closeClick, null, null, null, true);
			close_btn.scaleX = buttonScale;
			close_btn.scaleY = buttonScale;
			close_btn.x = this.width - close_btn.width / 2;
			close_btn.y = close_btn.height / 2;
			close_btn.alpha = 0;
			addChild( close_btn );
			
			subtitleText.bounds.width = this.width;
			subtitle = new clsGradientDynamicText(cMain, "REMOVE", subtitleText);
			subtitle.Image.bitmapData = clsSupport.CreateBitmapDataGlow(subtitle.Image.bitmapData, cMain.gc.navbar_Videos_SecondStroke_Size, cMain.gc.navbar_Videos_SecondStroke_Color, false);
			subtitle.Image.x = 0 - (subtitle.Image.width/2);
			subtitle.Image.y = 0 - (subtitle.Image.height/2);
			subtitle.x = this.width / 2;
			subtitle.y = close_btn.y + close_btn.height / 2;
			subtitle.Image.bitmapData = null;
			addChild( subtitle );
			
			volume_bg = new Bitmap( GlobalData.getSheetBitmap( "Videos_VolumeMeter" ) );
			volume_bg.x += this.width - volume_bg.width - 20;
			volume_bg.y += this.height - volume_bg.height - 10;
			addChild( volume_bg );
			
			volume_bar = new Sprite();
			volume_bar.addChild( new Bitmap( GlobalData.getSheetBitmap( "Videos_VolumeMeter_Bar" ) ) );
			volume_bar.x = volume_bg.x + ( volume_bg.width / 2 ) - volume_bar.width / 2;
			volume_bar.y = volume_bg.y - volume_bar.height / 2;
			addChild( volume_bar );
			
			top = volume_bar.y;
			bottom = volume_bg.y + volume_bg.height - volume_bar.height;
			span = bottom - top;
			
			vol = new DragTracker( volume_bar );
			vol.constraint = DragTracker.CONSTRAIN_VERTICAL;
			vol.onDrag = checkVol;
			
			onComplete = videoDone;
			onSubtitle = updateSubtitle;
			onBackgroundTap = backgroundTap;
			
			MsgboxInfo.ok_Settings.textInfo.fontSize = fontSize;
			
			cMain.addStageMouseDown( beginTap );
			cMain.addStageMouseUp( endTap );
			
			Utils.addHandler( fadeTimer.timer, TimerEvent.TIMER, fadeTimer_Tick );
		}
		
		public override function playVideo():void
		{
			subtitle.Image.bitmapData = null;
			soundTrans = App.voiceVolume.sndTransform;
			super.playVideo();
		}
		
		public function playWWRVideo( videoIndex:int ):void
		{
			volume_bar.y = top + ( span - ( span * ( clsOptions.voiceVolume / 2 ) ) );
			vol.Start();
			if ( cMain.CurrentPage == -1 ) cMain.cMainMenu.cMusic.Pause();
			cMain.hideArrows( true );
			cMain.Paused = true;
			cMain.Modal.push(this);
			cMain.addChild( this );
			setVideo( videoIndex );
			fadeTimer.start();
			playVideo();
		}
		
		// Private and protected functions
		private function backgroundTap( e:VideoBox ):void
		{
			/*if ( _playing ) togglePause();
				else playVideo();*/
		}
		
		private final function checkVol( e:DragTracker ):void
		{
			if ( volume_bar.y < top ) volume_bar.y = top;
			if ( volume_bar.y > top + span ) volume_bar.y = top + span;
			clsOptions.voiceVolume = ( ( span - ( volume_bar.y - top ) ) / span ) * 2;
			App.voiceVolume.volume = clsOptions.voiceVolume;
			_ns.soundTransform = new SoundTransform( clsOptions.voiceVolume );
		}
		
		public function closeClick( e:MouseEvent ):void
		{
			if ( e != null ) { e.stopImmediatePropagation(); }
			stopVideo();
			clsOptions.Save();
			
			if(this.parent != null) {
				if(this.parent.contains(this)) this.parent.removeChild(this);
			}
			
			if(cMain != null) {
				cMain.PageObjects.visible = true;
				cMain.cNavBar.visible = true;
				var Index:int = cMain.Modal.indexOf(this);
				if(Index >= 0) {
					cMain.Modal.splice(Index,1);
				}		
				cMain.Paused = (cMain.Modal.length > 0);
				if ( cMain.CurrentPage == -1 && cMain.Modal.length == 0 ) cMain.cMainMenu.cMusic.Resume();
				if ( cMain.CurrentPage >= 0 ) { cMain.showArrows(); }
			}
		}
		
		private function pauseClick( e:MouseEvent ):void { togglePause(); }
		
		private function playClick( e:MouseEvent ):void { playVideo(); }
		
		private function stopClick( e:MouseEvent ):void
		{
			stopVideo();
			subtitle.Image.bitmapData = null;
		}
		
		private function updateSubtitle( e:String ):void
		{
			if ( e.indexOf( "XXX" ) == 0 || e == "" ) 
			{
				subtitle.Image.bitmapData = null;
			}
			else
			{
				subtitle.text = e;
				subtitle.RedrawText();
				subtitle.Image.bitmapData = clsSupport.CreateBitmapDataGlow(subtitle.Image.bitmapData, cMain.gc.navbar_Videos_SecondStroke_Size, cMain.gc.navbar_Videos_SecondStroke_Color, false);
				subtitle.Image.x = 0 - (subtitle.Image.width/2);
				subtitle.Image.y = 0 - (subtitle.Image.height/2);
			}
		}
		
		private final function videoDone():void
		{
			fadeControls();
			fadeTimer.stop();
			_playing = false;
		}
		
		private function fadeControls( fadeIn:Boolean = true ):void
		{
			var time:Number = .25 * ( fadeIn ? 1 - play_btn.alpha : play_btn.alpha );
			var newAlpha:Number = fadeIn ? 1 : 0;
			
			if ( time == 0 ) return;
			
			TweenLite.to( play_btn, time, { alpha: newAlpha } );
			TweenLite.to( pause_btn, time, { alpha: newAlpha } );
			TweenLite.to( close_btn, time, { alpha: newAlpha } );
		}
		
		private function beginTap( e:MouseEvent ):void 
		{
			fadeTimer.stop();
			fadeControls(); 
		}
		
		private function endTap( e:MouseEvent ):void
		{
			if ( _playing )
			{
				fadeTimer.reset();
				fadeTimer.start();
			}
		}
		
		private function fadeTimer_Tick( e:TimerEvent ):void { fadeControls( false ); }
	}
}

﻿package com.cupcake
{
	import flash.utils.Timer;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.net.URLRequest;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.media.SoundTransform;
	import com.cupcake.Utils;
	import com.cupcake.SoundEx;
	
	public class SoundManager
	{
		private static var _sounds:Array					= [];
		private static var _music:Array						= [];
		
		private static var _ambianceChannel:SoundChannel	= null;
		private static var _ambianceMute:Boolean			= false;
		private static var _ambianceVolume:Number			= .3;
		private static var _ambianceTrans:SoundTransform	= new SoundTransform( _ambianceVolume );
		
		private static var _soundMute:Boolean				= false;
		private static var _soundVolume:Number				= 1;
		private static var _soundTrans:SoundTransform		= new SoundTransform( _soundVolume );
		
		private static var _musicChannel:SoundChannel		= null;
		private static var _musicMute:Boolean				= false;
		private static var _musicVolume:Number				= 1;
		private static var _musicTrans:SoundTransform		= new SoundTransform( _musicVolume );
		private static var _musicIndex:uint					= 0;
		private static var _currentMusic:int				= -1;
		
		private static var fadeValue:Number					= 0;
		private static var fadeIncrements:Number			= 0;
		private static var fadeTimer:Timer					= new Timer( 50, 20 );
		private static var fading:Boolean					= false;
		
		// Static initializer
		{
			fadeTimer.addEventListener( TimerEvent.TIMER, fadeTime );
			fadeTimer.addEventListener( TimerEvent.TIMER_COMPLETE, fadeComplete );
		}
		
		// *** AMBIANCE ***
		public static function get ambianceVolume( ):Number { return _ambianceVolume; }
		public static function set ambianceVolume( nVol:Number ):void
		{
			_ambianceVolume = Utils.Clamp( nVol, 0, 1 );
			_ambianceTrans.volume = _ambianceVolume;
			if ( _ambianceChannel != null )
				{ _ambianceChannel.soundTransform = _ambianceTrans; }
		}
		
		public static function get ambianceMute( ):Boolean { return _ambianceMute; }
		public static function set ambianceMute( bMute:Boolean )
		{
			if ( bMute != _ambianceMute )
			{
				if ( bMute )
				{
					stopAmbiance();
					_ambianceMute = true;
				}
				else
					{ _ambianceMute = false; }
			}
		}		
		
		public static function playAmbiance( iSound:uint, iDex:uint = 0, 
											 repeat:int = int.MAX_VALUE, 
											 callback:Function = null ):void
		{
			stopAmbiance();
			
			if ( !_ambianceMute && !_soundMute )
			{
				_ambianceChannel =  _sounds[ iSound ].play( iDex, repeat );
				_ambianceChannel.soundTransform = _ambianceTrans;
				
				if ( callback != null )
					{ _ambianceChannel.addEventListener( Event.SOUND_COMPLETE, callback ); }
				else if ( _sounds[ iSound ].callback != null )
					{ _ambianceChannel.addEventListener( Event.SOUND_COMPLETE, _sounds[ iSound ].callback ) };
			}
		}
		
		public static function stopAmbiance( ):void
			{ if ( _ambianceChannel != null ) { _ambianceChannel.stop(); } }
			
		
		// *** SOUND ***
		public static function get soundVolume( ):Number { return _soundVolume; }
		public static function set soundVolume( nVol:Number ):void
			{ _soundVolume = Utils.Clamp( nVol, 0, 1 ); }

		public static function get soundMute( ):Boolean { return _soundMute; }
		public static function set soundMute( bMute:Boolean )
		{
			if ( bMute != _soundMute )
			{
				if ( bMute )
				{
					stopAmbiance();
					stopMusic();
					SoundMixer.stopAll();
					_soundMute = true;
				}
				else
				{
					_soundMute = false;
					playMusic( _currentMusic );
				}
			}
		}
		
		public static function playSound( iSnd:uint, iDex:uint = 0, 
										  repeat:int = 0, 
										  callback:Function = null, 
										  vol:Number = -1,
										  playTo:uint = 0 ):SoundChannel
		{
			var chan:SoundChannel = null;
			
			if ( !_soundMute )
			{
				var sVol:Number = ( vol > 0 ) ? vol : soundVolume;
				chan = _sounds[ iSnd ].play( iDex, repeat );
				chan.soundTransform = new SoundTransform( sVol );
				
				if ( callback != null )
				{ chan.addEventListener( Event.SOUND_COMPLETE, callback ); }
			}
			
			return chan;
		}
		
		public static function toggleSound( ):void
			{ soundMute = !_soundMute; }
			
		public static function addSound( oSound:SoundEx ):void
			{ _sounds.push( oSound ); }

		public static function addSoundEx( sFile:String ):void
			{ _sounds.push( new SoundEx( new URLRequest( sFile ) ) ); }
		
		public static function isLoaded( snd:uint ):Boolean
			{ return _sounds[ snd ].isLoaded; }
		
		// *** MUSIC ***
		public static function get musicVolume( ):Number { return _musicVolume; }
		public static function set musicVolume( nVol:Number ):void
		{
			_musicVolume = Utils.Clamp( nVol, 0, 1 );
			_musicTrans.volume = _musicVolume;
			if ( _musicChannel != null )
				{ _musicChannel.soundTransform = _musicTrans; }
		}
		
		public static function get musicMute( ):Boolean { return _musicMute; }
		public static function set musicMute( bMute:Boolean )
		{
			if ( bMute != _musicMute )
			{
				if ( bMute )
				{
					stopMusic( );
					_musicMute = true;
				}
				else
				{
					playMusic( _currentMusic, _musicIndex );
					_musicMute = false;
				}
			}
		}
		
		public static function playMusic( iMus:uint, iDex:uint = 0, 
										  repeat:int = int.MAX_VALUE, 
										  callback:Function = null ):void
		{
			if ( iMus != _currentMusic )
			{
				stopMusic();
				_currentMusic = iMus;
			}
			else
			{
				iDex = _musicIndex;
			}
			
			if ( !_musicMute && !_soundMute )
			{
				_musicChannel =  _music[ iMus ].play( iDex, repeat );
				_musicChannel.soundTransform = new SoundTransform( musicVolume );
				
				if ( callback != null )
					{ _musicChannel.addEventListener( Event.SOUND_COMPLETE, callback ); }
				else if ( _music[ iMus ].callback != null )
					{ _musicChannel.addEventListener( Event.SOUND_COMPLETE, _music[ iMus ].callback ); }
			}			
		}
		
		public static function stopMusic( ):void
		{
			if ( _musicChannel != null )
			{
				_musicIndex = _musicChannel.position;
				_musicChannel.stop();
			}
		}
		
		public static function toggleMusic( ):void
			{ musicMute = !_musicMute; }
		
		public static function addMusic( oMusic:SoundEx ):void
			{ _music.push( oMusic ); }
		
		public static function addMusicEx( sFile:String ):void
			{ _music.push( new SoundEx( new URLRequest( sFile ) ) ); }
			
		public static function fadeMusic( timerInc:int = 50 ):void
		{
			if ( _musicChannel != null && !fading )
			{
				fading = true;
				fadeTimer.delay = timerInc;
				fadeValue = musicVolume;
				fadeIncrements = musicVolume / 20;
				fadeTimer.reset();
				fadeTimer.start();
			}
		}
		
		public static function fadeTime( e:TimerEvent ):void
		{
			fadeValue -= fadeIncrements;
			_musicChannel.soundTransform = new SoundTransform( fadeValue );
		}
		
		public static function fadeComplete( e:TimerEvent ):void
		{
			fading = false;
		}
	}
}
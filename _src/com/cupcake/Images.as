﻿package com.cupcake
{
	import flash.display.Sprite;
	
	public class Images 
	{
		public static var defaultExternal:Boolean	= false;
		public static var page:WBZ_BookPage			= null;
		
		// Optional vars:
		// align (int): how to align the images (default: Align.UPPER_LEFT)
		// container (Sprite/String): image container. If a String, will look for a matching object in scene_mc
		// enableMouse (Boolean): if true, container will be set to receive mouse events (default: true)
		// fromFile (Boolean): if true, will pull the image from an individual file rather than the Page sprite sheet
		// imageName (String): the name of the sprite sheet image or file (minus the .png extension) to use
		// reusable (Boolean): if true, draw image from the reusable folder (default: false). Ignored if not external.
		// smooth (Boolean): if true, apply smoothing to image (default: false)
		public static function Add( asset:String, vars:Object = null ):void
		{
			var container:Sprite		= ( vars.container ) ? ( ( vars.container is Sprite ) ? vars.container : page.scene_mc.getChildByName( vars.container ) as Sprite )
														 	 : ( page.scene_mc.getChildByName( asset ) as Sprite );
															 
			if ( container == null ) { App.Log( "Images.Add(): container '" + asset + "' is null." ); return; }
			
			var align:int				= ( vars.align ) ? vars.align : Align.UPPER_LEFT;
			var enableMouse:Boolean		= ( vars.hasOwnProperty( "enableMouse" ) ) ? vars.enableMouse : true;
			var fromFile:Boolean		= ( vars.fromFile ) ? vars.fromFile : defaultExternal;
			var imageName:String 		= ( vars.imageName ) ? vars.imageName : asset;
			var reusable:Boolean		= ( vars.reusable ) ? vars.reusable : false;
			var smooth:Boolean			= ( vars.smooth ) ? vars.smooth : false;
			var prefix:String			= ( reusable ) ? "Reusable/" : "";
			
			if ( fromFile )
			{
				page.assets.push( new ExternalImage( prefix + imageName + ".png", container, align, enableMouse, smooth ) );
			}
			else
			{
				page.imagesLoader.Add( imageName, align, smooth, container );
				container.mouseEnabled = enableMouse;
				container.mouseChildren = enableMouse;
			}
		}
		
		// Optional vars:
		// align (int): how to align the images (default: Align.UPPER_LEFT)
		// container (Sprite/String): image container for the first image. If a String, will look for a matching object in scene_mc
		// enableMouse (Boolean): if true, container will be set to receive mouse events (default: true)
		// fromFile (Boolean): if true, will pull the image from individual files rather than the Page sprite sheet
		// reusable (Boolean): if true, draw image from the reusable folder (default: false). Ignored if not external.
		// smooth (Boolean): if true, apply smoothing to image (default: false)
		public static function GetBatch( images:Vector.<String>, vars:Object = null ):Vector.<Sprite>
		{
			var assets:Vector.<Sprite>	= null;
			var align:int				= ( vars.align ) ? vars.align : Align.UPPER_LEFT;
			var container:Sprite		= ( vars.container ) ? ( ( vars.container is Sprite ) ? vars.container : page.scene_mc.getChildByName( vars.container ) as Sprite ) : null;
			var enableMouse:Boolean		= ( vars.hasOwnProperty( "enableMouse" ) ) ? vars.enableMouse : true;
			var fromFile:Boolean		= ( vars.fromFile ) ? vars.fromFile : defaultExternal;
			var reusable:Boolean		= ( vars.reusable ) ? vars.reusable : false;
			var smooth:Boolean			= ( vars.smooth ) ? vars.smooth : false;
			var prefix:String			= ( reusable ) ? "Reusable/" : "";
			
			if ( fromFile )
			{
				for ( var t:int = 0 ; t < images.length ; t++ ) { images[ t ] = prefix + images[ t ] + ".png"; }
				assets = ExternalImage.getBatch( images, page.assets, container, align, enableMouse, smooth );
			}
			else
			{
				assets = page.imagesLoader.GetBatch( images, container, align, smooth );
				for each ( var sprite:Sprite in assets ) { sprite.mouseEnabled = enableMouse; sprite.mouseChildren = enableMouse; }
			}
			
			return assets;
		}
	}	
}
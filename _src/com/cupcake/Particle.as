﻿package com.cupcake 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import com.cupcake.IControllable;
	import com.cupcake.Utils;
	import com.gskinner.motion.*;
	import com.gskinner.motion.easing.*;
	
	public class Particle extends Sprite implements IControllable
	{	
		public var endAlpha:Number				= 0;
		public var endRotation:Number			= 0;
		public var endScale:Number				= 1;
		public var endX:Number					= 0;
		public var endY:Number					= 0;
		public var onComplete:Function			= null;
		
		private var alphaComplete:Boolean		= false;
		private var positionComplete:Boolean	= false;
		private var rotationComplete:Boolean	= false;
		private var sizeComplete:Boolean		= false;
		
		private var _alphaDuration:Number		= 1;
		private var _positionDuration:Number	= 1;
		private var _rotationDuration:Number	= 1;
		private var _sizeDuration:Number		= 1;
		
		private var _alphaEasing:Function;
		private var _positionEasing:Function;
		private var _rotationEasing:Function;
		private var _sizeEasing:Function;
		
		private var _initialized:Boolean		= false;
		private var _paused:Boolean				= false;
		private var _running:Boolean			= false;
		
		private var bitmap:Bitmap;
		private var container:MovieClip;
		
		private var alphaTween:GTween;
		private var positionTween:GTween;
		private var rotationTween:GTween;
		private var sizeTween:GTween;

		public function Particle( particleImage:Bitmap, container:MovieClip ) 
		{
			// need to create the tween objects on construction so we can set properties
			alphaTween = new GTween( this );
			positionTween = new GTween( this );
			rotationTween = new GTween( this );
			sizeTween = new GTween( this );
			
			bitmap = new Bitmap();
			bitmap.bitmapData = new BitmapData( particleImage.width, particleImage.height, true, 0 );
			bitmap.bitmapData.draw( particleImage, new Matrix );
			bitmap.smoothing = true;
			bitmap.x = -bitmap.width / 2;
			bitmap.y = -bitmap.height / 2;
			addChild( bitmap );
			container.addChild( this );
			
			mouseEnabled = false;
			mouseChildren = false;
		}
		
		public function Destroy():void
		{
			if ( isRunning ) Stop();
			
			if ( bitmap != null && this.contains( bitmap ) ) removeChild( bitmap );
			bitmap = null;
			
			Utils.killTween( alphaTween ); alphaTween = null;
			Utils.killTween( positionTween ); positionTween = null;
			Utils.killTween( rotationTween ); rotationTween = null;
			Utils.killTween( sizeTween ); sizeTween = null;
			
			if ( container != null && container.contains( this ) ) container.removeChild( this );
		}
		
		// Public functions
		public function get alphaDuration():Number { return _alphaDuration; }
		public function set alphaDuration( val:Number )
		{
			_alphaDuration = val;
			alphaTween.duration = val;
		}
		
		public function get alphaEasing():Function { return _alphaEasing; }
		public function set alphaEasing( val:Function )
		{
			_alphaEasing = val;
			alphaTween.ease = _alphaEasing;
		}
		
		public function get isInitialized():Boolean { return _initialized; }
		public function get isPaused():Boolean { return _paused; }
		public function get isRunning():Boolean { return _running; }
		
		public function get positionDuration():Number { return _positionDuration; }
		public function set positionDuration( val:Number )
		{
			_positionDuration = val;
			positionTween.duration = val;
		}
		
		public function get positionEasing():Function { return _positionEasing; }
		public function set positionEasing( val:Function )
		{
			_positionEasing = val;
			positionTween.ease = _positionEasing;
		}
		
		public function get rotationDuration():Number { return _rotationDuration; }
		public function set rotationDuration( val:Number )
		{
			_rotationDuration = val;
			rotationTween.duration = val;
		}
		
		public function get rotationEasing():Function { return _rotationEasing; }
		public function set rotationEasing( val:Function )
		{
			_rotationEasing = val;
			rotationTween.ease = _rotationEasing;
		}
		
		public function get sizeDuration():Number { return _sizeDuration; }
		public function set sizeDuration( val:Number )
		{
			_sizeDuration = val;
			sizeTween.duration = val;
		}
		
		public function get sizeEasing():Function { return _sizeEasing; }
		public function set sizeEasing( val:Function )
		{
			_sizeEasing = val;
			sizeTween.ease = _sizeEasing;
		}
		
		public function go():void
		{
			alphaTween.resetValues();
			positionTween.resetValues();
			rotationTween.resetValues();
			sizeTween.resetValues();
			
			alphaTween.setValue( "alpha", endAlpha );
			positionTween.setValue( "x", endX );
			positionTween.setValue( "y", endY );
			rotationTween.setValue( "rotation", endRotation );
			sizeTween.setValue( "scaleX", endScale );
			sizeTween.setValue( "scaleY", endScale );
		}
		
		public function Init():void 
		{
			if ( !isInitialized )
			{
				alphaTween.onComplete = tweenComplete;
				positionTween.onComplete = tweenComplete;
				rotationTween.onComplete = tweenComplete;
				sizeTween.onComplete = tweenComplete;
				
				_initialized = true;
			}
		}
		
		public function Pause():void 
		{ 
			if ( isRunning && !isPaused ) 
			{
				_paused = true;
				alphaTween.paused = true;
				positionTween.paused = true;
				rotationTween.paused = true;
				sizeTween.paused = true;
			}
		}
		
		public function Reset():void
		{
			if ( !isInitialized ) Init();
			if ( isRunning ) Stop();
			_paused = false;
		}
		
		public function Resume():void
		{
			if ( isRunning && isPaused )
			{
				_paused = false;
				alphaTween.paused = false;
				positionTween.paused = false;
				rotationTween.paused = false;
				sizeTween.paused = false;
			}
		}
		
		public function Start():void
		{
			if ( !isInitialized ) Init();
			if ( isRunning ) Reset();			
			_running = true;
			_paused = false;
			go();
		}
		
		public function Stop():void
		{			
			if ( isRunning )
			{
				_running = false;
				alphaTween.paused = true;
				positionTween.paused = true;
				rotationTween.paused = true;
				sizeTween.paused = true;
			}
		}
		
		// Private and protected functions
		protected function tweenComplete( e:GTween ):void
		{
			if ( isRunning && !isPaused )
			{
				switch ( e )
				{
					case alphaTween:
						alphaComplete = true;
						break;
						
					case positionTween:
						positionComplete = true;
						break;
						
					case rotationTween:
						rotationComplete = true;
						break;
						
					case sizeTween:
						sizeComplete = true;
						break;
				}
				
				if ( alphaComplete && positionComplete && rotationComplete && sizeComplete )
				{
					if ( onComplete != null ) onComplete( this );
						else Destroy();
				}
			}
		}

	}
}

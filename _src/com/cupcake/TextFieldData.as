﻿package com.cupcake  
{
	import flash.geom.Rectangle;
	import flash.text.TextFormatAlign;
	import flash.text.Font;
	
	public class TextFieldData 
	{
		public var alignment:String;
		public var bounds:Rectangle;
		public var color:uint;
		public var dropShadow:Boolean;
		public var dropShadowAngle:int;
		public var dropShadowDistance:Number;
		public var filters:Array;
		public var font:Font;
		public var fontSize:Number;
		public var leading:int;
		public var letterSpacing:Number;
		public var strokeColor:uint;
		public var strokeSize:int;
		public var wordWrap:Boolean;

		public function TextFieldData( FontFace:Font, FontColor:uint = 0x000000, FontSize:Number = 12, Bounds:Rectangle = null, 
									   StrokeSize:int = 0, StrokeColor:uint = 0x000000, DropShadow:Boolean = false,
									   DropShadowAngle:int = 45, DropShadowDistance:Number = 6, WordWrap:Boolean = true,
									   Leading:int = 0, LetterSpacing:Number = 0, Filters:Array = null, 
									   Alignment:String = TextFormatAlign.CENTER ) 
		{
			font = FontFace;
			color = FontColor;
			fontSize = FontSize;
			bounds = Bounds != null ? Bounds : new Rectangle;
			strokeSize = StrokeSize;
			strokeColor = StrokeColor;
			dropShadow = DropShadow;
			dropShadowAngle = DropShadowAngle;
			dropShadowDistance = DropShadowDistance;
			wordWrap = WordWrap;
			leading = Leading;
			letterSpacing = LetterSpacing;
			filters = Filters;
			alignment = Alignment;
		}
	}
}

﻿package com.cupcake
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.PixelSnapping;
	import flash.display.Sprite;
	import flash.display.StageQuality;
	import flash.filters.*;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextLineMetrics;
	
	import com.cupcake.TextFieldData;
	import com.cupcake.Utils;
	
	public class TextFieldEx extends MovieClip
	{
		public var bounds:Rectangle;
		public var fixedPara:String			= "";
		public var format:TextFormat;
		public var reader_bg:MovieClip;
		public var tfData:TextFieldData;
		
		private var _boxHeight:Number		= 0;
		private var _boxStartY:Number		= 0;
		private var _fillRect:Rectangle		= new Rectangle(0,0,0,0);
		private var _textLength:int	= 0;
		private var _text:String;
		private var _words:Vector.<String>	= null;
		private var bg:Bitmap				= new Bitmap;
		private var bmp:Bitmap				= new Bitmap;
		public var tf:TextField			= new TextField;
		
		public static function FixNewlines( txt:String ):String { return txt.split( "\\n" ).join( "\n" ); }

		public function TextFieldEx( Text:String, Data:TextFieldData, Bounds:Rectangle = null ) 
		{
			_text = FixNewlines( Text );
			tfData = Data;
			bounds = Bounds != null ? Bounds : tfData.bounds;
			
			this.x = bounds.x;
			this.y = bounds.y;
			
			_boxStartY = this.y;
			_boxHeight = bounds.height;
			
			format = new TextFormat( tfData.font.fontName, Object(tfData.fontSize), tfData.color, null, null, null, null, null, tfData.alignment, null, null, null, tfData.leading );
			format.letterSpacing = tfData.letterSpacing;
			
			bmp.pixelSnapping = PixelSnapping.ALWAYS;
			this.addChild( bmp );

			Draw();
		}
		
		public function get text():String { return _text; }
		public function set text( Text:String ):void { _text = Text; Draw(); }
		
		public function get textLength():int { return _textLength; }
		
		public function Destroy():void { if ( bmp != null && bmp.bitmapData != null ) { bmp.bitmapData.dispose(); } }
		
		public function Draw():void
		{
			bg = new Bitmap( new BitmapData( bounds.width, bounds.height, true, 0 ) );
			this.addChild(bg);
			
			
			
			tf.antiAliasType = AntiAliasType.NORMAL;
			tf.embedFonts = true;
			tf.multiline = tfData.wordWrap;
			tf.selectable = false;
			tf.width = bounds.width;
			tf.wordWrap = tfData.wordWrap;
			
			var filters:Array = tfData.filters != null ? tfData.filters : new Array;
			
			if ( tfData.strokeSize > 0 )
			{
				var glowFilter:GlowFilter = new GlowFilter( tfData.strokeColor, 1, tfData.strokeSize, tfData.strokeSize, 20, 3 );
				filters.push( glowFilter );
			}
			
			if ( tfData.dropShadow )
			{
				var dsFilter:DropShadowFilter = new DropShadowFilter( tfData.dropShadowDistance, tfData.dropShadowAngle, 0x000000,
																	  .8, 6, 6, 1, 3 );
				filters.push( dsFilter );
			}
			
			if ( filters.length > 0 ) { tf.filters = filters; }
			
			this.addChild( tf );
			tf.text = _text;
			tf.setTextFormat( format );
			tf.height = this.height * 2;
			
			var fontSize:Number = Number(format.size);
			while ( ( tf.height > this.height || tf.width > this.width ) && fontSize > 3 )
			{
				tf.text = _text;
				tf.width = this.width;
				tf.setTextFormat( format );
				tf.autoSize = tfData.alignment;
				
				if ( tf.height > this.height || tf.width > this.width )
				{
					fontSize--;
					format.size = fontSize;
				}
			}
			
			tf.autoSize = "none";
			tf.setTextFormat( format );
			removeChild( tf );
			
			// cut up text into words
			fixedPara = _text.replace(/-/g," ");
			var fixedStr:String = fixedPara.replace(/\n/g, "");
			_words = Vector.<String>(fixedStr.split(" "));
			_textLength = int( _words.length );
			
			Redraw();
			
			bg.bitmapData.dispose();
			bg = null;
		}
		
		public function Redraw():void
		{
			var rect:Rectangle = getBoundsByWordIndex( _textLength - 1 );
			this.y = int( _boxStartY + ( _boxHeight - rect.bottom ) / 2 );
			_fillRect.bottom = rect.bottom;
			_fillRect.width = tf.width;
			
			if ( bmp.bitmapData != null ) { bmp.bitmapData.dispose(); }
			bmp.bitmapData = new BitmapData( bounds.width, rect.bottom, true, 0 );
			bmp.bitmapData.fillRect( _fillRect, 0 );
			bmp.bitmapData.drawWithQuality( tf, null, null, null, null, false, StageQuality.BEST );
		}
		
		public function findCharClicked( localX:Number, localY:Number ):int
		{
			var pt:Point = new Point( localX, localY );
			var charIndex:int = tf.getCharIndexAtPoint( pt.x, pt.y );
			charIndex = _text.charAt( charIndex ) == " " ? -1 : charIndex;
			return charIndex;
		}
		
		public function getCharBounds( target:int ):Rectangle
		{
			return tf.getCharBoundaries( target );
		}
		
		public function getBoundsByWordIndex( target:int ):Rectangle
		{
			const breakChars:String					= " \n-";
			var rect:Rectangle						= new Rectangle;
			var highlightMetrics:TextLineMetrics	= null;
			var startDex:int						= getCharByWordIndex( target );
			var curWord:String						= "";
			
			while ( breakChars.indexOf( _text.charAt( startDex ) ) > -1 ) { startDex++; }
			
			var endDex:int = startDex;
			while ( endDex < _text.length && breakChars.indexOf( _text.charAt( endDex ) ) == -1 )
				{ endDex++; }
				
			endDex--;
			
			var startRect:Rectangle = tf.getCharBoundaries( startDex );
			var endRect:Rectangle = tf.getCharBoundaries( endDex );
			
			rect.top = startRect.top;
			rect.left = startRect.left;
			rect.width = endRect.right - startRect.left;
			rect.height = endRect.bottom - startRect.top;
			
			return rect;
		}
		
		public function getCharByWordIndex( target:int )
		{
			const breakChars:String	= " \n-";
			var curWord:int			= 0;
			var curChar:int			= 0;
			
			if ( target >= _textLength ) { App.Log( "Tried to get word after end of text" ); return; }
			
			while ( curWord < target )
			{
				while ( fixedPara.charAt( curChar ) != " " ) { curChar++; }
				while ( breakChars.indexOf( fixedPara.charAt( curChar ) ) > -1 ) { curChar++; }
				curWord++;
			}
			
			return curChar;
		}
		
		public function getWordByCharIndex( target:int )
		{
			var word:int		= -1;
			var charCount:int	= 0;
			
			while ( charCount <= target && word < _textLength )
			{
				while ( fixedPara.charAt( charCount ) != " " && charCount <= target ) { charCount++; }
				word++; charCount++;
			}
			
			if ( word >= _textLength ) { word = -1; }
			
			return word;
		}
	}
}
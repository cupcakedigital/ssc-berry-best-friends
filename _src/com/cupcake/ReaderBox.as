﻿package com.cupcake 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.PixelSnapping;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.display.StageQuality;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.*;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.text.AntiAliasType;
	import flash.text.Font;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextLineMetrics;
	
	import com.cupcake.IControllable;
	
	import com.greensock.TweenLite;
	import com.greensock.easing.Linear;
	import com.greensock.events.TweenEvent;
	
	public class ReaderBox extends MovieClip implements IControllable
	{
		public static const BREAK_PATTERN:RegExp	= /[\n\r\f]/;
		public static const REPLAY_MARGIN:int		= 10;
		
		public static var addEnterFrame:Function	= null;
		public static var removeEnterFrame:Function	= null;
		public static var addMouseDown:Function		= null;
		public static var removeMouseDown:Function	= null;
		public static var stageQuality:String		= "";
		
		public var app:MovieClip;
		public var reader_bg:MovieClip;
		
		public var onComplete:Function				= null;
		public var onTapWord:Function				= null;
		public var onWord:Function					= null;
		
		public var fontColor:uint					= 0x000000;
		public var fontLeading:int					= 6;
		public var fontSize:int						= 30;
		public var highlightColor:uint				= 0xffff00;
		public var paragraph:String					= "";
		public var fixedPara:String					= "";
		public var readTappedWords:Boolean			= true;
		public var textHeight:Number				= 0;
		
		public var cues:Vector.<int>				= null;
		public var font:Font						= null;
		public var voiceover:Sound					= null;
		
		private var _boxHeight:Number				= 0;
		private var _boxStartY:Number				= 0;
		private var _cuesLength:int					= 0;
		private var _currentWord:int				= -1;
		private var _fillRect:Rectangle				= new Rectangle(0,0,0,0);
		private var _highlightedWord:int			= -1;
		private var _initialized:Boolean			= false;
		private var _paused:Boolean					= false;
		private var _pauseLoc:Number				= -1;
		private var _paragraphLength:int			= 0;
		private var _reading:Boolean				= false;
		private var _running:Boolean				= false;
		private var _textSet:Boolean				= false;
		private var _volume:Number					= 1;
		
		private var highlight_spr:Sprite			= new Sprite;
		private var highlight_txt:TextField			= new TextField;
		
		private var reader_bmp:Bitmap				= new Bitmap;
		private var reader_spr:Sprite				= new Sprite;
		private var reader_txt:TextField			= new TextField;
		
		private var word_bounds:Vector.<Rectangle>	= new Vector.<Rectangle>;
		private var highlights:Vector.<Bitmap>		= new Vector.<Bitmap>;
		
		private var _regularFormat:TextFormat		= new TextFormat;
		private var _highlightFormat:TextFormat		= new TextFormat;
		private var _voiceChan:SoundChannel			= null;
		private var _words:Vector.<String>			= null;
		
		// Constructor and standard functions
		public function Destroy():void
		{
			if ( isRunning ) Stop();
				
			removeFrameHandler();
			
			cues = null;
			font = null;
			voiceover = null;
			
			if ( reader_bmp != null && reader_bmp.bitmapData != null ) 
				{ reader_bmp.bitmapData.dispose(); reader_bmp = null; }
			
			reader_txt = null;
			reader_spr = null;
			highlight_txt = null;
			highlight_spr = null;
			_highlightFormat = null;
			_regularFormat = null;
			_voiceChan = null;
		}
		
		public function ReaderBox( sizeRect:Rectangle = null ) 
		{ 
			if ( reader_bg == null && sizeRect != null ) 
			{
				reader_bg = new MovieClip;
				reader_bg.addChild( new Bitmap( new BitmapData( 1, 1, false, 0 ), "auto", false ) );
				reader_bg.width = sizeRect.width;
				reader_bg.height = sizeRect.height;
				addChild( reader_bg );
			}
			
			reader_bg.visible = false;
			reader_bmp.name = "ReaderBox_Bitmap";
			reader_spr.name = "ReaderBox_Sprite";
			reader_txt.name = "ReaderBox_Text";
			
			with ( reader_txt )
			{
				width = this.width;
				multiline = true;
				wordWrap = true;
				selectable = false;
				embedFonts = true;
				antiAliasType = AntiAliasType.NORMAL;
			}
			
			with ( highlight_txt )
			{
				multiline = false;
				wordWrap = false;
				selectable = false;
				embedFonts = true;
				antiAliasType = AntiAliasType.NORMAL;
			}
			
			_boxStartY = this.y;
			_boxHeight = this.height;
			
			scaleX = 1;
			scaleY = 1;
		}
		
		public function get currentWord():int { return _currentWord; }
		public function set currentWord( val:int ) { _currentWord = val; }
		
		public function get highlitedWord():int { return _highlightedWord; }
		
		public function get readingVolume():Number { return _volume; }
		public function set readingVolume( val:Number )
		{
			_volume = val;
			if ( _voiceChan != null ) _voiceChan.soundTransform = new SoundTransform( _volume );
		}
		
		public function getWord( word:int ):String
			{ return _words[word].toString().replace( BREAK_PATTERN, "" ); }
		
		public function hideHighlight( withRedraw:Boolean = true ):void
		{
			if ( _highlightedWord > -1 ) TweenLite.to( highlights[ _highlightedWord ], 1, { alpha: 0, onComplete: fadeComplete, onCompleteParams: [_highlightedWord] } );
			_highlightedWord = -1;
		}
		
		private function fadeComplete( word:int ):void { highlights[ word ].visible = false; }
		
		public function highlightWord( word:int ):String
		{
			var retval:String = "";
			
			if ( word < _paragraphLength && word >= 0 )
			{
				hideHighlight( false );
				TweenLite.killTweensOf( highlights[ word ] );
				highlights[ word ].visible = true;
				highlights[ word ].alpha = 1;
				_highlightedWord = word;
			} else hideHighlight();
			
			return retval;
		}
		
		public function init( readerText:String, oCues:Vector.<int>, oVoiceover:Sound,
							  regSize:Object, regFont:Font, regColor:uint = 0x000000, 
							  hiliteColor:uint = 0xffff00 ):void
		{
			paragraph = fixNewlines( readerText );
			cues = oCues;
			voiceover = oVoiceover;
			font = regFont;
			fontSize = int( regSize );
			fontColor = regColor;
			highlightColor = hiliteColor;
			
			Init();
		}
		
		public function pauseReading():void
		{
			_paused = true;
			if ( _reading && _voiceChan != null )
			{
				_pauseLoc = _voiceChan.position;
				_voiceChan.stop();
				_voiceChan = null;
			}
		}
		
		public function resumeReading():void
		{
			_paused = false;
			if ( _reading )
			{
				if ( _voiceChan != null ) _voiceChan.stop();
				if ( _pauseLoc == -1 ) { _pauseLoc = cues[ 0 ]; }
				_voiceChan = voiceover.play( _pauseLoc, 1, new SoundTransform( _volume ) );
			}
		}
		
		public function fixNewlines( txt:String ):String { return txt.split( "\\n" ).join( "\n" ); }
				
		public function setText( newText:String = "", newCues:Vector.<int> = null ):void
		{
			// examine newText for formatting
			if ( _textSet ) { return; }
			if ( newText != "" ) { paragraph = fixNewlines( newText ); }
			if ( paragraph == reader_txt.text ) { return; }
			if ( newCues != null ) cues = newCues;
			
			//Console.write( "Setting text: \"" + paragraph + "\"" );
			
			// Need to add the textbox to the object for sizing purposes
			addChild( reader_txt );
			
			reader_txt.text = paragraph;
			reader_txt.setTextFormat( _regularFormat );
			reader_txt.height = _boxHeight * 2;
			
			var FontSize:Number = Number(_regularFormat.size);
			
			while( reader_txt.height > _boxHeight && FontSize > 3 )
			{
				reader_txt.text = paragraph;
				reader_txt.width = this.width;
				reader_txt.setTextFormat( _regularFormat );
				reader_txt.autoSize = _regularFormat.align;
				
				if( reader_txt.height > _boxHeight )
				{
					FontSize--;
					_regularFormat.size = FontSize;
				}
			}
			
			_highlightFormat.size = FontSize;
			
			reader_txt.autoSize = "none";
			reader_txt.setTextFormat( _regularFormat );
			removeChild( reader_txt );
			
			// cut up text into words
			fixedPara = paragraph.replace(/-/g," ");
			fixedPara = fixedPara.replace(/:/g," ");
			var fixedStr:String = fixedPara.replace(/\n/g, "");
			_words = Vector.<String>(fixedStr.split(" "));
			_paragraphLength = int( _words.length );
			_cuesLength = cues != null ? int( cues.length ) : 0;
			
			// gather bounding boxes for each word and create highlight bitmap
			for ( var t:int = 0 ; t < _paragraphLength ; t++ )
			{
				word_bounds[ t ] = getBoundsByWordIndex( t );
				var bmp:Bitmap = new Bitmap;
				highlights.push( bmp );
				highlight_txt.text = _words[ t ];
				highlight_txt.setTextFormat( _highlightFormat );
				highlights[ t ].name = "ReaderBox_Word" + t;
				highlights[ t ].bitmapData = new BitmapData( highlight_txt.width + 6, highlight_txt.height + 6, true, 0 );
				highlights[ t ].bitmapData.drawWithQuality( highlight_txt, null, null, null, null, false, StageQuality.BEST );
				highlights[ t ].x = word_bounds[ t ].x - 2;
				highlights[ t ].y = word_bounds[ t ].y - 2;
				highlights[ t ].visible = false;
				highlights[ t ].pixelSnapping = PixelSnapping.ALWAYS;
				reader_spr.addChild( bmp );
				//App.Log( "Word '" + highlight_txt.text + "' at " + highlights[ t ].x + ", " + highlights[ t ].y + " is " + highlights[ t ].width + "x" + highlights[ t ].height );
			}
			
			var rect:Rectangle = word_bounds[ _paragraphLength - 1 ];
			this.y = int( _boxStartY + ( _boxHeight - rect.bottom ) / 2 );
			_fillRect.bottom = rect.bottom;
			_fillRect.width = reader_txt.width;
			
			if ( reader_bmp.bitmapData != null ) reader_bmp.bitmapData.dispose();
			reader_bmp.bitmapData = new BitmapData( reader_txt.width, rect.bottom, true, 0 );
			Redraw();
			_textSet = true;
			textHeight = reader_txt.height;
		}
		
		public function startReading():void
		{
			if ( !_paused )
			{
				registerFrameHandler();
				if ( _voiceChan != null ) { _voiceChan.stop(); }
				
				_currentWord = 0;
				_pauseLoc = -1;
				_voiceChan = voiceover.play( cues[ 0 ], 1, new SoundTransform( _volume ) );
				_reading = true;
			}
		}
		
		public function stopReading():void
		{
			removeFrameHandler();
			if ( _voiceChan != null ) _voiceChan.stop();
			_reading = false;
			hideHighlight();
		}		
		
		// Standard functions
		public function get isInitialized():Boolean { return _initialized; }
		public function get isPaused():Boolean { return _paused; }
		public function get isRunning():Boolean { return _running; }
		
		public function Init():void
		{
			if ( isInitialized ) return;
			
			reader_bmp.pixelSnapping = PixelSnapping.ALWAYS;
			reader_spr.addChild( reader_bmp );
			addChild( reader_spr );
			addChild( highlight_spr );
			
			// set up font formats
			_regularFormat.size = fontSize;
			_regularFormat.align = TextFormatAlign.CENTER;
			_regularFormat.color = fontColor;
			_regularFormat.font = font.fontName;
			_regularFormat.leading = fontLeading;
			_regularFormat.kerning = false
			
			_highlightFormat.size = fontSize;
			_highlightFormat.align = TextFormatAlign.LEFT;
			_highlightFormat.color = highlightColor;
			_highlightFormat.font = font.fontName;
			_highlightFormat.leading = fontLeading;
			
			highlight_txt.autoSize = TextFieldAutoSize.LEFT;
			
			var glowFilt:GlowFilter = new GlowFilter( highlightColor, 1, 1, 1, 2, 3 );
			highlight_txt.filters = [glowFilt];
			
			_initialized = true;
		}
		
		public function Pause():void { _paused = true; }
		
		public function Redraw():void 
		{ 
			reader_bmp.bitmapData.fillRect( _fillRect, 0 );
			reader_bmp.bitmapData.drawWithQuality( reader_txt, null, null, null, null, false, StageQuality.BEST );
			//reader_bmp.smoothing = true;
		}
		
		public function Reset():void
		{
			if ( isRunning ) Stop();
			_paused = false;
			_reading = false;
			_currentWord = -1;
		}
		
		public function Resume():void { _paused = false; }
		
		public function Start():void 
		{ 
			_running = true; 
			if ( addMouseDown != null ) addMouseDown( readerTap );
				else Utils.addHandler( reader_spr, MouseEvent.MOUSE_DOWN, readerTap );
		}
		
		public function Stop():void 
		{ 
			if ( isRunning )
			{
				stopReading();
				if ( removeMouseDown != null ) removeMouseDown( readerTap );
					else Utils.removeHandler( reader_spr, MouseEvent.MOUSE_DOWN, readerTap );
				_running = false; 
			}
		}

		// *****************************
		// *PRIVATE/PROTECTED FUNCTIONS*
		// *****************************
		private function findCharClicked( localX:Number, localY:Number ):int
		{
			var pt:Point = new Point( localX, localY );
			var charIndex:int = reader_txt.getCharIndexAtPoint( pt.x, pt.y );
			charIndex = paragraph.charAt( charIndex ) == " " ? -1 : charIndex;
			return charIndex;
		}
		
		private function getBoundsByWordIndex( target:int ):Rectangle
		{
			const breakChars:String					= " \n-";
			var rect:Rectangle						= new Rectangle;
			var highlightMetrics:TextLineMetrics	= null;
			var startDex:int						= getCharByWordIndex( target );
			var curWord:String						= "";
			
			while ( breakChars.indexOf( paragraph.charAt( startDex ) ) > -1 ) { startDex++; }
			
			var endDex:int = startDex;
			while ( endDex < paragraph.length && breakChars.indexOf( paragraph.charAt( endDex ) ) == -1 )
				{ endDex++; }
				
			endDex--;
			
			var startRect:Rectangle = reader_txt.getCharBoundaries( startDex );
			var endRect:Rectangle = reader_txt.getCharBoundaries( endDex );
			
			rect.top = startRect.top;
			rect.left = startRect.left;
			rect.width = endRect.right - startRect.left;
			rect.height = endRect.bottom - startRect.top;
			
			return rect;
		}
		
		private function getCharByWordIndex( target:int )
		{
			const breakChars:String	= " \n-";
			var curWord:int			= 0;
			var curChar:int			= 0;
			
			while ( curWord < target )
			{
				while ( fixedPara.charAt( curChar ) != " " ) { curChar++; }
				while ( breakChars.indexOf( fixedPara.charAt( curChar ) ) > -1 ) { curChar++; }
				curWord++;
			}
			
			return curChar;
		}
		
		private function getWordByCharIndex( target:int )
		{
			var word:int		= -1;
			var charCount:int	= 0;
			
			while ( charCount <= target && word < _paragraphLength )
			{
				while ( fixedPara.charAt( charCount ) != " " && charCount <= target ) { charCount++; }
				word++; charCount++;
			}
			
			if ( word >= _paragraphLength ) { word = -1; }
			
			return word;
		}
		
		protected function handleFrame( e:Event ):void
		{
			if ( _reading && !isPaused && isRunning )
			{
				// Check voiceover playback for passing cues
				if ( _voiceChan != null && ( _voiceChan.position > cues[ _currentWord ] ) )
				{
					while ( _currentWord < _cuesLength && _voiceChan.position > cues[ _currentWord ] )
					{
						if ( _currentWord < ( _cuesLength - 1 ) ) highlightWord( _currentWord );
						_currentWord++;
						if ( onWord != null ) onWord( this );
					}
					
					if ( _currentWord >= _cuesLength )
					{
						if ( onComplete != null ) onComplete( this );
						stopReading();
					}					
				}
			}
		}
		
		private function readerTap( e:MouseEvent ):void
		{
			if ( e.target == reader_spr )
			{
				var char:int = findCharClicked( e.localX, e.localY );
			
				if ( char > -1 )
				{
					var word:int = getWordByCharIndex( char );
					if ( word > -1 ) if ( onTapWord != null ) onTapWord( this, word );
				} 
			}
			
			e.stopImmediatePropagation();
		}
		
		protected function registerFrameHandler():void
		{
			if ( addEnterFrame != null ) addEnterFrame( handleFrame );
				else Utils.addHandler( this, Event.ENTER_FRAME, handleFrame );
		}
		
		protected function removeFrameHandler():void
		{
			if ( removeEnterFrame != null ) removeEnterFrame( handleFrame );
				else Utils.removeHandler( this, Event.ENTER_FRAME, handleFrame );
		}		
	}
}
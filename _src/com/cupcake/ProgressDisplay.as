﻿package com.cupcake
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	
	import com.gskinner.motion.*;
	import com.gskinner.motion.easing.*;
	
	
	public class ProgressDisplay extends Sprite
	{
		public static const DEFAULT_DURATION:Number	= .50;
		
		private var _duration:Number				= DEFAULT_DURATION;
		private var _ease:Function					= Linear.easeNone;
		private var _scale:Number					= 0;
		private var _xOffset:Number;
		private var _yOffset:Number;
		
		private var _barImage:Bitmap;
		private var _fillColor:Object;
		private var _fillImage:Sprite;
		private var _edgeImage:Sprite;
		private var _edgeColor:Object;
		private var _edgeWidth:int;
		private var _tween:GTween;
		

		public function ProgressDisplay( barImage:Bitmap, fillColor:Object, xOffset:Number, yOffset:Number, edgeColor:Object, edgeWidth:int ) 
		{
			_barImage = barImage;
			_fillColor = fillColor;
			_xOffset = xOffset;
			_yOffset = yOffset;
			_edgeColor = edgeColor;
			_edgeWidth = edgeWidth;
			
			var fill_width:Number = _barImage.width - ( _xOffset * 2 );
			var fill_height:Number = _barImage.height - ( _yOffset * 2 );
			
			_fillImage = new Sprite();
			_edgeImage = new Sprite();
			with ( _fillImage.graphics )
			{
				beginFill( fillColor );
				drawRect( 0, 0, fill_width, fill_height );
				endFill();
			}
			
			_edgeImage.graphics.beginFill(uint(_edgeColor), 1);
			_edgeImage.graphics.drawRect(0,0,_edgeWidth, fill_height);
			_edgeImage.graphics.endFill();
			
			with ( _fillImage )
			{
				x = _xOffset;
				y = _yOffset;
				scaleX = 0;
			}
			
			_edgeImage.x = _fillImage.x + _fillImage.width;
			_edgeImage.y = _fillImage.y;
			
			addChild( _fillImage );
			addChild( _edgeImage) ;
			addChild( _barImage );
			
			_tween = new GTween( _fillImage, _duration );
			_tween.suppressEvents = false;
			_tween.onChange = onChange;
		}
		
		public function destroy():void
		{
			while(this.numChildren > 0) 
				this.removeChildAt(0);
			
			_ease = null;
			if(_barImage.bitmapData != null) _barImage.bitmapData.dispose();
			_barImage = null;
			
			_fillColor = null;
			
			_fillImage.graphics.clear();
			while(_fillImage.numChildren > 0)
				_fillImage.removeChildAt(0);
			_fillImage = null;
				
			_edgeImage.graphics.clear();
			while(_edgeImage.numChildren > 0)
				_edgeImage.removeChildAt(0);
			_edgeImage = null;
			
			_edgeColor = null;
			
			_tween.onChange = null;
			//_tween.ease = null;
			_tween = null;
			
		}
		
		private function onChange(src:GTween):void
		{
			_edgeImage.x = _fillImage.x + _fillImage.width;
			_edgeImage.y = _fillImage.y;
			
		}
		
		public function set duration( val:Number ):void
		{
			_duration = val;
			_tween.duration = _duration;
		}
		
		public function set ease( val:Function ):void
		{
			_ease = val;
			_tween.ease = _ease;
		}
		
		public function get scale( ):Number { return _scale; }
		public function set scale( val:Number ):void
		{
			_scale = val;
			_tween.setValue( 'scaleX', _scale );
		}
		
		public function reset():void
		{
			_scale = 0;
			_fillImage.scaleX = 0;
			_edgeImage.x = _fillImage.x + _fillImage.width;
		}

	}
	
}

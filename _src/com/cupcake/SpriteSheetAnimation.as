﻿package com.cupcake
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	import fw.anim.AnimSeqData;
	import fw.anim.AnimSprite;
	import fw.anim.AnimTextureSheet;
	import fw.anim.TileSheetHelper;
	
	public class SpriteSheetAnimation implements IControllable
	{
		public static var addEnterFrame:Function		= null;
		public static var removeEnterFrame:Function		= null;
		
		public var defaultFrameRate:int					= 30;
		
		public var anim:AnimSprite						= null;
		public var container:Sprite;
		public var onComplete:Function					= null;
		public var onFrame:Function						= null;
		public var onLoad:Function						= null;
		public var sheet:SpriteSheet;
		public var sequence:String						= "all";
		
		private var frames:Array						= null;
		private var reverseFrames:Array					= null;
		private var reversingFrames:Array				= null;
		private var texSheet:AnimTextureSheet			= null;
		
		private var _initialized:Boolean				= false;
		private var _paused:Boolean						= false;
		private var _running:Boolean					= false;

		public function SpriteSheetAnimation( con:Sprite, spriteSheet:SpriteSheet, _defaultFrameRate:int = 30 ) 
		{
			container = con;
			sheet = spriteSheet;
			defaultFrameRate = _defaultFrameRate;
			if ( sheet.isLoaded ) { sheetLoaded( sheet ); }
				else { sheet.onLoad = sheetLoaded; }
		}
		
		public function get isInitialized():Boolean { return _initialized; }
		public function get isPaused():Boolean { return _paused; }
		public function get isRunning():Boolean { return _running; }
		
		public function Destroy():void 
		{ 
			if ( isRunning ) Stop(); 
		}
		
		public function Init():void
		{
			if ( isInitialized ) return;
			
			var helper:TileSheetHelper = new TileSheetHelper();
			texSheet = helper.prepareAnimTexture( sheet.spriteSheet, sheet.json );
			frames = createSeqArray( texSheet.numFrames );
			reverseFrames = createReverseSequence( texSheet.numFrames );
			reversingFrames = createReversingSequence( texSheet.numFrames );
			anim = new AnimSprite();
			anim.onFrame = OnFrame;
			anim.initialize( texSheet );
			anim.addSequence( "reset", [0], defaultFrameRate, false );
			anim.addSequence( "all", frames, defaultFrameRate );
			anim.addSequence( "noloop", frames, defaultFrameRate, false );
			anim.addSequence( "reverse", reverseFrames, defaultFrameRate, false );
			anim.addSequence( "reversing", reversingFrames, defaultFrameRate, false );
			container.addChild( anim );
			
			_initialized = true;
		}
		
		public function playSequence( seq:String ):void
		{
			sequence = seq;
			if ( isRunning ) 
			{
				startEnterFrame();
				anim.play( sequence );
			} else Start();
		}
		
		public function Pause():void
		{
			if ( isRunning && !isPaused )
			{
				stopEnterFrame();
				_paused = true;
			}
		}
		
		public function Reset():void
		{
			if ( !isInitialized ) Init();
			if ( isRunning ) Stop();
			
			anim.play("reset");
			anim.frame = 0;
			anim.drawFrame(true);
			anim.stop();
		}
		
		public function Resume():void
		{
			if ( isRunning && isPaused )
			{
				//startEnterFrame();
				if ( anim.isPlaying() ) { startEnterFrame(); }
				_paused = false;
			}
		}
		
		public function Start():void
		{
			if ( !isInitialized ) Init();
			if ( isRunning ) Reset();
			
			startEnterFrame();
			anim.play( sequence );
			
			_running = true;
			_paused = false;
		}
		
		public function Stop():void
		{
			if ( isRunning )
			{
				stopEnterFrame();
				_running = false;
			}
		}
		
		public function Update():void
		{
			if ( isRunning && !isPaused )
			{
				anim.updateAnimation();
				if ( !anim.isPlaying() )
				{
					stopEnterFrame();
					if ( onComplete != null ) onComplete( this );
				}
			}
		}
		
		private function createSeqArray(count:int):Array
		{
			var arOut:Array= []; // of int
			
			for (var j:int = 0; j < count; j++) {
				arOut.push(j);
			}
			
			return arOut;
		}
		
		private function createReverseSequence(count:int):Array
		{
			var arOut:Array = [];
			for (var j:int = count - 1 ; j >= 0 ; j-- )
			{
				arOut.push(j);
			}
			
			return arOut;
		}
		
		private function createReversingSequence(count:int):Array
		{
			var arOut:Array = [];
			for (var j:int = 0; j < count; j++)
			{
				arOut.push(j);
			}
			
			for ( j -= 2 ; j >= 0 ; j-- )
			{
				arOut.push(j);
			}
			
			return arOut;
		}
		
		private final function handleFrame( e:Event ):void { Update(); }
		
		private final function OnFrame( e:AnimSprite, frame:uint ):void
		{
			if ( onFrame != null ) { onFrame( this, frame ); }
		}
		
		private final function sheetLoaded( sheet:SpriteSheet ):void 
		{ 
			Init(); 
			if ( onLoad != null ) { onLoad( this ); }
		}
		
		private final function startEnterFrame():void
		{
			if ( addEnterFrame != null ) addEnterFrame( handleFrame );
				else Utils.addHandler( container, Event.ENTER_FRAME, handleFrame );
		}
		
		private final function stopEnterFrame():void
		{
			if ( removeEnterFrame != null ) removeEnterFrame( handleFrame );
				else Utils.removeHandler( container, Event.ENTER_FRAME, handleFrame );
		}
	}
}
﻿package com.cupcake
{
	import flash.net.NetConnection;
	import flash.net.NetStream;
	
	public class XMPNetStream extends NetStream
	{
		public var onCuePoint:Function = handleCuePoint;
		public var onMetaData:Function = handleMetaData;
		public var onPlayStatus:Function = handlePlayStatus;
		public var onXMPData:Function = handleXMPData;
		public var onTextData:Function = handleTextData;
		
		public function XMPNetStream( nc:NetConnection ) { super( nc ); }
		
		public function handleCuePoint( dataObject:Object ):void {}
		public function handleMetaData( dataObject:Object ):void {}
		public function handlePlayStatus( dataObject:Object ):void {}
		public function handleTextData( dataObject:Object ):void {}
		public function handleXMPData( dataObject:Object ):void {}
	}
	
}

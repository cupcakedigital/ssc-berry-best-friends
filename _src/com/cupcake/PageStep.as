﻿package com.cupcake
{
	import flash.text.Font;
	import com.gskinner.motion.*;
	import com.cupcake.ReaderBox;
	import com.cupcake.Utils;
	
	public class PageStep 
	{
		public var colorHighlight:Object	= null;
		public var colorReg:Object			= null;
		public var fontHighlight:Font		= null;
		public var fontReg:Font				= null;
		public var fontSize:Object			= null;
		public var strokeSize:Number		= 0;
		
		public var reader:ReaderBox			= null;
		public var textIndex:int			= 0;
		public var text:String				= "";
		public var cues:Vector.<int>		= null;
		public var timeline:GTweenTimeline	= null;
		public var playSimultaneous:Boolean	= false;

		public function PageStep( oReader:ReaderBox = null, iTextIndex = 0, oTimeline = null, bPlaySimultaneous:Boolean = false ) 
		{
			reader = oReader;
			textIndex = iTextIndex;
			timeline = oTimeline;
			playSimultaneous = bPlaySimultaneous;
		}
		
		public function destroy():void
		{
			reader = null;
			if ( timeline != null )
			{
				timeline.paused = true;
				timeline.onComplete = null;
				timeline = null;
			}
		}
		
		public function reset():void
		{
			if ( reader != null ) { reader.Reset(); reader.visible = false; }
			if ( timeline != null ) timeline.gotoAndStop(0);
		}
	}
	
}

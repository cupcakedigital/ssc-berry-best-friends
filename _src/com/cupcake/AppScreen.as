﻿package com.cupcake
{
	import flash.display.MovieClip;
	
	public class AppScreen extends MovieClip
	{
		protected var app:MovieClip;
		public var active:Boolean = false;
		
		public function AppScreen( appInit:MovieClip = null ):void
		{
			app = appInit;
		}
		
		public function Show():void
		{
			active = true;
			visible = true;
		}
		
		public function Hide():void
		{
			active = false;
			visible = false;
		}
		
		public function Activate():void
		{
			active = true;
		}
		
		public function Deactivate():void
		{
			active = false;
		}
		
		public function EndShow():void
		{
			// stub for override
		}
	}
}
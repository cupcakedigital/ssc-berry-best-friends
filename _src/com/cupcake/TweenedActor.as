﻿package com.cupcake
{
	import flash.display.InteractiveObject;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import com.cupcake.TimerEx;
	import com.gskinner.motion.GTween;
	
	public dynamic class TweenedActor implements IControllable
	{
		public var onTap:Function			= null;
		public var onTimer:Function			= null;
		public var state:int				= 0;
		
		public var actor:InteractiveObject	= null;
		public var timer:TimerEx			= null;
		public var tween:GTween				= null;
		
		private var _initialized:Boolean	= false;
		private var _paused:Boolean			= false;
		private var _running:Boolean		= false;
		
		public function TweenedActor( oActor:InteractiveObject, fOnTap:Function = null, fOnTimer:Function = null, iState:int = 0, autoStart = false ) 
		{
			actor = oActor;
			onTap = fOnTap;
			onTimer = fOnTimer;
			state = iState;
			if ( autoStart ) Start();
		}
		
		public function get isInitialized():Boolean { return _initialized; }
		public function get isPaused():Boolean { return _paused; }
		public function get isRunning():Boolean { return _running; }
		
		public function Destroy():void
		{
			if ( isRunning ) Stop();
			actor = null;
			if ( timer != null ) Utils.removeHandler( timer.timer, TimerEvent.TIMER, timerTick );
			timer = null;
			Utils.killTween( tween );
		}
		
		public function Init():void 
		{
			if ( !isInitialized )
			{
				tween = new GTween( actor );
				timer = new TimerEx( 5, 1 );
				Utils.addHandler( timer.timer, TimerEvent.TIMER, timerTick );
				_initialized = true;
			}
		}
		
		public function Pause():void { if ( isRunning && !isPaused ) _paused = true; }
		
		public function Reset():void 
		{
			if ( !isInitialized ) Init();
			if ( isRunning ) Stop();
			_paused = false; 
			timer.reset();
			timer.stop();
		}
		
		public function Resume():void { if ( isRunning && isPaused ) _paused = false; }
		
		public function Start():void 
		{ 
			if ( !isInitialized ) Init();
			if ( isRunning ) Reset();			
			_running = true; 
			_paused = false; 
		 	Utils.addHandler( actor, MouseEvent.MOUSE_DOWN, actorTap );
		}
		
		public function Stop():void 
		{ 
			if ( isRunning )
			{
				_running = false; 
				Utils.removeHandler( actor, MouseEvent.MOUSE_DOWN, actorTap );
				timer.stop();
				tween.paused = true;
			}
		}
		
		protected function actorTap( e:MouseEvent ):void
			{ if ( onTap != null && isRunning && !isPaused ) onTap( e ); }
			
		protected function timerTick( e:TimerEvent ):void
			{ if ( onTimer != null && isRunning && !isPaused ) onTimer( e ); }
	}
	
}

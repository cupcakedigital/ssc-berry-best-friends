﻿package com.cupcake
{
	// Use this class to ensure app stays locked in original orientation type
	import flash.display.Stage;
	import flash.display.StageOrientation;
	import flash.events.StageOrientationEvent;
	
	public class OrientationHandler 
	{
		public static var base:String;
		public static var reverse:String;

		public static function Start( appStage:Stage ):void
		{
			try { appStage.autoOrients = true; } 
				catch(e:Error) { App.Log("Can not set stage.autoOrients to true. " + e.message); }
				
			base = appStage.orientation;
			reverse = base == StageOrientation.DEFAULT ? StageOrientation.UPSIDE_DOWN :
					  base == StageOrientation.UPSIDE_DOWN ? StageOrientation.DEFAULT :
					  base == StageOrientation.ROTATED_LEFT ? StageOrientation.ROTATED_RIGHT :
					  StageOrientation.ROTATED_LEFT;
			
			appStage.addEventListener(StageOrientationEvent.ORIENTATION_CHANGING, orientationChange);
		}
		
		private static function orientationChange( e:StageOrientationEvent ):void
		{
			if ( e.afterOrientation != base && e.afterOrientation != reverse )
				e.preventDefault();
		}

	}
	
}

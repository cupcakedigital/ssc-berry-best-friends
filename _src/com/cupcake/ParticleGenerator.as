﻿package com.cupcake
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import com.cupcake.IControllable;
	
	public class ParticleGenerator extends MovieClip implements IControllable
	{
		private var _particleBitmap:Bitmap			= null;
		private var _particleImage:DisplayObject	= null;
		
		private var _initialized:Boolean			= false;
		private var _paused:Boolean					= false;
		private var _running:Boolean				= false;

		public function ParticleGenerator( particle:DisplayObject = null ) 
			{ particleImage = particle; mouseEnabled = false; mouseChildren = false; }
			
		public function addParticle( endAlpha:Number, endX:Number, endY:Number, endRotation:Number, endScale:Number ):Particle
		{
			var part:Particle = new Particle( _particleBitmap, this );
			part.endAlpha = endAlpha;
			part.endX = endX;
			part.endY = endY;
			part.endRotation = endRotation;
			part.endScale = endScale;
			
			return part;
		}
		
		public function generate():void { /* stub for override */ }
		
		public function set particleImage( val:DisplayObject )
		{
			_particleImage = val;
			
			if ( val != null )
			{
				if ( _particleBitmap != null ) _particleBitmap.bitmapData.dispose();
					else _particleBitmap = new Bitmap();
					
				_particleBitmap.bitmapData = new BitmapData( val.width, val.height, true, 0 );
				_particleBitmap.bitmapData.draw( val );
				_particleBitmap.smoothing = true;
			}
		}
		
		// Standard functions
		public function get isInitialized():Boolean { return _initialized; }
		public function get isPaused():Boolean { return _paused; }
		public function get isRunning():Boolean { return _running; }
		
		public function Destroy():void
		{
			if ( isRunning ) Stop();
			particleImage = null;
			_particleBitmap = null;
		}
		
		public function Init():void 
		{
			if ( !isInitialized )
			{
				_initialized = true;
				Reset();
			}
		}
		
		public function Reset():void 
		{
			if ( !isInitialized ) Init();
			if ( isRunning ) Stop();
			_paused = false;
		}
		
		public function Pause():void { if ( isRunning && !isPaused ) _paused = true; }
		public function Resume():void { if ( isRunning && isPaused ) _paused = false; }
		
		public function Start():void 
		{ 
			if ( !isInitialized ) Init(); 
			if ( isRunning ) Reset(); 
			_running = true;
			_paused = false; 
		}
		
		public function Stop():void { if ( isRunning ) _running = false; }
	}
}
﻿package com.cupcake
{
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.NetStatusEvent;
	import flash.filesystem.File;
	import flash.net.NetConnection;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.media.SoundTransform;
	import flash.media.Video;
	import flash.media.StageVideo;
	import flash.events.StageVideoAvailabilityEvent;
	import flash.media.StageVideoAvailability;
	import flash.xml.*;
	
	import com.cupcake.XMPNetStream;
	
	public class VideoBox extends Sprite
	{
		public static var xmlData:XML			= null;
		
		public var onBackgroundTap:Function		= null;
		public var onComplete:Function			= null;
		public var onCuePoint:Function			= null;
		public var onSubtitle:Function			= null;
		public var onVideoTap:Function			= null;
		public var subtitleFile:String			= "";
		public var subtitleTrack:int			= 0;
		public var videoFile:String				= "";
		public var videoFolder:String			= "";
		public var videoIndex:int				= -1;
		public var videoName:String				= "";
		
		protected var _paused:Boolean			= true;
		protected var _playing:Boolean			= false;
		
		private var _nc:NetConnection;
		protected var _ns:XMPNetStream;
		private var _soundTrans:SoundTransform = new SoundTransform();
		private var _spr:Sprite;
		private var _vid:Video;	
		
		// Constructor and standard functions
		public function VideoBox() 
		{
			// make background sprite for sizing purposes
			_spr = new Sprite();
			with ( _spr.graphics )
			{
				beginFill( 0xFFFFFF );
				drawRect( 0, 0, 1, 1 );
				endFill();
			}
			//_spr.visible = false;
			addChild( _spr );
			
			_nc = new NetConnection();
			_nc.connect( null );
			_ns = new XMPNetStream( _nc );
			_ns.onCuePoint = handleCuePoint;
			_ns.onMetaData = handleMetaData;
			_ns.onTextData = handleTextData;
			Utils.addHandler( _ns, NetStatusEvent.NET_STATUS, onNetStatus );			
		}
		
		public function destroy():void
		{
			_nc = null;
			_ns = null;			
		}
		
		public function init():void
		{
			_spr.width = this.width;
			_spr.height = this.height;
			
			Utils.addHandler( _spr, MouseEvent.CLICK, backTap );
			scaleX = 1;
			scaleY = 1;
			
			_vid = new Video(this.width, this.height);
			_vid.smoothing = true;			
			addChild( _vid );
		}
		
		// Public functions
		public function set soundTrans( val:SoundTransform ) 
		{
			_soundTrans = val;
			_ns.soundTransform = val; 
		}
		
		public function setVideo( index:int ):void
		{			
			videoIndex = index;
			var node:XMLList = xmlData.Video.(@index==index);
			videoFile = videoFolder + node.FileName.toString();
			//clsMain.log( "Video file: " + videoFile );
			videoName = node.VideoName.toString();
		}
		
		public function pauseVideo():void
		{
			if ( _playing && !_paused )
			{
				_ns.pause();
				_paused = true;
			}
		}
		
		public function playVideo():void
		{
			// make sure video file exists
			if ( ( !_playing || ( _playing && !_paused ) ) && videoFile != "" ) startStream();
			else resumeVideo();
		}
		
		public function resumeVideo():void
		{
			if ( _playing && _paused )
			{
				_ns.resume();
				_paused = false;
			}
		}
		
		public function stopVideo():void
		{
			_ns.close();
			_vid.clear();
			_playing = false;
		}
		
		public function togglePause():void
		{
			if ( _paused ) resumeVideo();
				else pauseVideo();
		}
		
		// Private and protected functions
		private function backTap( e:MouseEvent ):void
		{
			if ( onBackgroundTap != null )
				onBackgroundTap( this );
		}
		
		private function handleCuePoint( dataObject:Object ):void
		{
			if ( onCuePoint != null ) onCuePoint( dataObject );
				
			if ( videoIndex > -1 )
			{
				var sub:XMLList = xmlData.Video.(@index==videoIndex).Cue.(@point==String(dataObject["name"]));
				if ( onSubtitle != null )
					onSubtitle( sub.toString() );
			}
		}
		
		private function handleMetaData( dataObject:Object ):void
		{
			// Find correct scaling and position
			var vidScaleX:Number = width / dataObject.width;
			var vidScaleY:Number = height / dataObject.height;
			var vidScale:Number = vidScaleX > vidScaleY ? vidScaleY : vidScaleX;
			
			_vid.width = dataObject.width * vidScale;
			_vid.height = dataObject.height * vidScale;
			_vid.x = width / 2 - _vid.width / 2;
			_vid.y = height / 2 - _vid.height / 2;
		}
		
		private function handleTextData( dataObject:Object ):void
		{
			if ( onSubtitle != null && dataObject['trackid'] == subtitleTrack )
				onSubtitle( dataObject['text'] );						  
		}
		
		protected function onNetStatus( e:NetStatusEvent ):void
		{
			if ( e.info.code === "NetStream.Play.Stop" && onComplete != null )
				onComplete();
		}
		
		private function startStream():void
		{
			var file:File;
			
			if ( videoFile != "" )
			{
				file = File.applicationDirectory;
				file = file.resolvePath( videoFile );
			
				if ( file.exists )
				{				
				
					_vid.attachNetStream( _ns );
					_ns.play( videoFile );
					_ns.soundTransform = _soundTrans;
					_playing = true;
					_paused = false;
				}
			}
		}
		
		
	}
}
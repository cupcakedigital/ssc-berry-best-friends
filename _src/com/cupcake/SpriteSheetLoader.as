﻿package com.cupcake
{
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import com.cupcake.ExternalImage;
	import com.cupcake.SoundCollection;
	import com.cupcake.SpriteSheet;
	import com.cupcake.SpriteSheetSequence;
	
	public class SpriteSheetLoader 
	{
		public var anims:Vector.<TapSpritesheet>	= new Vector.<TapSpritesheet>;
		
		private var page:WBZ_BookPage;

		public function SpriteSheetLoader( _page:WBZ_BookPage ) { page = _page; }
		
		// reversing: animation yo-yos if true
		// reverse: animation will yo-yo on subsequent taps
		public function Add( sheetName:String, frameRate:int = 30, sounds:SoundCollection = null, soundRange:Array = null, reversing:Boolean = false, reverse:Boolean = false, sequences:Vector.<SpriteSheetSequence> = null, endFrames:Vector.<int> = null, container:Sprite = null, _button:SimpleButton = null ):TapSpritesheet
		{
			var actorName:String = sheetName.indexOf( "Reusable/" ) == 0 ? sheetName.substr(9) : sheetName;
			var actor:Sprite = ( ( container != null ) ? container : page.scene_mc.getChildByName( actorName ) ) as Sprite;
			var button:SimpleButton = ( ( _button != null ) ? _button : page.scene_mc.getChildByName( actorName + "_BTN" ) ) as SimpleButton;
			if ( actor == null ) { App.Log( "SpriteSheetLoader.Add(): actor '" + ( ( container != null ) ? container.toString() : actorName ) + "' is null." ); return null; }
			page.CheckSparkler( container != null ? container.name : sheetName, button );
			
			var sheet:SpriteSheet = new SpriteSheet( sheetName + ".png", sheetName + ".json" );
			page.animAssets.push( sheet );
			var anim:TapSpritesheet = new TapSpritesheet( button, actor, sheet, frameRate, sounds, soundRange, endFrames, sequences );
			anim.reverse = reverse;
			anim.reversing = reversing;
			anims.push( anim );
			return anim;
		}
							
		public function Init():void
		{
			for each ( var anim:TapSpritesheet in anims )
			{
				page.controls.push( anim );
			}
		}
		
		public function Destroy():void
		{
			anims.length = 0;
			page = null;
		}
	}
}
﻿package com.cupcake 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import com.cupcake.DragTracker;
	import com.cupcake.IControllable;
	import com.cupcake.Utils;
	import com.gskinner.motion.*;
	import com.gskinner.motion.easing.Elastic;
	
	public dynamic class DragObject extends Sprite implements IControllable
	{
		public static const DRAG_END:String					= "DragObject.DragEndEvent";
		public static const DRAG_START:String				= "DragObject.DragStartEvent";
		public static const SNAP_END:String					= "DragObject.SnapEndEvent";
		public static const SNAP_START:String				= "DragObject.SnapStartEvent";
		public static const TAP:String						= "DragObject.TapEvent";
		
		public static const DEFAULT_DRAG_THRESHOLD:Number	= 5;
		public static const DEFAULT_THRESHOLD_DIST:Number	= 100;
		
		public static const SNAP_CLOSEST:uint				= 0;
		public static const SNAP_THRESHOLD:uint				= 1;
		public static const SNAP_TARGET:uint				= 2;
		
		public var dragThreshold:Number						= DEFAULT_DRAG_THRESHOLD;
		public var endPoint:Point							= null;
		public var noStart:Boolean							= false;
		public var onDrag:Function							= null;
		public var onDragEnd:Function						= null;
		public var onDragStart:Function						= null;
		public var onSnapEnd:Function						= null;
		public var onSnapStart:Function						= null;
		public var onTap:Function							= null;
		public var snapPoint:Point							= null;
		public var snapMode:uint							= SNAP_TARGET;
		public var snapOnRelease:Boolean					= true;
		public var startPoint:Point							= null;
		public var thresholdDistance:Number					= DEFAULT_THRESHOLD_DIST;
		
		public var target:DisplayObject						= null;
		public var tracker:DragTracker						= null;
		public var tween:GTween 							= null;
		
		private var _initialized:Boolean					= false;
		private var _paused:Boolean							= false;
		private var _running:Boolean						= false;
		private var _snapping:Boolean						= false;

		// Constructor and standard functions
		public function DragObject( autoStart:Boolean = false ) 
			{ if ( autoStart ) { Start(); } }
		
		// Public functions
		public function captureStart():void
		{
			startPoint.x = x;
			startPoint.y = y;
		}
		
		public function setBody( obj:DisplayObject ):void
		{
			x = obj.x;
			y = obj.y;
			startPoint = new Point( x, y );
			obj.parent.addChild( this );
			obj.x = 0;
			obj.y = 0;
			addChild( obj );
		}
		
		public function snapTo( pt:Point ):void
		{
			snapPoint = pt;
			startSnap();
		}
		
		// Standard functions
		public function get isInitialized( ):Boolean { return _initialized; }
		public function get isPaused( ):Boolean { return _paused; }
		public function get isRunning( ):Boolean { return _running; }
		
		public function Destroy( ):void
		{
			if ( isRunning ) Stop();
			target = null;
			if ( tracker != null ) tracker.Destroy();
			tracker = null;
			Utils.killTween( tween );
			tween = null;
		}
		
		public function Init( ):void
		{
			if ( !isInitialized )
			{
				startPoint = new Point( x, y );
				snapPoint = new Point( x, y );
				
				tween = new GTween( this, .5 );
				tween.onComplete = endTween;
				tween.ease = Elastic.easeOut;
				tween.paused = true;
				
				tracker = new DragTracker( this );
				tracker.onDrag = objectDrag;
				tracker.onDragEnd = endObjectDrag;
				tracker.onDragStart = startObjectDrag;
				tracker.onTap = handleTap;
				tracker.dragThreshold = dragThreshold;
			}
			_initialized = true;
		}
		
		public function Pause():void 
		{
			if ( isRunning && !isPaused )
			{
				_paused = true;
				if ( _snapping && tween != null ) tween.paused = true;
				tracker.Pause();
			}
		}
		
		public function Reset():void
		{
			if ( !isInitialized ) Init();
			if ( isRunning ) Stop();
			
			x = startPoint.x;
			y = startPoint.y;
			_snapping = false;
			_running = false;
			_paused = false;
			tracker.Reset();
		}
		
		public function Resume():void
		{
			if ( isRunning && isPaused )
			{
				_paused = false;
				if ( _snapping && tween != null ) tween.paused = false;
				tracker.Resume();
			}
		}
		
		public function Start():void 
		{ 
			if ( !isInitialized ) Init();
			if ( isRunning ) Reset();
			_running = true;
			_paused = false;
			tracker.Start();
		}
		
		public function Stop():void 
		{ 
			if ( isRunning )
			{
				_running = false;
				if ( tween != null ) tween.paused = true;
				tracker.Stop();
			}
		}

		// Private and protected functions	
		protected function endObjectDrag( e:DragTracker ):void
		{
			if ( isRunning && !isPaused )
			{
				// check our distance and decide if/where we're snapping
				if ( snapOnRelease )
				{
					var curPoint:Point = new Point( x, y );
					var endDist:Number = Utils.getDistance( curPoint, endPoint );
					var startDist:Number = Utils.getDistance( curPoint, startPoint );
					
					snapPoint = ( ( snapMode == SNAP_CLOSEST && endDist < startDist ) ||
								( snapMode == SNAP_THRESHOLD && endDist < thresholdDistance ) ||
								( snapMode == SNAP_TARGET && target != null && this.hitTestObject( target ) ) ) 
								? endPoint : startPoint;
								
					startSnap();
				}
				
				if ( onDragEnd != null ) onDragEnd( this );
				if ( hasEventListener( DragObject.DRAG_END ) ) dispatchEvent( new Event( DragObject.DRAG_END ) );
			}
		}
		
		protected function endSnap():void
		{
			_snapping = false;
			if ( onSnapEnd != null ) onSnapEnd( this );
			if ( hasEventListener( DragObject.SNAP_END ) ) dispatchEvent( new Event( DragObject.SNAP_END ) );
		}
		
		private function endTween( t:GTween ):void { endSnap(); }
		
		private function handleTap( e:DragTracker ):void
		{
			if ( onTap != null ) onTap( this );
			if ( hasEventListener( DragObject.TAP ) ) dispatchEvent( new Event( DragObject.TAP ) );
		}
		
		protected function objectDrag( e:DragTracker ):void
			{ if ( onDrag != null ) onDrag( this ); }
		
		protected function startObjectDrag( e:DragTracker ):void
		{
			if ( onDragStart != null ) onDragStart( this );
			if ( hasEventListener( DragObject.DRAG_START ) ) dispatchEvent( new Event( DragObject.DRAG_START ) );
		}
		
		protected function startSnap():void
		{
			if ( noStart && snapPoint == startPoint ) return;
			
			_snapping = true;
			tween.setValue( 'x', snapPoint.x );
			tween.setValue( 'y', snapPoint.y );
			
			if ( onSnapStart != null ) onSnapStart( this );
			if ( hasEventListener( DragObject.SNAP_START ) ) dispatchEvent( new Event( DragObject.SNAP_START ) );
		}
		
	}
}

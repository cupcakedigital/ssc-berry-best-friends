﻿package com.cupcake
{
    import flash.utils.Timer;
    import flash.utils.getTimer;
     
    public class TimerEx
	{     
		public var _delay:Number;
		public var _lastTime:Number 	= 0;		// added by Dave - add the = 0  prevent a bug in the resume where _thisTime would be set to nan if _lastTime was nan
		public var _repeat:Number;
		public var _thisTime:Number 	= 0;
		public var timer:Timer;
		
		private var _paused:Boolean		= false;
		private var _running:Boolean	= false;
     
		public function TimerEx( delay:Number, repeat:uint = 0 ):void 
		{
			_delay = delay;
			_repeat = repeat;
			 
			timer = new Timer(delay,repeat);
		}
		
		public function get isInitialized():Boolean { return true; }
		public function get isPaused():Boolean { return _paused; }
		public function get isRunning():Boolean { return _running; }
     
		public function start():void 
		{
			_lastTime = getTimer();
			timer.start();
			_running = true;
			_paused = false;
		}
     
		public function pause():void 
		{
			if ( isRunning && !isPaused )
			{
				timer.stop();
				if(_lastTime == 0) _lastTime = getTimer();		// added by dave to prevent a bug in the resume where _thisTime would be set to nan if _lastTime was nan
				_thisTime = getTimer() - _lastTime;
				_paused = true;
			}
		}
     
		public function repeat(new_delay:Number):void 
		{
			_lastTime = getTimer();
			timer.delay = new_delay;
		}
		
		public function reset():void 
			{ if (isRunning) stop(); timer.reset(); }
		
		public function stop():void	{ if ( isRunning ) { timer.stop(); _running = false; } }
     
		public function resume():void 
		{
			if ( isRunning && isPaused )
			{
				if ( _thisTime > timer.delay ) { _thisTime = timer.delay; }
				timer.delay -= _thisTime;
				_lastTime = getTimer();
				timer.start();
				 
				_thisTime = 0;
				_paused = false;
			}
		}
	}
}

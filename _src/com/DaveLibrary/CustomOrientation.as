﻿/**
 * CustomOrientation.as
 * 
 * Instructions:
 * In iPhone OS Settings, turn 'Auto orientation' ON and set your 'Aspect ratio'
 * to the opposite of the one you want to use. Also, use only
 * 'Default-Portrait.png'; if you want a landscape app, rotate your landscape
 * image 90 degrees clockwise and save as 'Default-Portrait.png'.
 * 
 * Usage:
 * When you call the constructor, first pass in your stage, then either 'true'
 * or 'false' for each orientation in order:
 * 1 default, 2 upsideDown, 3 rotatedLeft, and 4 rotatedRight
 * You can listen for the 'CustomOrientation.INITIAL_ORIENTATION' event to
 * determine when the stage is set so that you can reveal the stage or remove
 * a black overlay.
 * 
 * Example (landscape only):
 * new CustomOrientation(stage, false, false, true, true);
 * 
 * Example (portrait only):
 * new CustomOrientation(stage, true, true, false, false);
 * 
 * by cr0ybot
 * www.cr0ybot.com
 */

package com.DaveLibrary  {
     
     import flash.events.Event;
     import flash.events.EventDispatcher;
     import flash.events.StageOrientationEvent;
     import flash.display.Stage;
     import flash.display.StageOrientation;
     import flash.display.StageAlign;
     import flash.display.StageScaleMode;
     import flash.utils.Dictionary;
     
     public class CustomOrientation extends EventDispatcher {
          
          public static const INITIAL_ORIENTATION:String = "initialOrientation";
          
          private var theStage:Stage;
          private var allowedOrients:Dictionary = new Dictionary();
          
          public function CustomOrientation(theStage:Stage, defaultPortrait:Boolean = true, upsideDown:Boolean = true, rotatedLeft:Boolean = true, rotatedRight:Boolean = true) {
               App.Log("AutoOrient class initialized; starting orientation: " + theStage.orientation);
               
               // store reference for the stage
               this.theStage = theStage;
               
               // dictionary object holds boolean for each orientation
               allowedOrients[StageOrientation.DEFAULT] = defaultPortrait;
               allowedOrients[StageOrientation.UPSIDE_DOWN] = upsideDown;
               allowedOrients[StageOrientation.ROTATED_LEFT] = rotatedLeft;
               allowedOrients[StageOrientation.ROTATED_RIGHT] = rotatedRight;
               
               //theStage.scaleMode = StageScaleMode.NO_SCALE;
               //theStage.align = StageAlign.TOP_LEFT;
               
               // listen for orientation changes and prevent unwanted ones
               theStage.addEventListener(StageOrientationEvent.ORIENTATION_CHANGING, stageOrientationHandler, false, 0, true);
               
               // listen for the end of the first rotation, if there is one
               theStage.addEventListener(StageOrientationEvent.ORIENTATION_CHANGE, initialOrient, false, 0, true);
               
               // check the orientation at startup and respond if necessary
               checkCurrentOrientation();
          }
          
          public function checkCurrentOrientation():void {
               App.Log("checking initial orientation...");
               // if current orientation is not allowed, force rotate
               if (!allowedOrients[theStage.orientation]) {
                    App.Log(theStage.orientation + " not allowed.");
                    switch(theStage.orientation) {
                         case StageOrientation.DEFAULT :
                         case StageOrientation.UPSIDE_DOWN :
                              theStage.setOrientation(StageOrientation.ROTATED_RIGHT); // flash default landscape orientation
                              App.Log("stage rotating to: " + StageOrientation.ROTATED_RIGHT);
                              break;
                         case StageOrientation.ROTATED_LEFT :
                         case StageOrientation.ROTATED_RIGHT :
                              theStage.setOrientation(StageOrientation.DEFAULT);
                              App.Log("stage rotating to: " + StageOrientation.DEFAULT);
                              break;
                    }
               } else { // if the orientation IS allowed, dispatch the event
                    App.Log(theStage.orientation + " allowed.");
                    dispatchOrientation();
               }
          }
          
          private function initialOrient(event:StageOrientationEvent):void {
               dispatchOrientation();
          }
          
          private function dispatchOrientation():void {
               //App.Log("dispatching INITIAL_ORIENTATION event");
               
               // dispatch custom event; listen for CustomOrientation.INITIAL_ORIENTATION
               this.dispatchEvent(new Event(INITIAL_ORIENTATION));
               
               // stop listening for the initial rotation
               theStage.removeEventListener(StageOrientationEvent.ORIENTATION_CHANGE, initialOrient);
          }
          
          private function stageOrientationHandler(event:StageOrientationEvent):void {
               //App.Log("stage attempting to rotate to:", event.afterOrientation,"...");
               // if orientation is not allowed, prevent orientation change
               if (!allowedOrients[event.afterOrientation]) {
                    event.preventDefault();
                    App.Log("forcing stage to remain at: " + theStage.orientation);
               } else {
                    App.Log("stage allowed to rotate to: " + event.afterOrientation);
               }
          }
     }
}
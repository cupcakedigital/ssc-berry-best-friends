﻿package com.DaveLibrary {
	import flash.utils.Dictionary;
	import flash.net.*;
	import flash.events.*;
	

	// the main path finding class.
	public class clsXMLtoDict {

		
		public function clsXMLtoDict() {
			
		}

		public function Convert(xml:XML):Dictionary {
			return Dictionary(AddToDictionary(xml));
		}

		//-----------------------------------------------------------------------------------------------------------------------------

		// loop through the current xml try to break it down and return the dictionary/object value.
		private function AddToDictionary(xml:XML):Object {
			var i:Number = 0;
			var key:String = "";


			var xmlChildCount:Number = xml.elements().length();
			if (xmlChildCount > 0) {
				var dic:Dictionary = new Dictionary();
				i = 0;
				var c:int = 0;
				var append_count:int = 0;
				while (c < xmlChildCount) {				
					key = xml.child(i).localName();
					
					if(key.lastIndexOf("_append") == key.length - "_append".length) {
						append_count = append_count + 1;
						key = key.replace("_append", append_count.toString());
					}
					
					if (key != null) {
						var newxml:XML = new XML(xml.child(i).toXMLString());
						dic[key] = AddToDictionary(newxml);
						newxml = null;
						c++;
					}
					i++;
				}
				return dic;
			} else {
				// we are at the end of the key, so add this child and it's value to the dictionary.
				var RetVal:Object = null;
				key = xml.localName();
				var raw:String = xml.toString();

				var ConvertToNum:Boolean = false;
				var ConvertToUINT:Boolean = false;

				if (raw.charAt(0) == "*") {
					ConvertToNum = true;
					raw = raw.substr(1);
				}
				if (raw.charAt(0) == "#") {
					ConvertToUINT = true;
					raw = raw.substr(1);
				}
				// look to see if it starts with an *, if it does, then convert it to a number.
				if (raw.charAt(0) == "[" && raw.charAt(raw.length - 1) == "]") {
					raw = raw.substr(1);
					raw = raw.substr(0,raw.length - 1);
					var a:Array = raw.split(",");
					if (ConvertToNum) {
						for (i = 0; i < a.length; i++) {
							a[i] = Number(f_trim(a[i]));
						}
					}
					if (ConvertToUINT) {
						for (i = 0; i < a.length; i++) {
							a[i] = uint("0x" + f_trim(a[i]));
						}
					}
					RetVal = a;
				} else {
					if (ConvertToNum) {
						RetVal = Number(f_trim(raw));
					} else if (ConvertToUINT) {
						RetVal = uint("0x" + f_trim(raw));
					} else {
						if(key == "Divider") {
							RetVal=RetVal;
						}
						RetVal = raw;						
						RetVal = RetVal.split("\\n").join("\n");
						RetVal = RetVal.split("\\s").join(" ");
						
					}

				}
				raw = null;
				return RetVal;
			}
			return null;
		}

		//-----------------------------------------------------------------------------------------------------------------------------


		public static function f_trim($s_base:String):String {
			///* remove whitespace
			return ($s_base.replace(/^\s+|\s+$/g, ""));
		}
	}
}
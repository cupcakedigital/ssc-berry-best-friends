﻿package com.DaveLibrary  {
	
	import com.DaveLibrary.clsSound;
	import com.DaveLibrary.clsSoundTransformEX;
	import flash.display.MovieClip;
	import flash.media.Sound;
	import MM2_Mobile;
	
	public class clsSoundPool {
		
		private var Root:MM2_Mobile;
		private var sfx:Sound;
		private var soundCompleteCallback:Function;
		private var looping:Boolean;
		private var Pool:Vector.<clsSound>;

		public function clsSoundPool(TheRoot:MM2_Mobile, sound:Sound, SoundCompleteCallback:Function = null, Looping:Boolean = false) {
			// constructor code
			Root = TheRoot;
			sfx = sound;
			soundCompleteCallback = SoundCompleteCallback;
			Pool = new Vector.<clsSound>;
			looping = Looping;
		}
		
		public function destroy():void
		{
			sfx = null;
			Root = null;
			soundCompleteCallback = null;
			while(Pool.length > 0) {
				Pool[0].destroy();
				Pool.shift();
			}
			Pool = null;
		}
		
		public function GetSound():clsSound
		{
			var s:clsSound;
			if(Pool.length == 0) {
				s = new clsSound(sfx, Root.soundTrans, looping, soundCompleteCallback, false);
			} else {
				s = Pool.shift();
			}
			return s;
		}
		
		public function RecycleSound(s:clsSound):void
		{
			Pool.push(s);
		}


	}
	
}

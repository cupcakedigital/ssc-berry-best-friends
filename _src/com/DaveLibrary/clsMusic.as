﻿package com.DaveLibrary
{
	import flash.events.*;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	
	import com.DaveLibrary.clsSoundTransformEX;
	import com.cupcake.IControllable;
	import com.cupcake.Utils;

	public class clsMusic implements IControllable
	{
		public var adjustVolume:Number					= 1;
		public var channel:SoundChannel					= null;		
		
		private var _initialized:Boolean				= false;
		private var _pausePosition:Number				= 0;
		private var _paused:Boolean						= false;
		private var _running:Boolean					= false;
		
		private var _musicTrans:clsSoundTransformEX;
		private var _sound:Sound;
		private var _sndTransform:SoundTransform;		

		// Constructor and standard functions
		public function clsMusic(TheSound:Sound, musicTrans:clsSoundTransformEX, AutoStart:Boolean, VolumeAdjust:Number = 1)
		{
			_sound = TheSound;
			_musicTrans = musicTrans;
			adjustVolume = VolumeAdjust;
			if (AutoStart) Start();
		}
		
		public function get isInitialized():Boolean { return _initialized; }
		public function get isPaused():Boolean { return _paused; }
		public function get isRunning():Boolean { return _running; }

		public function Destroy():void
		{
			if ( isRunning ) Stop();
			if (_musicTrans != null) _musicTrans.RemoveListener(updateVolume);
		}
		
		public function Init():void
		{
			if ( isInitialized ) return;
			_sndTransform = new SoundTransform(_musicTrans.sndTransform.volume,0);
			_musicTrans.AddListener(updateVolume);
			_initialized = true;
			Reset();
		}
		
		public function Pause():void
		{
			if ( isRunning && !isPaused )
			{
				if ( channel != null )
				{
					_pausePosition = channel.position;
					channel.stop();
				}
				_paused = true;
			}
		}
		
		public function Reset():void
		{
			if ( !isInitialized ) Init();
			if ( isRunning ) Stop();
			_pausePosition = 0;			
		}
		
		public function Resume():void
		{
			if ( isRunning && isPaused )
			{
				_paused = false;
				StartMusic( _pausePosition );
				_pausePosition = 0;
			}
		}

		public function Start():void
		{
			if ( !isInitialized ) Init();
			if ( isRunning ) Reset();
			StartMusic();			
			_running = true;
			_paused = false;
		}

		public function Stop():void
		{
			if ( isRunning )
			{
				if ( channel != null )
				{
					Utils.removeHandler( channel, Event.SOUND_COMPLETE, loopMusic );
					channel.stop();
				}
				_running = false;
				_musicTrans.RemoveListener(updateVolume);
			}
		}

		// Public functions
		public function StartMusic( pos:int = 0 ):void
		{
			if ( channel != null )
			{
				channel.stop();
				Utils.removeHandler( channel, Event.SOUND_COMPLETE, loopMusic );
			}
			
			_sndTransform.volume = _musicTrans.sndTransform.volume * adjustVolume;
			channel = _sound.play( pos, 1, _sndTransform )
			Utils.addHandler( channel, Event.SOUND_COMPLETE, loopMusic );
		}

		// Private and protected functions
		private function loopMusic( e:Event ):void { if ( isRunning && !isPaused ) StartMusic(); }
		
		private function updateVolume():void
		{
			if (channel != null)
			{
				_sndTransform.volume = _musicTrans.sndTransform.volume * adjustVolume;
				channel.soundTransform = _sndTransform;
			}
		}
	}
}
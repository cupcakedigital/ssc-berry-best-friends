﻿// This class is a way to create a dynamic text field and have the font color a gradient.

package com.DaveLibrary
{
	import flash.display.MovieClip;
	import flash.display.BitmapDataChannel;
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.StageQuality;
	import flash.display.Sprite;
	import flash.display.Graphics;
	import flash.display.SpreadMethod;
	import flash.display.GradientType;
	import flash.display.BitmapData;
	import flash.text.TextFormatAlign;
	import flash.text.TextLineMetrics;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	
	import com.DaveLibrary.clsGradient;
	import com.DaveLibrary.clsTextData;
	import com.DaveLibrary.clsSupport;
	
	
	// the main path finding class.
	public class clsGradientDynamicText extends Sprite 
	{
		private var Root:MovieClip;

		public var textData:clsTextData;

		public var wordWrap:Boolean;
		public var altBounds:clsSize;
	
		public var Image:Bitmap = null;
		private var OriginalText:String;

		public var redrawQuality:String;
		public var ForceRedrawOnSet:Boolean;
		
		public var forceWrappingByNewLine:Boolean;
		
		public var lines:Array;
		
		public var useSmoothing:Boolean;
		
		public var strokeStrength:Number;
				
		//-----------------------------------------------------------------------------------------------------------------------------
	
		public function clsGradientDynamicText(TheRoot:MovieClip, Text:String, TextData:clsTextData = null, WordWrap:Boolean = false, AltBounds:clsSize = null, RedrawQuality:String = "best", ForceWrappingByNewLine:Boolean = true, UseSmoothing:Boolean = false, StrokeStrength:Number = 10)
		{
			Root = TheRoot;			
			
			if(TextData == null) textData = new clsTextData();
			if(AltBounds == null) AltBounds = new clsSize(TextData.bounds.width, TextData.bounds.height);
			
			forceWrappingByNewLine = ForceWrappingByNewLine;
			textData = TextData;
			wordWrap = WordWrap;
			altBounds = AltBounds;
			ForceRedrawOnSet = false;
			useSmoothing = UseSmoothing;
			strokeStrength = StrokeStrength;
			
			OriginalText = Text;
			
			Image = new Bitmap();
			Image.x = 0;
			Image.y = 0;
			this.addChild(Image);
			
			lines = new Array();
			
			
			redrawQuality = RedrawQuality;
			RedrawText();
			
			this.mouseEnabled = false;
			this.mouseChildren = false;
			
	
		}	
	
		public function get text():String
		{
			return OriginalText;
		}
	
		public function set text(newValue:String):void
		{
			if(OriginalText == newValue && ForceRedrawOnSet == false) return;
			OriginalText = newValue;
			RedrawText();	
		}
		
		public function RedrawText():void
		{
			var t:String = OriginalText;
			var SizeArray:Array;
			var i:int;
			var matrix:Matrix = new Matrix();
			var m:Matrix = new Matrix();
			var g:Graphics;
			var bmData:BitmapData;
			
			SizeArray = clsSupport.CreateTextImages(OriginalText, textData.font, textData.fontSize, textData.alignment, altBounds.width, altBounds.height, wordWrap, false, forceWrappingByNewLine, textData.leading, textData.letterSpacing);
									
			var bmText:BitmapData = SizeArray[0];
			var metrics:TextLineMetrics;
			lines = SizeArray[1];
			OriginalText = SizeArray[2];

			bmData = new BitmapData(bmText.width, bmText.height, true, 0);
			
			var y:int = 2;
			var Fill:Sprite = new Sprite();
			g = Fill.graphics;
			g.clear();
			
			if(lines.length > 1) {
				i=i;
			}
			
			for(i = 0; i < lines.length; i++) {
				metrics = lines[i];
				Fill.width = bmText.width;
				Fill.height = metrics.height;		
				matrix.createGradientBox(bmText.width, metrics.height, textData.color.rotation * (Math.PI/180), textData.color.translation.x, textData.color.translation.y);
				g.beginGradientFill(textData.color.type, textData.color.colors, textData.color.alphas, textData.color.ratios, matrix, textData.color.spread);
				g.drawRect(0, 0, bmText.width, metrics.height + 4);
				g.endFill();
				
				m.setTo(1,0,0,1,0,y);
				bmData.draw(Fill, m);
				g.clear();
				y = y + metrics.height + metrics.leading - textData.leading ;
			}

			
			m.setTo(1,0,0,1,0,0);
			bmData.copyChannel(bmText, bmText.rect, new Point(0,0), BitmapDataChannel.ALPHA, BitmapDataChannel.ALPHA);
			
			
			Fill.graphics.clear();
			Fill = null;			
			g = null;
			matrix = null;
			
			if(textData.stroke > 0)
				bmData = clsSupport.CreateBitmapDataGlow(bmData, textData.stroke, textData.strokeColor, false, useSmoothing, strokeStrength);
			
			Image.bitmapData = bmData;
			Image.smoothing = useSmoothing;
			
			if ( textData.alignment == TextFormatAlign.LEFT )
			{
				Image.x = 0;
				Image.y = 0;
			}
			else
			{
				Image.x = 0 - (Image.bitmapData.width/2);
				Image.y = 0 - (Image.bitmapData.height/2);
			}
		}
		
		public function destroy():void
		{
			
			clsSupport.DisposeOfSprite(this);
			
			textData = null;
			wordWrap = false;
			altBounds = null;
			
			Image.bitmapData.dispose();
			Image = null;			
			
			lines.length = 0;
			lines = null;
			
			Root = null;
		}
		
	}
}
﻿package com.DaveLibrary 
{
	import flash.events.*;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import com.DaveLibrary.clsSoundTransformEX;
	import com.DaveLibrary.clsSoundPool;
	import com.cupcake.Utils;
	
	public class clsSound {

		private var _soundTrans:clsSoundTransformEX;
		private var sndTransform:SoundTransform;
		public var Channel:SoundChannel;
		private var _TheSound:Sound;
		private var _Loop:Boolean;
		public var Callback:Function;
		public var AdjustVolume:Number;
		
		public var running:Boolean;
		public var paused:Boolean;		
		private var PausePosition:Number;
		
		public function clsSound(TheSound:Sound, soundTrans:clsSoundTransformEX, Loop:Boolean, SoundCompleteCallback:Function = null, AutoStart:Boolean = true, VolumeAdjust:Number = 1) {
			// constructor code
			_Loop = Loop;
			_soundTrans = soundTrans;
			sndTransform = new SoundTransform(_soundTrans.sndTransform.volume, 0);		
			
			_TheSound = TheSound;
			Callback = SoundCompleteCallback;
			
			running = false;
			paused = false;
			
			AdjustVolume = VolumeAdjust;
			if(AutoStart) Start(); 
		}
		
		public function destroy():void 
		{
			if(_soundTrans != null) _soundTrans.RemoveListener(UpdateVolume);
			_soundTrans = null;
			if(Channel != null) { Utils.removeHandler( Channel, Event.SOUND_COMPLETE, SOUND_COMPLETE); }
			if(Channel != null) { Channel.stop(); }
			Channel = null;
			_TheSound = null;
			Callback = null;
		}
		
		public function Start(VolumeAdjust:Number = 1):void 
		{
			var adjust:Number = AdjustVolume * VolumeAdjust;
			//AdjustVolume = VolumeAdjust;
			sndTransform.volume = _soundTrans.sndTransform.volume * adjust;
			
			if(_Loop)
			{
				Channel = _TheSound.play(0,int.MAX_VALUE,sndTransform);
			} else {
				Channel = _TheSound.play(0,0,sndTransform);
			}
			
			Utils.addHandler( Channel, Event.SOUND_COMPLETE, SOUND_COMPLETE );
			_soundTrans.AddListener(UpdateVolume);
			running = true;
			paused = false;
		}
		
		public function Stop(UseCallback:Boolean = true):void
		{
			_soundTrans.RemoveListener(UpdateVolume);
			if(Channel != null) 
			{
				Utils.removeHandler( Channel, Event.SOUND_COMPLETE, SOUND_COMPLETE );
				Channel.stop();
				if(Callback != null && UseCallback) {
					Callback(this);
				}
			}
			running = false;
			paused = false;
			Channel = null;
		}

		private function SOUND_COMPLETE(e:Event):void
		{
			//try {
				var c = e.currentTarget;
				if(c != null) { Utils.removeHandler( c, Event.SOUND_COMPLETE, SOUND_COMPLETE ); }
				if(Callback != null) {
					Callback(this);
				}
				Channel = null;
				
				running = false;
				paused = false;
				
				if(_Loop) Start();

			//} catch (e:Error) {
				//App.Log("SOUND_COMPLETE ERROR: " + e);
			//}
		}
		
		public function UpdateVolume():void
		{
			if(Channel != null) 
			{
				sndTransform.volume = _soundTrans.sndTransform.volume * AdjustVolume;
				Channel.soundTransform = sndTransform;
			}
		}
		
		public function Pause():void
		{
			if(Channel != null) 
			{
				PausePosition = Channel.position;
				Channel.stop();
				paused = true;
			}
			
		}
		
		public function Resume():void
		{
			paused = false;
			if(Channel != null) 
			{
				if(_Loop)
				{
					Channel = _TheSound.play(PausePosition,int.MAX_VALUE,sndTransform);
				} else {
					Channel = _TheSound.play(PausePosition,0,sndTransform);
				}
				PausePosition = 0;
			} else {
				Start();
			}		
			
		}
		
	}
	
}

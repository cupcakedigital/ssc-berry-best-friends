﻿// Perfect Life player class.

package com.DaveLibrary
{
	import flash.display.Stage;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	import flash.utils.Proxy;
	import flash.utils.flash_proxy;

	/**
	 * The KeyObject class recreates functionality of
	 * Key.isDown of ActionScript 1 and 2
	 *
	 * Usage:
	 * var key:clsKeyObject = new clsKeyObject(stage);
	 * if (key.isDown(key.LEFT)) { ... }
	 */
	dynamic public class clsKeyObject extends Proxy {
	
		private static var stage:Stage;
		private static var keysDown:Object;
	
		public var KEY_BACKSPACE:int = 8;
		public var KEY_TAB:int = 9;
		public var KEY_ENTER:int = 13;
		public var KEY_SHIFT:int = 16;
		public var KEY_CONTROL:int = 17;
		public var KEY_PAUSE:int = 19;
		public var KEY_CAPSLOCK:int = 20;
		public var KEY_ESC:int = 27;
		public var KEY_SPACEBAR:int = 32;
		public var KEY_PAGEUP:int = 33;
		public var KEY_PAGEDOWN:int = 34;
		public var KEY_END:int = 35;
		public var KEY_HOME:int = 36;
		public var KEY_LEFT:int = 37;
		public var KEY_UP:int = 38;
		public var KEY_RIGHT:int = 39;
		public var KEY_DOWN:int = 40;
		public var KEY_INSERT:int = 45;
		public var KEY_DELETE:int = 46;
		public var KEY_0:int = 48;
		public var KEY_1:int = 49;
		public var KEY_2:int = 50;
		public var KEY_3:int = 51;
		public var KEY_4:int = 52;
		public var KEY_5:int = 53;
		public var KEY_6:int = 54;
		public var KEY_7:int = 55;
		public var KEY_8:int = 56;
		public var KEY_9:int = 57;
		public var KEY_A:int = 65;
		public var KEY_B:int = 66;
		public var KEY_C:int = 67;
		public var KEY_D:int = 68;
		public var KEY_E:int = 69;
		public var KEY_F:int = 70;
		public var KEY_G:int = 71;
		public var KEY_H:int = 72;
		public var KEY_I:int = 73;
		public var KEY_J:int = 74;
		public var KEY_K:int = 75;
		public var KEY_L:int = 76;
		public var KEY_M:int = 77;
		public var KEY_N:int = 78;
		public var KEY_O:int = 79;
		public var KEY_P:int = 80;
		public var KEY_Q:int = 81;
		public var KEY_R:int = 82;
		public var KEY_S:int = 83;
		public var KEY_T:int = 84;
		public var KEY_U:int = 85;
		public var KEY_V:int = 86;
		public var KEY_W:int = 87;
		public var KEY_X:int = 88;
		public var KEY_Y:int = 89;
		public var KEY_Z:int = 90;
		public var KEY_NUMPAD_0:int = 96;
		public var KEY_NUMPAD_1:int = 97;
		public var KEY_NUMPAD_2:int = 98;
		public var KEY_NUMPAD_3:int = 99;
		public var KEY_NUMPAD_4:int = 100;
		public var KEY_NUMPAD_5:int = 101;
		public var KEY_NUMPAD_6:int = 102;
		public var KEY_NUMPAD_7:int = 103;
		public var KEY_NUMPAD_8:int = 104;
		public var KEY_NUMPAD_9:int = 105;
		public var KEY_NUMPAD_MULTIPLY:int = 106;
		public var KEY_PLUS:int = 107;
		public var KEY_SUBTRACT:int = 109;
		public var KEY_DOT:int = 110;
		public var KEY_DIVISION:int = 111;
		public var KEY_F1:int = 112;
		public var KEY_F2:int = 113;
		public var KEY_F3:int = 114;
		public var KEY_F4:int = 115;
		public var KEY_F5:int = 116;
		public var KEY_F6:int = 117;
		public var KEY_F7:int = 118;
		public var KEY_F8:int = 119;
		public var KEY_F9:int = 120;
		public var KEY_F11:int = 122;
		public var KEY_F12:int = 123;
		public var KEY_F13:int = 124;
		public var KEY_F14:int = 125;
		public var KEY_F15:int = 126;
		public var KEY_NUMLOCK:int = 144;
		public var KEY_SCROLLLOCK:int = 145;
		public var KEY_SEMICOLON:int = 186;
		public var KEY_EQUAL:int = 187;
		public var KEY_COMMA:int = 188;
		public var KEY_MINUS:int = 189;
		public var KEY_PERIOD:int = 190;
		public var KEY_SLASH:int = 191;
		public var KEY_BACKQUOTE:int = 192;
		public var KEY_LEFTBRACKET:int = 219;
		public var KEY_BACKSLASH:int = 220;
		public var KEY_RIGHTBRACKET:int = 221;
		public var KEY_QUOTE:int = 222;
	
		public function clsKeyObject(stage:Stage)
		{
			construct(stage);
		}
	
		public function construct(stage:Stage):void
		{
			clsKeyObject.stage = stage;
			keysDown = new Object();
			stage.addEventListener(KeyboardEvent.KEY_DOWN, keyPressed, false, 0, true);
			stage.addEventListener(KeyboardEvent.KEY_UP, keyReleased, false, 0, true);
		}
	
		flash_proxy override function getProperty(name:*):*
		{
			return (name in Keyboard) ? Keyboard[name] : -1;
		}
	
		public function isDown(keyCode:uint):Boolean
		{
			return Boolean(keyCode in keysDown);
		}
	
		public function deconstruct():void
		{
			stage.removeEventListener(KeyboardEvent.KEY_DOWN, keyPressed);
			stage.removeEventListener(KeyboardEvent.KEY_UP, keyReleased);
			keysDown = new Object();
			clsKeyObject.stage = null;
		}
	
		private function keyPressed(evt:KeyboardEvent):void
		{
			keysDown[evt.keyCode] = true;
		}
	
		private function keyReleased(evt:KeyboardEvent):void
		{
			delete keysDown[evt.keyCode];
		}
	}

}
﻿package com.DaveLibrary {

	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	
	
	public class clsSoundTransformEX  {

		public var sndTransform:SoundTransform;
		private var Listeners:Array = new Array();
		
		public function clsSoundTransformEX(InitialVolume:Number = 1)
		{
			sndTransform = new SoundTransform(InitialVolume,0);
		}
		
		public function destroy():void
		{
			sndTransform = null;
			if(Listeners != null) Listeners.length = 0;
			Listeners = null;
		}
		
		public function AddListener(CallBack:Function):void
		{
			var index:int = Listeners.indexOf(CallBack);
			if(index < 0) {
				Listeners.push(CallBack);
			}
		}
		
		public function RemoveListener(CallBack:Function):void
		{
			var index:int = Listeners.indexOf(CallBack);
			if(index >= 0) {
				Listeners.splice(index,1);
			}
		}

		public function get volume():Number
		{
			return sndTransform.volume;
		}

		public function set volume(newVolume:Number):void
		{
			sndTransform.volume = newVolume;
			for(var i:int = 0; i < Listeners.length; i++)
			{
				Listeners[i]();
			}
			
		}

	}
	
}

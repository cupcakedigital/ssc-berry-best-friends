﻿package com.DaveLibrary  {

	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	import flash.geom.Point;


	public class clsParticleBurstSettings {

		public var radius_Min:Number = 0;
		public var radius_Max:Number = 0;
		
		public var spawnRect:Rectangle = new Rectangle();
		
		public var steps:int = 30;
		
		public var UseBezier:Boolean = true;
		public var controlPoint1Offset:Point = new Point();
		public var controlPoint2Offset:Point = new Point();
		
		public var numberOfParticles:int = 10;	
		
		public var fadeOutAt:Number = 0.9;
		public var fadeOutSpeed:Number = 0.1;
		public var startAlpha:Number = 1;
		public var fadeInSpeed:Number = 100;
		public var maxAlpha:Number = 1;
		
		public var rotation_Min:Number = 0;
		public var rotation_Max:Number = 0;

		public var individualParticleSettings:Boolean = false;
		public var randomAngles:Boolean = false;

		public var particleDelay:int;
		
		public var particleImageScale_Start:Number;
		public var particleImageScale_End:Number;

		public function clsParticleBurstSettings() {
			// constructor code
		}

		public function destroy():void
		{
			spawnRect = null;
			controlPoint1Offset = null;
			controlPoint2Offset = null;
		}
		

	}
	
}

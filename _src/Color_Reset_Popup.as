﻿package  
{	
	import flash.utils.Dictionary;	
	import com.DaveLibrary.clsMsgBox;
	import com.DaveLibrary.clsSupport;
	import com.DaveLibrary.clsSpriteEX;
	
	public class Color_Reset_Popup 
	{
		public static var callback:Function;	
		private static var msgBox:clsMsgBox = null;
		
		public static function Show(Callback:Function):void
		{			
			callback = Callback;
			if ( callback == null ) { return; }			 
			
			var d:Dictionary = Dictionary(clsSupport.copyObject(clsMain.Main.gStrings["Messages"]["Msgbox_Reset_Color"]));
			
			msgBox = new clsMsgBox(clsMain.Main,								 /*TheRoot:clsMain*/
								   d,											 /*Text:Dictionary*/
								   clsMain.Main.gc.messages_captcha,			 /*MsgBoxInfo:clsMsgboxDefaults*/
								   "YesNo",										 /*Buttons:String*/
								   "Captcha",									 /*MessageBoxId:String*/
								   CaptchaComplete,								 /*CallBack:Function = null*/				
								   null,										 /*CallBackParameters:Dictionary = null*/
								   null,										 /*AddToParent:Sprite = null*/
								   clsMain.Main.gStrings["General"]["Download"], /*Button1Text:String = null*/
								   clsMain.Main.gStrings["General"]["Cancel"],	 /*Button2Text:String = null*/
								   true,										 /*ButtonsInContent:Boolean = false*/
								   true,										 /*DimBackground:Boolean = true*/
								   false); 
								   
			
			GlobalData.playSound( "Button_Generic" );					
			
		}		
		
		private static function CaptchaComplete(bb:clsSpriteEX, Value:String, rE:Dictionary):void
		{						
			switch(Value){
				case "Yes":{callback(true);}break;
				case "No": {callback(false);}break;
			}
			
		}
		
	}
}
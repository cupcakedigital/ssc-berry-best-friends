﻿package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	
	import asData.clsGlobalConstants;
	
	import com.DaveLibrary.clsMsgBox;
	import com.DaveLibrary.clsStandardButton;
	import com.DaveLibrary.clsStandardButtonSettings;
	import com.DaveLibrary.clsSupport;
	import com.DaveLibrary.clsGradientDynamicText;
	
	import com.cupcake.App;
	import com.cupcake.ScrollingMenu;
	import com.cupcake.Utils;
	
	import com.greensock.TweenLite;
	import com.greensock.easing.Sine;

	public class mcNavBar extends Sprite
	{
		public var Background:Sprite;
		public var mcHitArea_Open1:SimpleButton;
		public var mcHitArea_Open3:SimpleButton;
		
		public var btnArcade:clsStandardButton;
		public var btnHome:clsStandardButton;
		public var btnOptions:clsStandardButton;
		public var btnPages:clsStandardButton;
		public var btnVideos:clsStandardButton;
		public var btnColorBook:clsStandardButton;
		
		public var txtMenu:clsGradientDynamicText;
		
		public var ArcadeMenu:clsArcadeMenu;
		public var InteractivePages:clsPageMenu;
		public var PagesMenu:WBZ_ScrollingMenu;
		public var StaticPages:clsStaticMenu;
		public var VideoMenu:clsVideoMenu;
		
		private var ArcadeMouseDown:Boolean		= false;
		private var HomeMouseDown:Boolean		= false;
		private var OpenMouseDown:Boolean		= false;
		private var OptionsMouseDown:Boolean	= false;
		private var PagesMouseDown:Boolean		= false;
		private var VideosMouseDown:Boolean		= false;
		private var ColoringMouseDown:Boolean	= false;
		
		private var CenterY:int					= 0;
		private var Closed:Boolean				= true;
		private var MaxY:Number					= 0;
		private var MidY:Number					= 0;
		private var MinY:Number					= 0;
		private var NewY:Number					= 0;
		private var Opened:Boolean				= false;
		private var OpenInitial:int;
		private var OpenInitialMouseY:int		= 0;
		private var SetCloseNav:Boolean			= true;
		
		public var ArcadePages:Vector.<Class>	= new Vector.<Class>;
		public var The_Color_Page:Vector.<Class>= new Vector.<Class>;
		
		private var cMain:clsMain;
		private var gc:clsGlobalConstants;
		private var pMouse:Point				= new Point;
		
		private var Full_Close:Boolean;
		
		// called by clsMain
		public final function Initialize(Main:clsMain):void
		{
			cMain = Main;
			gc = cMain.gc;
			
			The_Color_Page.push(Coloring_Book);
				
			this.mouseEnabled = false;
			this.mouseChildren = false;
			Background.mouseEnabled = false;
			
			var BackgroundBmp:Bitmap = new Bitmap( GlobalData.getBitmapData( "Navbar_Background" ) );
			Background.addChild( BackgroundBmp );
			
			txtMenu = new clsGradientDynamicText( cMain, cMain.gStrings["Navbar"]["Menu"], gc.navbar_MenuText );
			txtMenu.x = GlobalData.localXML.Navbar.MenuText.x - 15;
			txtMenu.y = GlobalData.localXML.Navbar.MenuText.y;
			Background.addChild( txtMenu );
						
			btnPages = new clsStandardButton(cMain, cMain.gStrings["Navbar"]["Pages"], gc.navbar_PagesButton, GlobalData.getBitmapData( "Navbar_Pages" ), clsSupport.CreateHighlightedImage( GlobalData.getBitmapData( "Navbar_Pages" ), 0xFFFFFF, gc.button_Highlight), mcPages_MouseUp, mcPages_MouseDown, null, null, true, false, false);
			btnPages.x = gc.navbar_Pages.x;
			btnPages.y = gc.navbar_Pages.y;
			this.addChild(btnPages);
			/*
			btnVideos = new clsStandardButton(cMain, cMain.gStrings["Navbar"]["Videos"], gc.navbar_VideosButton, GlobalData.getBitmapData( "Navbar_Videos" ), clsSupport.CreateHighlightedImage( GlobalData.getBitmapData( "Navbar_Videos" ), 0xFFFFFF, gc.button_Highlight), mcVideos_MouseUp, mcVideos_MouseDown, null, null, true, false, false);
			btnVideos.x = gc.navbar_Videos.x;
			btnVideos.y = gc.navbar_Videos.y;
			this.addChild(btnVideos);
			*/
			btnHome = new clsStandardButton(cMain, cMain.gStrings["Navbar"]["Home"], gc.navbar_HomeButton, GlobalData.getBitmapData( "Navbar_Home" ), clsSupport.CreateHighlightedImage( GlobalData.getBitmapData( "Navbar_Home" ), 0xFFFFFF, gc.button_Highlight), mcHome_MouseUp, mcHome_MouseDown, null, null, true, false, false);
			btnHome.x = gc.navbar_Home.x;
			btnHome.y = gc.navbar_Home.y;
			this.addChild(btnHome);
						
			btnOptions = new clsStandardButton(cMain, cMain.gStrings["Navbar"]["Options"], gc.navbar_OptionsButton, GlobalData.getBitmapData( "Navbar_Options" ), clsSupport.CreateHighlightedImage( GlobalData.getBitmapData( "Navbar_Options" ), 0xFFFFFF, gc.button_Highlight), mcOptions_MouseUp, mcOptions_MouseDown, null, null, true, false, false);
			btnOptions.x = gc.navbar_Options.x;
			btnOptions.y = gc.navbar_Options.y;
			this.addChild(btnOptions);

			btnArcade = new clsStandardButton(cMain, cMain.gStrings["Navbar"]["Arcade"], gc.navbar_ArcadeButton, GlobalData.getBitmapData( "Navbar_Games" ), clsSupport.CreateHighlightedImage( GlobalData.getBitmapData( "Navbar_Games" ), 0xFFFFFF, gc.button_Highlight), mcArcade_MouseUp, mcArcade_MouseDown, null, null, true, false, false);
			btnArcade.x = gc.navbar_Arcade.x;
			btnArcade.y = gc.navbar_Arcade.y;
			this.addChild(btnArcade);
			
			btnColorBook = new clsStandardButton(cMain, cMain.gStrings["Navbar"]["Color"], gc.navbar_ArcadeButton, GlobalData.getBitmapData( "Navbar_Paint" ), clsSupport.CreateHighlightedImage( GlobalData.getBitmapData( "Navbar_Paint" ), 0xFFFFFF, gc.button_Highlight), mcColor_MouseUp, mcColor_MouseDown, null, null, true, false, false);
			btnColorBook.x = gc.navbar_Coloring.x;
			btnColorBook.y = gc.navbar_Coloring.y;
			this.addChild(btnColorBook);

			InteractivePages = new clsPageMenu(PagesMenuButton, cMain);
			InteractivePages.Init();
			InteractivePages.visible = false;
			InteractivePages.x = Background.x + (Background.width/2) - (InteractivePages.width/2);
			InteractivePages.OpenY = Background.y + gc.navbar_SliderY_Opened;
			InteractivePages.CloseY = Background.y + gc.navbar_SliderY_Closed;
			InteractivePages.InitializeButtons();
			this.addChildAt(InteractivePages,0);
			
			InteractivePages.y = Background.y + gc.navbar_SliderY_Opened;
			var bnds:Rectangle = this.getBounds(this);
			InteractivePages.y = Background.y + gc.navbar_SliderY_Closed;
			
			StaticPages = new clsStaticMenu(PagesMenuButton, cMain);
			StaticPages.Init();
			StaticPages.visible = false;
			StaticPages.x = Background.x + (Background.width/2) - (StaticPages.width/2);
			StaticPages.y = Background.y + gc.navbar_SliderY_Closed;
			StaticPages.OpenY = Background.y + gc.navbar_SliderY_Opened;
			StaticPages.CloseY = Background.y + gc.navbar_SliderY_Closed;
			StaticPages.InitializeButtons();
			this.addChildAt(StaticPages,0);
			
			PagesMenu = clsOptions.interactive ? InteractivePages : StaticPages;			
			
			VideoMenu  = new clsVideoMenu(VideoMenuButton, cMain);
			VideoMenu.Init();
			this.addChildAt(VideoMenu,0);
			VideoMenu.visible = false;
			VideoMenu.x = Background.x + (Background.width/2) - (VideoMenu.width/2);
			VideoMenu.y = Background.y + gc.navbar_SliderY_Closed;
			VideoMenu.OpenY = Background.y + gc.navbar_SliderY_Opened;
			VideoMenu.CloseY = Background.y + gc.navbar_SliderY_Closed;
			VideoMenu.InitializeButtons();
			
			ArcadeMenu  = new clsArcadeMenu(ArcadeMenuButton, cMain);
			ArcadeMenu.Init();
			this.addChildAt(ArcadeMenu,0);
			ArcadeMenu.visible = false;
			ArcadeMenu.x = Background.x + (Background.width/2) - (ArcadeMenu.width/2);
			ArcadeMenu.y = Background.y + gc.navbar_SliderY_Closed;
			ArcadeMenu.OpenY = Background.y + gc.navbar_SliderY_Opened;
			ArcadeMenu.CloseY = Background.y + gc.navbar_SliderY_Closed;
			ArcadeMenu.InitializeButtons();
			
			GlobalData.TrashSetup();
			//, btnVideos
			var btns:Array = [btnHome, btnPages, btnColorBook, btnArcade, btnOptions];
			var startX:int = 140;
			var endX:int = Background.width;
			var cell:int = ( endX - startX ) / ( btns.length );
			for ( var i:int = 0 ; i < btns.length ; i++ )
				( btns[i] as clsStandardButton ).x = startX + ( cell * i ) + ( cell * .5 );
			
			CenterY = -bnds.y;
			this.scrollRect = new Rectangle(0,bnds.y,Background.width,bnds.height);
			MinY = cMain.stage.stageHeight - this.height  - 15;
			MaxY = cMain.stage.stageHeight - CenterY;
			MidY = MaxY - ((MaxY - MinY) / 2);
			this.y = MaxY;
			OpenInitial = MaxY;
		}
		public function Show_NavBar():void{
			Full_Close = false;
			OpenNav();
		}
		public function Hide_NavBar():void{
			CloseNavFULL();
		}
		public final function updateButtonText():void
		{
			txtMenu.text = cMain.gStrings["Navbar"]["Menu"];
			txtMenu.x = GlobalData.localXML.Navbar.MenuText.x;
			txtMenu.y = GlobalData.localXML.Navbar.MenuText.y;
			btnHome.text = cMain.gStrings["Navbar"]["Home"];
			btnPages.text = cMain.gStrings["Navbar"]["Pages"];
			//btnVideos.text = cMain.gStrings["Navbar"]["Videos"];
			btnArcade.text = cMain.gStrings["Navbar"]["Arcade"];
			btnOptions.text = cMain.gStrings["Navbar"]["Options"];
			btnColorBook.text = cMain.gStrings["Navbar"]["Color"];
			ArcadeMenu.updateButtonText();
			InteractivePages.updateButtonText();
			StaticPages.updateButtonText();
			VideoMenu.updateButtonText();
		}
			
		public final function Activate():void
		{
			this.mouseChildren = true;
			cMain.addEnterFrame(ENTER_FRAME);
			cMain.addStageMouseUp(Stage_MouseUp);
			
			Utils.addHandler( mcHitArea_Open1, MouseEvent.MOUSE_DOWN, mcHitArea_Open_MouseDown);
			Utils.addHandler( mcHitArea_Open1, MouseEvent.MOUSE_UP, mcHitArea_Open_MouseUp);
			Utils.addHandler( mcHitArea_Open3, MouseEvent.MOUSE_DOWN, mcHitArea_Open_MouseDown);
			Utils.addHandler( mcHitArea_Open3, MouseEvent.MOUSE_UP, mcHitArea_Open_MouseUp);
		}
		
		public final function SetPages():void
		{
			if ( clsOptions.interactive )
			{
				InteractivePages.visible = PagesMenu.visible;
				PagesMenu = InteractivePages;
				StaticPages.visible = false;
			}
			else
			{
				StaticPages.visible = PagesMenu.visible;
				PagesMenu = StaticPages;
				InteractivePages.visible = false;
			}
		}
		
		// Private and protected functions
		private final function ENTER_FRAME(e:Event):void
		{
			if(stage == null) return;
			if(cMain.Paused) return;
			
			if(OpenMouseDown) 
			{
				pMouse.x = stage.mouseX;
				pMouse.y = stage.mouseY;
				
				NewY = OpenInitial - (OpenInitialMouseY - stage.mouseY);
				if(NewY > MaxY) NewY = MaxY;
				if(NewY < MinY) NewY = MinY;			
				this.y = NewY;
				
				if ( NewY < MaxY && !btnPages.visible ) ShowButtons();
			} 
		}
		
		private final function Stage_MouseUp(e:MouseEvent):void
		{
			if(this.alpha < 1) return;
			if(cMain.Paused) return;
			if(PagesMenu.visible && (PagesMenu.mouseDown || PagesMenu.dragging || PagesMenu.sliding || PagesMenu.snapping)) { return; }
			if(VideoMenu.visible && (VideoMenu.mouseDown || VideoMenu.dragging || VideoMenu.sliding || VideoMenu.snapping)) { return; }
			if(ArcadeMenu.visible && (ArcadeMenu.mouseDown || ArcadeMenu.dragging || ArcadeMenu.sliding || ArcadeMenu.snapping)) { return; }
			
			btnHome.ShowStaticState();
			btnPages.ShowStaticState();
			btnArcade.ShowStaticState();
			//btnVideos.ShowStaticState();
			btnOptions.ShowStaticState();
			btnColorBook.ShowStaticState();
			
			if(OpenMouseDown) mcHitArea_Open_MouseUp(e);
			else if (PagesMouseDown == false && OptionsMouseDown == false  && ArcadeMouseDown == false  
					 && VideosMouseDown == false && ColoringMouseDown == false && SetCloseNav ) 
				CloseNav(true);

			SetCloseNav = true;
			
			HomeMouseDown = false;
			OpenMouseDown = false;
			PagesMouseDown = false;
			OptionsMouseDown = false;
			ArcadeMouseDown = false;
			VideosMouseDown = false;
			ColoringMouseDown = false;
		}
		
		private final function mcHitArea_Open_MouseDown(e:MouseEvent):void
		{
			// Turned off for ONDEMAND
			/*if ( ( ( cMain.PageTurnNext.visible && cMain.PageTurnNext.percentage > 0 )
				|| ( cMain.PageTurnPrev.visible && cMain.PageTurnPrev.percentage > 0 ) ) ) return;*/
			if ( cMain.PageTurnNext != null || cMain.PageTurnPrev != null ) { return; }
			// ONDEMAND/
			OpenInitial = this.y;
			pMouse.x = stage.mouseX;
			pMouse.y = stage.mouseY;
			OpenInitialMouseY = stage.mouseY;
			OpenMouseDown = true;
			CloseTabs();
		}
		
		private final function mcHitArea_Open_MouseUp(e:MouseEvent):void
		{
			if(OpenMouseDown == false) return;
			OpenMouseDown = false;
			SetCloseNav = false;
			
			if ( pMouse.y > stage.mouseY ) 
			{
				if ( OpenInitial <= MinY ) CloseNav();
					else OpenNav();
			} 
			else if ( pMouse.y < stage.mouseY ) 
			{
				if ( OpenInitial >= MaxY ) OpenNav();
					else CloseNav();
			}
			else 
			{
				if ( OpenInitialMouseY > stage.mouseY ) 
				{
					if ( OpenInitial <= MinY ) CloseNav();
						else OpenNav();
				} 
				else if ( OpenInitialMouseY < stage.mouseY ) 
				{
					if ( OpenInitial >= MaxY ) OpenNav();
						else CloseNav();
				} 
				else 
				{
					if ( OpenInitial > MidY ) OpenNav()
						else CloseNav();
				}
			}			
		}
		
		private final function CloseNav( silent:Boolean = false ):void
		{
			if(Full_Close){return;}
			CloseTabs();
			var time:Number = (( MaxY - this.y ) / ( MaxY - MinY )) * .75;
			if ( this.y == MinY && !silent ) { GlobalData.playSound( "HUD_Slide_Down" ); }
			TweenLite.killTweensOf( this );
			TweenLite.to( this, time, { y: MaxY, ease: Sine.easeInOut, onComplete: CloseComplete } );
			
		}
		private final function CloseNavFULL( silent:Boolean = false ):void
		{
			Full_Close = true;
			CloseTabs();
			var time:Number = (( MaxY - this.y ) / ( MaxY - MinY )) * .75;
			if ( this.y == MinY && !silent ) { GlobalData.playSound( "HUD_Slide_Down" ); }
			TweenLite.killTweensOf( this );
			TweenLite.to( this, time, { y:900, ease: Sine.easeInOut, onComplete: CloseComplete } );			
		}
		
		private final function CloseComplete():void { HideButtons(); }
		
		private final function OpenNav():void
		{
			var time:Number = (( this.y - MinY ) / ( MaxY - MinY )) * .75;
			if ( this.y == MaxY ) { GlobalData.playSound( "HUD_Slide_Up" ); }
			TweenLite.killTweensOf( this );
			TweenLite.to( this, time, { y: MinY, ease: Sine.easeInOut } );
			ShowButtons();
		}
		
		private final function mcPages_MouseDown(e:MouseEvent):void
		{
			PagesMouseDown = true;
			e.stopImmediatePropagation();
		}
	
		private final function mcPages_MouseUp(e:MouseEvent):void
		{
			if(OpenMouseDown) 
			{
				Stage_MouseUp(e);
				return;
			}
			
			if(PagesMouseDown) 
			{
				btnPages.ShowStaticState();
				e.stopImmediatePropagation();
				VideoMenu.Close();
				ArcadeMenu.Close();
				
				if ( PagesMenu.Opened )
				{
					PlayButtonSound();
					PagesMenu.Close();
				}
				else
				{
					if ( PagesMenu.Closed ) 
					{
						PagesMenu.setCurrent(cMain.CurrentPage + 1);
						GlobalData.playSound( "HUD_Pages" );
					}
					
					PagesMenu.Open();
				}
				PagesMouseDown = false;
			}
		}
		
		private final function mcOptions_MouseDown(e:MouseEvent):void
		{
			OptionsMouseDown = true;
			e.stopImmediatePropagation();
		}
	
		private final function mcOptions_MouseUp(e:MouseEvent):void
		{
			if(OpenMouseDown) 
			{
				Stage_MouseUp(e);
				return;
			}
			
			if ( OptionsMouseDown ) 
			{
				btnOptions.ShowStaticState();
				e.stopImmediatePropagation();
				CloseTabs();
				//Captcha.Show( OptionsCaptchaReturn ); 
				cMain.showOptions();
				OptionsMouseDown = false;
			}
		}
		
		private final function OptionsCaptchaReturn( success:Boolean, cancel:Boolean ):void
			{ if ( success ) { cMain.showOptions(); } }
		
		private final function mcArcade_MouseDown(e:MouseEvent):void
		{
			ArcadeMouseDown = true;
			e.stopImmediatePropagation();
		}
	
		private final function mcArcade_MouseUp(e:MouseEvent):void
		{
			if ( OpenMouseDown ) 
			{
				Stage_MouseUp(e);
				return;
			}
			
			if( ArcadeMouseDown ) 
			{
				btnArcade.ShowStaticState();
				e.stopImmediatePropagation();
				PagesMenu.Close();
				VideoMenu.Close();
				
				if ( ArcadeMenu.Opened )
				{
					PlayButtonSound();
					ArcadeMenu.Close();
				}
				else
				{
					if ( ArcadeMenu.Closed ) 
					{
						ArcadeMenu.Update();
						GlobalData.playSound( "HUD_Games" );
					}
					
					ArcadeMenu.Open();
				}
				ArcadeMouseDown = false;
			}
		}
		
		private final function mcVideos_MouseDown(e:MouseEvent):void
		{/*
			VideosMouseDown = true;
			e.stopImmediatePropagation();*/
		}
	
		private final function mcVideos_MouseUp(e:MouseEvent):void
		{/*
			if(OpenMouseDown) 
			{
				Stage_MouseUp(e);
				return;
			}
	
			if(VideosMouseDown) 
			{
				btnVideos.ShowStaticState();
				e.stopImmediatePropagation();
				PagesMenu.Close();
				ArcadeMenu.Close();
				
				if ( VideoMenu.Opened )
				{
					PlayButtonSound();
					VideoMenu.Close();
				}
				else
				{
					if ( VideoMenu.Closed ) 
					{
						VideoMenu.Update();
						GlobalData.playSound( "HUD_Videos" );
					}
					
					VideoMenu.Open();
				}
				VideosMouseDown = false;
			}*/
		}
		
		private final function mcColor_MouseUp(e:MouseEvent):void
		{			
			if(ColoringMouseDown) 
			{
				var targetPage:Class = The_Color_Page[0];
				
				if( ( cMain.cPages.CurrentPage != null && !( cMain.cPages.CurrentPage is targetPage ) )
				    || cMain.cPages.CurrentPage == null ) 
				{
					PlayButtonSound();
					//CloseNav();
					CloseNavFULL();
					OpenMouseDown = false;					
					cMain.FadeToPage( targetPage, 2 );
				}
				
			}
			ColoringMouseDown = false;			
		}
		private final function mcColor_MouseDown(e:MouseEvent):void
		{
			ColoringMouseDown = true;
			e.stopImmediatePropagation();
		}
		
		private final function mcHome_MouseDown(e:MouseEvent):void
		{
			HomeMouseDown = true;
			e.stopImmediatePropagation();
		}
		
		private final function mcHome_MouseUp(e:MouseEvent):void
		{			
			if(OpenMouseDown) 
			{
				Stage_MouseUp(e);
				return;
			}
			
			if( HomeMouseDown && cMain.CurrentPage != -1 ) 
			{
				e.stopImmediatePropagation();
				CloseNav();
				OpenMouseDown = false;
				cMain.PrevPage( -1 );
			}
			HomeMouseDown = false;
		}

		
		private final function PagesMenuButton(buttonIndex:int):void 
		{
			if( buttonIndex >= 0 && !PagesMenu.Closing )
			{
				if ( buttonIndex - 1 == cMain.CurrentPage ) return;
				
				PlayButtonSound();
				CloseNav();
				OpenMouseDown = false;
				
				if(buttonIndex == 0) cMain.PrevPage(buttonIndex - 1);
					else cMain.NextPage(buttonIndex - 1);
			}			
		}
		
		private final function VideoMenuButton(buttonIndex:int):void 
		{
			if ( buttonIndex >= 0 )
			{
				PlayButtonSound();
				clsMain.playVideo( buttonIndex );	
			}
		}
		
		private final function ArcadeMenuButton(buttonIndex:int):void 
		{
			if( buttonIndex < 0 || buttonIndex > ArcadePages.length - 1 || ArcadeMenu.Closing ) return;
			else 
			{
				var targetPage:Class = ArcadePages[buttonIndex];
				
				if( ( cMain.cPages.CurrentPage != null && !( cMain.cPages.CurrentPage is targetPage ) )
				    || cMain.cPages.CurrentPage == null ) 
				{
					PlayButtonSound();
					CloseNav();
					OpenMouseDown = false;
					trace(targetPage);
					cMain.FadeToPage( targetPage, 2 );
				}
			}		
		}
		
		public final function CloseTabs():void
		{
			VideoMenu.Close();
			PagesMenu.Close();
			ArcadeMenu.Close();
		}
		
		private final function HideButtons():void
		{
			btnArcade.visible = false;
			btnHome.visible = false;
			btnOptions.visible = false;
			btnPages.visible = false;
			//btnVideos.visible = false;
			btnColorBook.visible = false;
		}

		private final function PlayButtonSound():void { GlobalData.playSound( "Button_Generic" ); }
		
		private final function ShowButtons():void
		{
			btnArcade.visible = true;
			btnHome.visible = true;
			btnOptions.visible = true;
			btnPages.visible = true;
			//btnVideos.visible = true;
			btnColorBook.visible = true;
		}
	}
}
﻿package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.text.TextField;
	import flash.xml.*;
	
	import com.DaveLibrary.clsGradientDynamicText;
	import com.cupcake.App;
	import com.cupcake.ScrollingMenu;
	import com.cupcake.VideoBox;
	
	public class clsVideoMenu extends WBZ_ScrollingMenu 
	{
		public function clsVideoMenu( clickCallbackFunction:Function, Main:clsMain ) 
			{ super(3,clickCallbackFunction); cMain = Main; }
		
		public final function Update():void { setCurrent( -1 ); }
		
		public final override function InitializeButtons( ):void
		{
			super.InitializeButtons();
			
			var xml:XML = VideoBox.xmlData;
			var videoXML:XMLList = xml.Video;
			var dex:int = 0;
			var bmp:Bitmap;
			var bmpClass:Class;
			var txt:clsGradientDynamicText;
			var buttonText:String;
			
			for each ( var video:XML in videoXML )
			{
				dex = video.attribute("index"); 
				buttonText = video.VideoName.toString();
				
				txt = new clsGradientDynamicText( cMain, buttonText, cMain.gc.navbar_VideoPopupText );
				txt.x = TEXT_X;
				txt.y = TEXT_Y;
				buttonClips[ dex ].addChild( txt );
				buttonClips[ dex ].textClip = txt;
				buttonClips[ dex ].visible = true;
				
				bmp = GlobalData.getBitmap( video.attribute("icon").toString() );
				
				buttonClips[ dex ].icon_mc.addChild( bmp );
			}
		}
		
		public final function updateButtonText():void
		{
			var videoXML:XMLList = GlobalData.videoXML.Video;
			var dex:int = 0;
			
			for each ( var video:XML in videoXML )
			{
				dex = video.attribute("index");
				
				App.Log( "Updating video button " + dex + " to : " + video.VideoName.toString() );
				updateText( dex, video.VideoName.toString() );
			}
		}
	}
}
﻿package 
{
	import asData.clsGlobalConstants;
	
	import flash.net.FileReference;
    import flash.net.URLRequest;
    import flash.net.FileFilter;
		
	import com.cupcake.Utils;
	import com.greensock.*;
	import com.greensock.easing.*;
	import com.cupcake.ExternalImage;
	import com.adobe.images.*;	
	import com.cupcake.App;
	import com.cupcake.SoundEx;
	
	import flash.utils.ByteArray;
	import flash.display.Loader;
	import flash.events.*;
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;	
	import flash.filesystem.*;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.CameraRoll;
	import flash.utils.Dictionary;
	import flash.utils.Timer;
	import flash.utils.getDefinitionByName;
	import flash.xml.*;	
	import flash.geom.Matrix;
	import flash.display.StageQuality;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
			
	public final class Coloring_Book extends WBZ_BookPage
	{					
		
		//Zoom Level & Drag area
		private var colorZoomLevel:int						= 0;
		private var colorDragBounds:Rectangle;	
		private var dragResetTimer:Timer;
		private var nextEncouragement:int					= 0;
		private var spotsColored:int						= 0;	
		
		//Current Phase.
		private var Page_Phase:int;
						
		//Are Canvases.		
		private var Page:Sprite;
		private var Color_Lines:Bitmap 					= null;
		private var Color_Canvas:Bitmap 				= null;		
		private var Offest_Lines:Point;		
		private var Color_Datas:Vector.<BitmapData> 	= null; //Undos.
		private var Undos:Array;		
		
		//Selected Area.
		private var Selected_Peice:String;
		private var Brush:BitmapData;				
		private var Color_Transform:ColorTransform;
		private var Draw_Mask:Bitmap;
		private var Selected_Color:uint;
		private var Selected_Brush:String;
		
		//Color selector.
		private var Moving_Color:Boolean;
		private var Moving_Color_Pos_Offset:Point;
		private var Moving_Color_Pos:Point;
		private var Color_Pallet:BitmapData;
		
		//Selection page.
		private var Curent_Page:int;
		
		//Hud.
		private var Hud_Direction:String;
		
		//Painting check.
		private var we_Painting:Boolean;
		private var Mouse_New_Pos:Point;
		private var Mouse_Old_Pos:Point;
		private var Mouse_Dis_Pos:Point;
		private var Mouse_Dis:Number;
		
		//Cammera.
		public var camRoll:CameraRoll;
		public var camSave:Boolean;		
		
		private var Drawing_Colors:Vector.<uint>			= new <uint>[0xEF3A43, 0xEC5C46, 0xFFB955, 0xFDE073, 0xFFAD95, 0xe19aa4, 0xFEBAE3, 
																		 0xE96AA4, 0xE444BD, 0xD893E8, 0xBF99D4, 0x7876B6, 0x5E7BBD, 0x809CD1, 0xA8DEF2, 0x33C1E5, 
																		 0xADFEDD, 0x23806D, 0x60BEA8, 0x98D967, 0x70DF70, 0xFFFFFF, 0xB3B1A3, 0x000215, 0xAD6C29];		
		
		private var ThePage_Border:Sprite;
		
		private var Tools_Mode:String;
		private var Last_Tool_Mode:String;
		private var Tools_Menu:Sprite;
		private var Mask:Bitmap;
		private var Tools_ScrollBar:Sprite;
		private var Tools_Undo:Sprite;
		private var Tools_Fill_Glow:Sprite;
		private var Tools_Last_Mouse:Point;
		private var Page_Original_Mouse:Point;
		private var Selected_Color_Scroll:int;
				
		private var soundPhoto:SoundEx;
		
		//Tools menu.
		private var Selectable_Brushes:Array;	
		private var Selected_Scroll_Brush:int;	
		//Sheets to make draw tools out of.
		private var Tools_Template:SheetTemplate;
		private var Brushes_Template:SheetTemplate;
		private var Buckets_Template:SheetTemplate;
		private var Chalks_Template:SheetTemplate;
		private var Crayons_Template:SheetTemplate;
		private var Sprays_Template:SheetTemplate;
		private var Pages_Template:SheetTemplate;
		private var Stickers_Template:SheetTemplate;
		private var Stickers_Draw_Template:SheetTemplate;
		//Reusables.
		private var ToolReusable_Template:SheetTemplate;
		private var Draw_Brushes_Template:SheetTemplate;
		private var Preview_Brushes_Template:SheetTemplate;
		//Scaller Bar.		
		private var Tools_Scaler:Sprite;
		private var Scaller_Bar:Sprite;
		private var Pencil_Sprite_1:Sprite;
		private var Pencil_Sprite_2:Sprite;
		private var Scroller_Bar:Sprite;
		private var Scroller_Display:Sprite;
		
		//Confirm Menu.
		private var YesNo_Open:Boolean;
		
		//Stickers.
		private var Stickers:Array;
		private var Sticker_Image_Selected:int;
		
		private var SavingPic:Boolean;
		
		//More Tab.
		private var More_Tab:Sprite;
		private var More_BTN:Sprite;
		private var Recycle_BTN:Sprite;
		private var Save_BTN:Sprite;
		private var More_Extended:Boolean;
		
		//Moding.
		private var Touch_1_ID:int;
		private var Touch_1_Pos:Point;
		private var Touch_2_ID:int;
		private var Touch_2_Pos:Point;
		private var Zoomed:Boolean;
		private var Zoom_Last_Move:Point;
		
		//Hud visibility.
		private var Hud_Visible:Boolean;		
		
		// Reusable bitmaps for MaskDraw
		private var DB:Bitmap = new Bitmap();
		private var MB:Bitmap = new Bitmap();	
		
		private var Scroll_Sticker_Speed_Treshold:int;
		private var Select_Sticker_Speed_Treshold:int;			
		
		private var Tutorial_Skip_Button:Sprite;
		private var Tutorial_Hand:Sprite;
		private var Tutorial_Phase:int;
		
		
		private var SFX_T1:SoundEx;
		private var SFX_T2:SoundEx;
		private var SFX_T3:SoundEx;
		private var SFX_T4:SoundEx;
		private var SFX_T5:SoundEx;
		private var SFX_T6:SoundEx;
		private var SFX_T7:SoundEx;
		private var SFX_T8:SoundEx;
		private var SFX_T9:SoundEx;
		private var SFX_T10:SoundEx;
		private var SFX_T11:SoundEx;
		private var SFX_T12:SoundEx;
		
				
		//Core.
		public function Coloring_Book(aPageVars:Array = null){
			
			super();				
			
			//Cammera.
			camSave = false;
			camRoll = new CameraRoll();
			if((camRoll != null) && CameraRoll.supportsAddBitmapData){camSave = true;}
			
			scene_mc.Tools_Zoom_Out.visible 		= false;
			scene_mc.Tools_Zoom_In.visible 			= false;
			scene_mc.Tools_Sticker_Trash.visible 	= false;
			
			soundPhoto = new SoundEx("but_photo", CONFIG::DATA + "sounds/games/NailGame/but_photo.mp3", App.soundVolume,false,null,false,1 );
			SFX_T1  = new SoundEx( "SFX_T1",  CONFIG::DATA + "sounds/Coloring_Tutorial/Phase_1.mp3",  App.voiceVolume );
			SFX_T2  = new SoundEx( "SFX_T2",  CONFIG::DATA + "sounds/Coloring_Tutorial/Phase_2.mp3",  App.voiceVolume );
			SFX_T3  = new SoundEx( "SFX_T3",  CONFIG::DATA + "sounds/Coloring_Tutorial/Phase_3.mp3",  App.voiceVolume );
			SFX_T4  = new SoundEx( "SFX_T4",  CONFIG::DATA + "sounds/Coloring_Tutorial/Phase_4.mp3",  App.voiceVolume );
			SFX_T5  = new SoundEx( "SFX_T5",  CONFIG::DATA + "sounds/Coloring_Tutorial/Phase_5.mp3",  App.voiceVolume );
			SFX_T6  = new SoundEx( "SFX_T6",  CONFIG::DATA + "sounds/Coloring_Tutorial/Phase_6.mp3",  App.voiceVolume );
			SFX_T7  = new SoundEx( "SFX_T7",  CONFIG::DATA + "sounds/Coloring_Tutorial/Phase_7.mp3",  App.voiceVolume );
			SFX_T8  = new SoundEx( "SFX_T8",  CONFIG::DATA + "sounds/Coloring_Tutorial/Phase_8.mp3",  App.voiceVolume );
			SFX_T9  = new SoundEx( "SFX_T9",  CONFIG::DATA + "sounds/Coloring_Tutorial/Phase_9.mp3",  App.voiceVolume );
			SFX_T10 = new SoundEx( "SFX_T10", CONFIG::DATA + "sounds/Coloring_Tutorial/Phase_10.mp3", App.voiceVolume );
			SFX_T11 = new SoundEx( "SFX_T11", CONFIG::DATA + "sounds/Coloring_Tutorial/Phase_11.mp3", App.voiceVolume );
			SFX_T12 = new SoundEx( "SFX_T12", CONFIG::DATA + "sounds/Coloring_Tutorial/Phase_12.mp3", App.voiceVolume );			
						
			//Folders.
			imageFolder    = CONFIG::DATA + "images/pages/Coloring/";
			reusableFolder = CONFIG::DATA + "images/pages/reusable/";					
			
			Scaller_Bar = new Sprite();
			assets.push( new ExternalImage("Hud/Scaler_Bar.png", 	Scaller_Bar));
			assets.push( new ExternalImage( "Yes_No.png", 			scene_mc.YesNo.Plate ) );			
			
			assets.push( new ExternalImage("Plate.png", 			scene_mc.Tools_Plate));	
			
			More_Tab = new Sprite();
			assets.push( new ExternalImage("Flap.png", 				More_Tab));				
			
			assets.push( new ExternalImage("Page.png", 				scene_mc.Tools_Page, 	4));	
			assets.push( new ExternalImage("Save.png", 				scene_mc.Tools_Save, 	4));	
			assets.push( new ExternalImage("Select.png", 			scene_mc.Tools_Tools, 	4));	
			assets.push( new ExternalImage("Recycle.png", 			scene_mc.Tools_Recycle, 4));				
			
			
			assets.push( new ExternalImage("Zoom_Out.png", 			scene_mc.Tools_Zoom_Out));	
			assets.push( new ExternalImage("Zoom_In.png", 			scene_mc.Tools_Zoom_In));	
			assets.push( new ExternalImage("Sticker_Trash.png", 	scene_mc.Tools_Sticker_Trash));	
			
			Tools_Undo 	 	= new Sprite();
			More_BTN 		= new Sprite();
			Save_BTN		= new Sprite();
			Recycle_BTN 	= new Sprite();
			
			assets.push( new ExternalImage("Zoom_Out.png", 			Tools_Undo));			
			assets.push( new ExternalImage("Zoom_Out.png", 			More_BTN));				
			assets.push( new ExternalImage("Zoom_Out.png", 			Recycle_BTN));				
			assets.push( new ExternalImage("Zoom_Out.png", 			Save_BTN));							
			
			assets.push( new ExternalImage( "Loading.png", 			scene_mc.Loading_Logo, 4) );				
			
			ThePage_Border 	= new Sprite();
			assets.push( new ExternalImage("Page_Border.png", 		ThePage_Border));	
			
			
			//Tutorial.
			Tutorial_Skip_Button 	= new Sprite();
			Tutorial_Hand 	 		= new Sprite();
			assets.push( new ExternalImage("Skip.png", 		Tutorial_Skip_Button, 4));			
			assets.push( new ExternalImage("Hand.png", 		Tutorial_Hand,   	  4));		
			
			
			//New Hud.
			Build_Templates();
			
			//Color.
			Selected_Color 	= 0x00FF66;
			
			//Brush
			Selected_Brush 	= "Brush_0";			
			
			//Set scene.
			Curent_Page 	= 0;				
			
			More_Extended 	= false;
			SavingPic 		= false;
			Zoomed 			= false;
			
			scene_mc.Loading_Logo.alpha = 0.0;
			
			//Locations.
			Mouse_New_Pos = new Point(0, 0);
			Mouse_Old_Pos = new Point(0, 0);
			Mouse_Dis_Pos = new Point(0, 0);		
			Mouse_Dis     = 0.0;
			
			//Prep for draw.
			Initialize_Touch();
			Initialize_Stickers();					
			
			Hud_Visible = true;
			
			Scroll_Sticker_Speed_Treshold = 3;
			Select_Sticker_Speed_Treshold = 9;
			
			Initialize_Tutorial();
			
		}		
		public override function Destroy():void{
			super.Destroy();		
			
			camRoll = null;			
			
			
			Clear_Undos();
			Destroy_Stickers();
			Destroy_Touch();
			
			cMain.cNavBar.Show_NavBar();
			
			scene_mc.removeEventListener(TouchEvent.TOUCH_BEGIN, onTouchBegin);
			scene_mc.removeEventListener(TouchEvent.TOUCH_MOVE,  onTouchMove);
			scene_mc.removeEventListener(TouchEvent.TOUCH_END,   onTouchEnd);		
			
			cMain.removeStageMouseDown( MouseOnDown );	
			cMain.removeStageMouseUp( MouseOnUp );	
		}		
		public final override function Reset():void{										
			cMain.removeStageMouseDown( MouseOnDown );
			cMain.removeStageMouseUp( MouseOnUp );												
			super.Reset();	
		}		
		public final override function Start():void{
			super.Start();					
			
			Build_Tool_Selection();					
			
			cMain.addStageMouseDown( MouseOnDown );		
			cMain.addStageMouseUp( MouseOnUp );			
			
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			scene_mc.addEventListener(TouchEvent.TOUCH_BEGIN, onTouchBegin);
			scene_mc.addEventListener(TouchEvent.TOUCH_MOVE,  onTouchMove);
			scene_mc.addEventListener(TouchEvent.TOUCH_END,   onTouchEnd);		
			
			Setup_Page();		
		}			
		public final override function Stop():void{			
			super.Stop();	
		}
		public final override function Pause():void{			
			Save_Canvas();
			super.Pause();	
		}
		public final override function Init():void{
			if ( isInitialized ) return;			
			super.Init();			
			Reset();
		}	
		protected final override function handleFrame( e:Event ):void{
			if(isRunning && !isPaused){					
				
				if(Update_Tutorial()){return;}
				
				switch(Page_Phase){
					case -1:{/*Net*/}break;
					
					//Begin.
					case 0:{						
						Set_Brush();
						Hide_Templates();
						Selected_Scroll_Brush = 0;
						Tools_Fill_Glow.visible = false;
						Tools_Menu.y = 670;
						Show_Templates(Selectable_Brushes[Selected_Scroll_Brush]);			
						Scroll_Template(Selectable_Brushes[Selected_Scroll_Brush], 0);
						Tools_Mode = "Tool_Selection";
						Tools_Scaler.visible = false;						
						Page_Phase = 1;
					}break;
					
					//Interact.
					case 1:{								
						if(Move_Zoom()){return;}					
						Update_Tools();					
						if(we_Painting){								
							Process_Draw();							
						}						
					}break;
					case 2:{
						if(Moving_Color){Move_Color_Selector();}
					}break;					
					
					//Loading.
					case 99:{
						Page_Phase = 100;
					}break;
					case 100:{
						Setup_Page();
						scene_mc.Loading_Logo.alpha = 0;
					}break;
					
					//Saving.
					case 101:{
						Page_Phase = 102;
					}break;
					case 102:{
						Save_To_Cam();
						scene_mc.Loading_Logo.alpha = 0;
						Page_Phase = -1;
					}break;
				}
			}		
		}
		private function MouseOnDown( e:MouseEvent ):void{				
			
			if(Click_Tutorial()){return;}
			
			switch(Page_Phase){
				case 0:{}break;
				case 1:{				
					
					if(SavingPic){return;}					
					if(YesNo_Open){
						Click_YesNo(mouseX, mouseY);
						return;
					}						
					if(More_BTN.hitTestPoint(mouseX, mouseY, true)){
						Extend_More();
						return;
					}else{
						if(More_Extended){
							if(!scene_mc.Tools_Extend.hitTestPoint(mouseX, mouseY, true)){
								Extend_More();
							}
						}
					}									
					if(scene_mc.Tools_Page.hitTestPoint(mouseX, mouseY, true)){						
						Tools_Fill_Glow.visible = false;
						Tools_Menu.y = 670;						
						Tools_Scaler.visible = false;							
						Selected_Scroll_Brush = 6;
						Hide_Templates();
						
						Selectable_Brushes[Selected_Scroll_Brush][2] = ((25 - Curent_Page) * 240) - 2850;
						if(Selectable_Brushes[Selected_Scroll_Brush][2] > 2882){Selectable_Brushes[Selected_Scroll_Brush][2] = 2882;}
						if(Selectable_Brushes[Selected_Scroll_Brush][2] < -2850){Selectable_Brushes[Selected_Scroll_Brush][2] = -2850;}
						Show_Templates(Selectable_Brushes[Selected_Scroll_Brush], true);	
						Tools_Mode = "Page_Selection";						
						return;
					}						
					if(scene_mc.Tools_Save.hitTestPoint(mouseX, mouseY, true)){					
						Save_Canvas();			
						cMain.cNavBar.Show_NavBar();
						cMain.PrevPage( -1 );
						return;
					}								
					if(Recycle_BTN.hitTestPoint(mouseX, mouseY, true)){
						Show_YesNo();
						return;
					}
					if(Save_BTN.hitTestPoint(mouseX, mouseY, true)){						
						SavingPic = true;
						Hide_Menu_Save_TEMP();
						//Captcha.Show(Hide_Menu_Save);
						return;
					}					
					if(scene_mc.Tools_Recycle.hitTestPoint(mouseX, mouseY, true)){						
						Tools_Fill_Glow.visible = false;
						Tools_Menu.y = 670;						
						Tools_Scaler.visible = false;							
						Selected_Scroll_Brush = 7;
						Hide_Templates();
						Show_Templates(Selectable_Brushes[Selected_Scroll_Brush]);	
						Tools_Mode = "Sticker_Selection";								
						return;						
					}						
					if(scene_mc.Tools_Zoom_In.hitTestPoint(mouseX, mouseY, true)){						
						if ( colorZoomLevel == 0 ) { Zoom_Pic(); }			
						else if ( colorZoomLevel == 4 ) { Zoom_Pic(); }		
						return;
					}
					if(Tools_Undo.hitTestPoint(mouseX, mouseY, true)){						
						Draw_Undo();
						return;
					}
					if(Click_Tools()){return;}	
					if(scene_mc.Color_Hud_Colision_1.hitTestPoint(mouseX, mouseY, true)){return;}
					if(scene_mc.Color_Hud_Colision_2.hitTestPoint(mouseX, mouseY, true)){return;}
					//Draw if no interactions.
					if(Select_Draw_Peice()){
						Add_Undo("Bitmap");
						if(Selected_Brush != "Brush_4"){
							if(Hud_Visible){ShowHide_Hud();}
						}
						we_Painting = true;						
						Mouse_Old_Pos.x = scene_mc.Pages.mouseX;
						Mouse_Old_Pos.y = scene_mc.Pages.mouseY;
					}					
					
				}break;				
			}			
		}	
		private function MouseOnUp( e:MouseEvent ):void{				
			
			if(unClick_Tutorial()){return;}
			
			unClick_Tools();						
			if(we_Painting){we_Painting = false;}	
			if(!Hud_Visible){ShowHide_Hud();}
		}					
		
		//Loading Prep.
		private function Prep_Next_Page(The_Page:int):void{		
			Page_Phase 						= -1;
			scene_mc.Loading_Logo.alpha 	= 0.0;				
			scene_mc.Loading_Logo.visible 	= true;
			TweenLite.to(scene_mc.Loading_Logo, 1.0, {alpha:1, onComplete:To_Next_Page_Load, onCompleteParams:[The_Page]});	
			scene_mc.setChildIndex(scene_mc.Loading_Logo, (scene_mc.numChildren - 1));
		}		
		private function To_Next_Page_Load(X:int):void{			
			if(Curent_Page != 0){Save_Canvas();}
			Curent_Page 					= ((X - 4) + 1);			
			scene_mc.Loading_Logo.alpha	 	= 1.0;
			scene_mc.Loading_Logo.visible 	= true;			
			Page_Phase 						= 99;
			scene_mc.setChildIndex(scene_mc.Loading_Logo, (scene_mc.numChildren - 1));
		}		
		private function Hide_Menu(Switch:Boolean = false):void{					
			scene_mc.Loading_Logo.alpha = 0.0;				
			scene_mc.Loading_Logo.visible = true;
			TweenLite.to(scene_mc.Loading_Logo, 1.0, {alpha:1, onComplete:To_Load});	
			scene_mc.setChildIndex(scene_mc.Loading_Logo, (scene_mc.numChildren - 1));
		}		
		private function Hide_Menu_Save(Correct:Boolean, Cancel:Boolean):void{		
			
			if(Cancel){SavingPic = false; return;}
			if(!Correct){SavingPic = false; return;}
			SavingPic = false;
			
			Page_Phase = -1;
			scene_mc.Loading_Logo.alpha = 0.0;		
			scene_mc.Loading_Logo.x = (512 - (scene_mc.Loading_Logo.width  / 2));
			scene_mc.Loading_Logo.y = (384 - (scene_mc.Loading_Logo.height / 2));
			TweenLite.to(scene_mc.Loading_Logo, 1.0, {alpha:1, onComplete:To_Save});	
		}
		private function Hide_Menu_Save_TEMP():void{		
			
			
			SavingPic = false;
			
			Page_Phase = -1;
			scene_mc.Loading_Logo.alpha = 0.0;		
			scene_mc.Loading_Logo.x = (512 - (scene_mc.Loading_Logo.width  / 2));
			scene_mc.Loading_Logo.y = (384 - (scene_mc.Loading_Logo.height / 2));
			TweenLite.to(scene_mc.Loading_Logo, 1.0, {alpha:1, onComplete:To_Save});	
		}
		private function To_Load():void{
			scene_mc.Loading_Logo.alpha = 1.0;
			scene_mc.Loading_Logo.visible = true;
			scene_mc.setChildIndex(scene_mc.Loading_Logo, (scene_mc.numChildren - 1));
			Page_Phase = 99;
		}
		private function To_Save():void{
			scene_mc.Loading_Logo.alpha = 1.0;
			scene_mc.Loading_Logo.visible = true;
			scene_mc.setChildIndex(scene_mc.Loading_Logo, (scene_mc.numChildren - 1));
			Page_Phase = 101;
		}		
		
		//Setup Current Page.
		private function Setup_Page():void{			
			
			Destroy_Stickers();
			
			Page_Phase = -1;
			if(Curent_Page == 0){Curent_Page = 1;}
						
			//Not Painting.
			we_Painting = false;
			
			//Selecting Color.
			Moving_Color = false;
			if(Moving_Color_Pos_Offset == null){Moving_Color_Pos_Offset = new Point(0, 0);}
			if(Moving_Color_Pos        == null){Moving_Color_Pos 		= new Point(0, 0);}			
					
			//Setup the peice.
			Selected_Peice = "";
									
			//Offset.
			if(Offest_Lines == null){Offest_Lines = new Point(0, 0);}
			
			//For Undos.
			if(Color_Datas  == null){Color_Datas = new Vector.<BitmapData>();}
			if(Undos  	    == null){Undos 		 = new Array();}
			
			Clear_Undos();			
			
			//For Drawing.
			if(Color_Lines == null){
				Color_Lines   = new Bitmap();
				Color_Lines.smoothing = true;
				Color_Lines.x = 0;
				Color_Lines.y = 0;
				scene_mc.Pages.addChildAt(Color_Lines, 0);
			}
			if(Color_Canvas == null){
				Color_Canvas = new Bitmap();
				Color_Canvas.smoothing = true;
				Color_Canvas.x = 0;
				Color_Canvas.y = 0;
				scene_mc.Pages.addChildAt(Color_Canvas, 0);
			}				
					
			
			Load_Lines();	
			Load_Canvas();
			Load_Stickers();
		}	
		
		//Saving and Loading_Logo.
		private function onCanvasLoaded(e:Event):void{
			
			var loader:Loader = Loader(e.target.loader);
			var bitmapData:BitmapData = Bitmap(e.target.content).bitmapData;
				
			Color_Canvas.bitmapData = new BitmapData(	bitmapData.width, 
													 	bitmapData.height,
													 	false, 0xFFFFFF	    );	
			Color_Canvas.bitmapData.draw(bitmapData);		
			
			loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onCanvasLoaded);			
			
			Page_Phase = 0;
		}	
		private function onLinesLoaded(e:Event):void{
			
			var loader:Loader 	  = Loader(e.target.loader);
			var bitmapData:Bitmap = new Bitmap();
			bitmapData.bitmapData = Bitmap(e.target.content).bitmapData;			
				
			Color_Lines.bitmapData = new BitmapData(	bitmapData.width, 
													 	bitmapData.height,
													 	true, 0xFFFFFF	    );	
			
			Color_Lines.bitmapData.drawWithQuality(bitmapData);			
			loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onLinesLoaded);		
		}	
		private function Save_Canvas():void{
									
			var png:PNGEncoder = new PNGEncoder();
			var byteArray:ByteArray = png.encode( Color_Canvas.bitmapData );			
			
			// set an filename
			var filename:String = (("Page_" + Curent_Page.toString()) + ".png");		
			
			// get current path
			var file:File = File.applicationStorageDirectory.resolvePath( filename );
			
			// get the native path
			var wr:File = new File( file.nativePath );			
			
			// create filestream
			var stream:FileStream = new FileStream();
			
			//  open/create the file, set the filemode to write in order to save.
			stream.open( wr , FileMode.WRITE);
			
			// write your byteArray into the file.
			stream.writeBytes ( byteArray, 0, byteArray.length );
			
			// close the file.
			stream.close();			
			
			//Save Stickers aswell.
			Save_Stickers();
			
		}	
		
		private function Load_Lines():void{		
			
			Color_Lines.bitmapData  = new BitmapData(	1366, 
													 	768,
													 	true, 0x000000					);
			Color_Lines.x = 0;
			Color_Lines.y = 0;	
			
			
			
			//Extension.
			var FilePath:String = CONFIG::DATA + "images/pages/Coloring/Coloring_Pages/" + "Page_" + Curent_Page.toString() + ".png";			
			//Create the file.
			var myFile:File;
			myFile = File.applicationDirectory;
			myFile = myFile.resolvePath( FilePath );
			
			//Load it.
			if(myFile.exists){
								
				var stream:FileStream = new FileStream();
				stream.open(myFile, FileMode.READ);
				var ba:ByteArray = new ByteArray();
				stream.readBytes(ba); //this is the equivalent to stream.getByteArray():ByteArray				
				stream.close();				
				
				var loader:Loader = new Loader();
				loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLinesLoaded)
				loader.loadBytes(ba);				
				
			}						
		}
		private function Load_Canvas():void{			
			
			//Curent_Page
			var PageName:String = ("Page_" + Curent_Page.toString());
            var PageClass:Class;         
			PageClass = getDefinitionByName(PageName) as Class;
			if(Page != null){scene_mc.removeChild(Page); trace("Removed Shapes Page");}
            Page 	  = new PageClass();
			Page.name = "Shapes_Page";			
			Page.visible = false;			
			scene_mc.addChild(Page);
			Page.x = -171;
			Page.y = 0;
			
			Color_Canvas.bitmapData = new BitmapData(	1366, 
													 	768,
													 	false, 0xFFFFFF					);
			
			//Extension.
			var FilePath:String = (("Page_" + Curent_Page.toString()) + ".png");			
			//Create the file.
			var myFile:File;
			myFile = File.applicationStorageDirectory;
			myFile = myFile.resolvePath( FilePath );
			
			//Load it.
			if(myFile.exists){
								
				var stream:FileStream = new FileStream();
				stream.open(myFile, FileMode.READ);
				var ba:ByteArray = new ByteArray();
				stream.readBytes(ba); //this is the equivalent to stream.getByteArray():ByteArray				
				stream.close();				
				
				var loader:Loader = new Loader();
				loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onCanvasLoaded)
				loader.loadBytes(ba);				
				
			}else{
				Reset_Canvas();
				Page_Phase = 0;				
			}
			
		}	
		
		private function Save_Stickers():void{	
					
			var Sticker_XML:String;
			
			Sticker_XML = "<Stickers>\n";			
			
			for(var X:int = 0; X < Stickers.length; X++){					
				
				Sticker_XML = Sticker_XML + "<ThisSticker>\n";
				
				Sticker_XML = Sticker_XML + "<IdIndex>" + Stickers[X][2]   		+ "</IdIndex>" + "\n";
				Sticker_XML = Sticker_XML + "<pX>"  	+ Stickers[X][0].x 		+ "</pX>"  + "\n";
				Sticker_XML = Sticker_XML + "<pY>"  	+ Stickers[X][0].y 		+ "</pY>"  + "\n";
				Sticker_XML = Sticker_XML + "<cS>"  	+ Stickers[X][0].scaleX + "</cS>"  + "\n";				
				
				Sticker_XML = Sticker_XML + "</ThisSticker>\n";
				
			}
			
			Sticker_XML = Sticker_XML + "</Stickers>";			
			
			//Extension.
			var FilePath:String = (("Sticker_" + Curent_Page.toString()) + ".xml");		
			//Create the file.
			var myFile:File;
			myFile = File.applicationStorageDirectory;
			myFile = myFile.resolvePath( FilePath );
			
			//var WriteFile:File = new File( myFile.nativePath );			
						
			// create filestream
			var stream:FileStream = new FileStream();
			
			//  open/create the file, set the filemode to write in order to save.
			stream.open( myFile , FileMode.WRITE);
			
			// write your byteArray into the file.
			stream.writeUTFBytes(Sticker_XML.toString());
			
			// close the file.
			stream.close();
		}
		private function Load_Stickers():void{	
			
			//Extension.
			var FilePath:String = (("Sticker_" + Curent_Page.toString()) + ".xml");			
			//Create the file.
			var myFile:File;
			myFile = File.applicationStorageDirectory;
			myFile = myFile.resolvePath( FilePath );
			
			if(myFile.exists){						
								
				var stream:FileStream = new FileStream();
				stream.open(myFile, FileMode.READ);
				var Sticker_XML:XML = XML(stream.readUTFBytes(stream.bytesAvailable));    			
				stream.close();		
				
				Process_Stickers(Sticker_XML);
				
			}else{				
								
			}			
		}
		private function Process_Stickers(TheXML:XML):void{
			
			for(var X:int = 0; X < TheXML.ThisSticker.length(); X++){
				
				var Index:int = int(TheXML.ThisSticker[X].IdIndex.text());
				var Position:Point = new Point(int(TheXML.ThisSticker[X].pX.text()),
											   int(TheXML.ThisSticker[X].pY.text()));
				var Scale:Number = Number(TheXML.ThisSticker[X].cS.text());
				
				Create_Premade_Sticker(Index,
									   Position,
									   Scale);
			}			
		}
		
		private function Select_Draw_Peice():Boolean{
			
			var Peice_Counter:int = 2;
			
			Page.scaleX = scene_mc.Pages.scaleX;
			Page.scaleY = scene_mc.Pages.scaleY;
			
			Page.x = scene_mc.Pages.x;
			Page.y = scene_mc.Pages.y;
			//Page.visible = true;
			
			for(var X:int = 1; X < Peice_Counter; X++){				
				
				var Child_Name:String = ("P" + X.toString());				
				
				if(Page.getChildByName(Child_Name) != null){
					
					if(MovieClip(Page.getChildByName(Child_Name)).hitTestPoint(mouseX, mouseY, true)){
						
						Selected_Peice = Child_Name;					
						
						if(Draw_Mask == null){Draw_Mask = new Bitmap();}
						
						Draw_Mask.bitmapData = new BitmapData(MovieClip(Page.getChildByName(Selected_Peice)).width,
															  MovieClip(Page.getChildByName(Selected_Peice)).height,
															  true,
															  0x000000);	
															  
						Draw_Mask.bitmapData.draw(MovieClip(Page.getChildByName(Selected_Peice)), null, null, null, null, true);						
						
						return true;
						
					}
					
					Peice_Counter = (Peice_Counter + 1);
					
				}				
			}
			
			return false;
			
		}		
		
		private function Select_Color():void{					
			
			Selected_Color 			= Color_Pallet.getPixel32((Moving_Color_Pos.x + (scene_mc.Color_Wheel.Bar.width  / 2)),
															  (Moving_Color_Pos.y + (scene_mc.Color_Wheel.Bar.height / 2)));
			Color_Transform 		= new ColorTransform();
			Color_Transform.color 	= Selected_Color;
			
			scene_mc.Color_Wheel.Wheel_Center.Center_Wheel_Color.transform.colorTransform = Color_Transform;	
					
		}
		private function Prep_Move_Color_Selector():void{	
			
			Moving_Color_Pos_Offset.x = ((mouseX - 512) - Moving_Color_Pos.x);
			Moving_Color_Pos_Offset.y = ((mouseY - 384) - Moving_Color_Pos.y);
			
			Moving_Color = true;
		}
		private function Move_Color_Selector():void{	
		
			Moving_Color_Pos.x = ((mouseX - 512) - Moving_Color_Pos_Offset.x);
			Moving_Color_Pos.y = ((mouseY - 384) - Moving_Color_Pos_Offset.y);			
			
			//Set Bound so we cant go to far.
			if(Moving_Color_Pos.x < -150){Moving_Color_Pos.x = -150;}
			if(Moving_Color_Pos.x > 150){Moving_Color_Pos.x  = 150;}
			if(Moving_Color_Pos.y < -150){Moving_Color_Pos.y = -150;}
			if(Moving_Color_Pos.y > 150){Moving_Color_Pos.y  = 150;}
			
			scene_mc.Color_Wheel.Wheel_Center.x = Moving_Color_Pos.x;
			scene_mc.Color_Wheel.Wheel_Center.y = Moving_Color_Pos.y;
			
			Select_Color();			
		
		}
		private function Select_Brush(Brush:String):void{				
			Selected_Brush = Brush;			
			Set_Brush();			
		}			
		private function Set_Brush():void{					
				
			Color_Transform 		= new ColorTransform();
			if(Selected_Color_Scroll == -1){
				Color_Transform.color 	= 0xFFFFFF;
			}else{
				Color_Transform.color 	= Drawing_Colors[(Selected_Color_Scroll - 4)];
			}
			
			
			if(Selected_Brush == "Brush_4"){return;}					
			var TempBrush:Sprite = new Sprite();
			//Need brushes.
			var TheBrush:int = 0;
			switch(Selected_Brush){
				case "Brush_0":{TheBrush = 0;}break;
				case "Brush_1":{TheBrush = 1;}break;
				case "Brush_2":{TheBrush = 2;}break;
				case "Brush_3":{TheBrush = 3;}break;
			}
			var Draw_Sprite:Sprite = Draw_Brushes_Template.Get_Sprite(TheBrush);			
			
			var rec:Rectangle 		= new Rectangle(0, 0, Draw_Sprite.width,
													      Draw_Sprite.height);
			Brush 					= new BitmapData(Draw_Sprite.width,
													 Draw_Sprite.height, true, 0x000000);
			
			Brush.draw(Draw_Sprite);		
			Brush.colorTransform(rec, Color_Transform);
			
			// New DB/MB setup code - Sean
			if ( DB.bitmapData != null ) { DB.bitmapData.dispose(); }
			DB.bitmapData = new BitmapData(Brush.width, Brush.height, true, 0x000000);
			DB.bitmapData.draw(Brush, null, null, null, null, true);
			
			SetMaskBitmap();
			// New DB/MB setup code
		}
		
		private function SetMaskBitmap():void{
						
			if(Selected_Brush == "Brush_2" || Selected_Brush == "Brush_0"){
				DB.scaleX = (Scroller_Display.scaleX * 1.2);
				DB.scaleY = (Scroller_Display.scaleY * 1.2);
			}else{			
				DB.scaleX = Scroller_Display.scaleX;
				DB.scaleY = Scroller_Display.scaleY;
			}
			
			if ( MB.bitmapData != null ) { MB.bitmapData.dispose(); }			
			MB.bitmapData = new BitmapData(DB.width, DB.height, true, 0x000000);
			
			DB.mask = MB;
		}		
		private function Process_Draw():void{
						
			Mouse_New_Pos.x = (scene_mc.Pages.mouseX + 0);
			Mouse_New_Pos.y = scene_mc.Pages.mouseY;
			
			Mouse_Dis_Pos.x = (Mouse_New_Pos.x - Mouse_Old_Pos.x);
			Mouse_Dis_Pos.y = (Mouse_New_Pos.y - Mouse_Old_Pos.y);
			
			Mouse_Dis        = Point.distance(Mouse_New_Pos, Mouse_Old_Pos);
			var Size:int     = 10;
			var Steps:int    = int(Mouse_Dis / Size);			
			if(Steps < 1){Steps = 1;}
			
			for(var X:int = 0; X <= Steps; X++){				
				var pX:int = (Mouse_Old_Pos.x + ((Mouse_Dis_Pos.x / Steps) * X));
				var pY:int = (Mouse_Old_Pos.y + ((Mouse_Dis_Pos.y / Steps) * X));
				Draw_Peice(pX, pY);				
			}			
			
			Mouse_Old_Pos.x = Mouse_New_Pos.x;
			Mouse_Old_Pos.y = Mouse_New_Pos.y;
			
		}
		private function Draw_Peice(pX:int, pY:int):void{	
			
			switch(Selected_Brush){
				case "Brush_0":
				case "Brush_1":
				case "Brush_2":				
				case "Brush_3":{
					MaskDraw(pX, pY);
				}break;				
				case "Brush_4":{
					//Add_Undo();
					MaskFill();					
					we_Painting = false;
					return;
				}break;
			}		
			
		}				
		private function MaskDraw(pX:Number, pY:Number):void{				
			DB.x = ((pX - (DB.width / 2))  - MovieClip(Page.getChildByName(Selected_Peice)).x);
			DB.y = ((pY - (DB.height / 2)) - MovieClip(Page.getChildByName(Selected_Peice)).y);
			
			MB.bitmapData.fillRect( new Rectangle( 0, 0, MB.width, MB.height ), 0x00000000 );
			MB.bitmapData.copyPixels(Draw_Mask.bitmapData, new Rectangle(DB.x, DB.y, DB.width, DB.height), new Point(0, 0), null, null, true);	
			
			DB.x = 0;
			DB.y = 0;
			MB.x = 0;
			MB.y = 0;	
			
			DB.cacheAsBitmap   = true;
			MB.cacheAsBitmap   = true;
			DB.mask  		   = MB;
			
			var TempMovieClip:MovieClip = new MovieClip();
			TempMovieClip.addChild(DB);	
			TempMovieClip.addChild(MB);	
						
			var Draw_Final:BitmapData = new BitmapData(TempMovieClip.width, TempMovieClip.height, true, 0x000000);	
			Draw_Final.draw(TempMovieClip, null, null, null, null, true);				
			
			var Des:Point = new Point(((pX - (Draw_Final.width / 2)) + 0), (pY - (Draw_Final.height / 2)));			
			
			Color_Canvas.bitmapData.copyPixels(Draw_Final, new Rectangle(0, 0, Draw_Final.width, Draw_Final.height),
											   Des, null, null, true);												
			
			TempMovieClip = null;		
			Draw_Final.dispose();
		}		
		private function MaskFill():void{
		
			var rec:Rectangle 	= new Rectangle(0, 0, Draw_Mask.width  + 2,
												      Draw_Mask.height + 2);		
			
			Draw_Mask.bitmapData.colorTransform(rec, Color_Transform);
		
			var MP:Point 		= new Point(MovieClip(Page.getChildByName(Selected_Peice)).x,
										    MovieClip(Page.getChildByName(Selected_Peice)).y);					
			
			var Des:Point 		= new Point(MP.x, MP.y);
			
			Color_Canvas.bitmapData.copyPixels(Draw_Mask.bitmapData, new Rectangle(0, 0, Draw_Mask.width, Draw_Mask.height),
											   Des, null, null, true);			
			
		}
		
		//Undos.
		private function Add_Undo(UndoType:String):void{
			
			var NewUndo:Array = new Array();
			//Add the undo type.
			switch(UndoType){
				case "Sticker":{					
					//Creat Array of all stickers.					
					NewUndo.push("Sticker");
					NewUndo.push(Duplicate_Sticker_Array(Stickers));
					Undos.push(NewUndo);		
				}break;
				case "Bitmap":{
					var New_Save:BitmapData = new BitmapData(Color_Canvas.width, Color_Canvas.height, true, 0x000000);
					New_Save.draw(Color_Canvas);								
					NewUndo.push("Bitmap");
					NewUndo.push(New_Save);
					Undos.push(NewUndo);		
				}break;
			}				
			
			//If more then 5 remove.
			if(Undos.length > 5){
				if(Undos[0][0] == "Bitmap"){
					Undos[0][1].dispose();
				}
				Undos.splice(0, 1);
			}			
			
		}
		private function Draw_Undo():void{			
			if(Undos.length == 0){return;}			
			switch(Undos[(Undos.length - 1)][0]){
				case "Bitmap":{
					Color_Canvas.bitmapData = new BitmapData( Undos[(Undos.length - 1)][1].width, 
													 		  Undos[(Undos.length - 1)][1].height,
													 		  false, 0xFFFFFF	);															
					Color_Canvas.bitmapData.draw(Undos[(Undos.length - 1)][1]);
					Undos[(Undos.length - 1)][1].dispose();
					Undos.splice((Undos.length - 1), 1);
				}break;
				case "Sticker":{					
					//So here Clear stickers.
					Destroy_Stickers();
					//then recreate based on current Array.
					Recreate_Stickers(Undos[(Undos.length - 1)][1]);
					Undos[(Undos.length - 1)][1].length = 0;
					Undos.splice((Undos.length - 1), 1);
				}break;
			}						
		}
		private function Clear_Undos():void{
			for(var X:int = 0; X < Undos.length; X++){					
				switch(Undos[X][0]){
					case "Bitmap":{
						Undos[X][1].dispose();
					}break;
					case "Sticker":{
						Undos[X][1].length = 0;		
					}break;
				}
			}
			Undos.length = 0;			
		}
		
		//Reset Picture.
		private function Reset_Canvas():void{
			Clear_Undos();
			
			Color_Canvas.bitmapData = new BitmapData(	1366, 
													 	768,
													 	false, 0xFFFFFF	);
		}
		
		//Save To Roll.
		private function Save_To_Cam():void{		
			
			var Distance:int = ((1366 - cMain.get_localWidth()) / 2);
			
			//save to File.
			Save_Canvas();
			
			//Save To Cam.
			if(camSave)
			{			
				var Scene_Bitmap:BitmapData = new BitmapData(cMain.get_localWidth(), 768, true, 0x000000);
				var Full_Scene:BitmapData   = new BitmapData(1366, 768, true, 0x000000);
				Full_Scene.drawWithQuality(scene_mc.Pages);
				
				var Draw_Rect:Rectangle = new Rectangle(Distance, 0, cMain.get_localWidth(), Color_Canvas.height);
				var Draw_Pos:Point      = new Point(0, Color_Canvas.y);				
				Scene_Bitmap.copyPixels(Full_Scene, Draw_Rect, Draw_Pos, null, null, true);
				camRoll.addBitmapData(Scene_Bitmap);				
				soundPhoto.Start();
			}			
			
			TweenLite.delayedCall( 1, Save_Done );
			
		}
		public function Save_Done():void{
			TweenLite.killDelayedCallsTo( Save_Done );					
			Page_Phase = 1;
		}		
		
		private function ShowHide_Hud():void{
			if(!Hud_Visible){				
				
				Hud_Visible = true;				
				TweenLite.to(Tools_Menu, 				1, {alpha:1});
				TweenLite.to(scene_mc.Tools_Plate, 		1, {alpha:1});
				TweenLite.to(scene_mc.Tools_Page, 		1, {alpha:1});
				TweenLite.to(scene_mc.Tools_Save, 		1, {alpha:1});
				TweenLite.to(scene_mc.Tools_Tools, 		1, {alpha:1});
				TweenLite.to(scene_mc.Tools_Recycle, 	1, {alpha:1});				
				
			}else{
				
				Hud_Visible = false;
				TweenLite.to(Tools_Menu, 				1, {alpha:0});
				TweenLite.to(scene_mc.Tools_Plate, 		1, {alpha:0});
				TweenLite.to(scene_mc.Tools_Page, 		1, {alpha:0});
				TweenLite.to(scene_mc.Tools_Save, 		1, {alpha:0});
				TweenLite.to(scene_mc.Tools_Tools, 		1, {alpha:0});
				TweenLite.to(scene_mc.Tools_Recycle, 	1, {alpha:0});
				
			}
		}
		private function Extend_More():void{
			if(!More_Extended){
				More_Extended = true;
				TweenLite.to(scene_mc.Tools_Extend, 1, {y:350});
			}else{
				More_Extended = false;
				TweenLite.to(scene_mc.Tools_Extend, 1, {y:0});
			}
		}	
		
		//---------------------------------------------------------------------		
		//Tools.
		//---------------------------------------------------------------------		
		private function Build_Tool_Selection():void{			
			
			var MaskWidth:int = 740; //700			
			Tools_Mode = "Tool_Selection";			
			
			Tools_Menu 	 		= new Sprite();			
			Tools_Menu.x 		= 590;
			Tools_Menu.y 		= 670;
			
			Tools_ScrollBar   = new Sprite();
			Tools_ScrollBar.x = 0;
			Tools_ScrollBar.y = 0;		
			
			//Mask the scrolling tools.
			Mask 			= new Bitmap();			
			Mask.bitmapData = new BitmapData(MaskWidth, 482, false, 0x000000);
			Mask.x 			= -(Mask.width / 2);
			Mask.y 			= -(Mask.height / 2);
			Mask.name       = "The_Mask";
			Tools_ScrollBar.addChild(Mask);
			Tools_ScrollBar.mask = Mask;						
			
			//Add the pencils.
			Pencil_Sprite_1				= new Sprite();
			Pencil_Sprite_1 			= ToolReusable_Template.Get_Sprite(5);
			Pencil_Sprite_1.x 			= (MaskWidth / 2) - 10;		
			Pencil_Sprite_1.y 			= -15;
			Pencil_Sprite_1.name        = "Pencil_1";			
			
			Pencil_Sprite_2				= new Sprite();
			//Pencil_Sprite_2 			= ToolReusable_Template.Get_Sprite(5);
			Pencil_Sprite_2.x 			= ((-(MaskWidth / 2) - Pencil_Sprite_2.width) + 10);		
			Pencil_Sprite_2.y 			= -15;
			Pencil_Sprite_2.name        = "Pencil_2";			
			
			//Scaler hud.
			Tools_Scaler 			= new Sprite();
			Tools_Scaler.x 			= 0;
			Tools_Scaler.y 			= -54;	
			Tools_Scaler.visible 	= false;
			
			Scaller_Bar.scaleX      = 0.95;
			Scaller_Bar.scaleY      = 0.95;
			Scaller_Bar.x           = -(Scaller_Bar.width  / 2);
			Scaller_Bar.y           = -(Scaller_Bar.height / 2);
			Tools_Scaler.addChild(Scaller_Bar);
			
			Scroller_Bar 			= new Sprite();
			Scroller_Bar 			= ToolReusable_Template.Get_Sprite(1);
			Scroller_Bar.x 			= -(Scroller_Bar.width / 2);
			Scroller_Bar.y 			= -(Scroller_Bar.height / 2);
			Scroller_Bar.name       = "Scroller";		
			Tools_Scaler.addChild(Scroller_Bar);
			
			Scroller_Display 	= new Sprite();
			Scroller_Display 	= ToolReusable_Template.Get_Sprite(2);
			Scroller_Display.visible = false;
			Scroller_Display.scaleX = 1.0;
			Scroller_Display.scaleY = 1.0;
			
			Scroller_Display.x = (Scroller_Bar.x + (Scroller_Bar.width / 2)) - (Scroller_Display.width / 2) + (2 * Scroller_Display.scaleX);
			Scroller_Display.y = -(Scroller_Display.height + 26);
			Scroller_Display.name       	= "Display";		
			
			var PB_1:Sprite = new Sprite();
			PB_1            = Preview_Brushes_Template.Get_Sprite(0);
			var PB_2:Sprite = new Sprite();
			PB_2            = Preview_Brushes_Template.Get_Sprite(1);
			var PB_3:Sprite = new Sprite();
			PB_3            = Preview_Brushes_Template.Get_Sprite(2);
			var PB_4:Sprite = new Sprite();
			PB_4            = Preview_Brushes_Template.Get_Sprite(3);
			
			PB_1.x = -(PB_1.width  / 2) + (Scroller_Display.width  / 2);
			PB_1.y = -(PB_1.height / 2) + (Scroller_Display.height / 2) - 12;
			PB_2.x = -(PB_2.width  / 2) + (Scroller_Display.width  / 2);
			PB_2.y = -(PB_2.height / 2) + (Scroller_Display.height / 2) - 12;
			PB_3.x = -(PB_3.width  / 2) + (Scroller_Display.width  / 2);
			PB_3.y = -(PB_3.height / 2) + (Scroller_Display.height / 2) - 12;
			PB_4.x = -(PB_4.width  / 2) + (Scroller_Display.width  / 2);
			PB_4.y = -(PB_4.height / 2) + (Scroller_Display.height / 2) - 12;
			
			Scroller_Display.addChild(PB_1);
			Scroller_Display.addChild(PB_2);
			Scroller_Display.addChild(PB_3);
			Scroller_Display.addChild(PB_4);			
			
			Tools_Scaler.addChild(Scroller_Display);
					
			//assemble scrolling tools.
			Assemble_Templates();			
			for(var X:int = 0; X < Selectable_Brushes.length; X++){
				Scroll_Template(Selectable_Brushes[X], 0);
			}			
			
			Tools_Fill_Glow   = new Sprite();
			Tools_Fill_Glow   = ToolReusable_Template.Get_Sprite(4);
			Tools_Fill_Glow.x = -(Tools_Fill_Glow.width  / 2);
			Tools_Fill_Glow.y = -(Tools_Fill_Glow.height / 2);
			Tools_Fill_Glow.visible = false;
			Tools_ScrollBar.addChild(Tools_Fill_Glow);
			
			ThePage_Border.visible = false;
			Tools_ScrollBar.addChild(ThePage_Border);
			
			Tools_Menu.addChild(Tools_ScrollBar);
			Tools_Menu.addChild(Pencil_Sprite_1);
			Tools_Menu.addChild(Pencil_Sprite_2);
			Tools_Menu.addChild(Tools_Scaler);				
			
			scene_mc.addChild(Tools_Menu);
			
			
			
			Tools_Undo.x = 40;
			Tools_Undo.y = -220;
			Tools_Undo.visible = false;
			
			More_BTN.x = 40;
			More_BTN.y = 32;
			More_BTN.visible = false;
			
			Recycle_BTN.x = 40;
			Recycle_BTN.y = -350;
			Recycle_BTN.visible = false;
			
			Save_BTN.x = 40;
			Save_BTN.y = -120;
			Save_BTN.visible = false;
						
			//More_Tab.y = -420;
			More_Tab.y = -380;
			scene_mc.Tools_Extend.addChild(More_BTN);
			scene_mc.Tools_Extend.addChild(More_Tab);
			scene_mc.Tools_Extend.addChild(Recycle_BTN);
			scene_mc.Tools_Extend.addChild(Save_BTN);
			scene_mc.Tools_Extend.addChild(Tools_Undo);	
						
			
			//Zoom.
			scene_mc.Tools_Zoom_Out.x = ((1024 - Tools_Undo.width) - 16);
			scene_mc.Tools_Zoom_Out.y = 16;
			
			scene_mc.Tools_Zoom_In.x = scene_mc.Tools_Zoom_Out.x;
			scene_mc.Tools_Zoom_In.y = scene_mc.Tools_Zoom_Out.y;
			
			scene_mc.Tools_Zoom_Out.visible = false;
			scene_mc.Tools_Zoom_In.visible  = true;			
			
			scene_mc.Tools_Sticker_Trash.x = ((1024 - Tools_Undo.width) - 20);
			scene_mc.Tools_Sticker_Trash.y = 10;
			
		}
		private function Update_Tools():void{		
			
			switch(Tools_Mode){				
				
				case "Idle":{
					
				}break;
				
				case "Tool_Selection":{
					
				}break;
				
				case "Scrolling_Scale":{	
					
					var Limit:int 		 = ((Scaller_Bar.width / 2) - 80);		
					var Max_Scale:Number = 1.5;		
					var Min_Scale:Number = 0.5;		
					 
					var Position:int = (Scaller_Bar.x + (Scaller_Bar.width  / 2));
					Position = (Position + (mouseX - Tools_Menu.x));					
										
					if(Position > Limit){Position   = Limit;}
					if(Position < -Limit){Position = -Limit;}						
					
					Scroller_Bar.x = (Position - (Scroller_Bar.width / 2));					
					
					var Percent:Number = (((Position + Limit) * 100) / (Limit * 2));
					
					Scroller_Display.scaleX = (Min_Scale + ((Percent * Max_Scale) / 100));
					Scroller_Display.scaleY = Scroller_Display.scaleX;
					
					Scroller_Display.x = (Scroller_Bar.x + (Scroller_Bar.width / 2)) - (Scroller_Display.width / 2) + (2 * Scroller_Display.scaleX);
					Scroller_Display.y = -(Scroller_Display.height + 26);
					
					SetMaskBitmap();
					
				}break;						
				
				case "Color_Selection":{	
					
					
					
				}break;
				
				case "Color_Scroll":{						
					Scroll_Template(Selectable_Brushes[Selected_Scroll_Brush], (mouseX - Tools_Last_Mouse.x));				
					Tools_Last_Mouse.x = mouseX;
				}break;
				
				case "Page_Scroll":{						
					Scroll_Template(Selectable_Brushes[Selected_Scroll_Brush], (mouseX - Tools_Last_Mouse.x), true);				
					Tools_Last_Mouse.x = mouseX;
				}break;
				
				case "Sticker_Scroll":{					
					
					//SCroll Sticker.
					var MoveSpeed:int = (mouseX - Tools_Last_Mouse.x);
					if(MoveSpeed > Scroll_Sticker_Speed_Treshold){
						Scroll_Template(Selectable_Brushes[Selected_Scroll_Brush], MoveSpeed);	
					}else if(MoveSpeed < -Scroll_Sticker_Speed_Treshold){
						Scroll_Template(Selectable_Brushes[Selected_Scroll_Brush], MoveSpeed);	
					}
					Tools_Last_Mouse.x = mouseX;						
					
					//Select Sticker.
					var YDis:int = (mouseY - Tools_Last_Mouse.y);
					if(YDis < 0){YDis = -YDis;}							
					if(YDis > Select_Sticker_Speed_Treshold){Create_Sticker();}
					Tools_Last_Mouse.y = mouseY;					
					
					
				}break;
				case "Moving_Sticker":{
					Move_Sticker();
				}break;
				
			}
			
		}
		private function Click_Tools():Boolean{			
			
			if(scene_mc.Tools_Tools.hitTestPoint(mouseX, mouseY, true)){
				Tools_Fill_Glow.visible = false;
				Hide_Templates();
				Selected_Scroll_Brush = 0;
				Tools_Menu.y = 670;
				Show_Templates(Selectable_Brushes[Selected_Scroll_Brush]);			
				Scroll_Template(Selectable_Brushes[Selected_Scroll_Brush], 0);
				Tools_Mode = "Tool_Selection";
				Tools_Scaler.visible = false;
				return true;
			}
			
			switch(Tools_Mode){				
				case "Tool_Selection":{
					if(Click_Tool_Selection(Selectable_Brushes[Selected_Scroll_Brush])){
						return true;
					}
				}break;				
				case "Color_Selection":{
					if(Scroller_Bar.hitTestPoint(mouseX, mouseY, true) && Tools_Scaler.visible){
						Scroller_Display.visible = true;
						Tools_Mode = "Scrolling_Scale";
						return true;
					}
					if(Click_Colors(Selectable_Brushes[Selected_Scroll_Brush])){return true;}
					
				}break;			
				case "Page_Selection":{
					if(Click_Page(Selectable_Brushes[Selected_Scroll_Brush])){return true;}
				}break;
				case "Sticker_Selection":{
					if(Click_Stickers_Active()){return true;}
					if(Click_Sticker(Selectable_Brushes[Selected_Scroll_Brush])){return true;}
				}break;
				
				
			}
			
			if(Mask.hitTestPoint(mouseX, mouseY, true)){trace("Plate"); return true;}
			
			return false;
		}
		private function unClick_Tools():void{
			switch(Tools_Mode){				
				
				case "Scrolling_Scale":{
					Scroller_Display.visible = false;
					Tools_Mode = "Color_Selection";
					return;
				}break;
				case "Color_Scroll":{
					Tools_Mode = "Color_Selection";
					return;
				}break;
				case "Page_Scroll":{
					Tools_Mode = "Page_Selection";
					unClick_Page(Selectable_Brushes[Selected_Scroll_Brush]);
					return;
				}break;
				case "Sticker_Scroll":{
					Tools_Mode = "Sticker_Selection";					
					return;
				}break;
				case "Moving_Sticker":{					
					MouseUp_Sticker();
					return;
				}break;
								
			}
			
		}				
		//---------------------------------------------------------------------		
		//This is the scrolling brush selection pads.	
		private function Build_Templates():void{
			
			Selected_Scroll_Brush 	 = 0;
			Selected_Color_Scroll 	 = 12;
			
			Tools_Template 			 = new SheetTemplate(assets, imageFolder, "", 		"Tools");
			Brushes_Template 		 = new SheetTemplate(assets, imageFolder, "Brushes/", "Brushes");
			Buckets_Template 		 = new SheetTemplate(assets, imageFolder, "Brushes/", "Buckets");
			Chalks_Template 		 = new SheetTemplate(assets, imageFolder, "Brushes/", "Chalks");
			Crayons_Template 		 = new SheetTemplate(assets, imageFolder, "Brushes/", "Crayons");
			Sprays_Template 		 = new SheetTemplate(assets, imageFolder, "Brushes/", "Sprays");	
			Pages_Template 			 = new SheetTemplate(assets, imageFolder, "", 		"Pages");	
			Stickers_Template		 = new SheetTemplate(assets, imageFolder, "", 		"Stickers");	
			Stickers_Draw_Template	 = new SheetTemplate(assets, imageFolder, "", 		"Sticker_Draw");	
			
			//Other hud elements.
			ToolReusable_Template 	 = new SheetTemplate(assets, imageFolder, "Hud/",	   	"Reusables");
			Draw_Brushes_Template 	 = new SheetTemplate(assets, imageFolder, "Tips/",	"Draw_Brush");
			Preview_Brushes_Template = new SheetTemplate(assets, imageFolder, "Tips/",	"Preview_Brush");
			
			
		}
		private function Assemble_Templates():void{
			
			Selectable_Brushes = new Array();	
			Selectable_Brushes.push(Assemble_A_Template(Tools_Template, -1, 45));
			Selectable_Brushes.push(Assemble_A_Template(Brushes_Template, 37));
			Selectable_Brushes.push(Assemble_A_Template(Buckets_Template, 93));
			Selectable_Brushes.push(Assemble_A_Template(Chalks_Template, 37));
			Selectable_Brushes.push(Assemble_A_Template(Crayons_Template, 46));
			Selectable_Brushes.push(Assemble_A_Template(Sprays_Template, 54));
			Selectable_Brushes.push(Assemble_A_Template(Pages_Template, 240));
			Selectable_Brushes.push(Assemble_A_Template(Stickers_Template, -1, 25, 1.0));
			Selectable_Brushes.push(Assemble_A_Template(Stickers_Draw_Template, -1, 25, 1.0));
			
			Hide_Templates();
			
			Show_Templates(Selectable_Brushes[Selected_Scroll_Brush]);			
			
			//Fix Tools positions.
			for(var X:int = 4; X < Selectable_Brushes[0].length; X++){				
				Selectable_Brushes[0][X][0].y = (-Selectable_Brushes[0][X][0].height + 95);
			}
			
			//Fix Pages positions.
			for(var P:int = 4; P < Selectable_Brushes[6].length; P++){				
				Selectable_Brushes[6][P][0].y = -(Selectable_Brushes[6][P][0].height / 2);
			}
			
			//Fix Stickers positions.
			for(var S:int = 4; S < Selectable_Brushes[7].length; S++){				
				Selectable_Brushes[7][S][0].y = -(Selectable_Brushes[7][S][0].height / 2) + 32;
			}
			
			
		}		
		private function Assemble_A_Template(Template:SheetTemplate, Limit:int = -1, Offset:int = 0, Scaler:Number = 1.0):Array{
			
			//The item list.
			var Holder_Array:Array = new Array();
			
			//First node is there center.
			Holder_Array.push(new Point(0, 0)); //There Center.
			Holder_Array.push(2); 				//Movement Speed.
			Holder_Array.push(0); 				//Movement Current.
			Holder_Array.push(0); 				//Movement Max.
			
			//Position Incramentation.
			var Next_X:int = Holder_Array[0].x;		
			
			for(var X:int = 0; X < Template.Frame_Count(); X++){				
				
				var New_Element:Array = new Array(); 
				
				var New_Sprite:Sprite 	= Template.Get_Sprite(X);
				
				New_Sprite.scaleX 		= Scaler;
				New_Sprite.scaleY 		= Scaler;
				New_Sprite.x 			= Next_X;
				if(Limit == -1){
					Next_X 	   			= (Next_X + New_Sprite.width);
				}else{
					Next_X 	   			= (Next_X + Limit);
				}
				Next_X 	   			= (Next_X + Offset);
				
				
				New_Sprite.y 			= Holder_Array[0].y;
				
				
				if(X == (Template.Frame_Count() - 1)){
					if(Limit == -1){
						Holder_Array[3]		= (Holder_Array[3] + New_Sprite.width );
					}else{
						Holder_Array[3]		= (Holder_Array[3] + Limit);
					}	
				}else{
					if(Limit == -1){
						Holder_Array[3]		= (Holder_Array[3] + New_Sprite.width + Offset);
					}else{
						Holder_Array[3]		= (Holder_Array[3] + Limit + Offset);
					}	
				}
				
							
					
				Tools_ScrollBar.addChild(New_Sprite);
				
				New_Element.push(New_Sprite);		
				New_Element.push(New_Sprite.x);
				
				Holder_Array.push(New_Element);
				
			}	
			
			return Holder_Array;
			
		}	
		private function Hide_Templates(){			
			for(var X:int = 0; X < Selectable_Brushes.length; X++){
				Hide_Template(Selectable_Brushes[X]);
			}				
		}
		private function Hide_Template(Holder_Array:Array){			
			for(var X:int = 4; X < Holder_Array.length; X++){				
				Holder_Array[X][0].visible = false;					
			}				
		}
		private function Show_Templates(Holder_Array:Array, PageTime:Boolean = false){	
		
			if(Selected_Scroll_Brush == 0 || Selected_Scroll_Brush == 6 || Selected_Scroll_Brush == 7){
				//Tools_Scaler.visible 	= false;
				Pencil_Sprite_1.visible = false;
				Pencil_Sprite_2.visible = false;
			}else{
				//Tools_Scaler.visible 	= true;
				Pencil_Sprite_1.visible = true;
				Pencil_Sprite_2.visible = true;
			}
		
			for(var X:int = 4; X < Holder_Array.length; X++){				
				Holder_Array[X][0].visible = true;					
			}	
			
			Scroll_Template(Holder_Array, 0, PageTime)
			
		}
		private function Scroll_Template(Holder_Array:Array, Speed:int, PagesScroll:Boolean = false){
			
			Holder_Array[0].x 	= -(Holder_Array[3] / 2);
			Holder_Array[0].y 	= 0;			
			Holder_Array[1] 	= Speed;
			
			Holder_Array[2]    = (Holder_Array[2] + Holder_Array[1]);
			if(Holder_Array[2] > Holder_Array[3]){ Holder_Array[2] = 0;}
			if(Holder_Array[2] < -Holder_Array[3]){Holder_Array[2] = 0;}			
			
			if(PagesScroll){
				//trace(Holder_Array[2]);
				if(Holder_Array[2] > 593){ Holder_Array[2] = 593;}
				if(Holder_Array[2] < -563){Holder_Array[2] = -563;}
				ThePage_Border.visible = true;				
			}else{
				ThePage_Border.visible = false;
			}
						
			for(var X:int = 4; X < Holder_Array.length; X++){				
				//Height.
				if(Selected_Scroll_Brush == 6){
					Holder_Array[X][0].y = (Holder_Array[0].y - 30);
				}else{
					if(Selected_Scroll_Brush != 0 && Selected_Scroll_Brush != 7){
						if(Selected_Scroll_Brush == 2){
							Holder_Array[X][0].y = Holder_Array[0].y;						
						}else{
							if(Selected_Color_Scroll == X){
								Holder_Array[X][0].y = -32;
							}else{
								Holder_Array[X][0].y = Holder_Array[0].y;
							}		
						}
					}
				}
				//Move.
				Holder_Array[X][0].x = int((Holder_Array[0].x + Holder_Array[X][1]) + Holder_Array[2]);						
				//Pages.
				if(!PagesScroll){							
					//To Low.
					if(Holder_Array[X][0].x < Holder_Array[0].x){
						Holder_Array[X][0].x = int((Holder_Array[0].x + Holder_Array[3]) - (Holder_Array[0].x - Holder_Array[X][0].x));
					}				
					//To High.
					if(Holder_Array[X][0].x > (Holder_Array[0].x + Holder_Array[3])){
						Holder_Array[X][0].x = int(Holder_Array[0].x + (Holder_Array[X][0].x - (Holder_Array[0].x + Holder_Array[3])));
					}
				}else{
					if((Curent_Page + 3) == X){							
						ThePage_Border.x = Holder_Array[X][0].x;
						ThePage_Border.y = Holder_Array[X][0].y;
					}
				}
				//Fill Bucket.
				if(Selected_Scroll_Brush == 2){					
					if(Selected_Color_Scroll == -1){Tools_Fill_Glow.visible = false;}					
					if(Selected_Color_Scroll == X){					
						Tools_Fill_Glow.visible = true;
						Tools_Fill_Glow.x = int(Holder_Array[X][0].x + (Holder_Array[X][0].width  / 2) - (Tools_Fill_Glow.width  / 2) - 1);
						Tools_Fill_Glow.y = int(Holder_Array[X][0].y + (Holder_Array[X][0].height / 2) - (Tools_Fill_Glow.height / 2) + 4);
					}
				}				
			}				
			
		}		
		private function Click_Colors(Holder_Array:Array):Boolean{					
			if(Pencil_Sprite_1.hitTestPoint(mouseX, mouseY, true)){		
				Selected_Color_Scroll = -1;
				Pencil_Sprite_1.y = -30;
				Pencil_Sprite_2.y = -30;				
				Set_Brush();
				Scroll_Template(Holder_Array, 0);
				return true;
			}
			if(Pencil_Sprite_2.hitTestPoint(mouseX, mouseY, true)){		
				Selected_Color_Scroll = -1;
				Pencil_Sprite_1.y = -30;
				Pencil_Sprite_2.y = -30;				
				Set_Brush();
				Scroll_Template(Holder_Array, 0);
				return true;
			}		
			if(Mask.hitTestPoint(mouseX, mouseY, false)){	
				for(var X:int = 4; X < Holder_Array.length; X++){
					if(Holder_Array[X][0].hitTestPoint(mouseX, mouseY, true)){					
						Pencil_Sprite_1.y = -15;
						Pencil_Sprite_2.y = -15;
						Selected_Color_Scroll = X; trace(X);
						Set_Brush();
						if(Tools_Last_Mouse == null){Tools_Last_Mouse = new Point(0, 0);}
						Tools_Last_Mouse.x = mouseX; Tools_Last_Mouse.y = mouseY;
						Tools_Mode = "Color_Scroll";
						return true;
					}
				}
			}
			return false;
		}
		private function Click_Page(Holder_Array:Array):Boolean{					
			if(Mask.hitTestPoint(mouseX, mouseY, false)){	
				for(var X:int = 4; X < Holder_Array.length; X++){
					if(Holder_Array[X][0].hitTestPoint(mouseX, mouseY, true)){									
						if(Page_Original_Mouse == null){Page_Original_Mouse = new Point(0, 0);}
						if(Tools_Last_Mouse == null){Tools_Last_Mouse = new Point(0, 0);}						
						scene_mc.Loading_Logo.x = (Holder_Array[X][0].x + Tools_Menu.x) + (Holder_Array[X][0].width  / 2);
						scene_mc.Loading_Logo.y = (Holder_Array[X][0].y + Tools_Menu.y) + (Holder_Array[X][0].height / 2);						
						Tools_Last_Mouse.x 	  = mouseX; Tools_Last_Mouse.y 	  = mouseY;
						Page_Original_Mouse.x = mouseX; Page_Original_Mouse.y = mouseY;
						Tools_Mode = "Page_Scroll";
						return true;
					}
				}
			}
			return false;
		}
		private function unClick_Page(Holder_Array:Array):void{					
			if(Mask.hitTestPoint(mouseX, mouseY, false)){	
				for(var X:int = 4; X < Holder_Array.length; X++){
					if(Holder_Array[X][0].hitTestPoint(mouseX, mouseY, true)){								
						if(Point.distance(new Point(mouseX, 50), new Point(Page_Original_Mouse.x, 50)) < 15){
							Prep_Next_Page(X);								
						}
					}
				}
			}			
		}
		private function Click_Tool_Selection(Holder_Array:Array):Boolean{	
			for(var X:int = 4; X < Holder_Array.length; X++){
				if(Holder_Array[X][0].hitTestPoint(mouseX, mouseY, true)){					
					Hide_Templates();
					Tools_Fill_Glow.visible 			   = false;
					Scroller_Display.getChildAt(1).visible = false;
					Scroller_Display.getChildAt(2).visible = false;
					Scroller_Display.getChildAt(3).visible = false;
					Scroller_Display.getChildAt(4).visible = false;
					switch((X - 4)){
						case 0:{
							Scroller_Display.getChildAt(1).visible = true;
							Select_Brush("Brush_0");
							Selected_Scroll_Brush = 1;
							Tools_Scaler.visible = true;
							Tools_Menu.y = 712;
						}break;//Set
						case 2:{
							Scroller_Display.getChildAt(2).visible = true;
							Select_Brush("Brush_1");
							Selected_Scroll_Brush = 3;
							Tools_Scaler.visible = true;
							Tools_Menu.y = 712;
						}break;
						case 1:{
							Scroller_Display.getChildAt(4).visible = true;
							Select_Brush("Brush_3");
							Selected_Scroll_Brush = 4;
							Tools_Scaler.visible = true;
							Tools_Menu.y = 712;
						}break;
						case 4:{
							Tools_Fill_Glow.visible = true;
							Select_Brush("Brush_4");
							Selected_Scroll_Brush = 2;
							Tools_Menu.y = 660;
						}break;//Set
						case 3:{
							Scroller_Display.getChildAt(3).visible = true;
							Select_Brush("Brush_2");
							Selected_Scroll_Brush = 5;
							Tools_Scaler.visible = true;
							Tools_Menu.y = 712;
						}break;
					}
										
					Show_Templates(Selectable_Brushes[Selected_Scroll_Brush]);	
					Tools_Mode = "Color_Selection";
					return true;
				}
			}
			return false;
		}				
		private function Click_Sticker(Holder_Array:Array):Boolean{					
			if(Mask.hitTestPoint(mouseX, mouseY, false)){	
				for(var X:int = 4; X < Holder_Array.length; X++){
					if(Holder_Array[X][0].hitTestPoint(mouseX, mouseY, true)){									
						Sticker_Image_Selected = X;
						if(Page_Original_Mouse == null){Page_Original_Mouse = new Point(0, 0);}
						if(Tools_Last_Mouse == null){Tools_Last_Mouse = new Point(0, 0);}
						Tools_Last_Mouse.x 	  = mouseX; Tools_Last_Mouse.y 	  = mouseY;
						Page_Original_Mouse.x = mouseX; Page_Original_Mouse.y = mouseY;
						Tools_Mode = "Sticker_Scroll";
						return true;
					}
				}
			}
			return false;
		}		
		//---------------------------------------------------------------------		
		
		//---------------------------------------------------------------------		
		//Stickers.
		//---------------------------------------------------------------------		
		private function Initialize_Stickers():void{
			Stickers = new Array();
			Sticker_Image_Selected = 0;
		}
		private function Create_Sticker():void{
			
			Add_Undo("Sticker");
			
			var ThisStickerArray:Array = new Array();
			
			var Sticker_sprite:Sprite = new Sprite();
			var Sticker_Bitmap:Bitmap = new Bitmap();
			
			var OldScale:Number = Selectable_Brushes[8][Sticker_Image_Selected][0].scaleX;
			
			Selectable_Brushes[8][Sticker_Image_Selected][0].scaleX = 1.0;
			Selectable_Brushes[8][Sticker_Image_Selected][0].scaleY = 1.0;
			
			Sticker_Bitmap.bitmapData = new BitmapData(Selectable_Brushes[8][Sticker_Image_Selected][0].width,
													   Selectable_Brushes[8][Sticker_Image_Selected][0].height,
													   true,
													   0x000000);
			Sticker_Bitmap.bitmapData.draw(Selectable_Brushes[8][Sticker_Image_Selected][0]);
			Sticker_Bitmap.x = -(Sticker_Bitmap.width  / 2);
			Sticker_Bitmap.y = -(Sticker_Bitmap.height / 2);
			Sticker_sprite.addChild(Sticker_Bitmap);
			Sticker_sprite.scaleX = 0.5;
			Sticker_sprite.scaleY = 0.5;
			Sticker_sprite.x = mouseX;
			Sticker_sprite.y = mouseY;
			
			Selectable_Brushes[8][Sticker_Image_Selected][0].scaleX = OldScale;
			Selectable_Brushes[8][Sticker_Image_Selected][0].scaleY = OldScale;			
			
			scene_mc.addChild(Sticker_sprite);			
			
			ThisStickerArray.push(Sticker_sprite);
			ThisStickerArray.push(true);
			ThisStickerArray.push(Sticker_Image_Selected);
			ThisStickerArray.push(0.5);
			Stickers.push(ThisStickerArray);
			Tools_Mode = "Moving_Sticker";
			scene_mc.Tools_Sticker_Trash.visible = true;
		}		
		private function Move_Sticker():void{			
			for(var X:int = 0; X < Stickers.length; X++){
				if(Stickers[X][1]){
										
					//Here if double finger we scale.
					if((Touch_1_ID != -1) && (Touch_2_ID != -1)){
						
						var Max_S:Number = 0.5;
						var Min_S:Number = 0.0;
						
						var Max_D:Number = 200;
						var Min_D:Number = 64;						
						
						var Distance:Number = Point.distance(Touch_1_Pos, Touch_2_Pos);
						if(Distance > Max_D){Distance = Max_D;}
						if(Distance < Min_D){Distance = Min_D;}
						
						var Scaler:Number = ((Distance * Max_S) / Max_D) + 0.5;
						Stickers[X][3] = Scaler;
						
						if(Zoomed){
							Stickers[X][0].scaleX = (Stickers[X][3] * 2);
							Stickers[X][0].scaleY = Stickers[X][0].scaleX;					
						}else{
							Stickers[X][0].scaleX = Stickers[X][3];
							Stickers[X][0].scaleY = Stickers[X][3];
						}
												
					}else{
						Stickers[X][0].x = mouseX;
						Stickers[X][0].y = mouseY;
					}			
					
				}
			}			
		}
		private function MouseUp_Sticker():void{
			
			if(scene_mc.Tools_Sticker_Trash.hitTestPoint(mouseX, mouseY, false)){	
				for(var D:int = 0; D < Stickers.length; D++){
					if(Stickers[D][1]){
						scene_mc.removeChild(Stickers[D][0]);
						Stickers.splice(D, 1);
						Tools_Mode = "Sticker_Selection";
						scene_mc.Tools_Sticker_Trash.visible = false;
						return;
					}
				}
			}
			
			for(var X:int = 0; X < Stickers.length; X++){
				if(Stickers[X][1]){					
					if(Stickers[X][0].hitTestObject(scene_mc.Color_Hud_Colision_1) ||
					   Stickers[X][0].hitTestObject(scene_mc.Color_Hud_Colision_2)   ){
						scene_mc.removeChild(Stickers[X][0]);
						Stickers.splice(X, 1);
						X = (X - 1);
						continue;
					}else{					
						Stickers[X][1] = false;				
						scene_mc.removeChild(Stickers[X][0]);
						Stickers[X][0].x = (Stickers[X][0].x + 171);
						Stickers[X][0].scaleX = Stickers[X][3];
						Stickers[X][0].scaleY = Stickers[X][3];	
						scene_mc.Pages.addChild(Stickers[X][0]);
					}
				}
			}
			Tools_Mode = "Sticker_Selection";
			scene_mc.Tools_Sticker_Trash.visible = false;
		}
		private function Click_Stickers_Active():Boolean{
			for(var X:int = 0; X < Stickers.length; X++){
				if(Stickers[X][0].hitTestPoint(mouseX, mouseY, false)){	
					Add_Undo("Sticker");
					Stickers[X][1] = true;
					scene_mc.Pages.removeChild(Stickers[X][0]);
					scene_mc.addChild(Stickers[X][0]);
					Stickers[X][0].x = mouseX;
					Stickers[X][0].y = mouseY;
					if(Zoomed){
						Stickers[X][0].scaleX = (Stickers[X][3] * 2);
						Stickers[X][0].scaleY = Stickers[X][0].scaleX;					
					}							
					Tools_Mode = "Moving_Sticker";					
					scene_mc.Tools_Sticker_Trash.visible = true;				
					return true;
				}
			}
			return false;
			
		}
		private function Destroy_Stickers():void{
			for(var X:int = 0; X < Stickers.length; X++){				
				scene_mc.Pages.removeChild(Stickers[X][0]);				
			}
			Stickers.length = 0;
		}		
		private function Duplicate_Sticker_Array(Passed_Array:Array):Array{			
			var New_Array:Array = new Array();			
			for(var X:int = 0; X < Stickers.length; X++){				
				var New_Sticker:Array = new Array();
				New_Sticker.push(Stickers[X][2]);
				New_Sticker.push(new Point(Stickers[X][0].x, Stickers[X][0].y));
				New_Sticker.push(Stickers[X][0].scaleX);
				New_Array.push(New_Sticker);
			}			
			return New_Array;			
		}		
		private function Recreate_Stickers(StickerArray:Array):void{
			for(var X:int = 0; X < StickerArray.length; X++){				
				Create_Premade_Sticker(StickerArray[X][0], StickerArray[X][1], StickerArray[X][2]);
			}
		}
		private function Create_Premade_Sticker(Index:int, Position:Point, Scale:Number):void{
			
			var ThisStickerArray:Array = new Array();			
			var Sticker_sprite:Sprite  = new Sprite();
			var Sticker_Bitmap:Bitmap  = new Bitmap();			
			var OldScale:Number        = Selectable_Brushes[8][Index][0].scaleX;
			
			Selectable_Brushes[8][Index][0].scaleX = 1.0;
			Selectable_Brushes[8][Index][0].scaleY = 1.0;
			
			Sticker_Bitmap.bitmapData = new BitmapData(Selectable_Brushes[8][Index][0].width,
													   Selectable_Brushes[8][Index][0].height,
													   true,
													   0x000000);
			Sticker_Bitmap.bitmapData.draw(Selectable_Brushes[8][Index][0]);
			Sticker_Bitmap.x = -(Sticker_Bitmap.width  / 2);
			Sticker_Bitmap.y = -(Sticker_Bitmap.height / 2);
			Sticker_sprite.addChild(Sticker_Bitmap);
			Sticker_sprite.x = Position.x;
			Sticker_sprite.y = Position.y;
			Sticker_sprite.scaleX = Scale;
			Sticker_sprite.scaleY = Scale;
			
			Selectable_Brushes[8][Index][0].scaleX = OldScale;
			Selectable_Brushes[8][Index][0].scaleY = OldScale;			
			
			scene_mc.Pages.addChild(Sticker_sprite);			
			
			ThisStickerArray.push(Sticker_sprite);
			ThisStickerArray.push(false);
			ThisStickerArray.push(Index);
			ThisStickerArray.push(Scale);
			Stickers.push(ThisStickerArray);
			
		}
		//---------------------------------------------------------------------				
		
		//---------------------------------------------------------------------		
		//Show Yes No.
		//---------------------------------------------------------------------		
		private function Show_YesNo():void{		
			//scene_mc.setChildIndex(scene_mc.YesNo,(scene_mc.numChildren - 1));
			//YesNo_Open = true;
			//TweenLite.to(scene_mc.YesNo, 0.5, {y:384});
			
			Color_Reset_Popup.Show(Reset_Girl);
			
		}
		private function Reset_Girl(Correct:Boolean):void{		
			if(Correct){
				Reset_Canvas();	
				Destroy_Stickers();
				//Hide_YesNo();					
				return;
			}
		}
		private function Click_YesNo(pX:int, pY:int):void{				
			if(scene_mc.YesNo.Yes.hitTestPoint(pX, pY)){		
				Reset_Canvas();	
				Destroy_Stickers();
				Hide_YesNo();					
				return;
			}			
			if(scene_mc.YesNo.No.hitTestPoint(pX, pY)){	
				Hide_YesNo();
				return;
			}			
		}
		private function Hide_YesNo():void{		
			YesNo_Open = false;
			TweenLite.to(scene_mc.YesNo, 0.5, {y:-500});
		}
		//---------------------------------------------------------------------		
		
		//---------------------------------------------------------------------		
		//Zomms.
		//---------------------------------------------------------------------		
		private function Zoom_Pic():void{			
			if(!Zoomed){
				Zoom_Page();
				Zoomed = true;
			}else{
				Zoom_Reset();
				Zoomed = false;
			}			
		}		
		private function Zoom_Reset():void{
			scene_mc.Tools_Zoom_Out.visible = false;
			scene_mc.Tools_Zoom_In.visible  = true;
			TweenLite.to(scene_mc.Pages, 1, {scaleX:1, scaleY:1, x:-171, y:0, ease:Elastic.easeOut});
		}				
		private function Zoom_Page():void{				
							
			var Center_X:int = (512 - scene_mc.Pages.width);
			var Center_Y:int = (384 - scene_mc.Pages.height);
			
			if(Center_X > -171){Center_X = -171;}
			if(Center_Y > 0){Center_Y = 0;}
			
			if(Center_X < -(((scene_mc.Pages.width * 2) - 1366) + 171)){Center_X = -(((scene_mc.Pages.width * 2) - 1366) + 171);}
			if(Center_Y < -((scene_mc.Pages.height * 2) - 768)){Center_Y = -((scene_mc.Pages.height * 2) - 768);}
			
			scene_mc.Tools_Zoom_Out.visible = true;
			scene_mc.Tools_Zoom_In.visible  = false;
		
			TweenLite.to(scene_mc.Pages, 1, {scaleX:2, scaleY:2, x:Center_X, y:Center_Y, ease:Elastic.easeOut});
				
		}
		private function Move_Zoom():Boolean{
			
			if(Tools_Mode == "Moving_Sticker"){return false;}
			
			if(Zoomed){
				if((Touch_1_ID == -1) || (Touch_2_ID == -1)){return false;}	
			}else{return false;}			
			
			var Center_X:int = scene_mc.Pages.x;
			var Center_Y:int = scene_mc.Pages.y;
			
			var Offset_X:int = Math.round(Touch_1_Pos.x - Zoom_Last_Move.x);
			var Offset_Y:int = Math.round(Touch_1_Pos.y - Zoom_Last_Move.y);
			
			Center_X = (Center_X + Offset_X);
			Center_Y = (Center_Y + Offset_Y);
			
			if(Center_X > -171){Center_X = -171;}
			if(Center_Y > 0){Center_Y = 0;}
			
			if(Center_X < -((scene_mc.Pages.width - 1366) + 171)){Center_X = -((scene_mc.Pages.width - 1366) + 171);}
			if(Center_Y < -(scene_mc.Pages.height - 768)){Center_Y = -(scene_mc.Pages.height - 768);}
			
			scene_mc.Pages.x = Center_X;
			scene_mc.Pages.y = Center_Y;		
			
			Zoom_Last_Move.x = Touch_1_Pos.x;
			Zoom_Last_Move.y = Touch_1_Pos.y;
			
			return true;
			
		}		
		//---------------------------------------------------------------------		
		
		
		//---------------------------------------------------------------------		
		//Zomms.
		//---------------------------------------------------------------------		
		private function Initialize_Tutorial():void{
			
			Tutorial_Skip_Button.x = 512;
			Tutorial_Skip_Button.y = 25;
			
			Tutorial_Hand.x = -900;
			Tutorial_Hand.y = -900;
			
			scene_mc.Tutorial_Holder.addChild(Tutorial_Skip_Button);
			scene_mc.Tutorial_Holder.addChild(Tutorial_Hand);
			
			Tutorial_Hand.visible 		 = false;
			Tutorial_Skip_Button.visible = false;
			Tutorial_Phase = -1;
			
			/*if(cMain.Color_Tutorial_Active){
				Tutorial_Phase = 0;
			}else{
				Tutorial_Hand.visible 		 = false;
				Tutorial_Skip_Button.visible = false;
				Tutorial_Phase = -1;
			}*/
			
		}
		private function Update_Tutorial():Boolean{
			if(Tutorial_Phase == -1){return false;}
			scene_mc.setChildIndex(scene_mc.Tutorial_Holder, scene_mc.numChildren - 1);
			switch(Tutorial_Phase){
				case 0:{
					SFX_T1.Start();
					Tutorial_Phase = 1;
				}break;
				case 1:{
					if(!SFX_T1.isRunning){
						SFX_T2.Start();
						Go_Hand(857, 542, 857, 632, 90);
						Tutorial_Phase = 2;
					}
				}break;
				case 2:{
					/*Hold*/
				}break;
				case 8:{
					SFX_T8.Start();		
					Go_Hand(281, 550, 844, 550, 90);		
					Tutorial_Phase = 9;
				}break;
				case 9:{
					if(!SFX_T8.isRunning){						
						Kill_Hand();
						SFX_T9.Start();
						Go_Hand(968, 542, 968, 632, 90);
						Tutorial_Phase = 10;
					}
				}break;
				case 10:{
					if(!SFX_T9.isRunning){						
						Kill_Hand();
						SFX_T10.Start();
						Extend_More();
						Go_Hand(203, 272, 110, 272, 180);
						Tutorial_Phase = 11;
					}
				}break;
				case 11:{
					if(!SFX_T10.isRunning){						
						Kill_Hand();
						SFX_T11.Start();
						Extend_More();
						Go_Hand(280, 727, 190, 727, 180);
						Tutorial_Phase = 12;
					}
				}break;
				case 12:{
					if(!SFX_T11.isRunning){						
						Kill_Hand();
						SFX_T12.Start();
						Tutorial_Phase = 13;
						TweenLite.to(Tutorial_Skip_Button, 1, {alpha:0.0});
					}
				}break;
				case 13:{
					if(!SFX_T12.isRunning){						
						//cMain.Color_Tutorial_Active = false;
						Kill_Tutorial();
					}
				}break;
				
				
				case 99:{
					if(we_Painting){								
						Process_Draw();							
					}	
				}break;
				
			}
			return true;
		}
		private function Click_Tutorial():Boolean{
			if(Tutorial_Phase == -1){return false;}
			if(Tutorial_Skip_Button.hitTestPoint(mouseX, mouseY, true)){
				//cMain.Color_Tutorial_Active = false;
				Kill_Tutorial();
				return true;
			}
			switch(Tutorial_Phase){
				
				//Click Paint.
				case 2:{					
					
					if(Selectable_Brushes[Selected_Scroll_Brush][8][0].hitTestPoint(mouseX, mouseY, true)){
						
						Hide_Templates();
						Tools_Fill_Glow.visible 			   = false;
						Scroller_Display.getChildAt(1).visible = false;
						Scroller_Display.getChildAt(2).visible = false;
						Scroller_Display.getChildAt(3).visible = false;
						Scroller_Display.getChildAt(4).visible = false;
						
						Tools_Fill_Glow.visible = true;
						Select_Brush("Brush_4");
						Selected_Scroll_Brush = 2;
						Tools_Menu.y = 660;
						Show_Templates(Selectable_Brushes[Selected_Scroll_Brush]);	
						Tools_Mode = "Color_Selection";
						
						Tutorial_Phase = 3;
						Kill_Hand();
						if(SFX_T2.isRunning){SFX_T2.Stop();}
						SFX_T3.Start();
						
						Go_Hand(281, 600, 844, 600, 90);
						
					}
					
				}break;
				
				//Pick Color.
				case 3:{
					if(Click_Colors(Selectable_Brushes[Selected_Scroll_Brush])){
						Scroll_Template(Selectable_Brushes[Selected_Scroll_Brush], 0);	
						Tutorial_Phase = 4;
						Kill_Hand();
						if(SFX_T3.isRunning){SFX_T3.Stop();}
						SFX_T4.Start();						
					}
				}break;
				
				case 4:{
					if(Select_Draw_Peice()){
						Add_Undo("Bitmap");
						if(Selected_Brush != "Brush_4"){
							if(Hud_Visible){ShowHide_Hud();}
						}
						we_Painting = true;						
						Process_Draw();	
						
						Tutorial_Phase = 5;
						Kill_Hand();
						if(SFX_T4.isRunning){SFX_T4.Stop();}
						SFX_T5.Start();		
						
						Go_Hand(47, 509, 47, 578, 90);
					}		
				}break;
				
				case 5:{
					if(scene_mc.Tools_Tools.hitTestPoint(mouseX, mouseY, true)){
						
						Tools_Fill_Glow.visible = false;
						Hide_Templates();
						Selected_Scroll_Brush = 0;
						Tools_Menu.y = 670;
						Show_Templates(Selectable_Brushes[Selected_Scroll_Brush]);			
						Scroll_Template(Selectable_Brushes[Selected_Scroll_Brush], 0);
						Tools_Mode = "Tool_Selection";
						Tools_Scaler.visible = false;
						
						Tutorial_Phase = 6;
						Kill_Hand();
						if(SFX_T5.isRunning){SFX_T5.Stop();}
						SFX_T6.Start();		
						
						Go_Hand(306, 563, 306, 688, 90);
						
					}
				}break;
				
				case 6:{
					if(Selectable_Brushes[Selected_Scroll_Brush][4][0].hitTestPoint(mouseX, mouseY, true)){
						
						Hide_Templates();
						Tools_Fill_Glow.visible 			   = false;
						Scroller_Display.getChildAt(1).visible = false;
						Scroller_Display.getChildAt(2).visible = false;
						Scroller_Display.getChildAt(3).visible = false;
						Scroller_Display.getChildAt(4).visible = false;
						
						Scroller_Display.getChildAt(1).visible = true;
						Select_Brush("Brush_0");
						Selected_Scroll_Brush = 1;
						Tools_Scaler.visible = true;
						Tools_Menu.y = 712;
						Show_Templates(Selectable_Brushes[Selected_Scroll_Brush]);	
						Tools_Mode = "Color_Selection";
						
						Tutorial_Phase = 7;
						Kill_Hand();
						if(SFX_T6.isRunning){SFX_T6.Stop();}
						SFX_T7.Start();		
						
						Go_Hand(281, 600, 844, 600, 90);
						
					}
				}break;
				
				case 7:{
					
					if(Click_Colors(Selectable_Brushes[Selected_Scroll_Brush])){
						Scroll_Template(Selectable_Brushes[Selected_Scroll_Brush], 0);							
						Kill_Hand();
						if(SFX_T7.isRunning){SFX_T7.Stop();}						
						SFX_T4.Start();		
						Tutorial_Phase = 99;
						TweenLite.delayedCall(5, Tutorial_Draw_Wait );
					}
					
				}break;
				
				case 99:{
					if(Select_Draw_Peice()){
						Add_Undo("Bitmap");
						if(Selected_Brush != "Brush_4"){
							if(Hud_Visible){ShowHide_Hud();}
						}
						we_Painting = true;						
						Mouse_Old_Pos.x = scene_mc.Pages.mouseX;
						Mouse_Old_Pos.y = scene_mc.Pages.mouseY;
					}					
				}break;
				
			}
			return true;
		}
		private function unClick_Tutorial():Boolean{
			if(Tutorial_Phase == -1){return false;}
			switch(Tutorial_Phase){
				case 99:{
					if(we_Painting){we_Painting = false;}	
					if(!Hud_Visible){ShowHide_Hud();}
				}break;
			}
			return true;
		}
		private function Tutorial_Draw_Wait():void{
			if(Tutorial_Phase == 99){
				if(we_Painting){we_Painting = false;}	
				if(!Hud_Visible){ShowHide_Hud();}
				Tutorial_Phase = 10;
			}
		}
		private function Kill_Tutorial():void{			
			TweenLite.killDelayedCallsTo(Tutorial_Draw_Wait);
			if(we_Painting){we_Painting = false;}	
			if(!Hud_Visible){ShowHide_Hud();}
			TweenLite.killTweensOf(Tutorial_Hand);
			TweenLite.killTweensOf(Tutorial_Skip_Button);
			Tutorial_Hand.visible 		 = false;
			Tutorial_Skip_Button.visible = false;
			
			Tools_Fill_Glow.visible = false;
			Hide_Templates();
			Selected_Scroll_Brush = 0;
			Tools_Menu.y = 670;
			Show_Templates(Selectable_Brushes[Selected_Scroll_Brush]);			
			Scroll_Template(Selectable_Brushes[Selected_Scroll_Brush], 0);
			Tools_Mode = "Tool_Selection";
			Tools_Scaler.visible = false;
			
			if(SFX_T1.isRunning){SFX_T1.Stop();}
			if(SFX_T2.isRunning){SFX_T2.Stop();}
			if(SFX_T3.isRunning){SFX_T3.Stop();}
			if(SFX_T4.isRunning){SFX_T4.Stop();}
			if(SFX_T5.isRunning){SFX_T5.Stop();}
			if(SFX_T6.isRunning){SFX_T6.Stop();}
			if(SFX_T7.isRunning){SFX_T7.Stop();}
			if(SFX_T8.isRunning){SFX_T8.Stop();}
			if(SFX_T9.isRunning){SFX_T9.Stop();}
			if(SFX_T10.isRunning){SFX_T10.Stop();}
			if(SFX_T11.isRunning){SFX_T11.Stop();}
			if(SFX_T12.isRunning){SFX_T12.Stop();}
			
			Tutorial_Phase = -1;	
			
		}		
		private function Go_Hand(sX:int, sY:int, eX:int, eY:int, Rotation:int):void{
			TweenLite.killTweensOf(Tutorial_Hand);
			Tutorial_Hand.alpha   = 0.0;
			Tutorial_Hand.visible = true;
			Tutorial_Hand.x = sX;
			Tutorial_Hand.y = sY;
			if(Rotation == 180){
				Tutorial_Hand.scaleX = -1;
				Tutorial_Hand.rotation = 0;
			}else{
				Tutorial_Hand.scaleX = 1;
				Tutorial_Hand.rotation = Rotation;
			}
			
			TweenLite.to(Tutorial_Hand, 2, {x:eX, y:eY, alpha:1.0, onComplete:Hand_In, onCompleteParams:[sX, sY, eX, eY]});
		}
		private function Hand_In(sX:int, sY:int, eX:int, eY:int):void{
			TweenLite.to(Tutorial_Hand, 2, {x:sX, y:sY, onComplete:Hand_Out, onCompleteParams:[sX, sY, eX, eY]});
		}
		private function Hand_Out(sX:int, sY:int, eX:int, eY:int):void{
			TweenLite.to(Tutorial_Hand, 2, {x:eX, y:eY, onComplete:Hand_In,  onCompleteParams:[sX, sY, eX, eY]});
		}
		private function Kill_Hand():void{
			TweenLite.killTweensOf(Tutorial_Hand);
			TweenLite.to(Tutorial_Hand, 1, {alpha:0.0, onComplete:Hand_Dead});
		}
		private function Hand_Dead():void{
			TweenLite.killTweensOf(Tutorial_Hand);
			Tutorial_Hand.x       = -900;
			Tutorial_Hand.y       = -900;
			Tutorial_Hand.visible = false;
		}
		//---------------------------------------------------------------------	
		
		
		
		//---------------------------------------------------------------------
		//Multy Touch for Ipad.
		//---------------------------------------------------------------------
		private function Initialize_Touch():void{
			Touch_1_ID 	= -1;
			Touch_1_Pos = new Point(0, 0);
			Touch_2_ID 	= -1;
			Touch_2_Pos = new Point(0, 0);
		}
		private function Destroy_Touch():void{
			Touch_1_Pos = null;
			Touch_2_Pos = null;
		}
		private function onTouchBegin(e:TouchEvent):void{					
						
			//Check if full up.
			if((Touch_1_ID != -1) && (Touch_2_ID != -1)){return;}		
			
			if(Touch_1_ID != -1){				
				Touch_2_ID 		= e.touchPointID;
				Touch_2_Pos.x 	= e.stageX; 
				Touch_2_Pos.y 	= e.stageY; 
				
				if(Zoomed){
					if(we_Painting){
						if(Zoom_Last_Move == null){Zoom_Last_Move = new Point(0, 0);}
						Zoom_Last_Move.x = Touch_1_Pos.x;
						Zoom_Last_Move.y = Touch_1_Pos.y;
						we_Painting = false;
						if(!Hud_Visible){ShowHide_Hud();}
						Draw_Undo();		
					}
				}
				
				
				return;
			}else{				
				Touch_1_ID 		= e.touchPointID;
				Touch_1_Pos.x 	= e.stageX; 
				Touch_1_Pos.y 	= e.stageY;
				return;				
			}			
		}
		private function onTouchMove(e:TouchEvent):void{						
			if(Touch_1_ID == e.touchPointID){
				Touch_1_Pos.x 	= e.stageX; 
				Touch_1_Pos.y 	= e.stageY;	
			}			
			if(Touch_2_ID == e.touchPointID){
				Touch_2_Pos.x 	= e.stageX; 
				Touch_2_Pos.y 	= e.stageY;	
			}			
		} 
		private function onTouchEnd(e:TouchEvent):void{				
			if(Touch_1_ID == e.touchPointID){
				Touch_1_ID 	  = -1;
				Touch_2_ID    = -1;			
				if(Tools_Mode == "Moving_Sticker"){MouseUp_Sticker();}				
			}
			if(Touch_2_ID == e.touchPointID){
				Touch_1_ID 	  = -1;
				Touch_2_ID    = -1;	
				if(Tools_Mode == "Moving_Sticker"){MouseUp_Sticker();}			
			}
		}		
		//---------------------------------------------------------------------	
				
	}
}
﻿package  
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.media.SoundTransform;
	import flash.text.TextFormatAlign;
	
	import asData.clsGlobalConstants;
	import asData.clsMsgboxDefaults;
	
	//import com.DaveLibrary.clsDeviceInfo;
	import com.DaveLibrary.clsGradientDynamicText;
	import com.DaveLibrary.clsStandardButton;
	import com.DaveLibrary.clsSupport;
	import com.DaveLibrary.clsTextData;
	
	import com.cupcake.DragTracker;
	import com.cupcake.TimerEx;
	import com.cupcake.Utils;
	import com.cupcake.VideoBox;
	
	import com.greensock.TweenLite;

	public class WBZ_VideoBox extends VideoBox
	{		
		private var cMain:clsMain;
		
		public function WBZ_VideoBox( Main:clsMain ){
			
			videoFolder = CONFIG::VIDEOS;
			
			cMain = Main;		
			
			init();
		}		
		public override function destroy():void{			
			super.destroy();
		}
		
		public override function init():void{
			
			width  = clsMain.localWidth;
			height = clsMain.localHeight;
			x      = -clsMain.offsetX;
			super.init();			
			onComplete = videoDone;			
		}		
		public override function playVideo():void{			
			//soundTrans = cMain.voiceTrans.sndTransform;
			super.playVideo();
		}		
		public function playWWRVideo( videoIndex:int ):void{			
			cMain.Paused = true;
			cMain.Modal.push(this);
			cMain.addChild( this );
			setVideo( videoIndex );			
			playVideo();
		}		
		
		private final function videoDone():void{			
			_playing = false;
			overrideDone();
		}			
		
		private function overrideDone():void{
			
			stopVideo();
			clsOptions.Save();			
			if(this.parent != null) {
				if(this.parent.contains(this)) this.parent.removeChild(this);
			}			
			if(cMain != null) {
				cMain.PageObjects.visible = true;
				cMain.cNavBar.visible = true;
				var Index:int = cMain.Modal.indexOf(this);
				if(Index >= 0) {
					cMain.Modal.splice(Index,1);
				}		
				cMain.Paused = (cMain.Modal.length > 0);				
				
			}			
			cMain.cMainMenu.Go_Animation();			
		}
	}
}

﻿package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.display.StageQuality;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.utils.Dictionary;
		
	import asData.clsGlobalConstants;
	
	import com.DaveLibrary.clsGradientDynamicText;
	import com.DaveLibrary.clsSound;
	import com.DaveLibrary.clsStandardButton;
	import com.DaveLibrary.clsSupport;
	
	import com.cupcake.App;
	import com.cupcake.Console;
	import com.cupcake.MemoryTracker;
	import com.cupcake.ReaderBox;
	import com.cupcake.Sequence;
	import com.cupcake.SequenceTracker;
	import com.cupcake.Utils;
	
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.ImageLoader;
	
	public class clsOptionsDialog extends Sprite 
	{
		private var cMain:clsMain;
		private var gc:clsGlobalConstants;
		private var gStrings:Dictionary;
		
		private var MouseIntercept:Sprite;
		private var Background:Sprite;
		private var BackgroundLoader:ImageLoader;
		
		private var txtTitle:clsGradientDynamicText;
		private var txtMusic:clsGradientDynamicText;
		private var txtShowMore:clsGradientDynamicText;
		private var txtSounds:clsGradientDynamicText;
		private var txtVoice:clsGradientDynamicText;
				
		private var bmMusicSlider:Bitmap;
		private var bmSoundsSlider:Bitmap;
		private var bmVoiceSlider:Bitmap;
		
		private var MusicSlider:Sprite;
		private var SoundsSlider:Sprite;
		private var VoiceSlider:Sprite;		
		
		private var bMusic:Boolean					= false;
		private var bSounds:Boolean					= false;
		private var bVoice:Boolean					= false;
		
		private var btnClose:clsStandardButton;
		private var btnShowMore:clsStandardButton;
				
		public function clsOptionsDialog(Main:clsMain) 
		{
			// constructor code			
			cMain = Main;
			gStrings = cMain.gStrings;
			gc = cMain.gc;
		}
		
		public function Show():void
		{
			Background = new Sprite;
			BackgroundLoader = new ImageLoader( CONFIG::DATA + GlobalData.appXML.Options.Background,
											    { container: Background, onComplete: BackgroundLoaded } );
			BackgroundLoader.load();
		}
		
		private function BackgroundLoaded( e:LoaderEvent ):void
		{
			var bmData:BitmapData;
			var img:Bitmap;
			
			// create the mouse intercept;
			MouseIntercept = new Sprite;			
			bmData = new BitmapData(1,1,false,0);
			img = new Bitmap(bmData)
			MouseIntercept.addChild(img);
			MouseIntercept.x = clsMain.APP_X;
			MouseIntercept.y = clsMain.APP_Y;
			MouseIntercept.width = clsMain.APP_WIDTH;
			MouseIntercept.height = clsMain.APP_HEIGHT;
			MouseIntercept.alpha = gc.msgboxDefaults.backgroundAlpha;
			this.addChild(MouseIntercept);
			
			Background.x = 512 - (Background.width/2);
			Background.y = 384 - (Background.height/2) + gc.optionsDialog_YOffset;
			this.addChild(Background);
			
			txtTitle = new clsGradientDynamicText(cMain, gStrings["OptionsDialog"]["Title"], gc.optionsDialog_Title);
			txtTitle.x = Background.x + (Background.width/2) + gc.optionsDialog_Title.offset.x;
			txtTitle.y = Background.y + gc.optionsDialog_Title.offset.y;
			this.addChild(txtTitle);
			
			txtMusic = new clsGradientDynamicText(cMain, gStrings["OptionsDialog"]["Music"], gc.optionsDialog_MusicText);
			txtMusic.x = Background.x + gc.optionsDialog_MusicText.offset.x;
			txtMusic.y = Background.y + gc.optionsDialog_MusicText.offset.y;
			this.addChild(txtMusic);
			
			txtShowMore = new clsGradientDynamicText(cMain, gStrings["OptionsDialog"]["ShowMore"], gc.optionsDialog_ShowMoreText);
			txtShowMore.x = Background.x + gc.optionsDialog_ShowMoreText.offset.x;
			txtShowMore.y = Background.y + gc.optionsDialog_ShowMoreText.offset.y;
			this.addChild(txtShowMore);
			
			txtSounds = new clsGradientDynamicText(cMain, gStrings["OptionsDialog"]["Sound"], gc.optionsDialog_SoundsText);
			txtSounds.x = Background.x + gc.optionsDialog_SoundsText.offset.x;
			txtSounds.y = Background.y + gc.optionsDialog_SoundsText.offset.y;
			this.addChild(txtSounds);
			
			txtVoice = new clsGradientDynamicText(cMain, gStrings["OptionsDialog"]["Voice"], gc.optionsDialog_VoiceText);
			txtVoice.x = Background.x + gc.optionsDialog_VoiceText.offset.x;
			txtVoice.y = Background.y + gc.optionsDialog_VoiceText.offset.y;
			this.addChild(txtVoice);
			
			MusicSlider = new Sprite();
			bmMusicSlider = new Bitmap( GlobalData.getSheetBitmap( "Options_Slider" ) );
			bmMusicSlider.x = 0 - (bmMusicSlider.width/2);
			bmMusicSlider.y = 0 - (bmMusicSlider.height/2);
			MusicSlider.addChild(bmMusicSlider);
			MusicSlider.x = Background.x + gc.optionsDialog_Sliders_MinX;
			MusicSlider.y = Background.y + gc.optionsDialog_MusicSlider;
			this.addChild(MusicSlider);
			Utils.addHandler( MusicSlider, MouseEvent.MOUSE_DOWN, MusicSlider_MouseDown);
			
			SoundsSlider = new Sprite();
			bmSoundsSlider = new Bitmap( GlobalData.getSheetBitmap( "Options_Slider" ) );
			bmSoundsSlider.x = 0 - (bmSoundsSlider.width/2);
			bmSoundsSlider.y = 0 - (bmSoundsSlider.height/2);
			SoundsSlider.addChild(bmSoundsSlider);
			SoundsSlider.x = Background.x + gc.optionsDialog_Sliders_MinX;
			SoundsSlider.y = Background.y + gc.optionsDialog_SoundsSlider;
			this.addChild(SoundsSlider);
			Utils.addHandler( SoundsSlider, MouseEvent.MOUSE_DOWN, SoundsSlider_MouseDown);
			
			VoiceSlider = new Sprite();
			bmVoiceSlider = new Bitmap( GlobalData.getSheetBitmap( "Options_Slider" ) );
			bmVoiceSlider.x = 0 - (bmVoiceSlider.width/2);
			bmVoiceSlider.y = 0 - (bmVoiceSlider.height/2);
			VoiceSlider.addChild(bmVoiceSlider);
			VoiceSlider.x = Background.x + gc.optionsDialog_Sliders_MinX;
			VoiceSlider.y = Background.y + gc.optionsDialog_VoiceSlider;
			this.addChild(VoiceSlider);
			Utils.addHandler( VoiceSlider, MouseEvent.MOUSE_DOWN, VoiceSlider_MouseDown);
			
			btnShowMore = new clsStandardButton(cMain, "", gc.optionsDialog_CloseButton, GlobalData.getSheetBitmap("Options_Checkmark"), GlobalData.getSheetBitmap("Options_Checkmark"), btnShowMore_MouseUp );
			btnShowMore.x = Background.x + gc.optionsDialog_ShowMoreApps.x;
			btnShowMore.y = Background.y + gc.optionsDialog_ShowMoreApps.y;
			btnShowMore.alpha = clsOptions.showMoreApps ? 1 : 0;
			this.addChild( btnShowMore );
			
			btnClose = new clsStandardButton(cMain, gStrings["OptionsDialog"]["Close"], gc.optionsDialog_CloseButton, GlobalData.getSheetBitmap( "Dialog_Button_Normal_s" ), GlobalData.getSheetBitmap( "Dialog_Button_Normal_h" ), btnClose_MouseUp, null, null, null, true, false);
			btnClose.x = Background.x + (Background.width/2) + gc.optionsDialog_CloseButtonOffset.x;
			btnClose.y = Background.y + Background.height + gc.optionsDialog_CloseButtonOffset.y;
			this.addChild(btnClose);

			MusicSlider.x = Background.x + gc.optionsDialog_Sliders_MinX + ((gc.optionsDialog_Sliders_MaxX - gc.optionsDialog_Sliders_MinX) * ( clsOptions.musicVolume / .5 ));
			SoundsSlider.x = Background.x + gc.optionsDialog_Sliders_MinX + ((gc.optionsDialog_Sliders_MaxX - gc.optionsDialog_Sliders_MinX) * ( clsOptions.soundVolume / .5 ));
			VoiceSlider.x = Background.x + gc.optionsDialog_Sliders_MinX + ((gc.optionsDialog_Sliders_MaxX - gc.optionsDialog_Sliders_MinX) * ( clsOptions.voiceVolume / 2));
			
			cMain.addEnterFrame(ENTER_FRAME);
			cMain.addStageMouseUp(Stage_MouseUp);
			
			btnClose.Enabled = true;
			
			cMain.Paused = true;
			cMain.Modal.push(this);			
			cMain.addChild(this);
			
			SequenceTracker.Clear();
			SequenceTracker.AddSequence( "aabac", ToggleStats );
			SequenceTracker.AddSequence( "cabba", ToggleConsole );
			SequenceTracker.AddSequence( "bcacb", ToggleNav );
			SequenceTracker.AddSequence( "aaabc", ToggleLang );
			SequenceTracker.AddSequence( "ccccc", DumpMoreApps );
			SequenceTracker.AddSequence( "aaaaa", ShowMemory );
			
			Utils.addHandler( txtSounds, MouseEvent.MOUSE_DOWN, TapSounds );
			txtSounds.mouseEnabled = true;
			
			Utils.addHandler( txtVoice, MouseEvent.MOUSE_DOWN, TapVoice );
			txtVoice.mouseEnabled = true;
			
			Utils.addHandler( txtMusic, MouseEvent.MOUSE_DOWN, TapMusic );
			txtMusic.mouseEnabled = true;

			GlobalData.playSound( "HUD_Options" );
		}
		
		public function destroy():void
		{
			cMain.removeEnterFrame(ENTER_FRAME);
			cMain.removeStageMouseUp(Stage_MouseUp);
			
			Utils.removeHandler( MusicSlider, MouseEvent.MOUSE_DOWN, MusicSlider_MouseDown);
			Utils.removeHandler( SoundsSlider, MouseEvent.MOUSE_DOWN, SoundsSlider_MouseDown);
			Utils.removeHandler( VoiceSlider, MouseEvent.MOUSE_DOWN, VoiceSlider_MouseDown);
			
			Utils.removeHandler( txtMusic, MouseEvent.MOUSE_DOWN, TapMusic );
			Utils.removeHandler( txtSounds, MouseEvent.MOUSE_DOWN, TapSounds );
			Utils.removeHandler( txtVoice, MouseEvent.MOUSE_DOWN, TapVoice );
				
			if(txtTitle != null) { txtTitle.destroy(); txtTitle = null; }
			if(txtMusic != null) { txtMusic.destroy(); txtMusic = null; }
			if(txtShowMore != null) { txtShowMore.destroy(); txtShowMore = null; }
			if(txtSounds != null) { txtSounds.destroy(); txtSounds = null; }
			if(txtVoice != null) { txtVoice.destroy(); txtVoice = null; }
			
			if ( bmMusicSlider != null ) { bmMusicSlider.bitmapData.dispose(); bmMusicSlider = null; }
			if ( bmSoundsSlider != null ) { bmSoundsSlider.bitmapData.dispose(); bmSoundsSlider = null; }
			if ( bmVoiceSlider != null ) { bmVoiceSlider.bitmapData.dispose(); bmVoiceSlider = null; }
			
			if(MouseIntercept != null) 
			{
				while(MouseIntercept.numChildren > 0) { MouseIntercept.removeChildAt(0); }
				MouseIntercept = null;
			}
			
					
			if(MusicSlider != null) 
			{
				while(MusicSlider.numChildren > 0) { MusicSlider.removeChildAt(0); }
				MusicSlider = null;
			}
			
		
			if(SoundsSlider != null) 
			{
				while(SoundsSlider.numChildren > 0) { SoundsSlider.removeChildAt(0); }
				SoundsSlider = null;
			}
			
		
			if(VoiceSlider != null) 
			{
				while(VoiceSlider.numChildren > 0) { VoiceSlider.removeChildAt(0); }
				VoiceSlider = null;
			}
			
			if ( btnClose != null )  { btnClose.destroy(); btnClose = null; }
			if ( btnShowMore != null ) { btnShowMore.destroy(); btnShowMore = null; }
			
			cMain = null;
			gc = null;
			gStrings = null;
			
			SequenceTracker.Clear();
			BackgroundLoader.dispose(true);
		}
		
		public function Hide():void
		{
			cMain.removeEnterFrame(ENTER_FRAME);
			cMain.removeStageMouseUp(Stage_MouseUp);
						
			clsOptions.Save();
			
			if(cMain != null) {
				var Index:int = cMain.Modal.indexOf(this);
				if(Index >= 0) {
					cMain.Modal.splice(Index,1);
				}		
				cMain.Paused = (cMain.Modal.length > 0);
				
				cMain.Options_Changed();
			}
			
			cMain.destroyOptions();
		}
		
		private function ENTER_FRAME(e:Event):void
		{
			var x:int;
			x = stage.mouseX;
			if(x < Background.x + gc.optionsDialog_Sliders_MinX) x = Background.x + gc.optionsDialog_Sliders_MinX;
			if(x > Background.x + gc.optionsDialog_Sliders_MaxX) x = Background.x + gc.optionsDialog_Sliders_MaxX;
			
			if(bMusic) {
				MusicSlider.x = x;
				clsOptions.musicVolume = (((x - Background.x) - gc.optionsDialog_Sliders_MinX) / (gc.optionsDialog_Sliders_MaxX - gc.optionsDialog_Sliders_MinX)) * .5;
				App.musicVolume.volume = clsOptions.musicVolume;
			}
			
			if(bSounds) {
				SoundsSlider.x = x;
				clsOptions.soundVolume = (((x - Background.x) - gc.optionsDialog_Sliders_MinX) / (gc.optionsDialog_Sliders_MaxX - gc.optionsDialog_Sliders_MinX)) * .5;
				App.soundVolume.volume = clsOptions.soundVolume;
			}
			
			if(bVoice) {
				VoiceSlider.x = x;
				clsOptions.voiceVolume = (((x - Background.x) - gc.optionsDialog_Sliders_MinX) / (gc.optionsDialog_Sliders_MaxX - gc.optionsDialog_Sliders_MinX)) * 2;
				App.voiceVolume.volume = clsOptions.voiceVolume;
			}
		}
		
		private function Stage_MouseUp(e:MouseEvent):void
		{
			bMusic = false;
			bSounds = false;
			bVoice = false;
		}
		
		private function MusicSlider_MouseDown(e:MouseEvent):void { bMusic = true; }
		private function SoundsSlider_MouseDown(e:MouseEvent):void { bSounds = true; }
		private function VoiceSlider_MouseDown(e:MouseEvent):void { bVoice = true; }
		
		private function btnShowMore_MouseUp( e:MouseEvent ):void
		{
			GlobalData.playSound( "Button_Generic" );
			clsOptions.showMoreApps = !clsOptions.showMoreApps;
			btnShowMore.alpha = clsOptions.showMoreApps ? 1 : 0;
			clsOptions.Save();
		}
		
		private function btnClose_MouseUp(e:MouseEvent):void
		{
			GlobalData.playSound( "Button_Generic" );
			Hide();
			e.stopImmediatePropagation();
		}
		
		private function TapMusic( e:MouseEvent ) { SequenceTracker.CheckSequence( "a" ); }
		private function TapSounds( e:MouseEvent ) { SequenceTracker.CheckSequence( "b" ); }
		private function TapVoice( e:MouseEvent ) { SequenceTracker.CheckSequence( "c" ); }
		
		private function ToggleConsole( e:Sequence ):void 
			{ Console.toggleButton(); Console.toggleConsole(); }
		
		private function ToggleStats( e:Sequence ):void  { cMain.toggleStats(); }
		
		private function ToggleNav( e:Sequence ):void 
			{ cMain.cNavBar.alpha = cMain.cNavBar.alpha == 1 ? 0 : 1; }
		
		private function ToggleLang( e:Sequence ):void
		{
			CONFIG::DEBUG
			{
				var newLang:String = clsOptions.language == 'en' ? 'dt' : 'en';
				Hide();
				clsMain.setLanguage( newLang );
			}
		}
		
		private function DumpMoreApps( e:Sequence ):void
		{
			App.Log( "Dumping local moreapps xml" );
			Utils.deleteFile( "moreapps_" + clsOptions.language + ".xml" );
		}
		
		private function ShowMemory( e:Sequence ):void { MemoryTracker.gcAndCheck(); }
	}
}
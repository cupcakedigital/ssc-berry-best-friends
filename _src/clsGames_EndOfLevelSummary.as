﻿package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.utils.Dictionary;

	import asData.clsGlobalConstants;
	
	import com.DaveLibrary.clsGradientDynamicText;
	import com.DaveLibrary.clsSupport;
	import com.DaveLibrary.clsStandardButton;
	
	import com.cupcake.App;
	import com.cupcake.MouseIntercept;
	import com.cupcake.SoundEx;
	import com.cupcake.Utils;
	
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.ImageLoader;
	
	public class clsGames_EndOfLevelSummary extends Sprite 
	{
		private var cMain:clsMain;
		private var gc:clsGlobalConstants;
		private var gStrings:Dictionary;
		
		private var Game:String;
		
		private var mouseIntercept:MouseIntercept;
		private var Background:Sprite;
		private var BackgroundLoader:ImageLoader;
		private var txtMessage:clsGradientDynamicText;
		private var txtProceed:clsGradientDynamicText;
		
		private var textSparkler:Sparkler;
		private var buttonSparkler:Sparkler;
		private var sndCongrats:SoundEx;
		
		private var btnClose:clsStandardButton;
				
		public var returnToLastPage:Boolean;
		
		public function clsGames_EndOfLevelSummary(Main:clsMain, GameName:String, ReturnToLastPage:Boolean, Congrats:SoundEx = null) 
		{
			cMain = Main;
			gc = cMain.gc;
			gStrings = cMain.gStrings;
			
			Game = GameName;
			
			// create the mouse intercept;
			var bmData:BitmapData;
			
			mouseIntercept = new MouseIntercept( this );			
			
			Background = new Sprite;
			BackgroundLoader = new ImageLoader( CONFIG::DATA + "images/global/EndOfLevel_Background.png", { container: Background, onComplete: ShowSummary } );
			BackgroundLoader.load();
			this.addChild(Background);
			
			sndCongrats = Congrats;
			returnToLastPage = ReturnToLastPage;
		}
			
		private function ShowSummary( e:LoaderEvent ):void
		{
			Background.x = 512 - (Background.width/2);
			Background.y = 384 - (Background.height/2);
			
			var dropShadow:DropShadowFilter = new DropShadowFilter(6,45,0,.8,6,6,1,3);
			var stroke:GlowFilter = new GlowFilter( 0x0f6da3, 1, 5, 5, 20, 3 );
			
			txtMessage = new clsGradientDynamicText(cMain, gStrings["Pages"][Game + "_EndOfLevel_Message"], gc.pages_Games_EndOfLevel_Line);
			txtMessage.Image = Utils.replaceDisplayObjectWithBitmap( txtMessage.Image, 1, false, [stroke,dropShadow] );
			txtMessage.x = Background.x + (Background.width/2);
			txtMessage.y = Background.y + gc.pages_Games_EndOfLevel_Line.offset.y + 35;
			this.addChild(txtMessage);
			txtMessage.mouseEnabled = true;
			Utils.addHandler( txtMessage, MouseEvent.MOUSE_DOWN, btnClose_MouseUp );
			
			txtProceed = new clsGradientDynamicText(cMain, gStrings["Pages"][Game + "_EndOfLevel_Proceed"], gc.pages_Games_EndOfLevel_Proceed);
			txtProceed.x = Background.x + gc.pages_Games_EndOfLevel_Proceed.offset.x;
			txtProceed.y = Background.y + gc.pages_Games_EndOfLevel_Proceed.offset.y - 45;
			txtProceed.visible = false;
			this.addChild(txtProceed);
			txtProceed.mouseEnabled = true;
			Utils.addHandler( txtProceed, MouseEvent.MOUSE_DOWN, btnClose_MouseUp );

			btnClose = new clsStandardButton(cMain, "", gc.pages_Games_EndOfLevel_Close, GlobalData.getSheetBitmap( "EndOfLevel_Arrow" ), GlobalData.getSheetBitmap( "EndOfLevel_Arrow" ), btnClose_MouseUp, null, null, null, true, false);
			btnClose.x = Background.x + gc.pages_Games_EndOfLevel_Close.textInfo.offset.x;
			btnClose.y = Background.y + gc.pages_Games_EndOfLevel_Close.textInfo.offset.y - 85;
			this.addChild(btnClose);
			
			//textSparkler = new Sparkler( txtProceed, 10 );
			//buttonSparkler = new Sparkler( btnClose );
			
			//textSparkler.Start();
			//buttonSparkler.Start();
			sndCongrats.Start();		
		}
		
		public function destroy():void
		{			
			clsSupport.DisposeOfSprite(this);
			Background = null;
			if ( BackgroundLoader != null ) { BackgroundLoader.dispose(true); BackgroundLoader = null; }
			
			if ( txtMessage != null ) { txtMessage.destroy(); txtMessage = null; }
			if ( txtProceed != null ) { txtProceed.destroy(); txtProceed = null; }
			if ( btnClose != null ) { btnClose.destroy(); btnClose = null; }
			
			if ( textSparkler != null ) { textSparkler.Destroy(); textSparkler = null; }
			if ( buttonSparkler != null ) { buttonSparkler.Destroy(); buttonSparkler = null; }
			if ( sndCongrats != null ) { sndCongrats.Stop(); }
			
			Utils.removeHandler( txtProceed, MouseEvent.MOUSE_DOWN, btnClose_MouseUp );
			Utils.removeHandler( txtMessage, MouseEvent.MOUSE_DOWN, btnClose_MouseUp );
			
		}
		
		public function Stop():void { if ( sndCongrats != null ) sndCongrats.Stop(); }
		
		private function btnClose_MouseUp(e:MouseEvent):void
		{
			btnClose.Enabled = false;
			if(returnToLastPage) 
			{
				if ( cMain.ReturnPage == -1 ) { cMain.PrevPage( -1 ); }
					else { cMain.NextPage( cMain.ReturnPage ); }
			}
			else { cMain.NextPage(); }
		}		
	}
}
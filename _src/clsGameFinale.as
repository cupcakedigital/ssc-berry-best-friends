﻿package  {
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.*;
	import flash.geom.Vector3D;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.geom.Rectangle;
	import flash.display.Shape;
	
	import com.DaveLibrary.clsSupport;	
	import com.DaveLibrary.clsTimerEX;
	import com.DaveLibrary.clsGradientDynamicText;
	
	import asData.clsGlobalConstants;
	import com.DaveLibrary.clsSize;
	import com.DaveLibrary.clsTextData;
	import com.DaveLibrary.clsSound;
	
	import com.cupcake.App;
	
	public class clsGameFinale extends Sprite 
	{
		private var cMain:clsMain;
		
		public var FinaleText:clsGradientDynamicText;
		private var Start_Timer:clsTimerEX;
		public var Animate:Boolean;
		
		public var _Callback:Function;
		
		//public var FinaleStarbursts:Vector.<clsParticleBurst>;
		//public var FinaleStarBurstTimer:clsTimerEX;

		public function clsGameFinale(Main:clsMain, Text:String, TextData:clsTextData, Callback:Function = null) 
		{
			// constructor code
			
			cMain = Main;
			_Callback = Callback;
			
			FinaleText = new clsGradientDynamicText(cMain, Text, TextData,false, null, "best", true, true);			
			this.addChild(FinaleText);
			
			Start_Timer = new clsTimerEX(cMain.gc.pages_GameFinale_Pause, Start_Timer_Tick);		
			
			//FinaleStarbursts = new Vector.<clsParticleBurst>();
			//FinaleStarBurstTimer = new clsTimerEX(0, FinaleStarBurstTimer_Tick);
			
			Reset();
		}
		
		public function destroy():void
		{
			cMain.removeEnterFrame(ENTER_FRAME);
			clsSupport.DisposeOfSprite(this);
			
			/*if ( FinaleStarBurstTimer != null ) 
				{ FinaleStarBurstTimer.destroy(); FinaleStarBurstTimer = null; }
			*/
			if(FinaleText != null) { FinaleText.destroy(); FinaleText = null; }
			if(Start_Timer != null) { Start_Timer.Kill(); Start_Timer = null; }
			/*
			while(FinaleStarbursts.length > 0) {
				if(FinaleStarbursts[0].parent != null) {
					if(FinaleStarbursts[0].parent.contains(FinaleStarbursts[0])) {
						FinaleStarbursts[0].parent.removeChild(FinaleStarbursts[0]);
					}
				}*/
				//FinaleStarbursts[0].destroy();
				//FinaleStarbursts.shift();
			
			//FinaleStarbursts = null;
			
			_Callback = null;
		}
		
		public function Reset():void
		{
			/*while(FinaleStarbursts.length > 0) {
				if(FinaleStarbursts[0].parent != null) {
					if(FinaleStarbursts[0].parent.contains(FinaleStarbursts[0])) {
						FinaleStarbursts[0].parent.removeChild(FinaleStarbursts[0]);
					}
				}*/
				//FinaleStarbursts[0].stop();
				//FinaleStarbursts[0].destroy();
				//FinaleStarbursts.shift();
			
			
			//FinaleStarbursts.length = 0;
			Animate = false;
			this.alpha = 1;
			this.visible = false;
		}
		
		public function StartTimer():void
		{
			this.visible = true;
			this.alpha = 1;
			Animate = false;
			//FinaleStarBurstTimer.delay = clsSupport.randomEX(cMain.gc.pages_GameFinale_Particles_Delay_Min, cMain.gc.pages_GameFinale_Particles_Delay_Max);
			//FinaleStarBurstTimer.start();
			Start_Timer.start();
			
			GlobalData.playSound( "Game_Complete" );
			GlobalData.playSound( "Game_Adults_Cheer" );
		}
		
		public function Stop():void
		{
			Start_Timer.stop();
			cMain.removeEnterFrame(ENTER_FRAME);
		}
		
		private function Start_Timer_Tick(e:TimerEvent):void
		{
			Start_Timer.stop();
			Animate = true;
			cMain.addEnterFrame(ENTER_FRAME);
		}
		
		private function ENTER_FRAME(e:Event):void
		{
			if(cMain.Paused) return;
			
			if(Animate) 
			{
				if(this.alpha - cMain.gc.pages_GameFinale_FadeOutSpeed > 0) 
				{
					this.alpha = this.alpha - cMain.gc.pages_GameFinale_FadeOutSpeed;
				} 
				else 
				{
					if(_Callback != null) _Callback();
					Animate = false;
					this.visible = false;
					//FinaleStarBurstTimer.stop();
					cMain.removeEnterFrame(ENTER_FRAME);
				}				
			}
		}
		
		
/*		private function FinaleStarBurstTimer_Tick(e:TimerEvent):void
		{
			if(cMain.Paused) return;
			if(FinaleText == null) return;
			if(FinaleStarBurstTimer == null) return;
			if(FinaleStarbursts == null) return;

			FinaleStarBurstTimer.delay = clsSupport.randomEX(cMain.gc.pages_GameFinale_Particles_Delay_Min, cMain.gc.pages_GameFinale_Particles_Delay_Max);

			var r:Rectangle = FinaleText.getBounds(this);

			r.x = r.x + cMain.gc.pages_GameFinale_ParticleBurst.spawnRect.x;
			r.y = r.y + cMain.gc.pages_GameFinale_ParticleBurst.spawnRect.y;
			r.width = r.width + cMain.gc.pages_GameFinale_ParticleBurst.spawnRect.width;
			r.height = r.height + cMain.gc.pages_GameFinale_ParticleBurst.spawnRect.height;
			
			var sb:clsParticleBurst = new clsParticleBurst(cMain, cMain.gc.pages_GameFinale_ParticleBurst, GlobalData.getSheetBitmap( "Games_FinaleStar" ), StarBurstComplete);
			sb.x = clsSupport.randomEX(r.x, r.x + r.width);
			sb.y = clsSupport.randomEX(r.y, r.y + r.height);
			sb.visible = true;
			this.addChildAt(sb,0);
			sb.start();
			FinaleStarbursts.push(sb);
		}
		
		private function StarBurstComplete(sb:clsParticleBurst):void
		{
			if(FinaleText == null) return;
			if(FinaleStarBurstTimer == null) return;
			if(FinaleStarbursts == null) return;
			
			var index:int = FinaleStarbursts.indexOf(sb);
			sb.destroy();
			if(index >= 0) FinaleStarbursts.splice(index,1);
		}*/
	}
}
﻿package  {
	
	import com.greensock.loading.*;
	import com.greensock.events.LoaderEvent;
	import com.cupcake.ExternalImage;
	import flash.filesystem.*;
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.filters.*;
	import flash.filters.BitmapFilterQuality;
	import flash.geom.Matrix;	
	import flash.xml.*;	
	
	public class SheetTemplate {		
		
		private var File_Directory:String;
		private var File_Path:String;
		private var File_Name:String;
		
		private var queue:LoaderMax;
		
		private var The_Sheet:Sprite;
		private var The_SheetData:BitmapData;
		private var Sheet_Positions:Array;		
		
		private var CallBack:Function;
		
		//--------------------------------------------------------	
		//Initialize.
		//--------------------------------------------------------	
		public function SheetTemplate(ListAssets:Vector.<ExternalImage>, FileDir:String, FilePath:String, FileName:String, cBack:Function = null){			
			
			CallBack = cBack;
			
			File_Directory 	= FileDir;
			File_Path 		= FilePath;
			File_Name 		= FileName;
			
			//Image Holder.
			The_Sheet = new Sprite();						
			
			//Images.
			ListAssets.push(new ExternalImage((FilePath + FileName + ".png"), The_Sheet));
			
			//Prep XML.				
			queue = new LoaderMax( {name:File_Name, onComplete:XMLLoaded } );				
			
			//Load XML.
			LoadXML(File_Name);	
									
		}	
		//--------------------------------------------------------	
		
		//--------------------------------------------------------	
		//Call Functions.
		//--------------------------------------------------------	
		public function Frame_Count():int{return Sheet_Positions.length;}
		public function Get_Sprite(Frame:int = -1, Centered:Boolean = false, Effect:Boolean = false):Sprite{
			
			if(The_SheetData == null){
				The_SheetData = new BitmapData(The_Sheet.width, The_Sheet.height, true, 0x000000);
				The_SheetData.drawWithQuality(The_Sheet);
			}			
			
			var Return_Sprite:Sprite 		= new Sprite();			
			var New_Bitmap:Bitmap 			= new Bitmap();			
			New_Bitmap.smoothing 			= true;
			var Rectangle_Draw:Rectangle 	= new Rectangle(Sheet_Positions[Frame][0],
															Sheet_Positions[Frame][1],
															Sheet_Positions[Frame][2],
															Sheet_Positions[Frame][3]);
			
			New_Bitmap.bitmapData 			= new BitmapData(Sheet_Positions[Frame][2],
															 Sheet_Positions[Frame][3], true, 0x000000);
															 
			New_Bitmap.bitmapData.copyPixels(The_SheetData,
											 Rectangle_Draw,
											 new Point(0, 0),
											 null, null, true);			
			if(!Effect){ 
			
				if(Centered){
					New_Bitmap.x = -(New_Bitmap.width  / 2);
					New_Bitmap.y = -(New_Bitmap.height / 2);
				}
				
				Return_Sprite.addChild(New_Bitmap);			
			
				return Return_Sprite;
			
			}else{
				
				var TempSprite:Sprite = new Sprite();				
				TempSprite.addChild(New_Bitmap);	
				
				TempSprite.filters = [Get_drop_Shadow()];			
				
				var bounds:Rectangle = TempSprite.getBounds(TempSprite);
				var ReturnBitmap:Bitmap = new Bitmap();
				ReturnBitmap.bitmapData = new BitmapData(bounds.width + 30, bounds.height + 30, true, 0);			
				ReturnBitmap.bitmapData.draw(TempSprite, new Matrix(1, 0, 0, 1, 15, 15));
				
				if(Centered){
					ReturnBitmap.x = -(ReturnBitmap.width  / 2);
					ReturnBitmap.y = -(ReturnBitmap.height / 2);
				}				
				
				Return_Sprite.addChild(ReturnBitmap);					
				
				return Return_Sprite;
			
			}			
			
		}
		//--------------------------------------------------------			
		
		//--------------------------------------------------------		
		//Grabing XML.
		//--------------------------------------------------------		
		private function LoadXML(Box_Name:String):void{								
			queue.append( new XMLLoader( File_Directory + File_Path + File_Name + ".xml", { name:File_Name } ) );
			queue.load();			
		}		
		private function XMLLoaded( e:LoaderEvent ):void{
			var Sheet_XML:XML = queue.getContent( File_Name );		
			Parse_XML(Sheet_XML.toString());		
		}
		private function Parse_XML(The_String:String):void{
			
			Sheet_Positions 				= new Array();
			var Processing:Boolean 			= true;		
			var Gethering_Elements:Boolean  = false;	
			var Parse_String:String         = The_String;
			
			var iX:int;
			var iY:int;
			var iHeight:int;
			var iWidth:int;
			
			var Reuse_Int_1:int;
			var Reuse_Int_2:int;
			
			var Reuse_Str_1:String;
			
			var Safety_Count:int = 9000;
						
			do{
				
				iX 		= -1;
				iY 		= -1;
				iWidth 	= -1;
				iHeight = -1;				
				
				//Check if there is another element.
				if(Parse_String.indexOf('x=', 0) == -1){Processing = false;}
				else{	
						
					//trace("----------------------");
					//trace("Found Elements");
					
					Reuse_Int_1 = (Parse_String.indexOf('x=', 0) + 3);
					Reuse_Int_2 = Parse_String.indexOf('"', Reuse_Int_1);					
					iX 			= int(Parse_String.substr(Reuse_Int_1, (Reuse_Int_2 - Reuse_Int_1)));	
					//trace("Position X " + Parse_String.substr(Reuse_Int_1, (Reuse_Int_2 - Reuse_Int_1)));
					
					Reuse_Int_1 = (Parse_String.indexOf('y=', 0) + 3);
					Reuse_Int_2 = Parse_String.indexOf('"', Reuse_Int_1);
					iY 			= int(Parse_String.substr(Reuse_Int_1, (Reuse_Int_2 - Reuse_Int_1)));
					//trace("Position Y " + Parse_String.substr(Reuse_Int_1, (Reuse_Int_2 - Reuse_Int_1)));
					
					Reuse_Int_1 = (Parse_String.indexOf('width=', 0) + 7);
					Reuse_Int_2 = Parse_String.indexOf('"', Reuse_Int_1);
					iWidth 		= int(Parse_String.substr(Reuse_Int_1, (Reuse_Int_2 - Reuse_Int_1)));
					//trace("The Height " + Parse_String.substr(Reuse_Int_1, (Reuse_Int_2 - Reuse_Int_1)));
					
					Reuse_Int_1 = (Parse_String.indexOf('height=', 0) + 8);
					Reuse_Int_2 = Parse_String.indexOf('"', Reuse_Int_1);
					iHeight 	= int(Parse_String.substr(Reuse_Int_1, (Reuse_Int_2 - Reuse_Int_1)));
					//trace("The Width  " + Parse_String.substr(Reuse_Int_1, (Reuse_Int_2 - Reuse_Int_1)));
					
					//trace("----------------------");					
					
					Reuse_Str_1   = Parse_String.substr(Reuse_Int_2, Parse_String.length)
					Parse_String  = Reuse_Str_1;
					
					var New_Image:Array = new Array();
						New_Image.push(iX);
						New_Image.push(iY);
						New_Image.push(iWidth);
						New_Image.push(iHeight);
					
					Sheet_Positions.push(New_Image);
					
				}	
				
				Safety_Count = (Safety_Count - 1);
				
			}while(Processing && (Safety_Count > 0));		
			
			if(CallBack != null){CallBack();}
			
		}
		//--------------------------------------------------------				
		
		private function Get_drop_Shadow():DropShadowFilter{
			var color:Number = 0x000000;
            var angle:Number = 45;
            var alpha:Number = 0.5;
            var blurX:Number = 8;
            var blurY:Number = 8;
            var distance:Number = 15;
            var strength:Number = 0.5;
            var inner:Boolean = false;
            var knockout:Boolean = false;
            var quality:Number = BitmapFilterQuality.HIGH;
            return new DropShadowFilter(distance,
                                        angle,
                                        color,
                                        alpha,
                                        blurX,
                                        blurY,
                                        strength,
                                        quality,
                                        inner,
                                        knockout);
		}
		
	}
	
}

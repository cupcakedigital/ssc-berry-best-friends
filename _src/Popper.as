﻿package  
{
	import flash.display.InteractiveObject;
	import flash.events.TimerEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import com.cupcake.App;
	import com.cupcake.TweenedActor;
	import com.cupcake.Utils;
	
	import com.gskinner.motion.GTween;
	import com.gskinner.motion.easing.*;
	
	public dynamic class Popper extends TweenedActor 
	{
		public static const WAIT:int		= 0;
		public static const POP_OUT:int		= 1;
		public static const PAUSE:int		= 2;
		public static const POP_IN:int		= 3;
		
		public var cancel:Boolean			= false;
		public var endPoint:Point;
		public var startPoint:Point;
		public var minDelay:int;
		public var maxDelay:int;
		public var tapped:Boolean			= false;
		public var pauseTime:Number			= 3;
		public var popOutTime:Number		= 1;
		public var popInTime:Number			= .5;
		public var tapEnabled:Boolean		= true;
		public var tapFaster:Boolean		= true;
		
		public var onComplete:Function		= null;
		public var onHide:Function			= null;
		public var onPop:Function			= null;
		public var popOutEase:Function		= Sine.easeInOut;
		public var popInEase:Function		= Sine.easeInOut;
		
		public var hideSound:String			= "";
		public var showSound:String			= "";
		
		public function Popper( oActor:InteractiveObject, oEndPoint:Point, fOnTap:Function = null, fOnTimer:Function = null, iState:int = 0, autoStart = false ) 
		{
			super( oActor, fOnTap, fOnTimer, iState, autoStart );
			endPoint = oEndPoint;
		}
		
		public function doPause():void
		{
			if ( isRunning && !isPaused )
			{
				state = PAUSE;
				tween.ease = Linear.easeNone;
				tween.duration = pauseTime;
				tween.setValue( "x", endPoint.x + .1 );
			}
		}
		
		public function doTap():void
		{
			if ( isRunning && !isPaused )
			{
				tapped = true;
				switch ( state )
				{
					case PAUSE:
					case POP_OUT:
						popIn( true );
						break;
					
					case WAIT:
					case POP_IN:
						if ( onPop != null ) onPop( this );
						if ( cancel ) 
						{
							if ( state == WAIT ) Start();
							cancel = false;
						} else popOut( true );
						break;
				}
				tapped = false;
			}
		}
		
		public function popIn( fromTap:Boolean = false ):void
		{
			if ( isRunning && !isPaused )
			{
				state = POP_IN;
				if ( onHide != null ) onHide( this );
				if ( fromTap && hideSound != "" ) Utils.playSound( hideSound, App.soundVolume.volume );
				tween.ease = fromTap ? Linear.easeNone : popInEase;
				tween.duration = ( fromTap && tapFaster ) ? popInTime * .5 : popInTime;
				tween.setValue( "x", startPoint.x );
				tween.setValue( "y", startPoint.y );
			}
		}
		
		public function popOut( fromTap:Boolean = false ):void
		{
			if ( isRunning && !isPaused )
			{
				state = POP_OUT;
				if ( fromTap && showSound != "" ) Utils.playSound( showSound, App.soundVolume.volume );
				tween.ease = fromTap ? Linear.easeNone : popOutEase;
				tween.duration = popOutTime;
				tween.setValue( "x", endPoint.x );
				tween.setValue( "y", endPoint.y );
			}
		}
		
		// Standard public functions
		public override function Init():void
		{
			super.Init();
			tween.onComplete = tweenComplete;
		}
		
		public override function Pause():void
		{
			super.Pause();
			if ( isRunning )
			{
				switch ( state )
				{
					case PAUSE:
					case POP_IN:
					case POP_OUT:
						tween.paused = true;
						break;
					
					case WAIT:
						timer.pause();
						break;
				}
			}
		}
		
		public override function Reset():void
		{
			if ( startPoint == null ) { startPoint = new Point( actor.x, actor.y ); }
			actor.x = startPoint.x;
			actor.y = startPoint.y;
			state = WAIT;
			super.Reset();
		}
		
		public override function Resume():void
		{
			super.Resume();
			if ( isRunning )
			{
				switch ( state )
				{
					case PAUSE:
					case POP_IN:
					case POP_OUT:
						tween.paused = false;
						break;
					
					case WAIT:
						if ( maxDelay > 0 ) timer.resume();
						break;
				}
			}
		}
		
		public override function Start():void
		{
			super.Start();
			state = WAIT;
			if ( maxDelay > 0 )
			{
				timer.reset();
				timer.repeat( Utils.getRandomInt( minDelay, maxDelay ) );
				timer.start();
			}
		}
		
		// Private and protected functions
		protected override function actorTap( e:MouseEvent ):void
		{
			if ( isRunning && !isPaused && tapEnabled )
			{
				super.actorTap( e );
				doTap();
			}
		}
		
		protected override function timerTick( e:TimerEvent ):void
		{
			if ( isRunning && !isPaused )
			{
				super.timerTick( e );
				if ( onPop != null ) onPop( this );
				if ( cancel ) 
				{
					Start();
					cancel = false;
				} else popOut();
				
			}
		}
		
		protected function tweenComplete( e:GTween ):void
		{
			if ( isRunning && !isPaused )
			{
				switch ( state )
				{
					case POP_OUT:
						doPause();
						break;
					
					case PAUSE:
						popIn();
						break;
						
					case POP_IN:
						if ( onComplete != null ) onComplete( this );
						Start();
						break;
				}
			}
		}

	}
}

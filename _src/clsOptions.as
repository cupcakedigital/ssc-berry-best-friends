﻿package  
{
	import com.cupcake.App;
	import com.cupcake.Utils;
	
	public class clsOptions 
	{
		private static const soundMax:Number	= 0.5;
		private static const musicMax:Number	= 0.5;
		private static const voiceMax:Number	= 2.0;
		
		private static var Filename:String		= "Options.xml";
		private static var Version:int			= 12;
		
		public static var firstRun:Boolean		= false;
		
		public static var musicVolume:Number	= 0.1;
		public static var soundVolume:Number	= 0.5;
		public static var voiceVolume:Number	= 1.0;
		
		public static var bookCompleted:Boolean	= false;
		public static var interactive:Boolean	= true;
		public static var lastPage:int			= 0;
		public static var lastStaticPage:int	= 0;
		public static var language:String		= "en";
		public static var readToMe:Boolean		= true;
		public static var reviewHit:Boolean		= false;
		public static var reviewPrompts:int		= 0;
		
		public static var moreAppsVersion:int	= 0;
		public static var moreAppsShown:int		= 0;
		public static var showMoreApps:Boolean	= true;
		
		public static var pixelPostback:Boolean	= false;
		public static var deviceTracked:Boolean = false;
		
		{
			XML.ignoreComments = true;
			XML.ignoreProcessingInstructions = true;
			XML.ignoreWhitespace = true;
		}
		
		public static function Load():void
		{
			var str:String = Utils.loadStringFile( Filename );
			var xml:XML;
			var v:int;
			
			if ( str.length == 0 )
				{ Save(); firstRun = true; }
			else
			{
				xml = new XML(str);					
				v = xml.Version;
				soundVolume = xml.soundVolume != undefined ? xml.soundVolume : soundVolume;
				musicVolume = xml.musicVolume != undefined ? xml.musicVolume : musicVolume;
				voiceVolume = xml.voiceVolume != undefined ? xml.voiceVolume : voiceVolume;
				
				bookCompleted = xml.bookCompleted != undefined ? ( xml.bookCompleted == "1" ) : bookCompleted;
				interactive = xml.interactive != undefined ? ( xml.interactive == "1" ) : interactive;
				language = xml.language != undefined ? xml.language : language;
				lastPage = xml.lastPage != undefined ? xml.lastPage : lastPage;			
				lastStaticPage = xml.lastStaticPage != undefined ? xml.lastStaticPage : lastStaticPage;	
				readToMe = xml.readToMe != undefined ? ( xml.readToMe == "1" ) : readToMe;
				reviewHit = xml.reviewHit != undefined ? ( xml.reviewHit == "1" ) : reviewHit;
				reviewPrompts = xml.reviewPrompts != undefined ? xml.reviewPrompts : reviewPrompts; 
				
				moreAppsVersion = xml.moreAppsVersion != undefined ? xml.moreAppsVersion : moreAppsVersion;
				moreAppsShown = xml.moreAppsShown != undefined ? xml.moreAppsShown : moreAppsShown;
				showMoreApps = xml.showMoreApps != undefined ? ( xml.showMoreApps == "1" ) : showMoreApps;
				
				pixelPostback = xml.pixelPostback != undefined ? ( xml.pixelPostback == "1" ) : pixelPostback;
				deviceTracked = xml.deviceTracked != undefined ? ( xml.deviceTracked == "1" ) : deviceTracked;
				
				xml = null;
					
				if ( soundVolume > soundMax ) soundVolume = soundMax;
				if ( musicVolume > musicMax ) musicVolume = musicMax;
				if ( voiceVolume > voiceMax ) voiceVolume = voiceMax;
				
				// if the version in the file doesn't match that of this as file, then reset by default as the structure is different.
				if (v != Version) 
				{
					App.Log("Version in the options file (" + v + ") does not match that of the action script file (" + Version + ")... options have been saved");
					Save();
				}
			}
		}
		
		public static function Save():void
		{
			var s:String = "<Options>";
			
			s += "\t<Version>" + Version.toString() + "</Version>\n";
			s += "\t<soundVolume>" + soundVolume.toString() + "</soundVolume>\n";
			s += "\t<musicVolume>" + musicVolume.toString() + "</musicVolume>\n";
			s += "\t<voiceVolume>" + voiceVolume.toString() + "</voiceVolume>\n";
			
			s += "\t<bookCompleted>" + ( bookCompleted ? "1" : "0" ) + "</bookCompleted>\n";
			s += "\t<interactive>" + ( interactive? "1" : "0" ) + "</interactive>\n";
			s += "\t<language>" + language + "</language>\n";
			s += "\t<lastPage>" + lastPage.toString() + "</lastPage>\n";
			s += "\t<lastStaticPage>" + lastStaticPage.toString() + "</lastStaticPage>\n";
			s += "\t<readToMe>" + ( readToMe ? "1" : "0" ) + "</readToMe>\n";
			s += "\t<reviewHit>" + ( reviewHit ? "1" : "0" ) + "</reviewHit>\n";
			s += "\t<reviewPrompts>" + reviewPrompts.toString() + "</reviewPrompts>\n";
			
			s += "\t<moreAppsVersion>" + moreAppsVersion.toString() + "</moreAppsVersion>\n";
			s += "\t<moreAppsShown>" + moreAppsShown.toString() + "</moreAppsShown>";
			s += "\t<showMoreApps>" + ( showMoreApps? "1" : "0" ) + "</showMoreApps>";
			
			s += "\t<pixelPostback>" + ( pixelPostback? "1" : "0" ) + "</pixelPostback>";
			s += "\t<deviceTracked>" + ( deviceTracked? "1" : "0" ) + "</deviceTracked>";
			
			s += "</Options>\n";
			
			//App.Log("Options saved with the following xml: \n\n" + s);

			Utils.saveStringFile( s, Filename );			
		}
	}
}